package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem;

import java.awt.Graphics2D;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionSet;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Classifier;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Statistics;

public class LayerClassifier extends Classifier<LayerCondition, LayerAction>
{
	private static final long serialVersionUID = 1L;
	private final LayerCondition layerCondition;//"Clasisfier" condition
	private final ActionSet<LayerAction> layerActions;//Classifier "action"
	private  LayerAction layerAction= LayerAction.empty;//Classifier one "action"// add by Tony
	//private  LayerAction layerAction= LayerAction.initialRandomLayerAction();//Classifier one "action"// add by Tony

	//Reinforcer --> Classifier - Condition
	//Classifier --> Emotion (1 - level)
	//Emotion    --> Modifier
	public LayerClassifier(LayerCondition condition) {
		super(condition);
		this.layerCondition = condition;
		this.layerActions = new ActionSet<LayerAction>(LayerAction.getLayerAction());
		this.layerAction= LayerAction.initialRandomLayerAction();
	}

	public LayerClassifier(int id, LayerCondition condition, LayerAction action) {
		super(id, condition);


		this.layerCondition = condition;
		this.layerActions = new ActionSet<LayerAction>(LayerAction.getLayerAction());
		//this.layerAction= LayerAction.initialRandomLayerAction();
		this.layerAction = action;
	}
	public LayerClassifier(LayerCondition condition, int actionMaskID) {
		super(condition);
		this.layerCondition = condition;
		this.layerActions = new ActionSet<LayerAction>(LayerAction.getLayerAction());
		//set this layerAction
		setLayerClassifierAction(actionMaskID);
	}
	public LayerClassifier(LayerCondition condition, LayerAction action) {
		super(condition);

		this.layerCondition = condition;
		this.layerActions = new ActionSet<LayerAction>(LayerAction.getLayerAction());
		//set this layerAction
		this.layerAction= action;
	}
	public LayerClassifier(LayerCondition condition, LayerAction action,Statistics statistics ) {

		super(condition, statistics);
		this.layerCondition = condition;
		this.layerActions = new ActionSet<LayerAction>(LayerAction.getLayerAction());
		//set this layerAction
		this.layerAction= action;
		this.statistics= statistics;

	}

	
	public LayerClassifier(LayerClassifier classifier) {
		super(classifier);
		this.layerCondition = classifier.layerCondition;
		this.layerActions = classifier.layerActions;
		this.layerAction=classifier.layerAction;
	}
	
	public LayerClassifier(LayerCondition layerCondition, Statistics statistics) {
		super(layerCondition, statistics);
		this.layerCondition = layerCondition;
		this.layerActions = new ActionSet<LayerAction>(LayerAction.getLayerAction());
	}

	/***Following "createChild(C condition) method is replaced by "createChild(C condition,A action)"method //Tony
	@Override
	protected Classifier<LayerCondition, LayerAction> createChild(LayerCondition condition) {
		Statistics stats = new Statistics();
		stats.mutateStatistics(statistics);
		LayerClassifier c = new LayerClassifier((LayerCondition)condition.replicate(), stats);
		return c;
	}
	**/
	@Override
	protected Classifier<LayerCondition, LayerAction> createChild(LayerCondition condition, LayerAction layerAction) {

		Statistics stats = new Statistics();
		stats.mutateStatistics(statistics);
		LayerCondition clone_condition;
		LayerClassifier c=null;
		try
		{
			clone_condition= (LayerCondition) condition.cloneLayerCondition();
			c = new LayerClassifier(clone_condition, layerAction,stats );

		} catch(CloneNotSupportedException e) {}

		// replicate method is replaced by clone method //LayerClassifier c = new LayerClassifier((LayerCondition)condition.replicate(), stats);
		return c;
	}


	@Override
	protected Classifier<LayerCondition, LayerAction> createMitosisChild(LayerCondition condition, LayerAction layerAction) {

		Statistics stats = new Statistics();
		stats.inherit(statistics);
		LayerCondition clone_condition;
		LayerClassifier c=null;
		try
		{
			clone_condition= (LayerCondition) condition.cloneLayerCondition();
			c = new LayerClassifier(clone_condition, layerAction,stats );

		} catch(CloneNotSupportedException e) {}

		// replicate method is replaced by clone method //LayerClassifier c = new LayerClassifier((LayerCondition)condition.replicate(), stats);
		return c;
	}



	@Override
	public void display(Graphics2D g2d, int offset_x, int offset_y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected ActionSet<LayerAction> getActions() {
		// TODO Auto-generated method stub
		return layerActions;
	}

	@Override
	public LayerAction getLayerAction() {
		// TODO Auto-generated method stub
		return layerAction;
	}




	@Override
	public LayerCondition getCondition() {
		// TODO Auto-generated method stub
		return layerCondition;
	}

	@Override
	public  String showCondition()
	{
		String s = this.layerCondition.showCondition();
		return s;
	}

	public void setLayerClassifierAction(int maskID)
	{

		this.layerAction= this.layerAction.getLayerAction(maskID);
	}
	public LayerAction getLayerClassifierAction()
	{
		return this.layerAction;
	}
	@Override
	public LayerAction getClassifierAction()
	{
		return getLayerClassifierAction();
	}

	public String[] toStringArray_for_csv2()
	{
		/*
		System.out.println("	toStringArray_for_csv2 :	(Accuracy-based) most fitness classifier :" +
				"\t- id(" + this.getID() + ")," +
				" condition (" + this.getCondition() + " )," +
				" action(" + this.getLayerAction() + ")," +
				" acc("+ this.getAccuracy()+" ), " +
				" prd("+ this.getPredictedReward()+")," +
				" fit("+ this.getFitness()+")");
		*/
		//1.ID
		String[] id_array= new String[1];
		id_array[0]= Integer.toString(this.getID());


		//2.condition attributes
		LayerCondition a_condition = this.getCondition();
		String condition_string= a_condition.toString_for_csv();
		String[] condition_array= condition_string.split("#");
		String[] classifier_array =merge_two_stringArray(id_array,condition_array);
		/*
		System.out.println("	toStringArray_for_csv2 :	(Accuracy-based) most fitness classifier :" +
				"\t- id(" + this.getID() + ")," +
				" condition (" + this.getCondition() + " )");
		System.out.println("	toStringArray_for_csv2 :	(Accuracy-based) most fitness classifier :" +
				"\t- id(" + this.getID() + ")," +
				" condition_string (" + condition_string + " )");
		*/

		//3. action
		String[] action_array= new String[1];
		action_array[0]= this.getLayerAction().toString();
		classifier_array =merge_two_stringArray(classifier_array,action_array);

		//4. statistics
		String[] statistics_array = statistics.statistics_toCSVSet();
		classifier_array=merge_two_stringArray(classifier_array,statistics_array);

		return classifier_array;
	}
//	public String toString(){
//		String s = layerCondition.toString() +"\n";
//		s += layerActions.getBestAction(Emotion.getEmotions());
//		return s;
//	}
}
