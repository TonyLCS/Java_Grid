package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;




import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ConditionValue;


import java.util.Collection;
import java.util.HashMap;
import main.java.GridSim.EnvironmentManager.SingleTask;
//import GridSim.EnvironmentManager.SingleTask;
import main.java.GridSim.EnvironmentManager.Instance;

public class Reinforcer_Factory 
{
//	/p3dx/reinforcers/habitat
//	/p3dx/reinforcers/novelty
//	/p3dx/reinforcers/pain
//	/p3dx/reinforcers/progress
//	/p3dx/reinforcers/sense_of_agency
//	/p3dx/reinforcers/uncertainty
	private volatile Reinforcer pain;//0
	private volatile Reinforcer progress;//1
	private volatile Reinforcer novelty;//2
	private volatile Reinforcer sense_of_agency;//3
	private volatile Reinforcer uncertainty;//4
	
	private volatile Reinforcer habitat;


	private volatile Reinforcer location;
	private volatile Reinforcer locationCell;

	private volatile Reinforcer ternarySet;
	private volatile Reinforcer iris_task;
	private volatile Reinforcer instance_perception;
	double BOUNDARY=80;

	private volatile HashMap<Integer, Reinforcer> PrimaryReinforcerSet = new HashMap<Integer, Reinforcer>();



	public Reinforcer_Factory() {}

	


	public Reinforcer getPain() {
		return pain;
	}

	public void setPain(Reinforcer pain) {
		this.pain = pain;
	}
	public void  setTernarySet(Reinforcer ternarySet)
	{
		this.ternarySet= ternarySet;
	}

	public Reinforcer getProgress() {
		return progress;
	}

	public void setProgress(Reinforcer progress) {
		this.progress = progress;
	}

	public Reinforcer getNovelty() {
		return novelty;
	}

	public void setNovelty(Reinforcer novelty) {
		this.novelty = novelty;
	}

	public Reinforcer getSenseOfAgency() {
		return sense_of_agency;
	}

	public void setSenseOfAgency(Reinforcer sense_of_agency) {
		this.sense_of_agency = sense_of_agency;
	}

	public Reinforcer getUncertainty() {
		return uncertainty;
	}

	public void setUncertainty(Reinforcer uncertainty) {
		this.uncertainty = uncertainty;
	}

	public Reinforcer getHabitat() {
		return habitat;
	}

	public void setHabitat(Reinforcer habitat) {
		this.habitat = habitat;
	}
	public void setLocation(Reinforcer location) {
		this.location = location;
	}
	public void setLocationCell(Reinforcer locationCell) {
		this.locationCell = locationCell;
	}

	public Reinforcer getLocation() {
		return location;
	}

	public Reinforcer getLocationCell() {
		return locationCell;
	}
	public Reinforcer getTernarySet()
	{
		return ternarySet;
	}
	/*
	public void setTernarySet(Reinforcer ternarySet)
	{
		this.ternarySet=ternarySet;
	}*/



	//pain;//0
	//progress;//1
	//novelty;//2
	//sense_of_agency;//3
	//uncertainty;//4
	public Reinforcer[] getReinforcers()
	{
		//modify to my reinforcers but keep the shell of old reinforcer.
		Reinforcer[] reinforcerSet = new Reinforcer[1];	//[1] define the number of the reinforcers
		reinforcerSet[0] = getLocation();// this is position data
		//reinforcers[0] = getPain();// this is lidar data
		//reinforcers[1] = getProgress();
		//reinforcers[2] = getNovelty();
		//reinforcers[3] = getSenseOfAgency();
		//reinforcers[4] = getUncertainty();
		/*	modify to my reinforcers but keep the shell of old reinforcer.

		Reinforcer[] reinforcers = new Reinforcer[5];
		reinforcers[0] = getPain();
		reinforcers[1] = getProgress();
		reinforcers[2] = getNovelty();
		reinforcers[3] = getSenseOfAgency();
		reinforcers[4] = getUncertainty();
		*/
		return reinforcerSet;
	}

	public Reinforcer[] getReinforcerInnerStateSet()
	{
		//modify to my reinforcers but keep the shell of old reinforcer.
		Reinforcer[] reinforcerInnerSet = new Reinforcer[5];	//[1] define the number of the reinforcers
		//reinforcerSet[0] = getLocation();// this is position data
		reinforcerInnerSet[0] = getPain();// this is lidar data
		reinforcerInnerSet[1] = getProgress();
		reinforcerInnerSet[2] = getNovelty();
		reinforcerInnerSet[3] = getSenseOfAgency();
		reinforcerInnerSet[4] = getUncertainty();
		/*	modify to my reinforcers but keep the shell of old reinforcer.

		Reinforcer[] reinforcers = new Reinforcer[5];
		reinforcers[0] = getPain();
		reinforcers[1] = getProgress();
		reinforcers[2] = getNovelty();
		reinforcers[3] = getSenseOfAgency();
		reinforcers[4] = getUncertainty();
		*/
		return reinforcerInnerSet;
	}
	public Reinforcer[] getReinforcerCells()
	{
		Reinforcer[] reinforcerCellSet = new Reinforcer[1];	//[1] define the number of the reinforcers
		reinforcerCellSet[0] = getLocationCell();
		//showReinforcers(); this show PIDMM value in reinforcers
		return reinforcerCellSet;
	}

	public Reinforcer[] getReinforcerTernaryState()
	{
		Reinforcer[] reinforcerTernarySet = new Reinforcer[1];	//[1] define the number of the reinforcers
		reinforcerTernarySet[0] = getTernarySet();
		//showReinforcers(); this show PIDMM value in reinforcers
		return reinforcerTernarySet;

	}

	public void showReinforcers()
	{	//this show PIDMM value in reinforcers
		java.lang.String str="Show ReinforcerSet PIDMM Value:" +
				" Pain:"+ this.pain.showPIDMM()+"; ";
		str =str +"Progress:" + this.progress.showPIDMM()+"; ";
		str =str +"Novelty:" + this.novelty.showPIDMM()+"; ";
		str =str +"SenseOfAgency:" + this.sense_of_agency.showPIDMM()+"; ";
		str =str +"Uncertainty:" + this.uncertainty.showPIDMM()+"; ";
		System.out.println(str);
	}

	public Reinforcer[] getPrimaryReinforcers()
	{
		//modify to my reinforcers but keep the shell of old reinforcer.
		Reinforcer[] primaryReinforcerSet = new Reinforcer[1];	//[1] define the number of the reinforcers
		primaryReinforcerSet[0] = getLocation();// this is position data
		return primaryReinforcerSet;
	}
	public HashMap<Integer, Reinforcer> getPrimaryReinforcerSet()
	{
		PrimaryReinforcerSet.put(1,getLocation());
		//PrimaryReinforcerSet.put(2,getPain());
		return PrimaryReinforcerSet;
	}

	public void setIrisTask(SingleTask singleTask)
	{
		//System.out.println("irisTrain : a_singleTask: species("+singleTask.species+"): ("+singleTask.sepal_length+"/"+ singleTask.sepal_width+"/"+singleTask.petal_length+"/"+singleTask.petal_width+")");

		this.iris_task=new Reinforcer("iris",singleTask.sepal_length, singleTask.sepal_width, singleTask.petal_length, singleTask.petal_width);
	}

	public void setIrisTask_cellWorld(SingleTask singleTask)
	{
		//Train
		this.iris_task=new Reinforcer("iris","crystalEyes",singleTask.sepal_length, singleTask.sepal_width, singleTask.petal_length, singleTask.petal_width);

	}

	/*
	public void setIrisTask_cellWorld_for_instance(Instance an_instance)
	{
		//Train
		this.instance_task=new Reinforcer("instance","crystalEyes",an_instance.attribute);
	}
	*/

	public void setIrisTask_testCellWorld(SingleTask singleTask)
	{
		this.iris_task=new Reinforcer("iris","testEyes",singleTask.sepal_length, singleTask.sepal_width, singleTask.petal_length, singleTask.petal_width);
	}

	/*
	public void setIrisTask_testCellWorld_for_instance(Instance an_instance)
	{
		//no boundary
		this.instance_task= new Reinforcer("instance","testEyes",an_instance.attribute);
	}
	*/

	public void set_perception_state_for_instance(String which_task, String do_boundary, double[] attributes)
	{
		// "no_boundary",an_instance.attribute

		this.instance_perception= new Reinforcer(which_task, do_boundary,attributes,this.BOUNDARY);

	}

	public void set_perception_state_with_boundary(String which_task, String do_boundary, double[] attributes, double[][] perception_boundary )
	{
		// "no_boundary",an_instance.attribute

		this.instance_perception= new Reinforcer(which_task, do_boundary,attributes,perception_boundary);

	}

	public void set_perception_state_for_classifier( double[] condition_attributes_arr )
	{
		// "no_boundary",an_instance.attribute

		this.instance_perception= new Reinforcer(condition_attributes_arr);

	}
	public Reinforcer create_a_reinforcer(double[] condition_attributes_arr)
	{
		return new Reinforcer(condition_attributes_arr);
	}

	public Reinforcer[] getIrisTask()
	{
		Reinforcer[] primaryReinforcerSet = new Reinforcer[1];	//[1] define the number of the reinforcers
		primaryReinforcerSet[0] = this.iris_task;// this is position data
		return primaryReinforcerSet;
	}

	public Reinforcer[] get_perception_state_for_instance()
	{
		Reinforcer[] primaryReinforcerSet = new Reinforcer[1];	//[1] define the number of the reinforcers
		primaryReinforcerSet[0] = this.instance_perception;// this is position data
		return primaryReinforcerSet;
	}

	public Reinforcer[] get_perception_state_for_classifier(double[] condition_attributes_arr)
	{
		Reinforcer[] primaryReinforcerSet = new Reinforcer[1];	//[1] define the number of the reinforcers
		primaryReinforcerSet[0] = create_a_reinforcer(condition_attributes_arr);
		return primaryReinforcerSet;
	}

}
