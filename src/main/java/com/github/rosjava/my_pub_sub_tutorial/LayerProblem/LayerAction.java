package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem;

import java.util.ArrayList;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;

public enum LayerAction implements ActionInterface
{
	empty(0),
	one(1),
	two(2),
	three(3),
	four(4),
	five(5),
	six(6),
	seven(7),
	eight(8),
	nine(9),
	ten(10),
	eleven(11),
	twelve(12);


	private final int mask;
	
	private LayerAction(int mask)
	{
		this.mask = mask;
	}


	@Override
	public int getMask() {
		return mask;
	}

	public static LayerAction initialRandomLayerAction()
	{
		int diversityOfLayerAction= getLayerAction().size();
		int selectLayerActionID= (int)(Math.random()*diversityOfLayerAction+1);

		return getLayerAction(selectLayerActionID);
	}

	public static LayerAction getLayerAction(int mask)
    {
    	switch(mask)
    	{
    		case 0:
    			return empty;
    		case 1:
    			return one;
    		case 2:
    			return two;
			case 3:
				return three;
			case 4:
				return four;
			case 5:
				return five;
			case 6:
				return six;
			case 7:
				return seven;
			case 8:
				return eight;
			case 9:
				return nine;
			case 10:
				return ten;
			case 11:
				return eleven;
			case 12:
				return twelve;
    		default:
    			return empty; 
    	}
    }

	public static LayerAction getLayerAction(String mask)
	{
		switch(mask)
		{
			case "empty":
				return empty;
			case "one":
				return one;
			case "two":
				return two;
			case "three":
				return three;
			case "four":
				return four;
			case "five":
				return five;
			case "six":
				return six;
			case "seven":
				return seven;
			case "eight":
				return eight;
			case "nine":
				return nine;
			case "ten":
				return ten;
			case "eleven":
				return eleven;
			case "twelve":
				return twelve;
			default:
				return empty;
		}

	}


	public static ArrayList<LayerAction> getLayerAction()
	{
		ArrayList<LayerAction> layerActions = new ArrayList<LayerAction>();
		/*
		int i = 1;
		LayerAction e = getLayerAction(i++);
		while(e != empty)
		{
			layerActions.add(e);
			e = getLayerAction(i++);
		}
		*/
		//todo: check this seq_action .this method should be replaced by the following method for flexible number of actions!!!!!!!!!!
		int seq_action= 2;// the number of action, this is different according to agent
		LayerAction e;
		//System.out.println("	seq_action("+seq_action+")-LayerAction("+e+")");

		while(seq_action != 0)
		{
			e = getLayerAction(seq_action);

			layerActions.add(e);
			seq_action--;
			//e = getLayerAction(seq_action);
			//System.out.println("	seq_action("+seq_action+")-LayerAction("+e+")");

		}

		return layerActions;
	}

	public static ArrayList<LayerAction> getLayerActionSet(int seq_action)
	{
		ArrayList<LayerAction> layerActions = new ArrayList<LayerAction>();
		/*
		int i = 1;
		LayerAction e = getLayerAction(i++);
		while(e != empty)
		{
			layerActions.add(e);
			e = getLayerAction(i++);
		}
		*/
		//int seq_action= 4;// the number of action, this is different according to agent
		LayerAction e = getLayerAction(seq_action);
		//System.out.println("	seq_action("+seq_action+")-LayerAction("+e+")");

		while(seq_action != 0)
		{
			layerActions.add(e);
			seq_action--;
			e = getLayerAction(seq_action);
			//System.out.println("	seq_action("+seq_action+")-LayerAction("+e+")");

		}

		return layerActions;
	}
}
