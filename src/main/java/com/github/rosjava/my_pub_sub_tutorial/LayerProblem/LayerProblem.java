package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Stack;

import javax.swing.JPanel;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import main.java.com.github.rosjava.my_pub_sub_tutorial.Jama.Matrix;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.Filters.LayerModifyFilter;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LinearRegression.RewardBoundaryCheck;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Modifier_ContinuouslyProjected;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Debug;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters.*;

import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.Filters.LayerExecutionFilter;// add for new layer1's execution.

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

//for Matrix
import java.util.Arrays;

//for flexible linear regression
import main.java.com.github.rosjava.my_pub_sub_tutorial.LinearRegression.LinearRegression;


public class LayerProblem implements LCSProblem<LayerCondition, LayerAction>
{
	private int iterationCount = 0;

	private FilterMessage<LayerCondition, LayerAction> message = new FilterMessage<LayerCondition, LayerAction>();
	private FilterMessage<LayerCondition, LayerAction> message4 = new FilterMessage<LayerCondition, LayerAction>();

	private Stack<FilterMessage<LayerCondition, LayerAction>> messages = new Stack<FilterMessage<LayerCondition,LayerAction>>();
	//		private Stack<FilterMessage<LayerCondition, LayerAction>> layerMessages = new Stack<FilterMessage<LayerCondition,LayerAction>>();
	private Stack<FilterMessage<LayerCondition, LayerAction>> mcts_messages = new Stack<FilterMessage<LayerCondition,LayerAction>>();

	private Stack<FilterMessage<LayerCondition, LayerAction>> messages4 = new Stack<FilterMessage<LayerCondition,LayerAction>>();
	//		private Stack<FilterMessage<LayerCondition, LayerAction>> layerMessages = new Stack<FilterMessage<LayerCondition,LayerAction>>();

	ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters_portfolio = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();
	ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> test_filters_portfolio = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();
	ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> mcts_filters_portfolio = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();

	private Stack<Double> groundTruthStack 	= new Stack<Double>();
	private Stack<Double> assignedRewardStack	= new Stack<Double>();
	//replace assignedRewardStack by exploreReward
	private double explore_reward=0;

	private HashMap<Integer, Double> maxFitnessHashmap	= new HashMap<Integer, Double>();
	private HashMap<Integer, Double> assignedRewardHashMap = new HashMap<Integer, Double>();

	private HashMap<Integer, Double> maxFitXPrdHashmap	= new HashMap<Integer, Double>();

	//anticipated_reward
	private HashMap<Integer, Double> anticipatedReward_batch	= new HashMap<Integer, Double>();
	private HashMap<Integer, Boolean> exploreMode_batch	= new HashMap<Integer, Boolean>();

	// reward matrix
	Matrix matrix_A =create_matrix_A();  // complete map matrix of actions applied in 3 step scenario
	Matrix matrix_batch_reward= creat_matrix_reward(27,50); // record complete map of rewards, which will gain at the end of 3steps scenario.
	Matrix matrix_to_update_reward=new Matrix(9,1,this.initial_default_reward);//reward of action 1 in step 1 is different from that of reward of action 1 in step 2 or step 3.
	//Matrix matrix_steps_rewards= new Matrix(3,1,this.initial_default_reward);
	Matrix matrix_steps_rewards; //  this matrix will be spanned flexibly

	//flexible matrix
	private LinearRegression linearRegressionRewardMethod = new LinearRegression();
	private HashMap<Integer,HashMap<Integer,Double>> reward_pan= new HashMap<Integer,HashMap<Integer,Double>>(); //replaced by OST_hashmap
	/**
	 * Integer hash-num = scenario*100 00 00 + step_num*100 + action_num
	 * HashMap<Integer,Double> is a behaviour_reward for O-scenario-and-S-step-and-T-action
	 * **/
	//private HashMap<Integer, Matrix> reward_received_matrix_hashmap= new HashMap<Integer, Matrix>();
	//private HashMap<Integer, Matrix> behaviours_matrix_hashmap= new HashMap<Integer, Matrix>();
	private HashMap<Integer,HashMap<String, Matrix>> OST_hashmap= new HashMap<Integer,HashMap<String, Matrix>>();


	private  double initial_default_reward=1;

	//	private FilterMessage<LayerCondition, LayerAction> layerMessage;

	private boolean isComplete = false;

	private String file;
	private String cl_file;
	//private final LayerPopulation population;
	private  LayerPopulation population;

	private double explore_chance = 20;//<<---defalut explore rate
	private int GA_THRESHOLD = 10;//<<---change this GA rate
	private double iterationCount_threshold;//iterationCount_threshold


	private boolean exploit = false;
	public int the_DIVERSITY_of_actions= 2;

	//reward check
	RewardBoundaryCheck reward_boundary_check;
	private int task_seq;

	public void  setExploreRate(double exploreRate)
	{
		//System.out.println("	set explore_chance("+explore_chance+")");
		this.explore_chance= exploreRate;
	}
	public void replace_select_filter(double exploreRate,double iterationCount_threshold)
	{
		// SelectFSAFilter is fixed at location index NO.2 of the fliters
		this.filters_portfolio.set(2, new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(exploreRate,iterationCount_threshold ));// Martin
	}
	public void replace_select_filter(double exploreRate,double iterationCount_threshold, String fiters_portfolio)
	{
		// this function is discard.
		if(fiters_portfolio.equals("filters_portfolio"))
		{
			this.filters_portfolio.set(2, new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(exploreRate,iterationCount_threshold ));// Martin
		}

		if(fiters_portfolio.equals("test_filters_portfolio"))
		{
			this.test_filters_portfolio.set(2, new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(exploreRate,iterationCount_threshold ));// Martin
		}

		if(fiters_portfolio.equals("mcts_filters_portfolio"))
		{
			this.mcts_filters_portfolio.set(2, new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(exploreRate,iterationCount_threshold ));// Martin
		}

		//this.filters_portfolio.set(2, new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(exploreRate,iterationCount_threshold ));// Martin
	}

	public void replace_GA_filter(int GATheshold)
	{
		// GenitcAlgorithmActionSetFilter is fixed at location index NO.3 of the fliters
		this.filters_portfolio.set(3, new GenitcAlgorithmActionSetFilter<LayerProblem, LayerCondition, LayerAction>(GATheshold));// iris
	}
	public void  setIterationCount_threshold(double iterationCount_threshold)
	{
		//System.out.println("	setiterationCount_threshold("+ iterationCount_threshold +")");
		this.iterationCount_threshold= iterationCount_threshold;
	}

	public void setGATheshold(int GA_THRESHOLD)
	{
		//System.out.println("	GA_THRESHOLD("+ GA_THRESHOLD +")");
		this.GA_THRESHOLD= GA_THRESHOLD;
	}
	/*
	public LayerProblem()
	{
		this.population = LayerPopulation.readThePopulation("layer1");
		//this.population = LayerPopulation.readThePopulation("emotion");
	}
	*/

	public LayerProblem(String file)
	{
		this.file=file;

		// todo: XStream class read extension
		// todo: does not read existing file, start a new one.
		//this.population = LayerPopulation.readThePopulation(file);// in popluation.agentFileName also keep the file name
		//this.population = LayerPopulation.readThePopulationfromCSV(file);
		this.population = LayerPopulation.getEmptyPopulation();
		// set the diversity of actions
		//this.population.set_The_DIVERSITY_of_actions(the_DIVERSITY_of_actions);
		//this.population =new LayerPopulation();
		this.population.agentFileName=file;
		this.reward_boundary_check = new RewardBoundaryCheck();

	}
	//refactor everything...
	@Override
	public void iterate() 
	{
		
	}
	public void set_the_DIVERSITY_of_actions(int the_DIVERSITY_of_actions)
	{
		this.the_DIVERSITY_of_actions=the_DIVERSITY_of_actions;
		this.population.set_The_DIVERSITY_of_actions(the_DIVERSITY_of_actions);
	}
	

	public void reward(double reward)
	{
		System.out.println("\n");
		System.out.println("\n");
		int loop_time=1;
		reward = reward/messages.size();
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!REINFORCEMENT LEARNING : update BOW-TIE STRUCTURE!!!!! :it supposed to update the reward here reward="+reward+"', messages.isEmpty()='"+messages.isEmpty()+"'<--if true, go to ACTIONSET update!!!!!");
		while(!messages.isEmpty())
		{
			System.out.println("\n");
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!REINFORCEMENT LEARNING : update BOW-TIE STRUCTURE!!!!! :" +
					"Loop_time"+loop_time);
			loop_time++;

			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!REINFORCEMENT LEARNING : update BOW-TIE STRUCTURE!!!!!" +
					" 0.0 with discount reward='"+reward+"'<-");
			FilterMessage<LayerCondition, LayerAction> message = messages.pop();

			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!REINFORCEMENT LEARNING : update BOW-TIE STRUCTURE!!!!! " +
					"1 begin of updating action ");
			this.population.updateClassifiersAction(message, reward);
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!REINFORCEMENT LEARNING : update BOW-TIE STRUCTURE!!!!!" +
					" 1 end of updating action ");
			System.out.println("\n");

			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!REINFORCEMENT LEARNING : update BOW-TIE STRUCTURE!!!!!" +
					" 2.1 begin of updating expectation(classifierFitness) ");
			this.population.updateExpectations(message.getCurrentAction(), reward);
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!REINFORCEMENT LEARNING : update BOW-TIE STRUCTURE!!!!! " +
					"2.1 end of updating expectation(classifierFitness) ");
			System.out.println("\n");

			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!REINFORCEMENT LEARNING : update BOW-TIE STRUCTURE!!!!!" +
					" 3.1 begin of updating MatchSet(classifiers)");
			this.population.update(message.getMatchSet(), reward);
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!REINFORCEMENT LEARNING : update BOW-TIE STRUCTURE!!!!!" +
					" 3.1 end of updating MatchSet(classifiers)");
			System.out.println("\n");

			// remove the discount//reward /= 2;
		}
	}
	public boolean irisReward(double species)
	{
		System.out.println("\n");

		int message_size=messages.size();
		int reward=0;
		System.out.println("	irisReward::  messages.size("+message_size+"),  prepare to goto the loop ");

			while(!messages.isEmpty())
			{

				//1.pop up a message.
				FilterMessage<LayerCondition, LayerAction> message = messages.pop();
				//2. show message information such as, action, action_set
				int modID= message.getCurrentAction().getMask();

				System.out.println("	irisReward:: message current action("+message.getCurrentAction()+") ("+modID+")," + " action_set("+message.getActionSet()+").");
				if((int)species==modID)
				{
					//if((int)species==1)
					{
						reward=1000;
						this.population.update(message.getActionSet(), reward);
						return true;
					}

				}
				else
				{
					reward =0;
					this.population.update(message.getActionSet(), reward);
					return false;
				}

			}
			//clear BowTieSet
			this.population.clearBowTieSet();
			if(reward==1000)
			{
				return true;
			}
			else
				{
					return false;
				}
		}

	public void irisRewardMultistep(int parasiteNum, int hostNum, double species)// parasite actions and weighted reward
	{
		//parasite and host problem
		System.out.println("\n");

		int message_size=messages.size();
		double reward=0;// once the agent reach this step, it assumes that it reach a goal.
		System.out.println("	irisRewardMultistep::  messages.size("+message_size+"),  prepare to goto the loop ");

		/**
		 * 1. check if the goal have been reached. check the peek message, which should contain a message from the host
		 * check if host make it
		 * ****************/

		FilterMessage<LayerCondition, LayerAction> message = messages.peek();//read the peek one, no remove, no pop
		//2. show message information such as, action, action_set
		int modID= message.getCurrentAction().getMask();

		System.out.println("	irisReward:: message current action("+message.getCurrentAction()+") ("+modID+")," + " action_set("+message.getActionSet()+").");

		if((int)species==modID)
		{
			reward=1000;
		}
		else
		{
			reward =-1000;
		}

		/**
		 * reward assignment
		 * */
		this.rewardAssignment(parasiteNum, hostNum, reward);

		/**
		 * replace two rewards for the parasite classifier and the host classifier
		 * **/
		while(!messages.isEmpty())
		{

			//1.pop up a message.
			message = messages.pop();
			//2. show message information such as, action, action_set
			modID= message.getCurrentAction().getMask();

			System.out.println("	irisReward:: message current action("+message.getCurrentAction()+") ("+modID+")," + " action_set("+message.getActionSet()+").");
			if((int)species==modID)
			{
				/*
				reward=1000;
				this.population.update(message.getActionSet(), reward);
				*/

				if(species==1)
				{
					reward= this.population.getRewardAssigned("one");
					if(reward>=0)
					{
						this.population.update(message.getActionSet(), reward);
					}
				}
				if(species==2)
				{
					reward= this.population.getRewardAssigned("two");
					this.population.update(message.getActionSet(), reward);
				}

			}
			else
			{
				reward =0;
				this.population.update(message.getActionSet(), reward);
			}

		}


		//clear BowTieSet
		this.population.clearBowTieSet();

	}

	public boolean reward_assign(double taskReward, String method, int O, int T)
	{

		int lengthOfMessages= messages.size();
		double a_step_reward=0;
		/**
		 * method: even, fitnessWeight,fitXprd,discount, linear,
		 * **********/

		if(method.equals("even"))
		{
			a_step_reward= taskReward/lengthOfMessages;
			for(int i=0; i< lengthOfMessages; i++)
			{
				assignedRewardStack.push(a_step_reward);
				System.out.println(" >> reward >> a_step_reward("+a_step_reward+") in ("+(i+1)+"/"+lengthOfMessages+")");
			}

			return true;
		}
		if(method.equals("mcts_even"))
		{
			lengthOfMessages= mcts_messages.size();
			a_step_reward= taskReward/lengthOfMessages;
			for(int i=0; i< lengthOfMessages; i++)
			{
				assignedRewardStack.push(a_step_reward);
				System.out.println(" >>mcts_even reward >> a_step_reward("+a_step_reward+") in ("+(i+1)+"/"+lengthOfMessages+")");
			}

			return true;
		}
		/** discount **/
		if(method.equals("discount_big_end"))
		{
			double discount = 0.8;
			Stack<Double> reverse_assignedRewardStack	= new Stack<Double>();
			a_step_reward= taskReward/lengthOfMessages;
			for(int i=0; i< lengthOfMessages; i++)
			{
				//assignedRewardStack.push(a_step_reward);
				reverse_assignedRewardStack.push(a_step_reward);
				//System.out.println(" >> reward >> a_step_reward("+a_step_reward+") in ("+(i+1)+"/"+lengthOfMessages+")");
				a_step_reward =  a_step_reward * discount;
			}
			while(!reverse_assignedRewardStack.empty())
			{
				assignedRewardStack.push(reverse_assignedRewardStack.pop());
			}

			return true;
		}
		if(method.equals("discount_big_start"))
		{
			double discount = 0.8;
			a_step_reward= taskReward/lengthOfMessages;
			for(int i=0; i< lengthOfMessages; i++)
			{
				assignedRewardStack.push(a_step_reward);
				//System.out.println(" >> reward >> a_step_reward("+a_step_reward+") in ("+(i+1)+"/"+lengthOfMessages+")");
				a_step_reward =  a_step_reward * discount;
			}

			return true;
		}

		/**
		 * according to fitness
		 * **********/
		if(method.equals("fit"))
		{
			reward_assign_method_fit(taskReward);
			return true;
		}

		if(method.equals("fitXprd"))
		{
			reward_assign_method_fitXprd(taskReward);
			return true;
		}
		/**
		 * only update one classifier which was in explore mode
		 * **/
		if(method.equals("onlyUpdateExploreMode"))
		{
			selectExploreAndAndAssignReward(taskReward);
			return true;
		}
		if(method.equals("linear_regression"))
		{
			boolean has_span_orthogonal_space = reward_assign_method_linear_regression(O,T,taskReward);

			if(has_span_orthogonal_space)
			{
				for(int i=0; i< lengthOfMessages; i++)
				{
					a_step_reward= this.matrix_steps_rewards.get(i,0);
					assignedRewardStack.push(a_step_reward);
					System.out.println(" >> reward >> assignedRewardStack.push >> a_step_reward("+a_step_reward+") in ("+(i+1)+"/"+lengthOfMessages+")");
				}



			}
			return has_span_orthogonal_space;

		}
		else
		{
			return false;//default...
		}


	}

	public int get_task_seq()
	{
		return this.task_seq;
	}
	public void set_task_seq(int task_seq)
	{
		this.task_seq= task_seq;
	}

	public boolean debug_check_reward_calculation(int O, Matrix reward_all_steps_matrix)
	{
		boolean pass_check_flag= this.linearRegressionRewardMethod.debug_check_reward_calculation(reward_all_steps_matrix);
		if(!pass_check_flag)
		{
			for(int i=0; i<50;i++)
			{
				System.out.println(" 						!!!!!!!!!!				" +
						"!!!!!!!!!	" +
						"ERROR: matrix calculation result does meet the expectation in scenario("+O+")");
			}
		}
		return pass_check_flag;
	}


	public boolean reward_assign_method_linear_regression(int O,int T, double batch_reward)
	{
		//2.
		span_matrix_steps_rewards_vectors();
		int S= get_number_of_steps();
		//3.
		boolean has_span_orthogonal_space =span_orthogonal_reward_space(batch_reward, O,  S);
		if(has_span_orthogonal_space)
		{
			//4.
			Matrix reward_all_steps_matrix = calculate_reward_all_steps_matrix(O, S, T);
			//check if the calculation is correct.

			if(!debug_check_reward_calculation(O, reward_all_steps_matrix))
			{
				task_seq= get_task_seq();
				this.reward_boundary_check.save_reward_error_log(task_seq,O);
			}

			//5.
			extract_reward_S_step_from_all( S,  reward_all_steps_matrix);

			return true;
		}
		else
		{
			return false;
		}


	}

	public void reward_assign_method_fit(double taskReward)
	{
		double sumMaxFit=0;
		int lengthOfMessages= messages.size();

		//1 get max fitness
		for(FilterMessage<LayerCondition, LayerAction> message: messages)
		{
			double maxFit = this.population.getMaxFitInActionSet(message.getActionSet());
			sumMaxFit = maxFit+sumMaxFit;
		}

		//2 assign a_step_reward according to weight of fitness
		int indexWeightFit=lengthOfMessages;
		for(FilterMessage<LayerCondition, LayerAction> message: messages)
		{
			double a_maxFit = this.population.getMaxFitInActionSet(message.getActionSet());
			double weightFit= a_maxFit/sumMaxFit;
			maxFitnessHashmap.put(indexWeightFit, weightFit);
			System.out.println(" 	>> reward >> indexWeightFit("+indexWeightFit+"), weightFit("+weightFit+")");
			indexWeightFit--;
		}

		sortHashmapAndAssignReward( maxFitnessHashmap,  taskReward);
		/***sort the maxFitnessStack ************/

	}

	public void reward_assign_method_fitXprd(double taskReward)
	{
		double sumfitXprd=0;
		int lengthOfMessages= messages.size();

		//double a_step_reward=0;
		/** assign the reward according to the reward times fitness**/

		for(FilterMessage<LayerCondition, LayerAction> message: messages)
		{
			//todo: copy this function from laptop
			//double maxFitXPrd = this.population.getMaxFitXPrdInActionSet(message.getActionSet());
			double maxFitXPrd = this.population.getMaxFitWithPrdInActionSet(message.getActionSet());
			sumfitXprd = sumfitXprd+maxFitXPrd;
			//debug	ok //System.out.println("		reward assignment fitXprd====>>>maxFitXPrd("+maxFitXPrd+"); sumfitXprd("+sumfitXprd+") ");

		}

		//2 sort: assign a_step_reward according to weight of fit*prd, save the discount into a hashmap, maxFitXPrdHashmap
		int indexWeightFit=lengthOfMessages;
		for(FilterMessage<LayerCondition, LayerAction> message: messages)
		{
			//todo: copy this function from laptop
			//double maxFitXPrd = this.population.getMaxFitXPrdInActionSet(message.getActionSet());
			double maxFitXPrd = this.population.getMaxFitWithPrdInActionSet(message.getActionSet());

			double weightFit= maxFitXPrd/sumfitXprd;
			maxFitXPrdHashmap.put(indexWeightFit, weightFit);
			System.out.println(" 	>> reward >>	indexWeightFit("+indexWeightFit+"), weightFit("+weightFit+")");
			indexWeightFit--;
		}

		//3 assignment
		sortHashmapAndAssignReward( maxFitXPrdHashmap,  taskReward);

	}

	/**
	 * anticipated reward calculation method: the anticipated reward of the most fitness classifier
	 * 1. select the most fitness classifier
	 * 2. anticipated reward (vote) = cl.acc *cl.prd
	 *
	 * in addition, in exploit mode, the best action will be selected according to the maximun of anticipated rewards among all actions
	 * just like what happen in "exploitOneBestAction" function
	 *
	 * **/

	/**
	 * 1. calculate the anticipated reward
	 * 2. calculate the difference of received reward and the anticipated reward
	 * 3. assign the difference to the step which applied explore mode
	 * **/

	/*
	public void reward_assign_method_onlyUpdateExploreMode(double taskReward,int exploreMode_index)
	{


		double sumfitXprd=0;
		int lengthOfMessages= messages.size();
		if(exploreMode_index> lengthOfMessages)
		{
			System.out.println(" error happened in  reward_assign_method_onlyUpdateExploreMode ");
		}

		//double a_step_reward=0;
		/// assign the reward according to the reward times fitness
		for(FilterMessage<LayerCondition, LayerAction> message: messages)
		{
			//todo: copy this function from laptop
			//double maxFitXPrd = this.population.getMaxFitXPrdInActionSet(message.getActionSet());
			double maxFitXPrd = this.population.getMaxFitWithPrdInActionSet(message.getActionSet());
			sumfitXprd = sumfitXprd+maxFitXPrd;
			//debug	ok //System.out.println("		reward assignment fitXprd====>>>maxFitXPrd("+maxFitXPrd+"); sumfitXprd("+sumfitXprd+") ");

		}

		//2 sort: assign a_step_reward according to weight of fit*prd, save the discount into a hashmap, maxFitXPrdHashmap
		int indexWeightFit=lengthOfMessages;
		for(FilterMessage<LayerCondition, LayerAction> message: messages)
		{
			//todo: copy this function from laptop
			//double maxFitXPrd = this.population.getMaxFitXPrdInActionSet(message.getActionSet());
			double maxFitXPrd = this.population.getMaxFitWithPrdInActionSet(message.getActionSet());

			double weightFit= maxFitXPrd/sumfitXprd;
			maxFitXPrdHashmap.put(indexWeightFit, weightFit);
			System.out.println(" 	>> reward >>	indexWeightFit("+indexWeightFit+"), weightFit("+weightFit+")");
			indexWeightFit--;
		}

		//3 assignment
		sortHashmapAndAssignReward( maxFitXPrdHashmap,  taskReward);

	}
	*/

	public void  sortHashmapAndAssignReward( HashMap<Integer, Double> tosort_hashMap, double taskReward)
	{
		/**
		 * assign the taskReward, which is gained at the end of a batch of training, according to the weight in the hashmap
		 * ***/
		double a_step_reward;
		int lengthOfMessages= messages.size();

		/***sort the maxFitXPrdHashmap (according to the decendent order of weighted fitness)************/

		ArrayList<Map.Entry<Integer, Double>> entries = new ArrayList<Map.Entry<Integer, Double>>(tosort_hashMap.entrySet());


		Collections.sort(entries, new Comparator<Map.Entry<Integer, Double>>() {
			@Override
			public int compare(Map.Entry<Integer, Double> entry1, Map.Entry<Integer, Double> entry2)
			{

				double fitnessDiff= entry1.getValue()-entry2.getValue();//descend mode
				int fitFlag=0;
				if(Math.abs(fitnessDiff)<=0.000001)
				{

					if(Math.random() >= 0.5)
					{
						fitFlag=1;
					}else
					{
						fitFlag=-1;
					}
					return (fitFlag);
				}
				//TODO: ways define best action:1. greater fitness win
				//TODO: 2.including reward in this process.
				if(fitnessDiff>0.000001)
				{
					fitFlag=-1;// descent mode
				}
				if(fitnessDiff<-0.000001)
				{
					fitFlag=1;
				}

				return (fitFlag);

				//return entry2.getValue().compareTo(entry1.getValue());// descent order
			}
		});		// end of sort
		/********************/

		//limit the maximum reward

		for(int i=0; i< entries.size();i++)
		{
			a_step_reward= taskReward*entries.get(i).getValue();
			//System.out.println(" Iris reward assign by fitness: : sort:  a_step_reward("+a_step_reward+");" + "fitWeight"+entries.get(i).getValue()+", step("+entries.get(i).getKey()+") ");
			if(Math.abs(a_step_reward)>1000)
			{//reward out of boundary, give it the maximun reward, and then reset rewards.

				if(a_step_reward>=1000)
				{
					a_step_reward=1000;
					assignedRewardHashMap.put((Integer)entries.get(i).getKey(),a_step_reward);
					//rescale weight
					taskReward= taskReward-1000;
				}
				if(a_step_reward<=-1000)
				{
					a_step_reward=-1000;
					assignedRewardHashMap.put((Integer)entries.get(i).getKey(),a_step_reward);
					//rescale weight
					taskReward= taskReward+1000;
				}

				//sum of weight
				double sum_weightFit=0;
				for(int j=i; j< entries.size();j++)
				{
					sum_weightFit += entries.get(j).getValue();
				}
				//recalculate weight
				for(int j=i; j< entries.size();j++)
				{
					double newWeight= entries.get(j).getValue()/sum_weightFit  ;
					HashMap<Integer, Double> a_hashMap=new HashMap<Integer, Double>();
					a_hashMap.put(entries.get(j).getKey(),newWeight);
					ArrayList<Map.Entry<Integer, Double>> a_new_entry = new ArrayList<Map.Entry<Integer, Double>>(a_hashMap.entrySet());
					entries.set(j,a_new_entry.get(0));
				}


			}
			else
			{
				assignedRewardHashMap.put((Integer)entries.get(i).getKey(),a_step_reward);
			}
		}


		for(int i=1; i<= lengthOfMessages; i++)
		{
			a_step_reward=assignedRewardHashMap.get(i);
			assignedRewardStack.push(a_step_reward);
		}

	}

	public boolean getExploreMode()
	{
		boolean explore_mode_flag= false;
		for( FilterMessage<LayerCondition, LayerAction> a_message: this.messages )
		{
			//System.out.println("trace 4 : explore_mode("+a_message.get_explore_mode()+")");
			if(a_message.get_explore_mode())
			{
				explore_mode_flag=true;
			}
		}
		return explore_mode_flag;
		//return message.get_explore_mode();
	}
	/**
	 * 1.calculate the anticipated_reward,
	 * 2.calculate the summery_different of a batch
	 * 3.assign the reward_summary_diff to the step which applied explore_mode
	 * **/
	public void selectExploreAndAndAssignReward(double taskReward)
	{
		double anticipated_reward_sum=0;
		int lengthOfMessages=messages.size();
		int index_batch=lengthOfMessages;
		boolean hasExploreMode=false;

		//1.the anticipated_reward
		for(FilterMessage<LayerCondition, LayerAction> message: messages)
		{

			anticipated_reward_sum= anticipated_reward_sum + message.get_anticipated_reward();

			anticipatedReward_batch.put(index_batch,message.get_anticipated_reward());
			exploreMode_batch.put(index_batch, message.get_explore_mode());

			//System.out.println("trace 5 : explore_mode("+message.get_explore_mode()+")");

			if(message.get_explore_mode())
			{
				hasExploreMode=true;
			}
			index_batch--;

			//debug	ok //System.out.println("		reward assignment fitXprd====>>>maxFitXPrd("+maxFitXPrd+"); sumfitXprd("+sumfitXprd+") ");

		}

		if(hasExploreMode)
		{
			double anticipated_reward_sum_diff= taskReward-anticipated_reward_sum;

			System.out.println("		reward assignment: anticipated_reward_sum_diff("+anticipated_reward_sum_diff+") " +
					"= taskReward("+taskReward+")- anticipated_reward_sum("+anticipated_reward_sum+")  ");

			for(int i=1; i<= lengthOfMessages; i++)
			{
				//if it is explore mode, reward= anticipated_reward + diff
				// otherwise, reward= anticipated_reward

				if(!exploreMode_batch.get(i))
				{
					//exploit mode
					double reward_update= anticipatedReward_batch.get(i);
					//double reward_update= taskReward/lengthOfMessages;
					assignedRewardHashMap.put(i, reward_update);
					//System.out.println(" >> reward >> explore/exploit mode-> A exploit: a_step_reward("+reward_update+")(=anticipated_reward) in ("+i+"/"+lengthOfMessages+")");
					//System.out.println(" >> reward >> explore/exploit mode-> A exploit: a_step_reward("+reward_update+")(=taskReward("+taskReward+")/lengthOfMessages) in ("+i+"/"+lengthOfMessages+")");

				}
				else
				{
					//explore mode
					double reward_update= anticipatedReward_batch.get(i) + anticipated_reward_sum_diff;
					//double reward_update= taskReward/lengthOfMessages;
					assignedRewardHashMap.put(i, reward_update);
					//System.out.println(" >> reward >> explore/exploit mode-> !B explore : a_step_reward("+reward_update+") (=anticipated_reward("+anticipatedReward_batch.get(i)+")+ diff("+anticipated_reward_sum_diff+"))in ("+i+"/"+lengthOfMessages+")");

					//PID
					explore_reward = 0.7*anticipatedReward_batch.get(i)+ 0.3*anticipated_reward_sum_diff;
					System.out.println(" >> reward >> explore/exploit mode-> !!! explore_reward("+explore_reward+")(=anticipated_reward("+anticipatedReward_batch.get(i)+")+ diff("+anticipated_reward_sum_diff+"))in ("+i+"/"+lengthOfMessages+")");

				}

			}


			//translate from hashmap into  stack, prepare for the update
			for(int i=1; i<= lengthOfMessages; i++)
			{
				double a_step_reward=assignedRewardHashMap.get(i);
				assignedRewardStack.push(a_step_reward);
				System.out.println(" >> reward >> translate from hashmap into  stack : a_step_reward("+a_step_reward+") in ("+i+"/"+lengthOfMessages+")");

			}

		}
		else
		{
			double a_step_reward= taskReward/lengthOfMessages;
			for(int i=0; i< lengthOfMessages; i++)
			{
				assignedRewardStack.push(a_step_reward);
				System.out.println(" >> reward >> no explore : a_step_reward("+a_step_reward+") in ("+(i+1)+"/"+lengthOfMessages+")");
			}

		}



	}

	public void reward_update_multiple_step()
	{
		System.out.println("reward_update_multiple_step :: apply reward");
		while(!messages.isEmpty())
		{
			FilterMessage<LayerCondition, LayerAction> message = messages.pop();

			double reward=assignedRewardStack.pop();
			//System.out.println("		reward_update_multiple_step only update explore mode :: pop reward"+reward+" to actionSet");

			/**
			 * only update explore mode (comment, not applied)
			 * **/
			/*
			if(message.get_explore_mode())
			{
				//System.out.println("		reward_update_multiple_step only update explore mode :: apply reward"+reward+" to actionSet");
				//this.population.update(message.getActionSet(), reward);

				System.out.println("		reward_update_multiple_step only update explore mode :: apply reward"+ this.explore_reward+" to actionSet");
				this.population.update(message.getActionSet(), this.explore_reward);

			}
			*/
			//System.out.println("		reward_update_multiple_step  generate AccSet from Actionset according to reward("+reward+")");

			//1. get mitosis set
			message.setMitosisSet( this.population.calculate_Mitosis_set(message.getActionSet(), reward));
			//2. apply mitosis operations on mitosis set

			apply_a_filter(message, new MitosisFilter<LayerProblem, LayerCondition, LayerAction>());

			System.out.println("		reward_update_multiple_step :: apply reward "+reward+" to actionSet");
			this.population.update(message.getActionSet(), reward);

		}

		//clear BowTieSet
		this.population.clearBowTieSet();

	}

	public void reward_update_multiple_step_dynamic(double learning_rate)
	{
		System.out.println("reward_update_multiple_step :: apply reward");
		while(!messages.isEmpty())
		{
			FilterMessage<LayerCondition, LayerAction> message = messages.pop();

			double reward=assignedRewardStack.pop();
			//System.out.println("		reward_update_multiple_step only update explore mode :: pop reward"+reward+" to actionSet");

			/**
			 * only update explore mode (comment, not applied)
			 * **/
			/*
			if(message.get_explore_mode())
			{
				//System.out.println("		reward_update_multiple_step only update explore mode :: apply reward"+reward+" to actionSet");
				//this.population.update(message.getActionSet(), reward);

				System.out.println("		reward_update_multiple_step only update explore mode :: apply reward"+ this.explore_reward+" to actionSet");
				this.population.update(message.getActionSet(), this.explore_reward);

			}
			*/
			//System.out.println("		reward_update_multiple_step  generate AccSet from Actionset according to reward("+reward+")");

			//1. get mitosis set
			message.setMitosisSet( this.population.calculate_Mitosis_set(message.getActionSet(), reward));
			//2. apply mitosis operations on mitosis set

			apply_a_filter(message, new MitosisFilter<LayerProblem, LayerCondition, LayerAction>());

			System.out.println("		reward_update_multiple_step :: apply reward "+reward+" to actionSet");
			this.population.update_learning_rate(message.getActionSet(), reward, learning_rate);

		}

		//clear BowTieSet
		this.population.clearBowTieSet();

	}

	public void reward_update_mcts_multiple_step()
	{
		System.out.println("MCTS: reward_update_multiple_step :: apply MCTS reward");
		while(!mcts_messages.isEmpty())
		{
			FilterMessage<LayerCondition, LayerAction> message = mcts_messages.pop();

			double reward=assignedRewardStack.pop();
			//System.out.print("		MCTS reward update process, reward: "+ reward+".");
			//1. get mitosis set
			message.setMitosisSet( this.population.calculate_Mitosis_set(message.getActionSet(), reward));
			//2. apply mitosis operations on mitosis set
			apply_a_filter(message, new MitosisFilter<LayerProblem, LayerCondition, LayerAction>());

			System.out.println("		MCTS: reward_update_multiple_step :: apply incremental reward "+reward+" to actionSet");
			this.population.update(message.getActionSet(), reward);
			//this.population.update_incremental_reward(message.getActionSet(), reward);

		}
		reward_update_mcts_multiple_step_clear();
		//mcts_messages.clear();
		//clear BowTieSet
		//this.population.clearBowTieSet();

	}

	public void reward_update_mcts_multiple_step_learning_rate(double learning_rate)
	{
		System.out.println("MCTS: reward_update_multiple_step :: apply MCTS reward");
		while(!mcts_messages.isEmpty())
		{
			FilterMessage<LayerCondition, LayerAction> message = mcts_messages.pop();

			double reward=assignedRewardStack.pop();
			//System.out.print("		MCTS reward update process, reward: "+ reward+".");
			//1. get mitosis set
			message.setMitosisSet( this.population.calculate_Mitosis_set(message.getActionSet(), reward));
			//2. apply mitosis operations on mitosis set
			apply_a_filter(message, new MitosisFilter<LayerProblem, LayerCondition, LayerAction>());

			System.out.println("		MCTS: reward_update_multiple_step :: apply incremental reward "+reward+" to actionSet");
			//this.population.update(message.getActionSet(), reward);
			this.population.update_learning_rate(message.getActionSet(), reward, learning_rate);
			//this.population.update_incremental_reward(message.getActionSet(), reward);

		}
		reward_update_mcts_multiple_step_clear();
		//mcts_messages.clear();
		//clear BowTieSet
		//this.population.clearBowTieSet();

	}
	public void reward_update_mcts_multiple_step_clear()
	{
		this.assignedRewardStack.clear();
		this.mcts_messages.clear();
	}
	public void donot_update_reward()
	{
		System.out.println("reward_update_multiple_step :: not yet, do not update reward");
		while(!messages.isEmpty())
		{
			//pop up...discard...
			messages.pop();
			//assignedRewardStack.pop();
		}
		//clear BowTieSet
		this.population.clearBowTieSet();
	}


	public void rewardAssignment(int parasiteNum, int hostNum, double reward)
	{
		//parasite and host problem
		/**
		 * assignment according to number of action
		 * use RL therefore each type of percentage will converge.
		 * ****/
		/**
		 *  1. calculate number of parasite classifiers and the number of host classifiers
		 *  classify reward according to parasite and host
		 * **/
		this.population.setInfectionRewardMap(parasiteNum, hostNum, reward);
		/**
		 * re-assign reward for parasite and host
		 * the final reward received is linear combination of the parasite reward and the host reward.
		 * to calculate the parasite reward and host reward, at least two entry of reward_map are required.
		 * **/
		this.population.calculateRewardMatrix();


	}

	public void calculateMatrix_example()
	{
		//this.population.calculateMatrix_example();
		System.out.println(" calculateMatrix_example");
		/*
		double[][] vals = {{1.,0},{0,1}};
		Matrix A = new Matrix(vals);
		double[][] vals_2 = {{4},{5}};
		Matrix b = new Matrix(vals_2);
		*/
		Matrix A= Matrix.random(4,4);
		Matrix b = Matrix.random(4,1);

		Matrix x = A.solve(b);
		System.out.println(" result b1("+x.get(0,0)+"),b1("+x.get(1,0)+")");

	}

	public void pseudoinverse()
	{
		//Matrix pseudoinverse_matrix= matrix_A.inverse();
		/*
		System.out.println("  matrix_A.rank(" +matrix_A.rank()+"). ");

		//double[][] vals = {{1.,2.,3},{4.,5.,6.},{7.,8.,10.}};
		//Matrix test = new Matrix(vals);
		//System.out.println("  matrix_A.rank(" +test.rank()+"). ");

		//double[][] vals_1 = {{0,0,1},{0,1,0},{1,0,0}};
		//test = new Matrix(vals_1);
		//System.out.println("  matrix_A.rank(" +test.rank()+"). ");

		//double[][] vals_2 = {{0,0,0,1},{0,0,1,0},{0,1,0,0}};
		//test = new Matrix(vals_2);
		//System.out.println("  matrix_A.rank(" +test.rank()+"). ");

		//double[][] vals_3 = {{1,0,0},{0,1,0},{0,0,1},{1,1,0 }};
		double[][] vals_3 = {{1,0,0},{0,1,0},{1,1,1}};
		Matrix test_m = new Matrix(vals_3);
		System.out.println("  matrix_A.rank(" +test_m.rank()+"). ");
		*/
		/*
		double[][] vals_4 = {{1000},{-1000},{0}};
		Matrix b = new Matrix(vals_4);


		Matrix x= new Matrix(3,1,0);
		x = test_m.solve(b);
		System.out.println("  x("+x.get(0,0)+"), ("+x.get(1,0)+"), ("+x.get(2,0)+"). ");
		*/

		double[] action_1 = {1,0};
		double[] action_2 = {0,1};
		double[] action_3 = {1,1};
		double[] action_4 = {0,0};
		//double[] batch_1 = Arrays.copyOf(action_1,6);
		//System.arraycopy(action_2, 0,batch_1, 3,3);

		/*
		double[][] a_metrix_2_steps_temp = new double[5][4];
		a_metrix_2_steps_temp[0] = link_two_array(action_1, action_1);
		a_metrix_2_steps_temp[1] = link_two_array(action_1, action_2);
		a_metrix_2_steps_temp[2] = link_two_array(action_2, action_1);
		a_metrix_2_steps_temp[3] = link_two_array(action_2, action_2);
		a_metrix_2_steps_temp[4] = link_two_array(action_3, action_4);
		Matrix a_metrix_2_steps = new Matrix(a_metrix_2_steps_temp,5,4);
		System.out.println("  1. a_metrix_2_steps_temp.rank(" +a_metrix_2_steps.rank()+"). <-4 ");

		a_metrix_2_steps_temp[4] = link_two_array(action_1, action_4);
		a_metrix_2_steps = new Matrix(a_metrix_2_steps_temp,5,4);
		System.out.println("  2. a_metrix_2_steps_temp.rank(" +a_metrix_2_steps.rank()+"). <-4 ");

		a_metrix_2_steps_temp[4] = link_two_array(action_3, action_3);
		a_metrix_2_steps = new Matrix(a_metrix_2_steps_temp,5,4);
		System.out.println("  3. a_metrix_2_steps_temp.rank(" +a_metrix_2_steps.rank()+"). <-4 ");

		*/

		/**
		 * 2 steps test, work!
		 * *******/
		/*
		double[][] a_metrix_2_steps_sub_temp = new double[4][4];
		a_metrix_2_steps_sub_temp[0] = link_two_array(action_1, action_1);// {1,0} {1,0}
		a_metrix_2_steps_sub_temp[1] = link_two_array(action_1, action_2);// {1,0} {0,1};
		a_metrix_2_steps_sub_temp[2] = link_two_array(action_2, action_2);// {0,1} {0,1};
		a_metrix_2_steps_sub_temp[3] = link_two_array(action_3, action_4);// {1,1} {0,0};
		Matrix a_metrix_2_steps_sub = new Matrix(a_metrix_2_steps_sub_temp,4,4);

		double[][] vals_4 = {{2},{0},{-2} ,{0} };
		Matrix b = new Matrix(vals_4);
		Matrix x= new Matrix(4,1,0);
		x = a_metrix_2_steps_sub.solve(b);
		System.out.println("  x("+x.get(0,0)+"), ("+x.get(1,0)+"), ("+x.get(2,0)+"), " +
				"("+x.get(3,0)+")");//, ("+x.get(4,0)+"), ("+x.get(5,0)+"), ("+x.get(6,0)+"). ");

		System.out.println("  1.a_metrix_2_steps_sub(" +a_metrix_2_steps_sub.rank()+"). <-4 ");

		a_metrix_2_steps_sub_temp[2] = link_two_array(action_2, action_1);//{0,1}{1,0}
		a_metrix_2_steps_sub = new Matrix(a_metrix_2_steps_sub_temp,4,4);

		double[][] vals_5 = {{2},{0},{0},{0} };
		Matrix b_5 = new Matrix(vals_5);
		x = a_metrix_2_steps_sub.solve(b_5);
		System.out.println("  x("+x.get(0,0)+"), ("+x.get(1,0)+"), ("+x.get(2,0)+"), " +
				"("+x.get(3,0)+")");//, ("+x.get(4,0)+"), ("+x.get(5,0)+"), ("+x.get(6,0)+"). ");
		System.out.println("  2.a_metrix_2_steps_sub(" +a_metrix_2_steps_sub.rank()+"). <-4 ");
		*/

		/**
		 * 3 steps : assign reward to each steps disregard the action in each step
		 * *********/



		//adding

		double[][] a_metrix_3_steps_full_temp = new double[11][6];
		a_metrix_3_steps_full_temp[0] = link_three_array(action_1, action_1, action_1);// {1,0} {1,0}
		a_metrix_3_steps_full_temp[1] = link_three_array(action_1, action_1, action_2);// {1,0} {0,1};
		a_metrix_3_steps_full_temp[2] = link_three_array(action_1, action_2, action_1);// {0,1} {0,1};
		a_metrix_3_steps_full_temp[3] = link_three_array(action_1, action_2, action_2);// {1,1} {0,0};

		a_metrix_3_steps_full_temp[4] = link_three_array(action_2, action_1, action_1);// {1,0} {1,0}
		a_metrix_3_steps_full_temp[5] = link_three_array(action_2, action_1, action_2);// {1,0} {0,1};
		a_metrix_3_steps_full_temp[6] = link_three_array(action_2, action_2, action_1);// {0,1} {0,1};
		a_metrix_3_steps_full_temp[7] = link_three_array(action_2, action_2, action_2);// {1,1} {0,0};

		a_metrix_3_steps_full_temp[8] = link_three_array(action_3, action_4, action_4);// {1,0} {0,1};
		a_metrix_3_steps_full_temp[9] = link_three_array(action_4, action_3, action_4);// {0,1} {0,1};
		a_metrix_3_steps_full_temp[10] = link_three_array(action_4, action_4, action_3);// {1,1} {0,0};


		Matrix a_metrix_3_steps_full = new Matrix(a_metrix_3_steps_full_temp,11,6);
		System.out.println("  1.a_metrix_3_steps_full(" +a_metrix_3_steps_full.rank()+"). <-6 ");


		double[][] a_metrix_3_steps_sub_temp = new double[6][6];
		a_metrix_3_steps_sub_temp[0] = link_three_array(action_1, action_1, action_1);// {1,0} {1,0}
		a_metrix_3_steps_sub_temp[1] = link_three_array(action_1, action_1, action_2);// {1,0} {0,1};
		a_metrix_3_steps_sub_temp[2] = link_three_array(action_1, action_2, action_1);// {0,1} {0,1};
		//a_metrix_3_steps_sub_temp[3] = link_three_array(action_1, action_2, action_2);// {1,1} {0,0};

		//a_metrix_3_steps_sub_temp[4] = link_three_array(action_2, action_1, action_1);// {1,0} {1,0}
		//a_metrix_3_steps_sub_temp[5] = link_three_array(action_2, action_1, action_2);// {1,0} {0,1};
		//a_metrix_3_steps_sub_temp[6] = link_three_array(action_2, action_2, action_1);// {0,1} {0,1};
		//a_metrix_3_steps_sub_temp[7] = link_three_array(action_2, action_2, action_2);// {1,1} {0,0};

		a_metrix_3_steps_sub_temp[3] = link_three_array(action_3, action_4, action_4);// {1,0} {0,1};
		a_metrix_3_steps_sub_temp[4] = link_three_array(action_4, action_3, action_4);// {0,1} {0,1};
		a_metrix_3_steps_sub_temp[5] = link_three_array(action_4, action_4, action_3);// {1,1} {0,0};

		Matrix a_metrix_3_steps_sub = new Matrix(a_metrix_3_steps_sub_temp,6,6);
		System.out.println("  2.a_metrix_3_steps_sub(" +a_metrix_3_steps_sub.rank()+"). <-6 ");















		/**
		 * step_lineIndex_row[0]= 9*(step_action[0]-1) + 3*(step_action[1]-1)+1*(step_action[2]-1) ; // start from line 0
		 * **/
		/*
		//normal steps
		double[] batch_0 =link_three_array(action_1, action_1, action_1);
		double[] batch_1 =link_three_array(action_1, action_1, action_2);

		double[] batch_2 =link_three_array(action_1, action_2, action_1);
		double[] batch_3 =link_three_array(action_1, action_2, action_2);

		double[] batch_4 =link_three_array(action_2, action_1, action_1);
		double[] batch_5 =link_three_array(action_2, action_1, action_2);

		double[] batch_6 =link_three_array(action_2, action_2, action_1);
		double[] batch_7 =link_three_array(action_2, action_2, action_2);

		// action 3and4 represents relationship between correct choice and wrong choice.
		double[] batch_8 =link_three_array(action_3, action_3, action_3);
		double[] batch_9 =link_three_array(action_4, action_3, action_4);
		double[] batch_10 =link_three_array(action_4, action_4, action_3);


		double[][] a_metrix_temp = new double[8][6];// 6 lines/rows, 6 column

		a_metrix_temp[0]= batch_0;
		a_metrix_temp[1]= batch_1;
		a_metrix_temp[2]= batch_2;
		a_metrix_temp[3]= batch_3;
		a_metrix_temp[4]= batch_4;
		a_metrix_temp[5]= batch_5;
		a_metrix_temp[6]= batch_6;
		a_metrix_temp[7]= batch_7;



		Matrix a_matrix= new Matrix(a_metrix_temp,8,6);
		System.out.println("  a_matrix.rank(" +a_matrix.rank()+"). <-6 ");

		double[][] a_metrix_inner_rules_temp = new double[11][6];// 6 lines/rows, 6 column
		a_metrix_inner_rules_temp[0]= batch_0;
		a_metrix_inner_rules_temp[1]= batch_1;
		a_metrix_inner_rules_temp[2]= batch_2;
		a_metrix_inner_rules_temp[3]= batch_3;
		a_metrix_inner_rules_temp[4]= batch_4;
		a_metrix_inner_rules_temp[5]= batch_5;
		a_metrix_inner_rules_temp[6]= batch_6;
		a_metrix_inner_rules_temp[7]= batch_7;

		a_metrix_inner_rules_temp[8]= batch_8;
		a_metrix_inner_rules_temp[9]= batch_9;
		a_metrix_inner_rules_temp[10]= batch_10;

		Matrix a_matrix_inner_rules= new Matrix(a_metrix_inner_rules_temp,8,6);
		System.out.println("  a_matrix_inner_rules.rank(" +a_matrix_inner_rules.rank()+"). <-6 ");
		*/

		/*
		int[] lineIndex_row={0,10,20,3,4,5,6};
		int[] lineIndex_column={0,1,2,3,5,6,8};
		Matrix a_fullrank_matrix= extract_subset_matrix(this.matrix_A,lineIndex_row,lineIndex_column);

		double[][] vals_4 = {{3},{1},{1} ,{1},{-1} ,{-1},{1} };
		Matrix b = new Matrix(vals_4);


		Matrix x= new Matrix(7,1,0);
		x = a_fullrank_matrix.solve(b);
		System.out.println("  x("+x.get(0,0)+"), ("+x.get(1,0)+"), ("+x.get(2,0)+"), " +
				"("+x.get(3,0)+"), ("+x.get(4,0)+"), ("+x.get(5,0)+"), ("+x.get(6,0)+"). ");
		*/

	}

	/**
	 * dynamically establish/span an orthogonal reward space for matrix calculation.
	 * O=scenario_seq
	 * S= number of steps (multistep)
	 * T= number of actions
	 *
	 * !!! OST : behaviours_matrix and reward_matrix stored here
	 * **/
	public boolean span_orthogonal_reward_space(double batch_reward,int O, int S)
	{
		int T=the_DIVERSITY_of_actions;

		int dimension_column= S*T;

		//1. setup a new OST
		HashMap<String, Matrix> an_OST = new  HashMap<String, Matrix>();
		Matrix orthogonal_behaviours_matrix= new Matrix(dimension_column,dimension_column,0);
		Matrix reward_received_matrix =new Matrix(dimension_column,1,0);

		int hashnum= O;//int hashnum= O*10000+ S*100+T;
		int rank_reward=0;

		//2. retrieve behaviours_matrix and reward_received_matrix
		if(	this.OST_hashmap.containsKey(hashnum))
		{
			//get behavious matrix
			an_OST= this.OST_hashmap.get(hashnum);
			orthogonal_behaviours_matrix 	= an_OST.get("behaviours_matrix");		//this.OST_hashmap.get(hashnum).get("behaviours_matrix");
			reward_received_matrix			= an_OST.get("reward_received_matrix");	//this.OST_hashmap.get(hashnum).get("reward_received_matrix");
		}
		else
		{
			// setup new behaviours_matrix  and reward_received_matrix for one OST


			//1. setup behaviours_matrix hashmap
			orthogonal_behaviours_matrix= this.linearRegressionRewardMethod.initate_orthogonal_behaviours_matrix(S,T);
			an_OST.put("behaviours_matrix",orthogonal_behaviours_matrix );
			//2.setup new  reward_received_matrix
			reward_received_matrix		= this.linearRegressionRewardMethod.initate_reward_received_matrix(S,T);
			an_OST.put("reward_received_matrix", reward_received_matrix );
			//3. put an_OST into OST pan
			this.OST_hashmap.put(hashnum,an_OST);
		}
		rank_reward= orthogonal_behaviours_matrix.rank();

		//3.update orthogonal_behaviours_matrix

		orthogonal_behaviours_matrix = this.linearRegressionRewardMethod.establish_orthogonal_behaviours_matrix( S, T, orthogonal_behaviours_matrix, generate_step_action_applied_stack());
		an_OST.put("behaviours_matrix",orthogonal_behaviours_matrix );

		//4. update reward_received_matrix
		if(orthogonal_behaviours_matrix.rank()>rank_reward)
		{
			reward_received_matrix= this.linearRegressionRewardMethod.establish_reward_received_matrix(rank_reward,reward_received_matrix, batch_reward);
			an_OST.put("reward_received_matrix",reward_received_matrix);
		}
		//5. update OST_hashmap
		this.OST_hashmap.put(hashnum,an_OST);

		//6.check if the space has been spanned orthogonally
		//check the behaviour matrix is full rank
		if(orthogonal_behaviours_matrix.rank() ==dimension_column)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public int get_number_of_steps()
	{
		int S= this.messages.size();
		return S;
	}
	public void span_matrix_steps_rewards_vectors()
	{
		int S= get_number_of_steps();

		this.matrix_steps_rewards= new Matrix(S, 1,initial_default_reward);
	}

	public Matrix calculate_reward_all_steps_matrix(int O, int S, int T)
	{

		int hashnum=O;//int hashnum= O*10000+ S*100+T;
		Matrix reward_all_steps_matrix=new Matrix(S*T,1,0);

		if(	this.OST_hashmap.containsKey(hashnum))
		{
			HashMap<String, Matrix> an_OST = new HashMap<String, Matrix>();
			an_OST= this.OST_hashmap.get(hashnum);
			reward_all_steps_matrix=this.linearRegressionRewardMethod.calculate_reward_all_steps_matrix(S, T, an_OST.get("behaviours_matrix"), an_OST.get("reward_received_matrix"));
		}

		return reward_all_steps_matrix;
	}



	public Matrix extract_reward_S_step_from_all(int S, Matrix  reward_all_steps_matrix)
	{
		int[] step_lineIndex_column =matrix_get_applied_column_from_messages();

		matrix_steps_rewards = this.linearRegressionRewardMethod.extract_reward_S_step_from_all(S, reward_all_steps_matrix, step_lineIndex_column);

		return matrix_steps_rewards;
	}


	public void calculate_step_reward_Matrix_flexible(double batch_reward,int scenario_seq)
	{
		/**
		 * 1. set flexible matrix_A according to "num_step_flexible" and "num_action_flexible"
		 * ****/
		int num_step_flexible=3;
		int num_action_flexible=3;


		Matrix behaviour_matrix_a_flexible = creat_matrix_A_flexible(num_step_flexible,num_action_flexible);
		System.out.println("		flexible_matrix_a.rank("+behaviour_matrix_a_flexible.rank()+"), 		" +
				"for num_step("+num_step_flexible+") and num_action("+num_action_flexible+")," +
				" row("+behaviour_matrix_a_flexible.getRowDimension()+")* column("+behaviour_matrix_a_flexible.getColumnDimension()+")");


		/**
		 * 2. update  matrix_reward based on what the recent batch
		 *  put TaskReward into reward metrix according to the row num
		 * ***/
		// rank = t+ (s-1)*(t-1), see my report, t=num_action_flexible, s=num_step_flexible
		//rank =7 when s=3 and t=3
		int behaviour_matrix_a_flexible_rank= num_action_flexible+ (num_step_flexible-1)*(num_action_flexible-1);

		int[] step_lineIndex_column =matrix_get_applied_column_from_messages(); // return columns according to the messages
		int step_lineIndex_row_seqNum =  matrix_get_applied_row_seqNum_of_behavirourMatrix(num_action_flexible);
		//System.out.println(" step_lineIndex_column[0]("+step_lineIndex_column[0]+");" + " step_lineIndex_column[1]("+step_lineIndex_column[1]+");" + " step_lineIndex_column[2]("+step_lineIndex_column[2]+") ");
		//System.out.println("step_lineIndex_row[0]("+step_lineIndex_row[0]+")");

		/**
		 * update current reward
		 * ***/
		//int matrix_batch_reward_updated_num = update_received_reward_matrix(batch_reward, scenario_seq, step_lineIndex_row_seqNum); //update  a member of class
		//
		//Matrix matrix_batch_reward_seq= select_which_matrix_batch(scenario_seq);

		/**
		 * create reward_pan
		 * **/
		update_reward_pan_map(scenario_seq,num_step_flexible,num_action_flexible,step_lineIndex_row_seqNum,batch_reward);



	}

	public void update_reward_pan_map(int scenario_seq, int num_step_flexible, int num_action_flexible,int step_lineIndex_row_seqNum, double batch_reward)
	{
		int reward_map_hashNum= scenario_seq*10000+ num_step_flexible*100+ num_action_flexible;
		int reward_updated_count=0;

		if(!this.reward_pan.containsKey(reward_map_hashNum))
		{
			//1.
			HashMap<Integer, Double> behaviours_reward_hashmap = new HashMap<Integer, Double>();
			behaviours_reward_hashmap.put(step_lineIndex_row_seqNum,batch_reward);// one row of 27 line behaviour matrix//(row, reward)
			// behaviours_reward_hashmap is dynamic

			//2.
			this.reward_pan.put(reward_map_hashNum, behaviours_reward_hashmap);

			reward_updated_count= behaviours_reward_hashmap.size();
			System.out.println(" reward_updated_count("+reward_updated_count+")<-1");
		}
		else
		{
			//behaviours_reward_hashmap <row, reward>
			HashMap<Integer, Double> behaviours_reward_hashmap =this.reward_pan.get(reward_map_hashNum);
			behaviours_reward_hashmap.put(step_lineIndex_row_seqNum,batch_reward);// one row of 27 line behaviour matrix//(row, reward)
			this.reward_pan.put(reward_map_hashNum, behaviours_reward_hashmap);
			//check
			reward_updated_count= behaviours_reward_hashmap.size();
			System.out.println(" reward_updated_count("+reward_updated_count+")");

			for(Map.Entry<Integer, Double> entry:behaviours_reward_hashmap.entrySet())
			{
				System.out.println("		check >>>>>>>>>>>>>>>" +
						"	 row_num('"+entry.getKey()+"') ==" +
						"	 a_reward('"+entry.getValue()+"')");
			}

		}
	}



	/**
	 * calculate_anticipatedReward_Matrix( ): calculate reward for each step
	 * input the action in each step, and calculate reward for the classifier updating by linear regression
	 * @param "matrix_A", which represents a complete map of multi-step scenario. this Matrix can be preset if the number of steps is preset.
	 * @param "step_reward_vect": 1 colon, each element is the step reward for the updating.
	 * @param "received_reward": 1 colon, each element is the total reward received at the end of a batch training.
	 * @param "step_action_applied_hashmap": record which action has been applied in each step during this batch training.
	 *                                   <step_seq, action_seq>,
	 * **/
	public void calculate_step_reward_Matrix(double batch_reward,int scenario_seq)
	{
		/**
		 * 1. get matrix
		 * **/
//		//Matrix matrix_A =create_matrix_A();
//		//this.matrix_A;
//		/*
//		for(int i=1;i<11;i++)
//		{
//			for(int j=1; j<11;j++)
//			{
//				int num_step_flexible=j;
//				int num_action_flexible=i;
//
//				Matrix flexible_matrix_a = creat_matrix_A_flexible(num_step_flexible,num_action_flexible);
//				System.out.println("		flexible_matrix_a.rank("+flexible_matrix_a.rank()+"), 		" +
//						"for num_step("+num_step_flexible+") and num_action("+num_action_flexible+")," +
//						" row("+flexible_matrix_a.getRowDimension()+")* column("+flexible_matrix_a.getColumnDimension()+")");
//			}
//		}
//		*/
		/*
		int num_step_flexible=3;
		int num_action_flexible=3;

		Matrix flexible_matrix_a = creat_matrix_A_flexible(num_step_flexible,num_action_flexible);
		System.out.println(" flexible_matrix_a.rank("+flexible_matrix_a.rank()+"), " +
				"for num_step("+num_step_flexible+") and num_action("+num_action_flexible+").");
		*/
		/**
		 * 2. update  matrix_reward based on what the recent batch
		 *  put TaskReward into reward metrix according to the row num
 		 * ***/

		//todo: change scenario
		//scenario_seq=0;

		//Matrix matrix_batch_reward= creat_matrix_reward(27); //don' need this //move it as a member of class
		//Matrix matrix_update_reward= creat_matrix_reward(9);
		int full_rank=9;// number of step * number of action
		int matrix_a_full_rank=27;
		int step_num=3;
		int action_num=3;
		int[] step_lineIndex_column =matrix_get_applied_column(step_num); // return columns, dimension number is 9
		int[] step_lineIndex_row= matrix_get_applied_row_one(7, action_num);// return the first row, dimension (row) num = full rank
		//System.out.println(" step_lineIndex_column[0]("+step_lineIndex_column[0]+");" + " step_lineIndex_column[1]("+step_lineIndex_column[1]+");" + " step_lineIndex_column[2]("+step_lineIndex_column[2]+") ");
		//System.out.println("step_lineIndex_row[0]("+step_lineIndex_row[0]+")");

		/**
		 * update current reward
		 * ***/
		int matrix_batch_reward_updated_num = update_received_reward_matrix(batch_reward, scenario_seq, step_lineIndex_row[0]); //update  a member of class
		//
		//Matrix matrix_batch_reward_seq= select_which_matrix_batch(scenario_seq);



		//make sure the matrix_batch_reward_updated_num has all experience
		//todo: this might be no necessary...
		// todo: remove from here
		if(matrix_batch_reward_updated_num <= (matrix_a_full_rank*0.4) )
		{
			System.out.println(" not yet!");
			return;
		}
		// todo:remove end here

		/**
		 * ! !!! generate_full_rank_compressed_matrix_ replaced by generate_full_rank_decomposed_matrix
		 *  ***/
		Matrix compressed_matrix = generate_full_rank_compressed_matrix(step_lineIndex_row, step_lineIndex_column,scenario_seq);

		System.out.println(" choose rows ( "+step_lineIndex_row[0]+", "+step_lineIndex_row[1]+", "+step_lineIndex_row[2]+"" +
				" , "+step_lineIndex_row[3]+" , "+step_lineIndex_row[4]+", "+step_lineIndex_row[5]+"" +
				", "+step_lineIndex_row[6]+") ");
		System.out.println(" choose column ( "+step_lineIndex_column[0]+", "+step_lineIndex_column[1]+", "+step_lineIndex_column[2]+")");
		if(compressed_matrix.rank()!=9)
		{
			System.out.println(" not yet! compressed_matrix.rank("+compressed_matrix.rank()+")");
			return;
		}


		//Matrix decomposed_matrix = generate_full_rank_decomposed_matrix(step_lineIndex_row, step_lineIndex_column,scenario_seq);

		//A * x = b;
		// A = compressed_matrix
		// b = matrix_compressed_received_reward
		// x = matrix_to_update_reward

		Matrix matrix_to_update_reward = new Matrix(full_rank,1,initial_default_reward); //x
		Matrix matrix_compressed_received_reward = new Matrix(full_rank,1,initial_default_reward);//b


		//get b
		// get b.ori
		int[] lineIndex_one_column={scenario_seq};
		Matrix matrix_compressed_received_reward_ori =  new Matrix(7,1,initial_default_reward);//b
		matrix_compressed_received_reward_ori = extract_subset_matrix(this.matrix_batch_reward, step_lineIndex_row, lineIndex_one_column);
		//matrix_compressed_received_reward_ori = extract_subset_matrix(this.matrix_batch_reward, step_lineIndex_row, lineIndex_one_column);

		//get b.add
		Matrix matrix_compressed_received_reward_add = new Matrix(2,1,initial_default_reward);
		matrix_compressed_received_reward_add.set(0,0,-1000); //(action_00, action_00, action_all)
		matrix_compressed_received_reward_add.set(1,0,-1000); //(action_00, action_all, action_00)


		matrix_compressed_received_reward = matrix_copy_from(matrix_compressed_received_reward,matrix_compressed_received_reward_ori,0,0);
		matrix_compressed_received_reward = matrix_copy_from(matrix_compressed_received_reward,matrix_compressed_received_reward_add,7,0);

		if(compressed_matrix.rank()==full_rank)
		{


			matrix_to_update_reward= compressed_matrix.solve(matrix_compressed_received_reward);

			System.out.println("  matric result:( "+matrix_to_update_reward.get(0,0)+")," + " ( "+matrix_to_update_reward.get(1,0)+"), ( "+matrix_to_update_reward.get(2,0)+")");
			System.out.println("  matric result:( "+matrix_to_update_reward.get(3,0)+")," + " ( "+matrix_to_update_reward.get(4,0)+"), ( "+matrix_to_update_reward.get(5,0)+")");
			System.out.println("  matric result:( "+matrix_to_update_reward.get(6,0)+")," + " ( "+matrix_to_update_reward.get(7,0)+"), ( "+matrix_to_update_reward.get(8,0)+")");

			//update rewardmap
			//Matrix matrix_steps_rewards= new Matrix(3,1);
			for(int i=0; i< matrix_steps_rewards.getRowDimension(); i++)
			{
				int update_index = step_lineIndex_column[i];
				double update_value = matrix_to_update_reward.get(update_index,0);
				matrix_steps_rewards.set(i,0,update_value);
				System.out.println("  matrix_steps_rewards :( "+update_value+") for step("+i+")");
			}

		}
		else
		{
			//don't update....lol
		}

		// to test
		pseudoinverse();

		//Matrix matrix_to_update_reward = new Matrix(3,1,0.0001);
		//Matrix matrix_compressed_received_reward = new Matrix(3,1,1000);
		//matrix_to_update_reward= compressed_matrix.solve(matrix_compressed_received_reward);
		/**
		 * matrix calculationcompressed_
		 * ***/

		//generate sub set MaxtrixA
		//generate_subset_matrixA(step_lineIndex_row,step_lineIndex_column);
		//matrix_update_reward= this.matrix_A.solve(matrix_received_reward);


				/*
		for(Integer an_action_mask:step_action_applied_stack)
		{
			// the first one
			System.out.println("	apply action mask("+an_action_mask+")");
		}

		while(!step_action_applied_stack.isEmpty())
		{
			//pop up the most recent one (the last one)
			Integer an_action_mask = step_action_applied_stack.pop();
			System.out.println("	pop up action mask("+an_action_mask+")");
		}
		*/
	}


//	public void calculate_step_reward_Matrix_dichotomy(double batch_reward,int scenario_seq) {
//
//		/**
//		 * 2. update  matrix_reward based on what the recent batch
//		 *  put TaskReward into reward metrix according to the row num
//		 * ***/
//
//
//		int full_rank = 9;// number of step * number of action
//		int matrix_a_full_rank = 27;
//		int step_num = 3;
//		int action_num = 3;
//		int[] step_lineIndex_column = matrix_get_applied_column(step_num); // return columns, dimension number is 9
//		int[] step_lineIndex_row = matrix_get_applied_row_one(6, action_num);// return the first row, dimension (row) num = full rank
//		System.out.println(" step_lineIndex_column[0]("+step_lineIndex_column[0]+");" + " step_lineIndex_column[1]("+step_lineIndex_column[1]+");" + " step_lineIndex_column[2]("+step_lineIndex_column[2]+") ");
//		//System.out.println("step_lineIndex_row[0]("+step_lineIndex_row[0]+")");
//
//		/**
//		 * 3. update current reward
//		 * ***/
//		int matrix_batch_reward_updated_num = update_received_reward_matrix(batch_reward, scenario_seq, step_lineIndex_row[0]); //update  a member of class
//
//		/**
//		 * 4. make sure the matrix_batch_reward_updated_num has all experience
//		 * **/
//		//todo: this might be no necessary...
//		// todo: remove from here
//		if(matrix_batch_reward_updated_num <= (matrix_a_full_rank*0.4) )
//		{
//			System.out.println(" not yet!");
//			return;
//		}
//		// todo:remove end here
//
//		/**
//		 * 5. calculated decomposed_matrix, which is matrix_A'
//		 * **/
//
//		Matrix decomposed_matrix = generate_full_rank_decomposed_matrix(step_lineIndex_row, step_lineIndex_column,scenario_seq);
//		if(decomposed_matrix.rank()!=6)
//		{
//			System.out.println(" not yet! compressed_matrix.rank("+decomposed_matrix.rank()+") should be 6 for 3 steps");
//			return;
//		}
//
//		/**
//		 * 6. get reward matrix b (matrix_compressed_received_reward)
//		 * **/
//
//		// A * x = b; 6*6 X 6*1= 6*1
//		// A = decomposed_matrix
//		// b = matrix_compressed_received_reward
//		// x = matrix_to_update_reward
//
//		Matrix matrix_to_update_reward = new Matrix(6,1,initial_default_reward); //x
//		Matrix matrix_compressed_received_reward = new Matrix(6,1,initial_default_reward);//b
//		//System.out.println(" step_lineIndex_row[0]("+step_lineIndex_row[0]+"), step_lineIndex_row[0]("+step_lineIndex_row[1]+")," + " step_lineIndex_row[0]("+step_lineIndex_row[2]+"), step_lineIndex_row[0]("+step_lineIndex_row[3]+")," + " step_lineIndex_row[0]("+step_lineIndex_row[4]+"), step_lineIndex_row[0]("+step_lineIndex_row[5]+").");
//		System.out.println(" step_lineIndex_column[0]("+step_lineIndex_column[0]+");" + " step_lineIndex_column[1]("+step_lineIndex_column[1]+");" + " step_lineIndex_column[2]("+step_lineIndex_column[2]+") ");
//
//
//		//get b
//		// get b.ori
//		int[] lineIndex_one_column={scenario_seq};
//		matrix_compressed_received_reward = extract_subset_matrix(this.matrix_batch_reward, step_lineIndex_row, lineIndex_one_column);
//
//
//		if(decomposed_matrix.rank()==6)
//		{
//
//
//			matrix_to_update_reward= decomposed_matrix.solve(matrix_compressed_received_reward);
//
//			System.out.println("  matric result:( "+matrix_to_update_reward.get(0,0)+")," + " ( "+matrix_to_update_reward.get(1,0)+"), ");
//			System.out.println("  matric result:( "+matrix_to_update_reward.get(2,0)+")," + " ( "+matrix_to_update_reward.get(3,0)+"), )");
//			System.out.println("  matric result:( "+matrix_to_update_reward.get(4,0)+")," + " ( "+matrix_to_update_reward.get(5,0)+"), )");
//
//			//update rewardmap
//			//Matrix matrix_steps_rewards= new Matrix(3,1);
//			/*
//			for(int i=0; i< matrix_steps_rewards.getRowDimension(); i++)
//			{
//				int update_index = step_lineIndex_column[i];
//				double update_value = matrix_to_update_reward.get(update_index,0);
//				matrix_steps_rewards.set(i,0,update_value);
//				System.out.println("  matrix_steps_rewards :( "+update_value+") for step("+i+")");
//			}
//			*/
//
//		}
//		else
//		{
//			//don't update....lol
//		}
//
//
//
//	}

//	public void calculate_step_reward_Matrix_dichotomy_LV(double batch_reward,int scenario_seq) {
//
//		/**
//		 * 2. update  matrix_reward based on what the recent batch
//		 *  put TaskReward into reward metrix according to the row num
//		 * ***/
//
//
//		int full_rank = 9;// number of step * number of action
//		int matrix_a_full_rank = 27;
//		int step_num = 3;
//		int action_num = 3;
//		int[] step_lineIndex_column = matrix_get_applied_column(step_num); // return columns, dimension number is 9
//		int[] step_lineIndex_row = matrix_get_applied_row_one(6, action_num);// return the first row, dimension (row) num = full rank
//		System.out.println(" step_lineIndex_column[0]("+step_lineIndex_column[0]+");" + " step_lineIndex_column[1]("+step_lineIndex_column[1]+");" + " step_lineIndex_column[2]("+step_lineIndex_column[2]+") ");
//		//System.out.println("step_lineIndex_row[0]("+step_lineIndex_row[0]+")");
//
//		/**
//		 * 3. update current reward
//		 * ***/
//		int matrix_batch_reward_updated_num = update_received_reward_matrix(batch_reward, scenario_seq, step_lineIndex_row[0]); //update  a member of class
//
//		/**
//		 * 4. make sure the matrix_batch_reward_updated_num has all experience
//		 * **/
//		//todo: this might be no necessary...
//		// todo: remove from here
//		if(matrix_batch_reward_updated_num <= (matrix_a_full_rank*0.4) )
//		{
//			System.out.println(" not yet!");
//			return;
//		}
//		// todo:remove end here
//
//		/**
//		 * 5. calculated decomposed_matrix, which is matrix_A'
//		 * **/
//
//		Matrix decomposed_matrix = generate_full_rank_decomposed_matrix_LV(step_lineIndex_row, step_lineIndex_column,scenario_seq);
//		if(decomposed_matrix.rank()!=6)
//		{
//			System.out.println(" not yet! compressed_matrix.rank("+decomposed_matrix.rank()+") should be 6 for 3 steps");
//			return;
//		}
//
//		/**
//		 * 6. get reward matrix b (matrix_compressed_received_reward)
//		 * **/
//
//		// A * x = b; 6*6 X 6*1= 6*1
//		// A = decomposed_matrix
//		// b = matrix_compressed_received_reward
//		// x = matrix_to_update_reward
//
//		Matrix matrix_to_update_reward = new Matrix(6,1,initial_default_reward); //x
//		Matrix matrix_compressed_received_reward = new Matrix(6,1,initial_default_reward);//b
//		//System.out.println(" step_lineIndex_row[0]("+step_lineIndex_row[0]+"), step_lineIndex_row[0]("+step_lineIndex_row[1]+")," + " step_lineIndex_row[0]("+step_lineIndex_row[2]+"), step_lineIndex_row[0]("+step_lineIndex_row[3]+")," + " step_lineIndex_row[0]("+step_lineIndex_row[4]+"), step_lineIndex_row[0]("+step_lineIndex_row[5]+").");
//		System.out.println(" step_lineIndex_column[0]("+step_lineIndex_column[0]+");" + " step_lineIndex_column[1]("+step_lineIndex_column[1]+");" + " step_lineIndex_column[2]("+step_lineIndex_column[2]+") ");
//
//
//		//get b
//		// get b.ori
//		int[] lineIndex_one_column={scenario_seq};
//		matrix_compressed_received_reward = extract_subset_matrix(this.matrix_batch_reward, step_lineIndex_row, lineIndex_one_column);
//
//
//		if(decomposed_matrix.rank()==6)
//		{
//
//
//			matrix_to_update_reward= decomposed_matrix.solve(matrix_compressed_received_reward);
//
//			System.out.println("  matric result:( "+matrix_to_update_reward.get(0,0)+")," + " ( "+matrix_to_update_reward.get(1,0)+"), ( "+matrix_to_update_reward.get(2,0)+")");
//			System.out.println("  matric result:( "+matrix_to_update_reward.get(3,0)+")," + " ( "+matrix_to_update_reward.get(4,0)+"), ( "+matrix_to_update_reward.get(5,0)+")");
//			//System.out.println("  matric result:( "+matrix_to_update_reward.get(6,0)+")," + " ( "+matrix_to_update_reward.get(7,0)+"), ( "+matrix_to_update_reward.get(8,0)+")");
//
//			//update rewardmap
//			//Matrix matrix_steps_rewards= new Matrix(3,1);
//			/*
//			for(int i=0; i< matrix_steps_rewards.getRowDimension(); i++)
//			{
//				int update_index = step_lineIndex_column[i];
//				double update_value = matrix_to_update_reward.get(update_index,0);
//				matrix_steps_rewards.set(i,0,update_value);
//				System.out.println("  matrix_steps_rewards :( "+update_value+") for step("+i+")");
//			}
//			*/
//
//		}
//		else
//		{
//			//don't update....lol
//		}
//
//
//
//	}


	/*
	public Matrix compose_reward_matrix(Matrix matrix_compressed_received_reward_ori, int[] step_lineIndex_column)
	{
		Matrix matrix_compressed_received_reward = new Matrix(5,0,0);
		for(int i=0; i<3;i++)
		{
			double a_value=matrix_compressed_received_reward_ori.get( step_lineIndex_column[0],0);
			matrix_compressed_received_reward.set(2*i,0,a_value);

			a_value=-matrix_compressed_received_reward_ori.get( step_lineIndex_column[0],0)
					+ matrix_compressed_received_reward_ori.get( 2*i,0)
					+ matrix_compressed_received_reward_ori.get( 2*i+1,0)
					+ matrix_compressed_received_reward_ori.get( 2*i+2,0);
			matrix_compressed_received_reward.set(2*i+1,0,a_value);

		}
		return matrix_compressed_received_reward;
	}
	*/

	/*
	public Matrix select_which_matrix_batch(int scenario_seq)
	{
		// copy a column form this.matrix_batch_reward. the sequence of the column is scenario_seq
		Matrix matrix_batch_reward_seq= creat_matrix_reward(this.matrix_batch_reward.getRowDimension()); // record complete map of rewards, which will gain at the end of 3steps scenario.

		for(int i=0;i<this.matrix_batch_reward.getRowDimension();i++)
		{
			double updated_value= this.matrix_batch_reward.get(i,scenario_seq);
			matrix_batch_reward_seq.set(i,0,updated_value);
		}

		return matrix_batch_reward_seq;
	}
	public void save_which_matrix_batch(int scenario_seq, Matrix matrix_batch_reward_seq)
	{
		// save matrix_batch_reward_seq (a column) to this.matrix_batch_reward
		for(int i=0;i<matrix_batch_reward_seq.getRowDimension();i++)
		{
			double updated_value= matrix_batch_reward_seq.get(i,0);
			this.matrix_batch_reward.set(i,scenario_seq,updated_value);
		}

	}
	*/

	public  Matrix generate_full_rank_compressed_matrix(int[] step_lineIndex_row, int[] step_lineIndex_column,int scenario_seq)
	{
		/** return a full rank(9) compressed matrix
		 * 	1. extract 6 lines from matrix A. these 6 lines is for row 1-6
		 * 	2. get a sub-matrix, whose rank equal to the rank of matrix A.
		 * 	3. generate pseudo_compressed_matrix by add 2 additonal line for row 7,8
		 * 	3. check the rank
		 * 	5. end
		 * 	@param: step_lineIndex_row: only the first line contains  row[0] which represents the current scenarios and is useful.
		 * 								row[1-6] randomly selected
		 *
		 * **/


		Matrix full_rank_compressed_matrix= new Matrix(9,9,1);

		//1. extract 6 lines from matrix A. these 6 lines is for row 1-6
		step_lineIndex_row = matrix_get_applied_row_others(step_lineIndex_row,6, scenario_seq);
		//step_lineIndex_row = matrix_get_applied_row_other(step_lineIndex_row);// return the rest of rows, start from 0
		if(step_lineIndex_row[1]==step_lineIndex_row[2] )
		{
			// the number of batch is small than 3, impossible to calculate
			System.out.println("the number of batch is small than 3, impossible to calculate!!!!!!!!!!!!!!!!!!!!!!!");
			//todo null
			return full_rank_compressed_matrix;
		}

		/**
		 * 2.get a sub-matrix, whose rank equal to the rank of matrix A.
		 * **/

		Matrix sub_matrix_full_rank = extract_sub_matrix_full_rank(this.matrix_A, step_lineIndex_row, scenario_seq);

		if(sub_matrix_full_rank.rank()!=this.matrix_A.rank())
		{
			System.out.println("  compressed_matrix.rank("+sub_matrix_full_rank.rank()+") is smaller than matrix_A.rank("+this.matrix_A.rank()+")," +
					"fail to generate a full rank matrix ;");
//			for(int i=0;i<5000;i++)
//			{
//				System.out.println("  compressed_matrix.rank("+sub_matrix_full_rank.rank()+") is smaller than matrix_A.rank("+this.matrix_A.rank()+")," +
//					"fail to generate a full rank matrix ;");
//
//			}
			return full_rank_compressed_matrix;
		}


		//Matrix compressed_matrix = convert_matrix_3by9_into_3by3(step_lineIndex_row);
		System.out.println("  compressed_matrix.rank("+sub_matrix_full_rank.rank()+");");





		/**
		 * 3. generate pseudo_compressed_matrix by add 2 additonal line for row 7,8
		 * **/


		//copy sub_matrix_full_rank to compressed_matrix
		full_rank_compressed_matrix= matrix_copy_from(full_rank_compressed_matrix,sub_matrix_full_rank,0,0);

		Matrix added_matrix= get_add_matrix();
		full_rank_compressed_matrix= matrix_copy_from(full_rank_compressed_matrix,added_matrix,7,0);

		System.out.println("  full_rank_compressed_matrix.rank("+full_rank_compressed_matrix.rank()+") <- 7? 8? 9?;");

		return full_rank_compressed_matrix;

	}

	public  Matrix generate_full_rank_decomposed_matrix(int[] step_lineIndex_row, int[] step_lineIndex_column,int scenario_seq)
	{

		/**
		 * new matrix decompose method
		 * **/

		/**
		 * 3 trnslate the matrix form 27*9 to matrix 6*6 with full rank 6
		 * ***/

		//from 27*9-> 6*9 with rank=6
		Matrix sub_matrix_full_rank_row = extract_sub_matrix_full_rank_row(this.matrix_A, step_lineIndex_row, scenario_seq,3);
		//from 6*9 -> 6*6 with rank=6
		Matrix full_rank_compressed_matrix_6by6 = extract_sub_matrix_full_rank_column(sub_matrix_full_rank_row,step_lineIndex_column);

		int full_rank_compressed_matrix_6by6_rank= full_rank_compressed_matrix_6by6.rank();

		int count_loop=0;
		while (full_rank_compressed_matrix_6by6_rank!=6)
		{
			//from 27*9-> 6*9 with rank=6
			sub_matrix_full_rank_row = extract_sub_matrix_full_rank_row(this.matrix_A, step_lineIndex_row, scenario_seq,3);
			//from 6*9 -> 6*6 with rank=6
			full_rank_compressed_matrix_6by6 = extract_sub_matrix_full_rank_column(sub_matrix_full_rank_row,step_lineIndex_column);

			full_rank_compressed_matrix_6by6_rank= full_rank_compressed_matrix_6by6.rank();
			//System.out.println(" new matrix_A' fail to generated,full_rank_compressed_matrix_6by6_rank("+full_rank_compressed_matrix_6by6_rank+")");
			count_loop++;
			if(count_loop>=100)
			{
				System.out.println(" new matrix_A' fail to generated in count loop 100 time!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				break;
			}

		}

		if(full_rank_compressed_matrix_6by6.rank()!=6)
		{
			full_rank_compressed_matrix_6by6= new Matrix(6,6,0);
			//return a fake one
			System.out.println(" return a fake full_rank_compressed_matrix_6by6");

		}
		else
		{
			System.out.println(" new matrix_A' generated! rank("+full_rank_compressed_matrix_6by6.rank()+")");
		}
		return  full_rank_compressed_matrix_6by6;
	}


	public  Matrix generate_full_rank_decomposed_matrix_LV(int[] step_lineIndex_row, int[] step_lineIndex_column,int scenario_seq)
	{

		/**
		 * new matrix decompose method
		 * **/

		/**
		 * 3 trnslate the matrix form 27*9 to matrix 6*6 with full rank 6
		 * ***/

		//from 27*9-> 6*9 with rank=6
		Matrix sub_matrix_full_rank_row = extract_sub_matrix_full_rank_row(this.matrix_A, step_lineIndex_row, scenario_seq,3);
		//from 6*9 -> 6*6 with rank=6



		if(sub_matrix_full_rank_row.rank()!=6)
		{
			sub_matrix_full_rank_row= new Matrix(6,6,0);
			//return a fake one
			System.out.println(" return a fake full_rank_compressed_matrix_6by6");

		}
		else
		{
			System.out.println(" new matrix_A' generated! rank("+sub_matrix_full_rank_row.rank()+")");
		}
		return  sub_matrix_full_rank_row;
	}


	public Matrix get_add_matrix()
	{
		double[] action_1 = {1,0,0};

		double[] action_12 = {1,1,0};
		double[] action_23 = {0,1,1};
		double[] action_13 = {1,0,1};
		double[] action_00 = {0,0,0};
		double[] action_all = {1,1,1};

		double[] batch_add1 =link_three_array(action_all, action_00, action_00);
		double[] batch_add2 =link_three_array(action_00, action_all, action_00);
//		double[] batch_add1 =link_three_array(action_00, action_00, action_all);
//		double[] batch_add2 =link_three_array(action_00, action_all, action_00);
		//double[] batch_add1 =link_three_array(action_00, action_all, action_1);
		//double[] batch_add2 =link_three_array(action_all, action_00, action_00);
		//double[] batch_add1 =link_three_array(action_00, action_all, action_13);
		//double[] batch_add2 =link_three_array(action_all, action_00, action_23);
		double[][] a_metrix_temp = new double[2][9];// 27 lines/rows, 9 column
		a_metrix_temp[0]= batch_add1;
		a_metrix_temp[1]= batch_add2;

		Matrix a_matrix= new Matrix(a_metrix_temp,2,9);
		return a_matrix;
	}

	public Matrix matrix_copy_from(Matrix dst_matrix, Matrix src_matrix,int coordinate_x, int coordinate_y)
	{
		for(int i=0; i< src_matrix.getRowDimension();i++)
		{
			for(int j=0; j<src_matrix.getColumnDimension();j++)
			{
				dst_matrix.set((i+coordinate_x),(j+coordinate_y),src_matrix.get(i,j));
			}
		}

		return dst_matrix;
	}

	public Matrix extract_sub_matrix_full_rank(Matrix original_matrix, int[] step_lineIndex_row, int scenario_seq)
	{
		//extract a full rank matrix from original_matrix
		int full_rank = original_matrix.rank();
		System.out.println(" in extract_sub_matrix_full_rank, original_matrix.rank("+full_rank+")");
		Matrix full_rank_matrix= new Matrix(full_rank,full_rank,0);

		int loop_count=0;
		while(full_rank_matrix.rank()!= full_rank)
		{

			loop_count++;
			if(loop_count>=100)
			{
				// the number of batch is small than 3, impossible to calculate
				System.out.println("loop_count>100! can not find a full rank!!!!!!!!!!!!!!!!!!!!!!");
				break;
			}

			//1. select random row
			step_lineIndex_row= matrix_get_applied_row_others(step_lineIndex_row,(full_rank-1), scenario_seq);//the number of row of step_lineIndex_row equals to "full_rank" // return the rest of rows, start from 0
			if(step_lineIndex_row[1]==step_lineIndex_row[2] )
			{
				// the number of batch is small than 3, impossible to calculate
				System.out.println("the number of batch is small than 3, impossible to calculate!!!!!!!!!!!!!!!!!!!!!!!");
				break;
			}
			int[] sub_step_lineIndex_row = new int[full_rank];
			for(int i=0; i<full_rank; i++)
			{
				sub_step_lineIndex_row[i]= step_lineIndex_row[i];
			}
			int[] lineIndex_all_column={0,1,2,3,4,5,6,7,8};
			//2. give random row to full_rank_matrix
			full_rank_matrix= extract_subset_matrix(matrix_A,sub_step_lineIndex_row,lineIndex_all_column);
		}


		return full_rank_matrix;
	}

	public Matrix extract_sub_matrix_full_rank_row(Matrix original_matrix, int[] step_lineIndex_row, int scenario_seq,int steps_total)
	{
		int row_num=  steps_total*2;
		int column_num = original_matrix.getColumnDimension();
		Matrix full_rank_matrix_row= new Matrix(row_num,column_num,0);
		int targeted_full_rank=row_num;
		int loop_count=0;

		while(full_rank_matrix_row.rank()!= targeted_full_rank) {

			loop_count++;
			if (loop_count >= 100) {
				// the number of batch is small than 3, impossible to calculate
				System.out.println("loop_count>100! can not find a full rank!!!!!!!!!!!!!!!!!!!!!!");
				break;
			}

			//1. select random rows
			step_lineIndex_row= matrix_get_applied_row_others(step_lineIndex_row,(targeted_full_rank-1), scenario_seq);//the number of row of step_lineIndex_row equals to "full_rank" // return the rest of rows, start from 0
			if(step_lineIndex_row[1]==step_lineIndex_row[2] )
			{
				// the number of batch is small than 3, impossible to calculate
				System.out.println("the number of batch is small than 3, impossible to calculate!!!!!!!!!!!!!!!!!!!!!!!");
				break;
			}
			//2. copy step_lineIndex_row into sub_step_lineIndex_row
			int[] sub_step_lineIndex_row = new int[targeted_full_rank];
			for(int i=0; i<targeted_full_rank; i++)
			{
				sub_step_lineIndex_row[i]= step_lineIndex_row[i];
			}


			int[] lineIndex_all_column={0,1,2,3,4,5,6,7,8};
			//2. give random row to full_rank_matrix
			full_rank_matrix_row= extract_subset_matrix(matrix_A,sub_step_lineIndex_row,lineIndex_all_column);
			System.out.println("");
			//System.out.println("full_rank_matrix_row.rank("+full_rank_matrix_row.rank()+")");

		}


		return 	full_rank_matrix_row;
	}

	public void get_matrix_compressed_received_reward(int[] step_lineIndex_row)
	{
		for(int i=0;i<step_lineIndex_row.length;i++)
		{

		}
	}

	public Matrix convert_matrix_9by9_from_matrix_a(int[] step_lineIndex_row, int matrix_a_full_rank, int full_rank_num )
	{
		int[] lineIndex_all_column={0,1,2,3,4,5,6,7,8};

		int[] head_of_step_lineIndex_row = new int [matrix_a_full_rank];
		for(int i=0;i<matrix_a_full_rank;i++)
		{
			head_of_step_lineIndex_row[i] = step_lineIndex_row[i];
		}
		Matrix to_process_matrix = new Matrix(full_rank_num,full_rank_num);
		to_process_matrix= extract_subset_matrix(this.matrix_A,head_of_step_lineIndex_row, lineIndex_all_column);
		//Matrix compressed_matrix= generate_compressed_matrix(to_process_matrix);
		return to_process_matrix;
	}

	//public Matrix convert_matrix_column_merge(Matrix to_process_matrix,int[] step_lineIndex_column)// rename as"extract_sub_matrix_full_rank_column"
	public Matrix extract_sub_matrix_full_rank_column(Matrix to_process_matrix,int[] step_lineIndex_column)
	{
		// the matrix is already with rank 7, this function will translate the matrix into a size of 7 by 7
		// column adding :  adding the redundum column
		// the redumdun column is the other column which is applied in this scenario
		int full_rank= to_process_matrix.rank();
		//int ori_column_dimension= to_process_matrix.getColumnDimension();
		int ori_row_dimension= to_process_matrix.getRowDimension();
		//System.out.println("		convert_matrix_column_merge 1, ori.rank("+full_rank+"), ori_row_dimension("+ori_row_dimension+"), ori_column_dimension("+ori_column_dimension+") ");
		//  ori_row_dimension = full_rank
		int column_length=step_lineIndex_column.length;
		Matrix compressed_matrix = new Matrix(ori_row_dimension,6,0);//6*7

		//System.out.println("		convert_matrix_column_merge 1," +" ori.rank("+full_rank+") <-6 , ori_row_dimension("+ori_row_dimension+")<-6, ori_column_dimension("+to_process_matrix.getColumnDimension()+"<-9) ");

		//System.out.println("		convert_matrix_column_merge 1," + " new.rank("+compressed_matrix.rank()+")<-0");

		if(full_rank <= column_length)
		{
			// just in case;
			return compressed_matrix;
		}

		for(int j=0; j< step_lineIndex_column.length; j++ )
		{
			//j is the step num, (operate num of scenario -1) times
			// for each column recorded in step_lineIndex_column[i], operating each row
			// 2 column each

			for(int i=0; i<ori_row_dimension; i++)
			{
				double a_value= to_process_matrix.get( i, step_lineIndex_column[j]);
				compressed_matrix.set(i, j*2, a_value); //

				/*
				double a_merged_value =  to_process_matrix.get( i, 2*j) + to_process_matrix.get( i, (2*j+1)) + to_process_matrix.get( i, (2*j+2)) -  to_process_matrix.get( i, step_lineIndex_column[j]);
				compressed_matrix.set(i, (j*2+1), a_merged_value);
				*/
				compressed_matrix.set(i, (j*2+1), a_value-1);
				//System.out.println("convert_matrix_column_merge , a_value("+a_value+")");

				//System.out.println(" convert_matrix_column_merge 2, in loop compressed_matrix.getColumnDimension("+compressed_matrix.getColumnDimension()+")<-4");
			}

		}//has new column (0,1) for (0,1,2); new column (2,3) for (3,4,5);



		//System.out.println(" convert_matrix_column_merge 2, compressed_matrix.rank("+compressed_matrix.rank()+")<-6, " + "row("+compressed_matrix.getRowDimension()+")<-6, col("+compressed_matrix.getColumnDimension()+")<-6, ");

		//new column (4,5,6) for (6,7,8);
		/*
		for(int j=0; j< 3; j++)
		{
			for(int i=0; i<ori_row_dimension;i++)
			{
				double a_value= to_process_matrix.get( i, (j+6)); //start form column 6
				compressed_matrix.set(i, (j+4), a_value); //start form column 4
			}
		}
		*/
		//System.out.println(" convert_matrix_column_merge 3, compressed_matrix.rank("+compressed_matrix.rank()+") <-6");
		return compressed_matrix;
	}

	public Matrix convert_matrix_3by9_into_3by3(int[] step_lineIndex_row)
	{
		int[] lineIndex_all_column={0,1,2,3,4,5,6,7,8};
		Matrix to_process_matrix= extract_subset_matrixA(step_lineIndex_row, lineIndex_all_column);
		Matrix compressed_matrix= generate_compressed_matrix(to_process_matrix);
		return compressed_matrix;
	}

	public Matrix generate_compressed_matrix(Matrix to_process_matrix)
	{
		// this function will subsumpt the same action in different steps
		int length_of_row = to_process_matrix.getRowDimension();
		int length_of_column = to_process_matrix.getColumnDimension();
		//int rank= to_process_matrix.rank();
		Matrix compressed_matrix = new Matrix(length_of_row,3);
		for(int i=0; i<length_of_row;i++)
		{
			int action_one_num=0;
			int action_two_num=0;
			int action_three_num=0;

			for(int j=0;j<length_of_column;j++)
			{
				if(to_process_matrix.get(i,j)==1)
				{
					if(j%3==0)
					{
						action_one_num++;
					}
					if(j%3==1)
					{
						action_two_num++;
					}
					if(j%3==2)
					{
						action_three_num++;
					}
				}
			}// end of accounting action translation
			//generate the new matrix

			compressed_matrix.set(i,0, action_one_num);
			compressed_matrix.set(i,1, action_two_num);
			compressed_matrix.set(i,2, action_three_num);
			System.out.println("		action_one_num("+action_one_num+"), action_two_num("+action_two_num+")," +
					" action_three_num("+action_three_num+")");
		}

		return compressed_matrix;
	}

	public Matrix extract_subset_matrixA(int[] lineIndex_row,int[] lineIndex_column)
	{
		Matrix matrix_A_subset= new Matrix(lineIndex_row.length,lineIndex_column.length);
		//System.out.println("		extract_subset_matrixA: lineIndex_row.length("+lineIndex_row.length+"), " + "lineIndex_column.length("+lineIndex_column.length+")");
		for(int i=0; i< lineIndex_row.length; i++)
		{
			for(int j=0; j<lineIndex_column.length;j++)
			{
				//System.out.println("		extract_subset_matrixA:" + " extract element["+lineIndex_row[i]+"]["+lineIndex_column[j]+"]");
				double a_value_in_matrix_A= this.matrix_A.get(lineIndex_row[i], lineIndex_column[j]);
				matrix_A_subset.set(i, j, a_value_in_matrix_A);
			}
		}
		return matrix_A_subset;
	}

	public Matrix extract_subset_matrix(Matrix to_process_matrix, int[] lineIndex_row,int[] lineIndex_column)
	{
		Matrix matrix_A_subset= new Matrix(lineIndex_row.length,lineIndex_column.length);
		//System.out.println("		extract_subset_matrix : lineIndex_row.length("+lineIndex_row.length+"), " + "lineIndex_column.length("+lineIndex_column.length+")");
		for(int i=0; i< lineIndex_row.length; i++)
		{
			for(int j=0; j<lineIndex_column.length;j++)
			{
				double a_value_in_matrix_A= to_process_matrix.get(lineIndex_row[i], lineIndex_column[j]);
				matrix_A_subset.set(i, j, a_value_in_matrix_A);

				//System.out.println("		extract_subset_matrix:" + " extract element["+lineIndex_row[i]+"]["+lineIndex_column[j]+"], value("+a_value_in_matrix_A+")");
			}
		}
		return matrix_A_subset;
	}

	public Matrix generate_subset_matrixA(int[] step_lineIndex_row, int[] step_lineIndex_column)
	{
		Matrix matrix_A_subset= new Matrix(3,3);
		for(int i=0; i<3;i++)
		{
			for(int j=0; j<3;j++)
			{
				double a_value_in_matrix_A= this.matrix_A.get(step_lineIndex_row[i],step_lineIndex_column[j]);
				matrix_A_subset.set(i, j, a_value_in_matrix_A);
			}
		}
		return matrix_A_subset;
	}

	public int update_received_reward_matrix(double batch_reward,int scenario_num, int row)
	{
		//scenario_num= column
		//this can apply beta to learn reward
		matrix_batch_reward.set(row,scenario_num,batch_reward);
		System.out.println(" batch_reward("+batch_reward+"), row("+row+"), rank("+matrix_batch_reward.rank()+") ");
		int row_index_count=0;
		//check

		for(int i=0; i< matrix_batch_reward.getRowDimension();i++)
		{
			double diff= Math.abs(this.matrix_batch_reward.get(i,scenario_num)- initial_default_reward);
			if(diff>=10)
			{
				row_index_count++;
			}
			System.out.println("	matrix_batch_reward("+matrix_batch_reward.get(i,scenario_num)+")");
		}
		System.out.println("	matrix_batch_reward has update ("+row_index_count+") times");
		return row_index_count;

	}

	/*
	public int update_received_reward_matrix(double batch_reward,int row, Matrix matrix_batch_reward_seq)
	{
		//this can apply beta to learn reward
		matrix_batch_reward_seq.set(row,0,batch_reward);
		System.out.println(" batch_reward("+batch_reward+"), row("+row+"), rank("+matrix_batch_reward_seq.rank()+") ");
		int row_index_count=0;
		//check

		for(int i=0; i< matrix_batch_reward_seq.getRowDimension();i++)
		{
			if(matrix_batch_reward_seq.get(i,0) != initial_default_reward)
			{
				row_index_count++;
			}
			System.out.println("	matrix_batch_reward("+matrix_batch_reward_seq.get(i,0)+")");
		}
		System.out.println("	matrix_batch_reward has update ("+row_index_count+") times");
		return row_index_count;

	}
	*/
	public int[] matrix_get_applied_column(int step_num)
	{
		Stack<Integer> step_action_applied_stack = generate_step_action_applied_stack();


		int[] step_lineIndex_column = new int[step_num];	//which row, start form 0
		//int[] step_action = new int[3]; //record which action it took in previous steps

		int step_action_applied_stack_index=0;
		for(Integer an_action_mask:step_action_applied_stack)
		{
			// the first one
			//System.out.println("			2. iterate through step_action_applied_stack action mask("+an_action_mask+") in getting column line");

			if(step_action_applied_stack_index==0)
			{
				//step_action[step_action_applied_stack_index]=an_action_mask.intValue();
				step_lineIndex_column[step_action_applied_stack_index]= 3*step_action_applied_stack_index+ an_action_mask.intValue()-1;
			}
			if(step_action_applied_stack_index==1)
			{
				//step_action[step_action_applied_stack_index]=an_action_mask.intValue();
				step_lineIndex_column[step_action_applied_stack_index]= 3*step_action_applied_stack_index+ an_action_mask.intValue()-1;
			}
			if(step_action_applied_stack_index==2)
			{
				//step_action[step_action_applied_stack_index]=an_action_mask.intValue();
				step_lineIndex_column[step_action_applied_stack_index]= 3*step_action_applied_stack_index+ an_action_mask.intValue()-1;
			}

			step_action_applied_stack_index++;
		}



		//System.out.println(" step_lineIndex[0]("+step_lineIndex[0]+"); step_lineIndex[1]("+step_lineIndex[1]+"); step_lineIndex[2]("+step_lineIndex[2]+") ");

		return step_lineIndex_column;
	}

	public int[] matrix_get_applied_column_from_messages()
	{
		Stack<Integer> step_action_applied_stack = generate_step_action_applied_stack();

		int step_num = step_action_applied_stack.size();
		//System.out.println(" step_num=step_action_applied_stack.size("+step_num+")");

		int[] step_lineIndex_column = new int[step_num];	//which row, start form 0
		//int[] step_action = new int[3]; //record which action it took in previous steps

		int step_action_applied_stack_index=0;
		for(Integer an_action_mask:step_action_applied_stack)
		{
			step_lineIndex_column[step_action_applied_stack_index]= 3*step_action_applied_stack_index+ an_action_mask.intValue()-1;
			step_action_applied_stack_index++;
		}



		//System.out.println(" step_lineIndex_column[0]("+step_lineIndex_column[0]+");" + " step_lineIndex_column[1]("+step_lineIndex_column[1]+");" + " step_lineIndex_column[2]("+step_lineIndex_column[2]+") ");

		return step_lineIndex_column;
	}

	public int[] matrix_get_applied_row_one(int full_rank, int num_action)
	{
		Stack<Integer> step_action_applied_stack = generate_step_action_applied_stack();

		int[] step_lineIndex_row = new int[full_rank]; //which row, start form 1 // value represents number of row of batch_(1-27)  // start from line 1
		int[] step_action = new int[num_action]; //record which action it took in previous steps


		int step_action_applied_stack_index=0; // represent which step
		for(Integer an_action_mask:step_action_applied_stack)
		{
			// the first one
			//System.out.println("			3.apply action mask("+an_action_mask+") in getting row line.");

			if(step_action_applied_stack_index==0)
			{
				step_action[step_action_applied_stack_index]=an_action_mask.intValue();
			}
			if(step_action_applied_stack_index==1)
			{
				step_action[step_action_applied_stack_index]=an_action_mask.intValue();
			}
			if(step_action_applied_stack_index==2)
			{
				step_action[step_action_applied_stack_index]=an_action_mask.intValue();
			}

			step_action_applied_stack_index++;
		}
		//System.out.println(" step_lineIndex[0]("+step_lineIndex[0]+"); step_lineIndex[1]("+step_lineIndex[1]+"); step_lineIndex[2]("+step_lineIndex[2]+") ");
		//System.out.println(" step_1_lineIndex("+step_1_lineIndex+");step_2_lineIndex("+step_2_lineIndex+"); step_3_lineIndex("+step_3_lineIndex+"). " );

		//System.out.println(" step_action("+step_action[0]+"),("+step_action[1]+"),("+step_action[2]+"). " );
		step_lineIndex_row[0]= 9*(step_action[0]-1) + 3*(step_action[1]-1)+1*(step_action[2]-1) ; // start from line 0
		//System.out.println("step_lineIndex_row[0]("+step_lineIndex_row[0]+")");

		return step_lineIndex_row;
	}
	public int matrix_get_applied_row_seqNum_of_behavirourMatrix(int num_action)
	{
		Stack<Integer> step_action_applied_stack = generate_step_action_applied_stack();
		int step_lineIndex_row_seq_num=0;

		int[] step_action = new int[num_action]; //record which action it took in previous steps


		int step_action_applied_stack_index=0; // represent which step
		for(Integer an_action_mask:step_action_applied_stack)
		{
			// the first one
			//System.out.println("			3.apply action mask("+an_action_mask+") in getting row line.");

			step_action[step_action_applied_stack_index]=an_action_mask.intValue();
			step_action_applied_stack_index++;
		}
		//System.out.println(" step_lineIndex[0]("+step_lineIndex[0]+"); step_lineIndex[1]("+step_lineIndex[1]+"); step_lineIndex[2]("+step_lineIndex[2]+") ");
		//System.out.println(" step_1_lineIndex("+step_1_lineIndex+");step_2_lineIndex("+step_2_lineIndex+"); step_3_lineIndex("+step_3_lineIndex+"). " );

		//System.out.println(" step_action("+step_action[0]+"),("+step_action[1]+"),("+step_action[2]+"). " );
		step_lineIndex_row_seq_num=9*(step_action[0]-1) + 3*(step_action[1]-1)+1*(step_action[2]-1) ; // start from line 0
		System.out.println("	>>>>>>>>>>>>>>>debug:	step_lineIndex_row_seq_num("+step_lineIndex_row_seq_num+")");

		step_lineIndex_row_seq_num=0;
		for(int i=0;i<step_action_applied_stack_index;i++)
		{
			step_lineIndex_row_seq_num +=( (int)Math.pow(num_action,(step_action_applied_stack_index-1-i)) )*(step_action[i]-1);

		}
		System.out.println("	>>>>>>>>>>>>>>>debug:dynamic counting:	step_lineIndex_row_seq_num("+step_lineIndex_row_seq_num+")");

		return step_lineIndex_row_seq_num;
	}

	/*
	public int[] matrix_get_applied_row_other_eight_full_rank(int[] step_lineIndex_row, int num_other)
	{
		ArrayList<Integer> row_index	= new ArrayList<Integer>();
		//int row_index_count=0;
		for(int i=0; i<27; i++)
		{
			if(this.matrix_batch_reward.get(i,0) != initial_default_reward)
			{
				//row_index_count++;
				//row_index.put(i,this.matrix_batch_reward.get(i,0));
				row_index.add(i);
			}
		}

		System.out.println("		row_index.size("+row_index.size()+"), in matrix_get_applied_row_other_eight");

		//remove the row that currently apply
		int index_of_current_row= row_index.indexOf(step_lineIndex_row[0]);
		row_index.remove(index_of_current_row);
		System.out.println("		row_index, remove row_one ("+step_lineIndex_row[0]+"), in the position("+index_of_current_row+") of row_index, in matrix_get_applied_row_other_eight");


		return step_lineIndex_row;
	}
	*/

	public int[] matrix_get_applied_row_others(int[] step_lineIndex_row, int num_other, int scenario_seq)
	{
		//return other 6 row num, for step_lineIndex_row[1-6]
		// the row is choosed randomly from the row with task_reward which has been updated.
		//HashMap<Integer, Double> row_index	= new HashMap<Integer, Double>();
		ArrayList<Integer> row_index	= new ArrayList<Integer>();
		//int row_index_count=0;
		for(int i=0; i<27; i++)
		{
			double diff= Math.abs(this.matrix_batch_reward.get(i,scenario_seq)- initial_default_reward);
			if(diff>=10)
			{
				//row_index_count++;
				//row_index.put(i,this.matrix_batch_reward.get(i,0));
					row_index.add(i);
			}
		}

		//System.out.println("		row_index.size("+row_index.size()+"), in matrix_get_applied_row_other_eight");

		//remove the row that currently apply
		int index_of_current_row= row_index.indexOf(step_lineIndex_row[0]);
		row_index.remove(index_of_current_row);
		//System.out.println("		row_index, remove row_one ("+step_lineIndex_row[0]+"), in the position("+index_of_current_row+") of row_index, in matrix_get_applied_row_other_eight");


		//boolean contain_row_one_flag = row_index.remove(step_lineIndex_row[0]);
		//System.out.println("		row_index, remove row_one ("+contain_row_one_flag+"), in matrix_get_applied_row_other_eight");
		if(row_index.size()>=num_other)
		{
			//randomly remove rows until it contains only 8 rows rest.
			while(row_index.size()>num_other)
			{
				int removed_row= (int)(Math.ceil(Math.random()*row_index.size()) )-1;
				row_index.remove(removed_row);
				//System.out.println("		random removed element in position("+removed_row+") of row_index,  in matrix_get_applied_row_other_eight");
			}
		}

		//put the rest of row_index into step_lineIndex_row[1-8]

		for(int i=0;i<num_other;i++)
		{
			step_lineIndex_row[i+1]=row_index.get(i);
		}
		return step_lineIndex_row;
	}


/*
	public int[] matrix_get_applied_row_other(int[] step_lineIndex_row)
	{
		//this is for " DataAna 5.22 linear Regression 3*3 3_instances" version

		//return 2nd and 3rd row of step_lineIndex_row, that is step_lineIndex_row[1], and step_lineIndex_row[2]
		// the row is choosed randomly from the row with task_reward which has been updated.
		//HashMap<Integer, Double> row_index	= new HashMap<Integer, Double>();
		ArrayList<Integer> row_index	= new ArrayList<Integer>();
		//int row_index_count=0;
		for(int i=0; i<27; i++)
		{
			if(this.matrix_batch_reward.get(i,0) != initial_default_reward)
			{
				//row_index_count++;
				//row_index.put(i,this.matrix_batch_reward.get(i,0));
				row_index.add(i);
			}
		}

		System.out.println("		row_index.size("+row_index.size()+")");

		int row_other_1=0;
		int row_other_2=0;
		if(row_index.size()>=3)
		{
			//todo: it should make it full rank
			// select other two row
			row_other_1=(int)(Math.ceil(Math.random()*row_index.size()) )-1;// arraylist start from 0
			while(row_index.get(row_other_1)==step_lineIndex_row[0])
			{
				row_other_1=(int)(Math.ceil(Math.random()*row_index.size()) )-1;// arraylist start from 0
			}

			row_other_2=(int)(Math.ceil(Math.random()*row_index.size()) )-1;
			while(row_index.get(row_other_2)==step_lineIndex_row[0] || row_other_2==row_other_1 )
			{
				row_other_2=(int)(Math.ceil(Math.random()*row_index.size()) )-1;
			}


			System.out.println("		row_other_1("+row_other_1+"); row_other_2("+row_other_2+"); step_lineIndex_row[0]("+step_lineIndex_row[0]+"). ");

			step_lineIndex_row[1]= row_index.get(row_other_1);
			step_lineIndex_row[2]= row_index.get(row_other_2);
			System.out.println("		step_lineIndex_row[1]("+step_lineIndex_row[1]+") , step_lineIndex_row[2]"+step_lineIndex_row[2]+"");
		}
		else
		{
			step_lineIndex_row[1]= -3;
			step_lineIndex_row[2]= -3;
		}



		return step_lineIndex_row;
	}
*/

	/**
	 * create Maxtrix reward 27*1
	 * ***/

	public Matrix creat_matrix_reward(int rowNum, int colunmNum)
	{
		Matrix matrix_reward= new Matrix(rowNum,colunmNum,this.initial_default_reward);
		//Matrix matrix_reward=  Matrix.random(rowNum,1);

		return  matrix_reward;
	}
	public Matrix creat_matrix_reward_single_column(int rowNum)
	{
		Matrix matrix_reward= new Matrix(rowNum,1,this.initial_default_reward);
		//Matrix matrix_reward=  Matrix.random(rowNum,1);

		return  matrix_reward;
	}
	public Matrix creat_matrix_A_flexible(int num_step, int num_action)
	{
		//System.out.println("	to create Matrix_A flexible");
		return linearRegressionRewardMethod.create_matrix_A(num_step,num_action);

	}
	/**
	 * create Maxtrix A 27*9(3 step* 3 actions)
	 * ***/
	public Matrix create_matrix_A()
	{
		int num_step=3;
		int num_action=3;


		double[] action_1 = {1,0,0};
		double[] action_2 = {0,1,0};
		double[] action_3 = {0,0,1};

		//double[] batch_1 = Arrays.copyOf(action_1,6);
		//System.arraycopy(action_2, 0,batch_1, 3,3);
		/**
		 * step_lineIndex_row[0]= 9*(step_action[0]-1) + 3*(step_action[1]-1)+1*(step_action[2]-1) ; // start from line 0
		 * **/
		double[] batch_0 =link_three_array(action_1, action_1, action_1);
		double[] batch_1 =link_three_array(action_1, action_1, action_2);
		double[] batch_2 =link_three_array(action_1, action_1, action_3);

		double[] batch_3 =link_three_array(action_1, action_2, action_1);
		double[] batch_4 =link_three_array(action_1, action_2, action_2);
		double[] batch_5 =link_three_array(action_1, action_2, action_3);

		double[] batch_6 =link_three_array(action_1, action_3, action_1);
		double[] batch_7 =link_three_array(action_1, action_3, action_2);
		double[] batch_8 =link_three_array(action_1, action_3, action_3);

		//
		double[] batch_9 =link_three_array(action_2, action_1, action_1);
		double[] batch_10 =link_three_array(action_2, action_1, action_2);
		double[] batch_11 =link_three_array(action_2, action_1, action_3);

		double[] batch_12 =link_three_array(action_2, action_2, action_1);
		double[] batch_13 =link_three_array(action_2, action_2, action_2);
		double[] batch_14 =link_three_array(action_2, action_2, action_3);

		double[] batch_15 =link_three_array(action_2, action_3, action_1);
		double[] batch_16 =link_three_array(action_2, action_3, action_2);
		double[] batch_17 =link_three_array(action_2, action_3, action_3);

		//
		double[] batch_18 =link_three_array(action_3, action_1, action_1);
		double[] batch_19 =link_three_array(action_3, action_1, action_2);
		double[] batch_20 =link_three_array(action_3, action_1, action_3);

		double[] batch_21 =link_three_array(action_3, action_2, action_1);
		double[] batch_22 =link_three_array(action_3, action_2, action_2);
		double[] batch_23 =link_three_array(action_3, action_2, action_3);

		double[] batch_24 =link_three_array(action_3, action_3, action_1);
		double[] batch_25 =link_three_array(action_3, action_3, action_2);
		double[] batch_26 =link_three_array(action_3, action_3, action_3);


		double[][] a_metrix_temp = new double[27][9];// 27 lines/rows, 9 column

		a_metrix_temp[0]= batch_0;
		a_metrix_temp[1]= batch_1;
		a_metrix_temp[2]= batch_2;

		a_metrix_temp[3]= batch_3;
		a_metrix_temp[4]= batch_4;
		a_metrix_temp[5]= batch_5;

		a_metrix_temp[6]= batch_6;
		a_metrix_temp[7]= batch_7;
		a_metrix_temp[8]= batch_8;

		a_metrix_temp[9]= batch_9;
		a_metrix_temp[10]= batch_10;
		a_metrix_temp[11]= batch_11;

		a_metrix_temp[12]= batch_12;
		a_metrix_temp[13]= batch_13;
		a_metrix_temp[14]= batch_14;

		a_metrix_temp[15]= batch_15;
		a_metrix_temp[16]= batch_16;
		a_metrix_temp[17]= batch_17;

		a_metrix_temp[18]= batch_18;
		a_metrix_temp[19]= batch_19;
		a_metrix_temp[20]= batch_20;

		a_metrix_temp[21]= batch_21;
		a_metrix_temp[22]= batch_22;
		a_metrix_temp[23]= batch_23;

		a_metrix_temp[24]= batch_24;
		a_metrix_temp[25]= batch_25;
		a_metrix_temp[26]= batch_26;


		Matrix a_matrix= new Matrix(a_metrix_temp,27,9);
		return a_matrix;
		/*
		for(int i=0; i<a_metrix_temp[0].length;i++)
		{
			System.out.println("	show a_matrix line 1 ("+a_matrix.get(0,i)+")");
		}
		for(int i=0; i<a_metrix_temp[1].length;i++)
		{
			System.out.println("	show a_matrix line 27 ("+a_matrix.get(26,i)+")");
		}
		*/
	}

	public double[] link_three_array(double[] one, double[] two,double[] three)
	{
		double[] result = link_two_array(one, two);
		result=link_two_array(result, three);
		return result;
	}
	public double[] link_two_array(double[] one, double[] two)
	{
		double[] result = Arrays.copyOf(one,(one.length+ two.length));
		System.arraycopy(two, 0,result, one.length,two.length);//(src, srcPos, dest, destPos, length)
		return result;
	}

	public Stack<Integer>  generate_step_action_applied_stack()
	{
		Stack<Integer> step_action_applied_stack = new Stack<Integer>();

		for(FilterMessage<LayerCondition, LayerAction> step_message:this.messages)
		{
			//System.out.println("	push action ("+step_message.getCurrentAction()+")");
			//System.out.println("			1. push action mask("+step_message.getCurrentAction().getMask()+") to generate step_action_applied_stack");
			step_action_applied_stack.push(	step_message.getCurrentAction().getMask());
		}

		/*
		int step_action_applied_stack_index=0;
		for(Integer an_action_mask:step_action_applied_stack)
		{
			// the first one
			System.out.println("			1.2 check iterate through step_action_applied_stack. action mask("+an_action_mask+")  to test seq, after .push");
			step_action_applied_stack_index++;
		}
		*/

		return step_action_applied_stack;
	}


	public boolean irisMeasure(double species)
	{
		System.out.println("\n");

		int message_size=messages.size();
		int reward=0;
		//System.out.println("	irisMeasure::  messages.size("+message_size+"),  prepare to goto the loop ");

		//while(!messages.isEmpty())
		{

			//1.pop up a message.
			FilterMessage<LayerCondition, LayerAction> message = messages.pop();
			//2. show message information such as, action, action_set
			int modID= message.getCurrentAction().getMask();

			System.out.println("	irisMeasure::species("+species+") VS. message current action("+message.getCurrentAction()+") ("+modID+")," +
					" action_set("+message.getActionSet()+").");
			//clear BowTieSet
			this.population.clearBowTieSet();

			if((int)species==modID)
			{
				//reward=1000;
				//this.population.update(message.getActionSet(), reward);
				System.out.println("	irisMeasure:: predict correct. IRIS RIGHT");
				/**
				 * record the result here
				 * */
				return true;

			}
			else
			{
				//reward =0;
				//this.population.update(message.getActionSet(), reward);

				System.out.println("	irisMeasure:: predict wrong.  IRIS WRONG");
				return false;
			}

		}
		// for single step update method, "	while(!messages.isEmpty())", which is designed for multisteps RL,  shall be remove.
		//Therefore, the follow line should no be applied....
		//return false;
	}


	public void rewardLayer1(double reward)
	{
		System.out.println("\n");
		int loop_time=1;
		int message_size=messages.size();
		//System.out.println("\n");
		if(messages.size()>0)	//if(!messages.isEmpty())
		{
			reward = reward/messages.size();

		}
		System.out.println("	rewardLayer1::  messages.size("+messages.size()+"), reward="+reward+"',  prepare to goto the loop ");
		System.out.println("\n");

		while(!messages.isEmpty())
		{
			if(loop_time==message_size)
			{
				System.out.println("\n");
				System.out.println("\n");
				System.out.println("	rewardLayer1::  ****************check statistics in final loop below***************\n");
			}
			System.out.println("	rewardLayer1::  in loop ("+loop_time+"), reward("+reward+"),update the action set");

			//1.pop up a message.
			FilterMessage<LayerCondition, LayerAction> message = messages.pop();
			//2. show message information such as, action, action_set

			System.out.println("	rewardLayer1:: message current action("+message.getCurrentAction()+")," +
					" action_set("+message.getActionSet()+").");

			//this.population.updateClassifiersAction(message, reward);
			//this.population.updateExpectations(message.getCurrentAction(), reward);
			this.population.update(message.getActionSet(), reward);
			//System.out.println("\n");

			// remove the discount//reward /= 2;

			loop_time++;

		}
		//clear BowTieSet
		this.population.clearBowTieSet();
	}

	public void saveAReward(double a_groundTruth)
	{
		this.groundTruthStack.push(a_groundTruth);
	}

	public double reward_summary()
	{
		//change the sequence of groundTruthStack
		Stack<Double> groundTruthStack_inverse_seq 	= new Stack<Double>();
		while(!groundTruthStack.empty())
		{
			groundTruthStack_inverse_seq.push( groundTruthStack.pop());
		}

		double totalReward=0;
		for(FilterMessage<LayerCondition, LayerAction> message:this.messages)
		{
			int modID= message.getCurrentAction().getMask();
			//double a_groundTruth= groundTruthStack.pop();
			double a_groundTruth= groundTruthStack_inverse_seq.pop();//for each method start from the button of stack. the most aged and old item will be indicated firstly.
			System.out.println(" in reward_summary,  has took action("+modID+"), a record Ground_truth took action("+a_groundTruth+") ");
			if((int)a_groundTruth== modID)
			{
				totalReward += 1000;
				//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				//System.out.println(">>>>>>>>>>>>>messages.size("+messages.size()+")VS groundTruthStack.size ("+groundTruthStack.size()+")>>>>>>>>>>>>>>>>>>>>>>");

				//System.out.println(">>>>>Reward->1000 :totalReward("+totalReward+")>>> a_groundTruth("+a_groundTruth+"), modID("+modID+")");
			}
			else
			{
				//System.out.println(">>>>>Reward->0 :totalReward("+totalReward+")>>> a_groundTruth("+a_groundTruth+"), modID("+modID+")");
				/**
				 * punishment reward -1000
				 * */
				totalReward += -1000;
			}
			// just for debug
			//message.get_explore_mode();
		}
		//System.out.println(" reward_summary :: totalReward"+totalReward+"");
		return totalReward;
	}

	public void cleanMessageFilters()
	{
		while(!messages.isEmpty())
		{
			FilterMessage<LayerCondition, LayerAction> message = messages.pop();
		}
		//clear BowTieSet
		this.population.clearBowTieSet();
	}

	public void deletionLayer1(int pop_size)
	{
		boolean flagIsDel =this.population.applyDeletionRW(pop_size);
		System.out.println("deletionLayer1 : does deletion operation ("+flagIsDel+")");
	}

	public void subsumption()
	{
		this.population.subsumption();
	}

	public void iterateReset(LayerCondition condition, int iteration)
	{
		this.message = null;
		messages = new Stack<FilterMessage<LayerCondition,LayerAction>>();
		if(iteration > 10)//change the explore_change which will directly influence //iteration for explore!//change this
			// the choice between the explore mode or the exploit mode
			this.explore_chance = 50;
		iterate(condition, 0, iteration);
	}
	
	public void iterate(LayerCondition condition, double reward, int iteration)
	{
		System.out.println("  ");
		System.out.println("  ");
		System.out.println("LayerProblem.java::iterate()<----iteration begin===========================");
		System.out.println("LayerProblem.java::iterate()<----iteration begin========= this.messages.isEmpty()='"+this.messages.isEmpty()+"', rewared ='"+reward+"', iteration='"+iteration+"'");

		/*//Tony: I do not agree with this one. the reward should not pass to any part of classifier until the reinforcement learning update!
		if(!this.messages.isEmpty())
		{	//Tony: I do not agree with this one. the reward should not pass to any part of classifier until the reinforcement learning update!
			this.population.updateClassifiersAction(this.messages.peek(), reward);
			System.out.println("LayerProblem.java::interate():: this.messages.isEmpty()='"+this.messages.isEmpty()+"<----0,', rewared ='"+reward+"' , iteration='"+iteration+"' , now updateClassifierAction, in which classifiers' statictics will be update!!!!");
		}
		*/


		this.message = new FilterMessage<LayerCondition, LayerAction>();
		this.message.setCurrentState(condition);
		this.message = filter(this.message, getFilters());//<--this.message ?

		System.out.println("LayerProblem.java::iterate()<----iteration finish===========================");
		System.out.println("  ");
		System.out.println("  ");
		System.out.println("  ");
	}
	public void iterateL1(LayerCondition current_reinforcer_cellWorld_state, int iteration)
	{
		System.out.println("  ");
		System.out.println("  ");
		System.out.println("===========================iteration LAYER1  begin=====iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration

		//the first layer of system: primary reinforcer layer
		//input: current_reinforcer_cellWorld_state
		//output: current_emotion_state
		//1. set condition message: current_reinforcer_cellWorld_state
		//?? this.message = new FilterMessage<LayerCondition, LayerAction>();

		//this.message.setCurrentState(current_reinforcer_cellWorld_state);
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);
		//this.layerMessage.setReState(current_reinforcer_cellWorld_state);
		System.out.println("===========================iteration layerMessage <--SET===========================");
		//Todo: check if it is necessary to change four filters! such as, MatchFilter->MatchReFilter
		//Filters contains 4 filters: MatchReFilter(Match), Cover, Select and GA
		this.message = layer1Filter(this.message, getLayer1Filters());

		System.out.println("===========================iteration LAYER1 finish ===========================");
	}

	public void iterateIris(LayerCondition current_reinforcer_cellWorld_state, int iteration)
	{

		//learning
		System.out.println("  ");
		System.out.println("  ");
		System.out.println("===========================iteration Iris  begin=====iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);
		this.message = layerIrisApplyFilter(this.message, getLayerIrisFilters());

		System.out.println("===========================iteration LAYER1 finish ====+=======================");
	}


	public void iterateIrisTest(LayerCondition current_reinforcer_cellWorld_state, int iteration)
	{
		//test
		System.out.println("  ");
		System.out.println("  ");
		System.out.println(">>>>>>>>>>>>Test>>>>>===iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);
		this.message = layerIrisApplyFilter(this.message, getLayerIrisTestFilters());

		System.out.println("===========================iteration LAYER1 finish ===========================");
	}



	public void iterateIrisTest_check_instance_action_anticipatedPRD(int iteration, int instance_id, LayerCondition current_reinforcer_cellWorld_state )
	{
		//test
		System.out.println("  ");
		System.out.println("  ");
		System.out.println(">>>>>>>>>>>>Test>>>>>===iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);

		//this.message = layerIrisApplyFilter(this.message, getLayerIrisTestFilters_save_instance_anticipation()); // todo: return action confidence

		this.message = apply_a_filter(this.message, new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		this.message = apply_a_filter(this.message, new SelectPATestModeFilter<LayerProblem, LayerCondition, LayerAction>(0,0, instance_id , iteration));//pass instance ID

		/**
		 * generated a modifier with the real value for a_targeted_action based on clossifiers
		 * 1. generated niche_set: classifiers rules which advocated the "a_targeted_action" : by MatchActionFilter
		 * 2. calculate niche_predicted_reward for each rules in niche_set
		 * 3. summarize niche_predicted_reward for the action.
		 * 4. select most strongest niche as advocated modifier
		 * ************/

		// generated nicheSet
		//todo: select an input action
		//manipulation about action.  generate selected action (sact)to go through each action.

		// replaced by niche_strength_advocate
		//this.message = apply_a_filter(this.message, new MatchActionFilter<LayerProblem, LayerCondition, LayerAction>());// input is currentAction in this.message. output is matchActionSet ids
		//this.message = apply_a_filter(this.message,new NicheStrengthFilter<LayerProblem, LayerCondition, LayerAction>(0,0, instance_id , iteration));//input is matchActionSet. output niche_reward for entire boundary


		this.message= filtermessage_push_in_state(this.message);

		System.out.println("===========================iteration LAYER1 finish ===========================");
	}

	public HashMap<LayerAction, LayerClassifier> get_instance_classifier( LayerCondition current_reinforcer_cellWorld_state )
	{

		//test
		//System.out.println("		L3 >>>>>>>>>>>>get_instance_classifier filters begin>>>>>======================");
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);

		//calculate matchset
		this.message = apply_a_filter(this.message, new MatchNicheFilter<LayerProblem, LayerCondition, LayerAction>());
		//apply matchset to get anticipated reward hashmap
		//System.out.println("		L3 >>>>>>>>>>>>get_instance_classifier matchset.size("+this.message.getMatchSet().size()+") ");
		HashMap<LayerAction, LayerClassifier> instance_classifier_hashmap = this.get_instance_classifier_hashmap(this.message.getMatchSet() );

		//show anticipated_reward_hashmap
		/*
		for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
		{
			System.out.println("		get_instance_action_anticipatedPRD_GO_filters :: (Accuracy-based) calcuate anticipated reward :" +
					"\taction("+a_anticipated_reward_map.getKey()+")" +
					"\t- reward("+a_anticipated_reward_map.getValue()+"). ");
		}
		*/

		/*show instance_classifier_hashmap
		for(Map.Entry<LayerAction, LayerClassifier> a_classifier_in_hashmap: instance_classifier_hashmap.entrySet()) {
			System.out.println("	L3:	(Accuracy-based) most fitness classifier :\t s-action(" + a_classifier_in_hashmap.getKey() + ")" +
					"\t- id(" + a_classifier_in_hashmap.getValue().getID() + ")," +
					" condition (" + a_classifier_in_hashmap.getValue().getCondition() + " )," +
					" action(" + a_classifier_in_hashmap.getValue().getLayerAction() + ")," +
					" acc("+ a_classifier_in_hashmap.getValue().getAccuracy()+" ), " +
					" prd("+ a_classifier_in_hashmap.getValue().getPredictedReward()+")," +
					" fit("+ a_classifier_in_hashmap.getValue().getFitness()+")");
		}
		*/
		//System.out.println("		L3 >>>>>>>>>>>>get_instance_classifier filters end>>>>>======================\n");

		return instance_classifier_hashmap;
	}

	public HashMap<LayerAction, Double> get_instance_action_anticipatedPRD_GO_filters( LayerCondition current_reinforcer_cellWorld_state )
	{

		//test
		System.out.println("		L3 >>>>>>>>>>>>NICHES filters begin>>>>>======================");
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);

		//calculate matchset
		this.message = apply_a_filter(this.message, new MatchNicheFilter<LayerProblem, LayerCondition, LayerAction>());
		//apply matchset to get anticipated reward hashmap
		System.out.println("		debug +++ matchset.size("+this.message.getMatchSet().size()+") ");
		HashMap<LayerAction, Double> anticipated_reward_hashmap = this.get_anticipated_reward_hashmap(this.message.getMatchSet() );

		//show anticipated_reward_hashmap

		for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
		{
			System.out.println("		get_instance_action_anticipatedPRD_GO_filters :: (Accuracy-based) calcuate anticipated reward :" +
					"\taction("+a_anticipated_reward_map.getKey()+")" +
					"\t- reward("+a_anticipated_reward_map.getValue()+"). ");
		}


		System.out.println("		L3 >>>>>>>>>>>>NICHES filters end>>>>>======================\n");

		return anticipated_reward_hashmap;
	}


	public void niche_strength_advocate(int iteration, LayerCondition current_reinforcer_cellWorld_state )
	{
		//test
		System.out.println("  ");
		System.out.println("  ");
		System.out.println(">>>>>>>>>>>>niche_strength_advocate>>>>>===iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration
		//this.message.setCurrentReState(current_reinforcer_cellWorld_state);

		//this.message = layerIrisApplyFilter(this.message, getLayerIrisTestFilters_save_instance_anticipation()); // todo: return action confidence

		//this.message = apply_a_filter(this.message, new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		//this.message = apply_a_filter(this.message, new SelectPATestModeFilter<LayerProblem, LayerCondition, LayerAction>(0,0, instance_id , iteration));//pass instance ID

		/**
		 * generated a modifier with the real value for a_targeted_action based on clossifiers
		 * 1. generated niche_set: classifiers rules which advocated the "a_targeted_action" : by MatchActionFilter
		 * 2. calculate niche_predicted_reward for each rules in niche_set
		 * 3. summarize niche_predicted_reward for the action.
		 * 4. select most strongest niche as advocated modifier
		 * ************/

		// generated nicheSet
		//todo: select an input action
		//manipulation about action.  generate selected action (sact)to go through each action.

		//this.message = apply_a_filter(this.message, new MatchActionFilter<LayerProblem, LayerCondition, LayerAction>());// input is currentAction in this.message. output is matchActionSet ids
		//this.message = apply_a_filter(this.message,new NicheStrengthFilter<LayerProblem, LayerCondition, LayerAction>(0,0, instance_id , iteration));//input is matchActionSet. output niche_reward for entire boundary

		ArrayList<LayerAction> actions_list =	this.population.getClassifiersPossibleActionSetList();
		LayerAction store_current_action = message.getCurrentAction();

		for(LayerAction sact: actions_list)
		{

			// set sact as message.current.action
			message.setCurrentAction(sact);
			System.out.println("niche_strength_advocate_filters:: sact ("+sact+") ");

			this.message = apply_a_filter(this.message, new MatchActionFilter<LayerProblem, LayerCondition, LayerAction>());// input is currentAction in this.message. output is matchActionSet ids
			this.message = apply_a_filter(this.message,new NicheStrengthFilter<LayerProblem, LayerCondition, LayerAction>(0,0 , iteration));//input is matchActionSet. output niche_reward for entire boundary

		}

		//recover current action
		message.setCurrentAction(store_current_action);


		//this.message= filtermessage_push_in_state(this.message);

		System.out.println("===========================iteration LAYER1 finish ===========================");
	}


	public void iterateIris_Learning(LayerCondition current_reinforcer_cellWorld_state, LayerCondition current_reinforcer_cellWorld_state_representation, int iteration)
	{
		//test
		System.out.println("  ");
		System.out.println("  ");
		System.out.println(">>>>>>>>>>>>iterateIris_Learning>>>>>===iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);// single eyes

		this.message.setCurrentState_representation(current_reinforcer_cellWorld_state_representation); // crystalEyes
		//this.message = layerIrisApplyFilter(this.message, getLayerIrisLearningFilters());
		// todo: Martin's exploit method
		if(this.filters_portfolio.size()<1)
		{
			//portfilo have not been initalized. perserve for PAOC work.
			this.message = layerIrisApplyFilter(this.message, getLayerFilters_before_nicheDistribution());
		}
		else
			{this.message = layerIrisApplyFilter(this.message, this.filters_portfolio);}

		System.out.println("===========================iterateIris_Learning finish ===========================\n");
	}

	public void iterate_Learning(LayerCondition current_reinforcer_cellWorld_state, LayerCondition current_reinforcer_cellWorld_state_representation, int iteration, double[][] boundary)
	{
		//test
		System.out.println("  ");
		System.out.println("  ");
		System.out.println(">>>>>>>>>>>>iterate_Learning>>>>>===iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration
		this.message.set_boundary(boundary);
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);// single eyes

		this.message.setCurrentState_representation(current_reinforcer_cellWorld_state_representation); // crystalEyes
		//this.message = layerIrisApplyFilter(this.message, getLayerIrisLearningFilters());
		// todo: Martin's exploit method
		if(this.filters_portfolio.size()<1)
		{
			//portfilo have not been initalized. perserve for PAOC work.
			this.message = layerIrisApplyFilter(this.message, getLayerFilters_before_nicheDistribution());
		}
		else
		{this.message = layerIrisApplyFilter(this.message, this.filters_portfolio);} // maze

		System.out.println("===========================iterateIris_Learning finish ===========================\n");
	}
	public int iterate_testing(LayerCondition current_reinforcer_cellWorld_state, LayerCondition current_reinforcer_cellWorld_state_representation, int iteration, double[][] boundary)
	{	// from iterate_Learning()
		this.messages.clear();  //just in case...
		//FilterMessage<LayerCondition, LayerAction> message_test_mode = new FilterMessage<LayerCondition, LayerAction>();

		//test
		System.out.println("  ");
		System.out.println("  ");
		System.out.println(">>>>>>>>>>>>iterate_testing>>>>>===iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration
		this.message.set_boundary(boundary);
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);// single eyes

		this.message.setCurrentState_representation(current_reinforcer_cellWorld_state_representation); // crystalEyes
		/*
		//change it to apply a filter
		this.message = apply_a_filter(this.message, new MatchReFilter<LayerProblem, LayerCondition, LayerAction>() );
		if(this.message.getMatchSet().size()>1)
		{	//has matchset
			this.message = apply_a_filter(this.message, new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(0,0 ));
		}
		else
		{
			//does not have match set
			this.message.setLastAction(LayerAction.empty);
		}
		*/
		this.message = ApplyFilters_test_mode(this.message, this.test_filters_portfolio); // maze test
		int advocate_action = this.message.getCurrentAction().getMask();
		System.out.println("===========================iterate_testing finish ,advocate_action ("+advocate_action+")===========================");
		this.messages.clear();  //just in case...
		return 	advocate_action;
	}

	public int iterate_mcts(LayerCondition current_reinforcer_cellWorld_state, LayerCondition current_reinforcer_cellWorld_state_representation, int iteration, double[][] boundary)
	{

		this.messages.clear(); // only one iteration, don't need messages.

		System.out.println("  ");
		System.out.println("  ");
		System.out.println(">>>>>>>>>>>>iterate_mcts>>>>>===iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration
		this.message.set_boundary(boundary);
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);// single eyes

		this.message.setCurrentState_representation(current_reinforcer_cellWorld_state_representation); // crystalEyes
		this.message = ApplyFilters_mcts_mode(this.message, this.mcts_filters_portfolio); // maze mcts

		int advocate_action = this.message.getCurrentAction().getMask();
		System.out.println("===========================iterate_testing finish ,advocate_action ("+advocate_action+")===========================");
		this.messages.clear();  //just in case...
		return 	advocate_action;
	}

	public void mcts_establish_MacthSet_ActionSet_filters(LayerCondition current_reinforcer_cellWorld_state, LayerCondition current_reinforcer_cellWorld_state_representation, int iteration, double[][] boundary,int action)
	{
		LayerAction chosen_action =  LayerAction.getLayerAction(action);

		// all the message will be store in mcts_messages

		this.messages.clear(); // only one iteration, don't need messages.
		System.out.println("  ");
		System.out.println(">>>>>>>>>>>>mcts_establish_MacthSet_ActionSet_filters>>>>>===chosen_action("+chosen_action+")======================");
		this.iterationCount =iteration;// here pass the iteration
		this.message.set_boundary(boundary);
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);// single eyes

		this.message.setCurrentState_representation(current_reinforcer_cellWorld_state_representation); // crystalEyes
		this.message = ApplyFilters_mcts_mode(this.message, this.mcts_filters_portfolio); // maze mcts

		this.message = apply_a_filter(this.message, new MatchReFilter<LayerProblem, LayerCondition, LayerAction>()); // mcts
		this.message = apply_a_filter(this.message, new CoverFilter_Representation<LayerProblem, LayerCondition, LayerAction>()); // mcts
		this.message.setCurrentAction(chosen_action);
		this.message = apply_a_filter(this.message, new ChosenActionFilter<LayerProblem, LayerCondition, LayerAction>( )); // mcts
		this.mcts_messages.push(this.message);
		//this.mcts_messages.clear();

	}

	public int get_action()
	{
		// debug ok// System.out.println(" getCurrentAction:"+this.message.getLastAction().getMask());
		return this.message.getLastAction().getMask();
	}
	public HashMap<LayerAction, Double> get_actions_reward_hashmap(String training_mode)
	{
		// similar to get_action() function, get last hashmap
		if(training_mode.equals("learning"))
		{
			return this.message.get_last_actions_reward_hashmap();
		}
		if(training_mode.equals("testing"))
		{
			return this.message.get_current_actions_reward_hashmap();
		}
		if (training_mode.equals("mcts"))
		{
			return this.message.get_current_actions_reward_hashmap();
		}
		//default
		return this.message.get_last_actions_reward_hashmap();

	}

	public void iterateIrisLearningMultiStep(LayerCondition current_reinforcer_cellWorld_state, int iteration) //for parasite
	{
		// multi-step learning//the same as iterateIris
		System.out.println("  ");
		System.out.println("===========================iterateIrisLearningMultiStep begin=====iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);
		this.message = layerIrisApplyFilter(this.message, getLayerIrisFilters()); //multi-step iris

		System.out.println("===========================iteration LAYER1 finish ====+=======================");
	}
	public void iterateIrisLearningMultiStep_4Fliters(LayerCondition current_reinforcer_cellWorld_state, int iteration)
	{
		// multi-step learning//the same as iterateIris
		System.out.println("  ");
		System.out.println("===========================iteration 4 filters begin=====iteration("+iteration+")======================");
		this.iterationCount =iteration;// here pass the iteration
		this.message.setCurrentReState(current_reinforcer_cellWorld_state);
		this.message = layerIrisApplyFilter(this.message, getLayerIrisFilters()); //multi-step iris 4 filters
		System.out.println("===========================iteration 4 filters finish ====+=======================");
	}



	public void iterateL4(LayerCondition current_state, int iteration)
	{
		System.out.println("  ");
		System.out.println("  ");
		System.out.println("===========================iteration LAYER4 begin" +
				"=====iteration("+iteration+")=========current_state("+current_state+")=============");
		this.iterationCount =iteration;// here pass the iteration


		this.message4.setCurrentReState(current_state);

		//Todo: check if it is necessary to change four filters! such as, MatchFilter->MatchReFilter
		//Filters contains 4 filters: MatchReFilter(Match), Cover, Select and GA
		this.message4 = layer4Filter(this.message4, getLayer4Filters());

		System.out.println("===========================iteration LAYER4 finish ===========================");
	}

	public void	create_a_classifier_through_layerProblem(int id,LayerCondition condition_input,String action_mask,double[] statistic)
	{

		this.createClassifierFromCSV(id,condition_input, action_mask, statistic);

		// test whether the population is valid or not
		//this.population.applyRuleDiscovery();
	}


	private FilterMessage<LayerCondition, LayerAction> filter(FilterMessage<LayerCondition, LayerAction> message, ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters)
	{
		System.out.println("===========>>>begin of filter(), begin of filter.apply");
//		Debug.Output("Iteration: "+iterationCount+"\n"+message.getCurrentState().toString());
		for(Filter<LayerProblem, LayerCondition, LayerAction> filter : filters)
			message = filter.apply(this, iterationCount, message);	// filter :: this is applies for bow-tie structure
		System.out.println("===========>>>in filter(), end of filter.apply");

		this.messages.push(new FilterMessage<LayerCondition, LayerAction>(message));

		FilterMessage<LayerCondition, LayerAction> nextMessage = new FilterMessage<LayerCondition, LayerAction>();
		nextMessage.setCurrentState(message.getNextState());
		nextMessage.setLastState(message.getCurrentState());
		nextMessage.setLastAction(message.getCurrentAction());
		System.out.println("===========>>>in filter(),to test 'message.isComplete'='"+message.isComplete()+"'<--it is always true, I guess. but if is false, then message never complete ");
		if(message.isComplete()) {
			nextMessage.setComplete();
			System.out.println("===========>>>in filter(),to test 'nextMessage.isComplete'='"+message.isComplete()+"'<--it is always true");
		}
		System.out.println("===========>>>end of filter()");
		return nextMessage;
	}

	private FilterMessage<LayerCondition, LayerAction> layer1Filter(FilterMessage<LayerCondition, LayerAction> layerMessage, ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters)
	{
		System.out.println("===========>>>begin of layer1Filter(), begin of layer1Filter.apply");
//		Debug.Output("Iteration: "+iterationCount+"\n"+message.getCurrentState().toString());
		for(Filter<LayerProblem, LayerCondition, LayerAction> filter : filters)
		{
			//TODO: check apply
			layerMessage = filter.apply(this, iterationCount, layerMessage);	// layer1Filter :: this is applies for layer1 structure
		}
		System.out.println("===========>>>in layer1Filter(), layer1Filter of filter.apply");

		this.messages.push(new FilterMessage<LayerCondition, LayerAction>(layerMessage));

		FilterMessage<LayerCondition, LayerAction> nextMessage = new FilterMessage<LayerCondition, LayerAction>();
		//TODO:check getNextState();getCurrentState();getCurrentAction();
		//nextMessage.setCurrentState(message.getNextState());
		nextMessage.setCurrentReState(message.getNextState());
		//nextMessage.setLastState(message.getCurrentState());
		nextMessage.setLastState(message.getCurrentReState());
		nextMessage.setLastAction(message.getCurrentAction());
		//System.out.println("===========>>>in layer1Filter(),to test 'message.isComplete'='"+layerMessage.isComplete()+"'<--it is always true, I guess. but if is false, then message never complete ");
		if(message.isComplete()) {
			nextMessage.setComplete();
			System.out.println("===========>>>in layer1Filter(),to test 'nextMessage.isComplete'='"+message.isComplete()+"'<--it is always true");
		}
		System.out.println("===========>>>end of layer1Filter()");
		return nextMessage;
	}

	private FilterMessage<LayerCondition, LayerAction> layerIrisApplyFilter(FilterMessage<LayerCondition, LayerAction> layerMessage, ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters)
{
	//System.out.println("===========>>>begin of layerIrisApplyFilter(), begin of layer1Filter.apply");
	for(Filter<LayerProblem, LayerCondition, LayerAction> filter : filters)
	{
		//TODO:explore mode 2018
		layerMessage = filter.apply(this, iterationCount, layerMessage);	// layerIrisApplyFilter :: this is applies for layer1 structure
	}
	//System.out.println("===========>>>in layerIrisApplyFilter(), filter.apply end");
	//System.out.println("===========>>>in layerIrisApplyFilter(), push layerMessage");

	this.messages.push(new FilterMessage<LayerCondition, LayerAction>(layerMessage));


	FilterMessage<LayerCondition, LayerAction> nextMessage = new FilterMessage<LayerCondition, LayerAction>();
	//TODO:check getNextState();getCurrentState();getCurrentAction();
	//nextMessage.setCurrentState(message.getNextState());
	nextMessage.setCurrentReState(message.getNextState());
	//nextMessage.setLastState(message.getCurrentState());
	nextMessage.setLastState(message.getCurrentReState());
	nextMessage.setLastAction(message.getCurrentAction());

	// set actions_reward_hashmap
	nextMessage.set_last_actions_reward_hashmap(message.get_current_actions_reward_hashmap());

	//System.out.println("===========>>>in layer1Filter(),to test 'message.isComplete'='"+layerMessage.isComplete()+"'<--it is always true, I guess. but if is false, then message never complete ");
	if(message.isComplete()) {
		nextMessage.setComplete();
		System.out.println("===========>>>in layerIrisApplyFilter(),to test 'nextMessage.isComplete'='"+message.isComplete()+"'<--it is always true");
	}
	//System.out.println("===========>>>end of layerIrisApplyFilter()");
	return nextMessage;
}

	private FilterMessage<LayerCondition, LayerAction> ApplyFilters_test_mode(FilterMessage<LayerCondition, LayerAction> layerMessage, ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters)
	{   // from layerIrisApplyFilter
		System.out.println("===========>>>begin of ApplyFilters_test_mode(), begin of layer1Filter.apply");
		for(Filter<LayerProblem, LayerCondition, LayerAction> filter : filters)
		{
			//TODO:explore mode 2018
			layerMessage = filter.apply(this, iterationCount, layerMessage);	// layerIrisApplyFilter :: this is applies for layer1 structure
		}

		return  layerMessage;
		/*
			this.messages.push(new FilterMessage<LayerCondition, LayerAction>(layerMessage));

		// set actions_reward_hashmap
		//nextMessage.set_last_actions_reward_hashmap(layerMessage.get_current_actions_reward_hashmap());
		//return layerMessage;
		//System.out.println("===========>>>in layerIrisApplyFilter(), filter.apply end");
		//System.out.println("===========>>>in layerIrisApplyFilter(), push layerMessage");
		// in test mode don't push messages
		//this.messages.push(new FilterMessage<LayerCondition, LayerAction>(layerMessage));

		FilterMessage<LayerCondition, LayerAction> nextMessage = new FilterMessage<LayerCondition, LayerAction>();
		//nextMessage.setCurrentState(message.getNextState());
		nextMessage.setCurrentReState(message.getNextState());
		//nextMessage.setLastState(message.getCurrentState());
		nextMessage.setLastState(message.getCurrentReState());
		nextMessage.setLastAction(message.getCurrentAction());

		// set actions_reward_hashmap
		nextMessage.set_last_actions_reward_hashmap(message.get_current_actions_reward_hashmap());

		//System.out.println("===========>>>in layer1Filter(),to test 'message.isComplete'='"+layerMessage.isComplete()+"'<--it is always true, I guess. but if is false, then message never complete ");
		//if(message.isComplete()) {
		//	nextMessage.setComplete();
		//	System.out.println("===========>>>in layerIrisApplyFilter(),to test 'nextMessage.isComplete'='"+message.isComplete()+"'<--it is always true");
		//}
		System.out.println("===========>>>end of layerIrisApplyFilter()");
		return nextMessage;
		*/
	}

	private FilterMessage<LayerCondition, LayerAction> ApplyFilters_mcts_mode(FilterMessage<LayerCondition, LayerAction> layerMessage, ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters)
	{   // from layerIrisApplyFilter
		System.out.println("===========>>>begin of ApplyFilters_test_mode(), begin of layer1Filter.apply");
		for(Filter<LayerProblem, LayerCondition, LayerAction> filter : filters)
		{
			//TODO:explore mode 2018
			layerMessage = filter.apply(this, iterationCount, layerMessage);	// layerIrisApplyFilter :: this is applies for layer1 structure
		}
		// push the current message into mcts_messages for future usages, such as reward
		this.mcts_messages.push(new FilterMessage<LayerCondition, LayerAction>(layerMessage));

		return  layerMessage;
		/*

		// set actions_reward_hashmap
		//nextMessage.set_last_actions_reward_hashmap(layerMessage.get_current_actions_reward_hashmap());
		//return layerMessage;
		//System.out.println("===========>>>in layerIrisApplyFilter(), filter.apply end");
		//System.out.println("===========>>>in layerIrisApplyFilter(), push layerMessage");
		// in test mode don't push messages
		//this.messages.push(new FilterMessage<LayerCondition, LayerAction>(layerMessage));

		FilterMessage<LayerCondition, LayerAction> nextMessage = new FilterMessage<LayerCondition, LayerAction>();
		//nextMessage.setCurrentState(message.getNextState());
		nextMessage.setCurrentReState(message.getNextState());
		//nextMessage.setLastState(message.getCurrentState());
		nextMessage.setLastState(message.getCurrentReState());
		nextMessage.setLastAction(message.getCurrentAction());

		// set actions_reward_hashmap
		nextMessage.set_last_actions_reward_hashmap(message.get_current_actions_reward_hashmap());

		//System.out.println("===========>>>in layer1Filter(),to test 'message.isComplete'='"+layerMessage.isComplete()+"'<--it is always true, I guess. but if is false, then message never complete ");
		//if(message.isComplete()) {
		//	nextMessage.setComplete();
		//	System.out.println("===========>>>in layerIrisApplyFilter(),to test 'nextMessage.isComplete'='"+message.isComplete()+"'<--it is always true");
		//}
		System.out.println("===========>>>end of layerIrisApplyFilter()");
		return nextMessage;
		*/
	}

	private FilterMessage<LayerCondition, LayerAction> filter_iterate(FilterMessage<LayerCondition, LayerAction> layerMessage, ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters)
	{
		System.out.println("===========>>> begin of filter_iterate");
		for(Filter<LayerProblem, LayerCondition, LayerAction> filter : filters)
		{
			layerMessage = filter.apply(this, iterationCount, layerMessage);	// layerIrisApplyFilter :: this is applies for layer1 structure
		}
		System.out.println("===========>>> end of filter_iterate");

		this.messages.push(new FilterMessage<LayerCondition, LayerAction>(layerMessage));


		FilterMessage<LayerCondition, LayerAction> nextMessage = new FilterMessage<LayerCondition, LayerAction>();
		//TODO:check getNextState();getCurrentState();getCurrentAction();
		//nextMessage.setCurrentState(message.getNextState());
		nextMessage.setCurrentReState(message.getNextState());
		//nextMessage.setLastState(message.getCurrentState());
		nextMessage.setLastState(message.getCurrentReState());
		nextMessage.setLastAction(message.getCurrentAction());
		//System.out.println("===========>>>in layer1Filter(),to test 'message.isComplete'='"+layerMessage.isComplete()+"'<--it is always true, I guess. but if is false, then message never complete ");
		if(message.isComplete()) {
			nextMessage.setComplete();
			System.out.println("===========>>>in layerIrisApplyFilter(),to test 'nextMessage.isComplete'='"+message.isComplete()+"'<--it is always true");
		}
		System.out.println("===========>>>end of layerIrisApplyFilter()");
		return nextMessage;
	}

	private FilterMessage<LayerCondition, LayerAction> apply_a_filter(FilterMessage<LayerCondition, LayerAction> layerMessage, Filter<LayerProblem, LayerCondition, LayerAction> filter)
	{
		//System.out.println("===========>>>begin of apply_a_filter");

		//TODO:explore mode 2018
		layerMessage = filter.apply(this, iterationCount, layerMessage);	// layerIrisApplyFilter :: this is applies for layer1 structure

		//System.out.println("===========>>>apply_a_filter end");
		return  layerMessage;
	}

	private FilterMessage<LayerCondition, LayerAction> filtermessage_push_in_state(FilterMessage<LayerCondition, LayerAction> layerMessage)
	{


		this.messages.push(new FilterMessage<LayerCondition, LayerAction>(layerMessage));


		FilterMessage<LayerCondition, LayerAction> nextMessage = new FilterMessage<LayerCondition, LayerAction>();
		//TODO:check getNextState();getCurrentState();getCurrentAction();
		//nextMessage.setCurrentState(message.getNextState());
		nextMessage.setCurrentReState(message.getNextState());
		//nextMessage.setLastState(message.getCurrentState());
		nextMessage.setLastState(message.getCurrentReState());
		nextMessage.setLastAction(message.getCurrentAction());
		//System.out.println("===========>>>in layer1Filter(),to test 'message.isComplete'='"+layerMessage.isComplete()+"'<--it is always true, I guess. but if is false, then message never complete ");
		if(message.isComplete()) {
			nextMessage.setComplete();
			System.out.println("===========>>>in layerIrisApplyFilter(),to test 'nextMessage.isComplete'='"+message.isComplete()+"'<--it is always true");
		}
		System.out.println("===========>>>end of layerIrisApplyFilter()");
		return nextMessage;
	}

	private FilterMessage<LayerCondition, LayerAction> layer4Filter(FilterMessage<LayerCondition, LayerAction> layerMessage, ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters)
	{
		System.out.println("===========>>>begin of layer4Filter(), begin of layer1Filter.apply");
//		Debug.Output("Iteration: "+iterationCount+"\n"+message.getCurrentState().toString());
		for(Filter<LayerProblem, LayerCondition, LayerAction> filter : filters)
		{
			//TODO: check apply
			layerMessage = filter.apply(this, iterationCount, layerMessage);	// layer4Filter:: this is applies for layer1 structure
		}
		System.out.println("===========>>>in layer1Filter(), layer1Filter of filter.apply");

		this.messages4.push(new FilterMessage<LayerCondition, LayerAction>(layerMessage));

		FilterMessage<LayerCondition, LayerAction> nextMessage = new FilterMessage<LayerCondition, LayerAction>();
		//TODO:check getNextState();getCurrentState();getCurrentAction();
		//nextMessage.setCurrentState(message.getNextState());
		nextMessage.setCurrentReState(message4.getNextState());
		//nextMessage.setLastState(message.getCurrentState());
		nextMessage.setLastState(message4.getCurrentReState());
		nextMessage.setLastAction(message4.getCurrentAction());
		//System.out.println("===========>>>in layer1Filter(),to test 'message.isComplete'='"+layerMessage.isComplete()+"'<--it is always true, I guess. but if is false, then message never complete ");
		if(message4.isComplete()) {
			nextMessage.setComplete();
			System.out.println("===========>>>in layer1Filter(),to test 'nextMessage.isComplete'='"+message4.isComplete()+"'<--it is always true");
		}
		System.out.println("===========>>>end of layer1Filter()");
		return nextMessage;
	}




	public ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> getFilters() {
		ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();
		filters.add(new MatchFilter<LayerProblem, LayerCondition, LayerAction>());
		filters.add(new CoverFilter<LayerProblem, LayerCondition, LayerAction>());
		filters.add(new SelectFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance));
		filters.add(new GenitcAlgorithmFilter<LayerProblem, LayerCondition, LayerAction>(GA_THRESHOLD));
		filters.add(new LayerModifyFilter());
		filters.add(new DelayedUpdateFilter<LayerProblem, LayerCondition, LayerAction>());
		return filters;
	}
	public ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> getLayer1Filters() {
		ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();
		filters.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		//filters.add(new MatchFilter<LayerProblem, LayerCondition, LayerAction>());
		filters.add(new CoverReFilter<LayerProblem, LayerCondition, LayerAction>());
		filters.add(new SelectPAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance));// set explore_chance =50%
		filters.add(new GenitcAlgorithmActionSetFilter<LayerProblem, LayerCondition, LayerAction>(GA_THRESHOLD));
		filters.add(new LayerExecutionFilter());//directly apply a specific modifier!//put this filter to the latter layer
		//filters.add(new DelayedUpdateFilter<LayerProblem, LayerCondition, LayerAction>());
		return filters;
	}
	public ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> getLayerIrisFilters() {
		ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();
		filters.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		//filters.add(new MatchFilter<LayerProblem, LayerCondition, LayerAction>());
		filters.add(new CoverReFilter<LayerProblem, LayerCondition, LayerAction>());
		filters.add(new SelectPAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance,iterationCount_threshold ));// set explore_chance =50%, getLayerIrisFilters,
		filters.add(new GenitcAlgorithmActionSetFilter<LayerProblem, LayerCondition, LayerAction>(GA_THRESHOLD));// iris
		//filters.add(new LayerExecutionFilter());//directly apply a specific modifier!//put this filter to the latter layer
		//filters.add(new DelayedUpdateFilter<LayerProblem, LayerCondition, LayerAction>());
		return filters;
	}


	public ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> getLayerIrisTestFilters() {
		ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();
		filters.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		//filters.add(new MatchFilter<LayerProblem, LayerCondition, LayerAction>());
		//filters.add(new CoverReFilter<LayerProblem, LayerCondition, LayerAction>());
		filters.add(new SelectPAFilter<LayerProblem, LayerCondition, LayerAction>(0,0 ));// set explore_chance =50%

		//filters.add(new SelectPAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance,iterationCount_threshold ));// set explore_chance =50%
		//filters.add(new GenitcAlgorithmActionSetFilter<LayerProblem, LayerCondition, LayerAction>(GA_THRESHOLD));// iris
		//filters.add(new LayerExecutionFilter());//directly apply a specific modifier!//put this filter to the latter layer
		//filters.add(new DelayedUpdateFilter<LayerProblem, LayerCondition, LayerAction>());
		return filters;
	}

	public ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> getLayerFilters_before_nicheDistribution() {
		ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();
		filters.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		//filters.add(new MatchFilter<LayerProblem, LayerCondition, LayerAction>());
		//todo: change the coverfilter
		filters.add(new CoverFilter_Representation<LayerProblem, LayerCondition, LayerAction>());
		//todo: test Martin's method
		filters.add(new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance,iterationCount_threshold ));// Martin
		//filters.add(new SelectPAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance,iterationCount_threshold ));

		filters.add(new GenitcAlgorithmActionSetFilter<LayerProblem, LayerCondition, LayerAction>(GA_THRESHOLD));// iris

		return filters;
	}

	public void initial_filters_portfolio() {
		this.filters_portfolio.clear();
		this.filters_portfolio.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		this.filters_portfolio.add(new CoverFilter_Representation<LayerProblem, LayerCondition, LayerAction>());
		this.filters_portfolio.add(new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance,iterationCount_threshold ));// this initial filter can be replaced by replace_select_filter()
		this.filters_portfolio.add(new GenitcAlgorithmActionSetFilter<LayerProblem, LayerCondition, LayerAction>(GA_THRESHOLD));// iris
	}
	public void initial_test_filters_portfolio() {
		// apply the same portfolio but with different mode setting.
		this.filters_portfolio.clear();
		this.filters_portfolio.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		this.filters_portfolio.add(new CoverFilter_Representation<LayerProblem, LayerCondition, LayerAction>());
		this.filters_portfolio.add(new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance,iterationCount_threshold ));// this initial filter can be replaced by replace_select_filter()

		/*
		this.test_filters_portfolio.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		this.test_filters_portfolio.add(new CoverFilter_Representation<LayerProblem, LayerCondition, LayerAction>());
		//this.test_filters_portfolio.add(new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(0,0 ));// Martin
		this.test_filters_portfolio.add(new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance,iterationCount_threshold ));// this initial filter can be replaced by replace_select_filter()
		*/
	}
	public void initial_mcts_filters_portfolio() {
		this.filters_portfolio.clear();
		this.filters_portfolio.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		this.filters_portfolio.add(new CoverFilter_Representation<LayerProblem, LayerCondition, LayerAction>());
		this.filters_portfolio.add(new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance,iterationCount_threshold ));// this initial filter can be replaced by replace_select_filter()
		/*
		this.mcts_filters_portfolio.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		this.mcts_filters_portfolio.add(new CoverFilter_Representation<LayerProblem, LayerCondition, LayerAction>());
		//this.mcts_filters_portfolio.add(new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(0,0 ));// Martin
		this.mcts_filters_portfolio.add(new SelectFSAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance,iterationCount_threshold ));// this initial filter can be replaced by replace_select_filter()
		*/
	}

	public ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> getLayerIrisLearningFilters() {
		ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();
		filters.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		//filters.add(new MatchFilter<LayerProblem, LayerCondition, LayerAction>());
		//todo: change the coverfilter
		filters.add(new CoverFilter_Representation<LayerProblem, LayerCondition, LayerAction>());
		//todo: change the parameters passing by
		filters.add(new SelectPAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance,iterationCount_threshold ));// IrisLearningFilters, set explore_chance =50%

		filters.add(new GenitcAlgorithmActionSetFilter<LayerProblem, LayerCondition, LayerAction>(GA_THRESHOLD));// iris

		return filters;
	}


	public ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> getLayer4Filters() {

		ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();
		filters.add(new MatchReFilter<LayerProblem, LayerCondition, LayerAction>());
		//filters.add(new MatchFilter<LayerProblem, LayerCondition, LayerAction>());
		filters.add(new CoverFilter<LayerProblem, LayerCondition, LayerAction>());
		filters.add(new SelectPAFilter<LayerProblem, LayerCondition, LayerAction>(explore_chance));// set explore_chance =50%
		filters.add(new GenitcAlgorithmActionSetFilter<LayerProblem, LayerCondition, LayerAction>(GA_THRESHOLD));
		//filters.add(new LayerExecutionFilter());//directly apply a specific modifier!//put this filter to the latter layer
		//filters.add(new DelayedUpdateFilter<LayerProblem, LayerCondition, LayerAction>());
		return filters;
	}
	@Override
	public LayerPopulation getPopulation()
	{
		return this.population;
	}
	public void setPopulation(LayerPopulation population)
	{
		this.population = population;
	}

	public ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> getExecutionFilters() {
		ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>> filters = new ArrayList<Filter<LayerProblem, LayerCondition, LayerAction>>();
		filters.add(new LayerModifyFilter());
		filters.add(new DelayedUpdateFilter<LayerProblem, LayerCondition, LayerAction>());
		return filters;
	}

	@Override
	public JPanel getTabs() {
		return null;
	}

	@Override
	public void render(Graphics2D g, int offset_x, int offset_y) {
		
	}

	@Override
	public boolean takeAction(LayerAction action) {
		System.out.println("Emotion:" + action+" Pop Size: "+population.size());
		this.population.effectModifier(action, exploit);
		return true;
	}

	@Override
	public boolean executeModifier(LayerAction action) {
		System.out.println("takeLayerAction:" + action+" Pop Size: "+population.size());
		this.population.executeModifier(action);
		return true;
	}


	@Override
	public void save() {
		System.out.println(" save population into a file("+file+")");

		//replace by csv file
		//this.population.saveThePopulation(file);
		//this.population.save_classifiers(file+"_classifiers");
		//this.population.save_txt_classifiers();

		this.population.save_csv_classifiers(file);
	}

	@Override
	public void save(int episode) {
		System.out.println(" save population into a file("+file+episode+")");

		//replace by csv file
		//this.population.saveThePopulation(file);
		//this.population.save_classifiers(file+"_classifiers");
		//this.population.save_txt_classifiers();

		this.population.save_csv_classifiers(file+episode);
		System.out.println(" save population into the lastest file(lastest.csv)");
		this.population.save_csv_classifiers("lastest");

	}

	@Override
	public void save(int episode, String folder_name) {
		File theDir = new File(folder_name);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			System.out.println("creating directory: " + theDir.getName());
			boolean result = false;

			try{
				theDir.mkdir();
				result = true;
			}
			catch(SecurityException se){
				//handle it
			}
			if(result) {
				System.out.println("DIR created");
			}
		}

		System.out.println(" save population into a file("+file+episode+")");

		//replace by csv file
		//this.population.saveThePopulation(file);
		//this.population.save_classifiers(file+"_classifiers");
		//this.population.save_txt_classifiers();

		this.population.save_csv_classifiers(file+episode, folder_name);
		System.out.println(" save population into the lastest file(lastest.csv)");
		this.population.save_csv_classifiers("lastest", folder_name);

	}
	public void save_agnet_grid()
	{
		// save the popluation here
		try
		{
			System.out.println("save population for grid version");
			XStream xstream = new XStream();
			String xml = xstream.toXML(this.population);
			BufferedWriter xmlOut = new BufferedWriter(new FileWriter(file+"_gridVersion.xml"));
			xmlOut.write(xml);
			xmlOut.close();

//			FileOutputStream fileOut = new FileOutputStream("population.ser");
//			ObjectOutputStream out = new ObjectOutputStream(fileOut);
//			out.writeObject(classifiers);
//			out.close();
//			fileOut.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

	}


	/*
	@Override
	public void save() {
		this.population.saveThePopulation("emotions");
	}
	*/



	@Override
	public void display() {
		
	}
	
	@Override
	public boolean isComplete() {
		return isComplete;
	}

	@Override
	public void applyGA(ArrayList<Integer> action_set, int iteration, int threshold)  {

		this.population.applyGA(action_set, iteration, threshold);

		if(Math.random()<0.5)
		{
			//this.population.applyRuleDiscovery(); // The real value problem might need another specific technology to find rules.
		}

	}

	@Override
	public void applyGA_boundary(ArrayList<Integer> action_set, int iteration, int threshold, double[][] boundary)  {

		this.population.applyGA_boundary(action_set, iteration, threshold, boundary);

	}


	@Override
	public int createClassifierFromState(LayerCondition currentState) {
		return this.population.createClassifierFromState(currentState);
	}

	@Override
	public int createClassifierFromStateWithAction(LayerCondition currentState, ArrayList<Integer> MatchSetIDs) {
		return this.population.createClassifierFromStateWithAction(currentState, MatchSetIDs);
	}

	@Override
	public void create_Classifier_by_mitosis(LayerCondition currentState, ArrayList<Integer> MitosisSetIDs) {
		this.population.create_classifier_by_mitosis(currentState, MitosisSetIDs);
	}


	@Override
	public void createClassifierFromCSV(int id, LayerCondition currentState,String action_mask,double[] statistic) {
		this.population.createClassifierFromCSV(id, currentState, action_mask, statistic);
	}


	@Override
	public boolean actionSetDiversity(ArrayList<Integer> MatchSetIDs) {
	return this.population.actionSetDiversity( MatchSetIDs);
	}

	@Override
	public ArrayList<LayerAction> getClassifiersPossibleActionSetList()
	{
		return this.population.getClassifiersPossibleActionSetList();
	}

	@Override
	public int getActionSetSizeInMatchSet(ArrayList<Integer> MatchSetIDs)
	{
		return this.population.getActionSetSizeInMatchSet(MatchSetIDs);
	}

	@Override
	public int getActionSetSizePossibility()
	{
		return this.population.getActionSetSizePossibility();
	}

	@Override
	public ArrayList<Integer> getMatchingClassifiers(LayerCondition condition) {
		return this.population.getMatchingClassifiers(condition);
	}

	@Override
	public ArrayList<Integer> getMatchingClassifiers(double[] state) {
		return this.population.getMatchingClassifiers(state);
	}

	@Override
	public ArrayList<Integer> getMatchingClassifiers_for_action(LayerAction action) {
		return this.population.getMatchingClassifiers_for_action(action);
	}

	@Override
	public LayerAction getNextAction(LayerCondition currentState, ArrayList<Integer> match_set, ArrayList<Integer> action_set, boolean exploit) {
		//fake, replaced by "getNextAction(****)" below
		this.exploit = exploit;
		return this.population.getNextAction(currentState, match_set, action_set, exploit);
	}
	@Override
	public void getActionSetFromChosenAction(ArrayList<Integer> matchSet, ArrayList<Integer> actionSet, LayerAction chosen_action)
	{
		this.population.getActionSetFromChosenAction(matchSet, actionSet, chosen_action);
	}
	@Override
	public HashMap<LayerAction, Double> get_actions_reward_hashmap(ArrayList<Integer> match_set)
	{
		return this.population.get_actions_reward_hashmap(match_set);
	}

	@Override
	public LayerAction getNextAction(LayerCondition currentState, ArrayList<Integer> match_set, ArrayList<Integer> action_set, boolean exploit, ArrayList<Double> anticipated_reward) {
		this.exploit = exploit;
		return this.population.getNextAction(currentState, match_set, action_set, exploit,anticipated_reward);
	}


	public HashMap<LayerAction, Double> get_anticipated_reward_hashmap(ArrayList<Integer> matchSet)
	{
		return this.population.get_anticipated_reward_hashmap( matchSet);
	}



	public HashMap<LayerAction, LayerClassifier> get_instance_classifier_hashmap(ArrayList<Integer> matchSet)
	{
		return this.population.get_instance_classifier_hashmap( matchSet);
	}

	public HashMap<Integer, Matrix>  calculate_niche_strength_distribution(ArrayList<Integer> match_niche_set) {

		return this.population.calculate_niche_strength_distribution(match_niche_set);
	}

	public HashMap<Integer, Modifier_ContinuouslyProjected> calculate_niche_strength_distribution_continuous(ArrayList<Integer> match_niche_set)
	{
		return this.population.calculate_niche_strength_distribution_continuous(match_niche_set);
	}

	public HashMap<Integer, Matrix>  transform_modifier_niches_set_into_matrix(HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches)
	{
		return this.population.transform_modifier_niches_set_into_matrix( modifier_niches);
	}

	public HashMap<Integer, Modifier_ContinuouslyProjected>  transform_modifier_niches_set_into_matrix_inline(HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches)
	{
		return this.population.transform_modifier_niches_set_into_matrix_inline( modifier_niches);
	}


	public void select_a_discrete_value_for_modifier(HashMap<Integer, Matrix> modifier_set, String s)
	{
		this.population.select_a_discrete_value_for_modifier( modifier_set,  s);
	}

	@Override
	public  void save_anticipated_reward_hashmap(int iteration, int instance_id) {
		this.population.save_anticipated_reward_hashmap(iteration,instance_id);
	}

	@Override
	public  void save_modifiers(String s, HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches) {
		this.population.save_modifiers(s,  modifier_niches);
	}


	@Override
	public void increaseTimeSinceGA() {
		this.population.increaseTimeSinceGA();
	}

	@Override
	public void updateClassifiersAction(FilterMessage<LayerCondition, LayerAction> message, double reward) {
		this.population.updateClassifiersAction(message, reward);
		System.out.println("!!! pay attention! in reinforcement learning, this function should never be called!");
	}
	
	@Override
	public void printMessage(String message)
	{
//		Debug.EmotionFilterOutput(message);
		System.out.println(message);
	}
}
