package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Statistics;

public class Modifier implements ActionInterface
{
	private static int NEXT_ID = 0;
	public final int ID;
		
	private Statistics statistics = new Statistics();
	
	private double inflation_radius;
	private double cost_scaling_factor;
	private double speed;
	private double occdist;
	private double lidar_weight;
	private double collision_weight;
	private double sonar_weight;
	private double exploration_weight;
	private boolean flag_inflation_radius_1=false;
	private boolean flag_inflation_radius_2=false;
	private boolean flag3 = false;
	private boolean flag4 = false;
	private boolean flag5 = false;

	/*
	public Modifier() 
	{
		this.ID = NEXT_ID++;
		if(this.ID==1)
		{
			//default setting
			inflation_radius = 		0.4;//1; near, short path
			cost_scaling_factor = 	10;//15;
			//rosrun dynamic_reconfigure dynparam set /p3dx/laser_mapper/costmap/inflation_layer cost_scaling_factor 10
			//rosrun dynamic_reconfigure dynparam set /p3dx/laser_mapper/costmap/inflation_layer inflation_radius 3
		}

		if(this.ID==2)
		{
			inflation_radius = 		10;//10; away, long path
			cost_scaling_factor = 	1;//1;
			//rosrun dynamic_reconfigure dynparam set /p3dx/laser_mapper/costmap/inflation_layer cost_scaling_factor 1
			//rosrun dynamic_reconfigure dynparam set /p3dx/laser_mapper/costmap/inflation_layer inflation_radius 10
		}
		/*else
			{

				//default setting
				inflation_radius = 		1;//2;
				cost_scaling_factor = 	15;//12;
			}
		*/
		//inflation_radius = 		0.2*this.ID+0.1;
		//cost_scaling_factor = 	7-this.ID;

		/*
		speed = 				5;//(1.4 -0.2*this.ID) ;//(50-this.ID) / 50*10 maxium speed from 9.5 - 0
		occdist = 				10;//0.025;
		lidar_weight = 			0.7;
		collision_weight = 		0.1;
		sonar_weight = 			0.2;
		exploration_weight = 	0.3;

		inflation_radius = 		Math.random() * 10;
		cost_scaling_factor = 	Math.random() * 10;
		speed = 				Math.random() * 10;//maxium speed...?
		occdist = 				Math.random() * 0.05;
		lidar_weight = 			Math.random();
		collision_weight = 		Math.random();
		sonar_weight = 			Math.random();
		exploration_weight = 	Math.random();
		*/
	/*}
	*/

	public Modifier(int mask)
	{
		this.ID = mask;

		if(this.ID==1)
		{
			if(!flag_inflation_radius_1)
			{
				inflation_radius = 		0.02;
				cost_scaling_factor = 	10;
				flag_inflation_radius_1=true;
			}

			//default setting
			//inflation_radius = 		0.4;// near
			//cost_scaling_factor = 	10;//

			//rosrun dynamic_reconfigure dynparam set /p3dx/laser_mapper/costmap/inflation_layer cost_scaling_factor 10
			//rosrun dynamic_reconfigure dynparam set /p3dx/laser_mapper/costmap/inflation_layer inflation_radius 3
		}

		if(this.ID==2)
		{
			if(!flag_inflation_radius_2)
			{
				inflation_radius = 		0.04;
				cost_scaling_factor = 	10;

				flag_inflation_radius_2=true;
			}


			//inflation_radius = 		10;//10; away
			//cost_scaling_factor = 	1;//1;
			//rosrun dynamic_reconfigure dynparam set /p3dx/laser_mapper/costmap/inflation_layer cost_scaling_factor 1
			//rosrun dynamic_reconfigure dynparam set /p3dx/laser_mapper/costmap/inflation_layer inflation_radius 10
		}
		if(this.ID==3)
		{
			if(!flag3)
			{
				inflation_radius = 		0.8;
				cost_scaling_factor = 	10;
				flag3=true;
			}
		}
		if(this.ID==4)
		{
			if(!flag4)
			{
				inflation_radius = 		0.02;
				cost_scaling_factor = 	10;
				flag4=true;
			}
		}
		if(this.ID==5)
		{
			if(!flag5)
			{
				inflation_radius = 		0.02;
				cost_scaling_factor = 	10;
				flag5=true;
			}
		}
		/*else
		{

			//default setting
			inflation_radius = 		1;//3;
			cost_scaling_factor = 	15;//10;
		}
		*/

		//inflation_radius = 		0.2*this.ID+0.1;
		//cost_scaling_factor = 	7-this.ID;
		//speed = 				(7-mask);
		/*
		speed = 				5;//(1.4-mask*0.2);//(50-this.ID) / 50*10 maxium speed from 9.5 - 0
		occdist = 				1;
		lidar_weight = 			0.7;
		collision_weight = 		0.1;
		sonar_weight = 			0.2;
		exploration_weight = 	0.3;
		*/
		/*
		inflation_radius = 		Math.random() * 10;
		cost_scaling_factor = 	Math.random() * 10;
		speed = 				Math.random() * 10;//maxium speed...?
		occdist = 				Math.random() * 0.05;
		lidar_weight = 			Math.random();
		collision_weight = 		Math.random();
		sonar_weight = 			Math.random();
		exploration_weight = 	Math.random();
		*/
	}

	public String toString(){
		//String s = inflation_radius + " " + cost_scaling_factor + " " + speed + " " + occdist +" " + lidar_weight + " " + sonar_weight + " " + collision_weight + " " + exploration_weight;
		String s = inflation_radius + " " + cost_scaling_factor;
		return s;
	}
	
	@Override
	public int getMask() {
		// TODO Auto-generated method stub
		return ID;
	}
	
	public void applyModifier()
	{
		Runnable run = new Runnable(){

			@Override
			public void run() {
				String command = ". dynamic_param.sh " + inflation_radius + " " + cost_scaling_factor;
				//String command = ". dynamic_param.sh " + inflation_radius + " "+ inflation_radius + " "+ inflation_radius + " " + cost_scaling_factor + " " + cost_scaling_factor+ " " + cost_scaling_factor;
				//String command = ". dynamic_param.sh " + inflation_radius + " " + cost_scaling_factor + " " + speed + " " + occdist +" " + lidar_weight + " " + sonar_weight + " " + collision_weight + " " + exploration_weight;
				ProcessBuilder builder = new ProcessBuilder("bash","-c", command);//"opt/ros/hydro/bin/rostopic");
				builder.directory(new File("/home/robot-base/tony_catkin/chris_catkin_ws"));
				//builder.directory(new File("/home/robot-base/tony_catkin/chris_catkin_ws"));//change this director to my fold

				try {
					Process p = builder.start();
					BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
					BufferedReader error_reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
					String line = "";
					String error_line = "";
					while((line = reader.readLine()) != null || (error_line = error_reader.readLine()) != null)
					{
						if(line != null)
							System.out.println(line);
						if(error_line != null)
							System.out.println(error_line);
					}
					
					int i = p.waitFor();
//
					String s = " inflation(" +inflation_radius + "),  cost_scaling(" + cost_scaling_factor+").";// + " speed:'" + speed + "' " + occdist +" " + lidar_weight + " " + sonar_weight + " " + collision_weight + " " + exploration_weight;
					System.out.println(s);
					System.out.println("modifier.java::modifiers have been applied: ok "+i);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		};
		
		Thread thread = new Thread(run);
		thread.setDaemon(true);
		thread.start();
		
		try {
			thread.join(20000);
		} catch (InterruptedException e) {

		}
		
		if(thread.isAlive()){
			thread.interrupt();
			
		}
	}
}
