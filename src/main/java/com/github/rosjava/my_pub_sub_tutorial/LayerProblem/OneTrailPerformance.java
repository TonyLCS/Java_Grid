package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javassist.bytecode.Descriptor;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ClassifierExpectation;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.FitnessCount;

import com.thoughtworks.xstream.XStream;


public class OneTrailPerformance
{
    public OneTrailPerformance(){}

    /** 1.time_reward**/
    public double timeConsumption=0;

    /** 2.Success Flag **/
    public boolean successFlag= false;

    /** 3.collision number**/
    public float timeOfCollision=0;

    /**4. collision map**/
    public LayerCondition a_CollisionLocation=null;

    /**5. record current location **/
    public LayerCondition currentLocation=null;




    /******************************************************************/
    /** 1.time_reward**/

    public void toSetTimeConsumption(double timeConsumption)
    {
        this.timeConsumption= timeConsumption;
    }

    /** 2.SuccessFlag **/
    public void toSetSuccessFlag(boolean successFlag)
    {
        this.successFlag=successFlag;
    }



    /** 3.collision number**/
    public void toSetTimeOfCollision(float timeOfCollision)
    {
        this.timeOfCollision= timeOfCollision;
    }

    /**4. collision map**/
    public void toSetCollisionMap(LayerCondition a_CollisionLocation)
    {
        //tempory record the last one
        //todo: extend into a complete map
        this.a_CollisionLocation= a_CollisionLocation;
    }
    /**5. record current location **/
    public void toSetCurrentLocation(LayerCondition currentLocation)
    {
        this.currentLocation= currentLocation;
    }
}