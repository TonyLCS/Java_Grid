package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem;

import java.util.ArrayList;
import java.util.HashMap;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ConditionValue;

public class Reinforcer
{
	
	//Pain, Progress, SenseOfAgency, Novelty, Habitat
	private HashMap<Integer, ConditionValue> a_reinforcer = new HashMap<Integer, ConditionValue>();
	//private HashMap<Integer, ConditionValue> a_reinforcerCell = new HashMap<Integer, ConditionValue>();

	//private ConditionValue value;			//nan
	//private ConditionValue proportional;	//0
	//private ConditionValue integral;		//1
	//private ConditionValue derivative;	//2
	//private ConditionValue mean;			//3
	//private ConditionValue median;		//4


	public Reinforcer(double proportional, double integral, double derivative, double mean, double median) {
		super();
		a_reinforcer.put(0, new ConditionValue(proportional));
		a_reinforcer.put(1, new ConditionValue(integral));
		a_reinforcer.put(2, new ConditionValue(derivative));
		a_reinforcer.put(3, new ConditionValue(mean));
		a_reinforcer.put(4, new ConditionValue(median));
	}
	public Reinforcer(double stimuli1, double stimuli2, double stimuli3) {
		super();
		a_reinforcer.put(0, new ConditionValue(stimuli1));
		a_reinforcer.put(1, new ConditionValue(stimuli2));
		a_reinforcer.put(2, new ConditionValue(stimuli3));

		//a_reinforcer.put(3, new ConditionValue(mean));
		//a_reinforcer.put(4, new ConditionValue(median));
	}
	public Reinforcer(double stimuli1, double stimuli2, double stimuli3,boolean doCell)
	{
		//it will replace cell world value with continue value
		a_reinforcer.put(0, new ConditionValue(stimuli1, doCell, 0));
		a_reinforcer.put(1, new ConditionValue(stimuli2, doCell, 1));
		a_reinforcer.put(2, new ConditionValue(stimuli3, doCell, 2));


		/*
		// combine a_reinforcerCell into a_reinforcer, so the  class "Reinforcer" will be the same
		a_reinforcerCell.put(0, new ConditionValue(stimuli1, doCell, 0));
		a_reinforcerCell.put(1, new ConditionValue(stimuli2, doCell, 1));
		a_reinforcerCell.put(2, new ConditionValue(stimuli3, doCell, 2));
		*/

	}


	public Reinforcer(String str,double sepal_length, double sepal_width, double petal_length, double petal_width)
	{
		super();
		if(str.equals("iris"))
		{
			a_reinforcer.put(0, new ConditionValue(sepal_length));
			a_reinforcer.put(1, new ConditionValue(sepal_width));
			a_reinforcer.put(2, new ConditionValue(petal_length));
			a_reinforcer.put(3, new ConditionValue(petal_width));
		}
	}

	public Reinforcer(String str, String eyes,double sepal_length, double sepal_width, double petal_length, double petal_width)
	{
		super();
		if(str.equals("iris"))
		{
			if(eyes.equals("crystalEyes"))
			{

				a_reinforcer.put(0, new ConditionValue(str, eyes, sepal_length));
				a_reinforcer.put(1, new ConditionValue(str, eyes, sepal_width));
				a_reinforcer.put(2, new ConditionValue(str, eyes, petal_length));
				a_reinforcer.put(3, new ConditionValue(str, eyes, petal_width));
			}
			if(eyes.equals("testEyes"))
			{

				a_reinforcer.put(0, new ConditionValue(str, eyes, sepal_length));
				a_reinforcer.put(1, new ConditionValue(str, eyes, sepal_width));
				a_reinforcer.put(2, new ConditionValue(str, eyes, petal_length));
				a_reinforcer.put(3, new ConditionValue(str, eyes, petal_width));
			}
		}
	}

	public Reinforcer(String which_task, String do_boundary,double[] attributes, double BOUNDARY)
	{
		// "no_boundary",an_instance.attribute
		//which_task, do_boundary,attributes
		super();
		int attribute_length= attributes.length;
		for(int i=0; i<attribute_length; i++)
		{
			//todo: automatic seting boundary
			/**  SET BOUNDARY HERE**/
			if(i<attribute_length-1)
			{
				BOUNDARY = 3;
			}
			else
			{
				BOUNDARY = 0.8;
			}
			BOUNDARY =6;
			a_reinforcer.put(i,new ConditionValue(which_task,do_boundary,attributes[i], BOUNDARY));
		}

	}


	public Reinforcer(String which_task, String do_boundary,double[] attributes, double[][] perception_boundary)
	{
		// "no_boundary",an_instance.attribute
		//which_task, do_boundary,attributes
		super();
		int attribute_length= attributes.length;
		for(int i=0; i<attribute_length; i++)
		{
			a_reinforcer.put(i,new ConditionValue(which_task,do_boundary,attributes[i], perception_boundary[i][1] - perception_boundary[i][0] ));
		}

	}

	public Reinforcer(double[] condition_attributes_arr)
	{
		/**
		 * condition_attributes_arr = C1L	C1V	C1H	C2L	C2V	C2H	C3L	C3V	C3H

		 *lower_value_upper = C1L C1V C1H
		 * ***************/

		super();
		double BOUNDARY;
		double[] lower_value_upper =  new double[3];

		int attribute_length= (condition_attributes_arr.length)/3;

		for(int i=0; i<attribute_length; i++)
		{
			//todo: automatic seting boundary
			/**  SET BOUNDARY HERE**/
			if(i<attribute_length-1)
			{
				BOUNDARY = 3;
			}
			else
			{
				BOUNDARY = 0.8;
			}

			/** set lower_value_upper**/
			lower_value_upper[0]= condition_attributes_arr[3*i];
			lower_value_upper[1]= condition_attributes_arr[3*i+1];
			lower_value_upper[2]= condition_attributes_arr[3*i+2];
			a_reinforcer.put(i,new ConditionValue(lower_value_upper, BOUNDARY));
		}


	}

	public Reinforcer(double[] condition_attributes_arr, double[] state)
	{
		/**
		 * condition_attributes_arr =
		 * ***************/

		super();
		double BOUNDARY;
		double[] lower_value_upper =  new double[3];

		int attribute_length= (condition_attributes_arr.length)/3;

		for(int i=0; i<attribute_length; i++)
		{
			//todo: automatic seting boundary
			/**  SET BOUNDARY HERE**/
			if(i<attribute_length-1)
			{
				BOUNDARY = 3;
			}
			else
			{
				BOUNDARY = 0.8;
			}

			/** set lower_value_upper**/
			lower_value_upper[0]= condition_attributes_arr[3*i];
			lower_value_upper[1]= condition_attributes_arr[3*i+1];
			lower_value_upper[2]= condition_attributes_arr[3*i+2];
			a_reinforcer.put(i,new ConditionValue(lower_value_upper, BOUNDARY));
		}


	}


	public void showInstanceValue()
	{
		for(ConditionValue c : a_reinforcer.values())
		{
			System.out.println(" 			>>>>>==		prepare for mutation: 			an current instance VALUE :("+c.getConditionVALUE()+") ");
		}
	}


	public int get_a_reinforcer_size()
	{
		int size;
		size= this.a_reinforcer.size();
		return size;
	}
	public ConditionValue get_a_condition_value(int index)
	{
		if(index< this.a_reinforcer.size())
		{
			return this.a_reinforcer.get(index);
		}
		else
		{
			return null;
		}
	}

	public Reinforcer(HashMap<Integer, ConditionValue> a_reinforcer)
	{
		this.a_reinforcer= a_reinforcer;
	}

	public Reinforcer(Reinforcer reinforcer)
	{
		super();
		//for(Map.Entry<Integer, ConditionValue> entry: reinforcer.reinforcers.entrySet())
		//Debug	ok	//
		/*
		for(int j=0; j< reinforcer.getAReinforcer().size();j++)
		{
			System.out.println("		==("+j+") getAReinforcer().size("+reinforcer.getAReinforcer().size()+")");

		}
		*/

		this.a_reinforcer= reinforcer.getAReinforcer();
		//Debug	ok	//System.out.println(" this a_reinforcer(0)= '"+this.a_reinforcer.get(0)+"'.");
		//this.a_reinforcerCell= reinforcer.getAReinforcerCell();


		//a_reinforcer.put(0, new ConditionValue(reinforcer.a_reinforcer.get(0)));
		//a_reinforcer.put(1, new ConditionValue(reinforcer.a_reinforcer.get(1)));
		//a_reinforcer.put(2, new ConditionValue(reinforcer.a_reinforcer.get(2)));
		//a_reinforcer.put(3, new ConditionValue(reinforcer.a_reinforcer.get(3)));
		//a_reinforcer.put(4, new ConditionValue(reinforcer.a_reinforcer.get(4)));
	}

	public Reinforcer(double[] values, String phenotype)
	{
		if(phenotype.equals("ternary"))
		{
			for(int i=0; i<10; i++)
			{
				//change each value in values[] into Attributes, and save into hashmap, a_reinforcer.
				a_reinforcer.put(i, new ConditionValue(values[i],"ternary"));

			}
			//conbine it into a string stream
			String str=		combineString(); //showPIDMM();
			//ok	//System.out.println("	a_reinforcer :"+str+"<< \n");

		}
		//System.out.println("				ternary:"+fakeTernary+" ");
		//todo: ConditionValue change into ternary
		//a_reinforcer.put(0, new ConditionValue(fakeTernary));


		/*
		double fakeTernary=0;
		if(phenotype.equals("ternary"))
		{
			for(int i=0; i<10; i++)
			{
				fakeTernary +=  values[i]*Math.pow(2, i);
				System.out.println("				ternary("+fakeTernary+"), values["+i+"]("+values[i]+") ");

			}

		}
		System.out.println("				ternary:"+fakeTernary+" ");
		*/


	}

	public HashMap<Integer, ConditionValue> getAReinforcer()
	{
		return this.a_reinforcer;
	}
	/*
	public HashMap<Integer, ConditionValue> getAReinforcerCell()
	{
		return this.a_reinforcerCell;
	}
	*/

	public boolean isWithin(Reinforcer other)
	{
		//System.out.println("************************************");
		//System.out.println("\n");
		for(int i = 0; i < a_reinforcer.values().size(); i++)
		{

			//System.out.println("				in compare reinforcer ("+i+"/"+a_reinforcer.values().size()+"). " + "one ("+this.a_reinforcer.get(i)+") VS two ("+other.a_reinforcer.get(i)+").");
			ConditionValue one = this.a_reinforcer.get(i);
			ConditionValue two = other.a_reinforcer.get(i);
			if(!one.compareTo(two))
			{
				//System.out.println("				No match");
				//System.out.println("************************************");
				return false;
			}
		}
		//System.out.println("				Match");
		//System.out.println("************************************");
		return true;
	}


	public boolean isTheSame_unique(Reinforcer other)
	{
		/**
		 * if they are the same, return true, otherwise, return false.
		 * **/
		//System.out.println("************************************");
		//System.out.println("\n");
		for(int i = 0; i < a_reinforcer.values().size(); i++)
		{

			//System.out.println("				in compare reinforcer ("+i+"/"+a_reinforcer.values().size()+"). " + "one ("+this.a_reinforcer.get(i)+") VS two ("+other.a_reinforcer.get(i)+").");

			ConditionValue one = this.a_reinforcer.get(i);
			ConditionValue two = other.a_reinforcer.get(i);
			if(!one.isEqualTo_unique(two))
			{
				//System.out.println("				No match");
				//System.out.println("************************************");
				return false;
			}
		}
		//System.out.println("				Match");
		//System.out.println("************************************");
		return true;
	}
	
	public boolean equals(Reinforcer other)
	{
		for(int i = 0; i < a_reinforcer.size(); i++)
		{
			ConditionValue one = this.a_reinforcer.get(i);
			ConditionValue two = other.a_reinforcer.get(i);
			if(!one.compareTo(two))
				return false;
		}
		return true;
	}
	public Reinforcer crossover(Reinforcer other)
	{
		ConditionValue doGACondition= new ConditionValue();


		for(int i = 0; i < a_reinforcer.size(); i++)
		{
			ConditionValue one = this.a_reinforcer.get(i);
			ConditionValue two = other.a_reinforcer.get(i);
				//dubug clone//	System.out.println("	IsCell("+one.getIsCell()+")	"+i+"" + "	before crossover: 	conditionValueCell: 	" + "	"+ " lower:("+one.getConditionValuelowerCell()+") and " + "upper("+one.getConditionValueupperCell()+") " + " VS  lower("+two.getConditionValuelowerCell()+")and upper("+two.getConditionValueupperCell()+")") ;

			//other.a_reinforcer.put(i, this.a_reinforcer.get(i).crossover(other.a_reinforcer.get(i)));
			// work 	crossoverConditionValue(this.a_reinforcer.get(i),other.a_reinforcer.get(i));
			//in the following line "this.a_reinforcer.get(i)." is calling "crossOverLOWER" function in "ConditionValue" class
			//this.a_reinforcer.get(i).crossOverLOWER(this.a_reinforcer.get(i),other.a_reinforcer.get(i));

			//doGACondition.crossOverLOWER(this.a_reinforcer.get(i),other.a_reinforcer.get(i));


			if(Math.random()<=0.5)
			{
				doGACondition.crossOverUPPER(this.a_reinforcer.get(i),other.a_reinforcer.get(i));
				//if the boundary is opposited, switch the boundary.
				checkSwitchBoundary(this.a_reinforcer.get(i));
				checkSwitchBoundary(other.a_reinforcer.get(i));


				//check if the lowerCell is lower than the upperCell
				ConditionValue oneCheck = this.a_reinforcer.get(i);
				ConditionValue twoCheck = other.a_reinforcer.get(i);

				if(!(checkBoundary(oneCheck)&&checkBoundary(twoCheck)))
				{ //if the boundary is wrong, change it back
				//	doGACondition.crossOverUPPER(this.a_reinforcer.get(i),other.a_reinforcer.get(i));
				//	System.out.println(" crossover:: the boundary is unusual, change it back");
					System.out.println(" crossover:: the boundary is  (upperCell ("+i+")), this should no happen...");
				}

			}
			else
			{
				doGACondition.crossOverLOWER(this.a_reinforcer.get(i),other.a_reinforcer.get(i));

				//if the boundary is opposited, switch the boundary.
				checkSwitchBoundary(this.a_reinforcer.get(i));
				checkSwitchBoundary(other.a_reinforcer.get(i));

				ConditionValue oneCheck = this.a_reinforcer.get(i);
				ConditionValue twoCheck = other.a_reinforcer.get(i);
				if(!(checkBoundary(oneCheck)&&checkBoundary(twoCheck)))
				{ //if the boundary is wrong, change it back
					//doGACondition.crossOverLOWER(this.a_reinforcer.get(i),other.a_reinforcer.get(i));
					//System.out.println(" crossover:: the boundary is unusual, change it back");

					System.out.println(" crossover:: the boundary is  (lowerCell ("+i+")), this should no happen...");
				}

			}


			/*//dubug clone//
			ConditionValue oneCheck = this.a_reinforcer.get(i);
			ConditionValue twoCheck = other.a_reinforcer.get(i);

			if(oneCheck.getIsCell())
			{
				System.out.println("	IsCell("+oneCheck.getIsCell()+")	"+i+"" +
						"	after crossover: 	 	conditionValueCell: 	" +
						"	"+ " lower:("+one.getConditionValuelowerCell()+") and " + "upper("+one.getConditionValueupperCell()+") "
						+ " VS  lower("+two.getConditionValuelowerCell()+")and upper("+two.getConditionValueupperCell()+")") ;


			}
			else
			{
				System.out.println("	IsCell("+oneCheck.getIsCell()+")	"+i+"" +
						"	after crossover: 	conditionValue: 	" +
						"upper("+oneCheck.getConditionValueUPPER()+")-" +
						" lower:("+oneCheck.getConditionValueLOWER()+") " +
						"VS upper("+twoCheck.getConditionValueUPPER()+")-" +
						"lower("+twoCheck.getConditionValueLOWER()+")" +
						".");
			}
			*/




		}
		return other;
	}
	public Reinforcer crossover_oneAttribute(Reinforcer other)
	{
		ConditionValue doGACondition= new ConditionValue();
		int attrubute_seq=(int)Math.ceil(a_reinforcer.size()* Math.random())-1;


			ConditionValue one = this.a_reinforcer.get(attrubute_seq);
			ConditionValue two = other.a_reinforcer.get(attrubute_seq);


			if(Math.random()<=0.5)
			{
				doGACondition.crossOverUPPER(this.a_reinforcer.get(attrubute_seq),other.a_reinforcer.get(attrubute_seq));
				//if the boundary is opposited, switch the boundary.

				checkSwitchBoundary(this.a_reinforcer.get(attrubute_seq));
				checkSwitchBoundary(other.a_reinforcer.get(attrubute_seq));
			}
			else
			{
				doGACondition.crossOverLOWER(this.a_reinforcer.get(attrubute_seq),other.a_reinforcer.get(attrubute_seq));

				//if the boundary is opposited, switch the boundary.
				checkSwitchBoundary(this.a_reinforcer.get(attrubute_seq));
				checkSwitchBoundary(other.a_reinforcer.get(attrubute_seq));
			}

		return other;
	}

	public Reinforcer doMorph(Reinforcer other)
	{
		ConditionValue doGACondition= new ConditionValue();
		int attribute_num=(int)Math.ceil(Math.random()*a_reinforcer.size()) -1 ;
		System.out.println(" 		in reinforcer,	the attribute_num ("+attribute_num+") (0-3)is override");
		ConditionValue one = this.a_reinforcer.get(attribute_num);
		ConditionValue two = other.a_reinforcer.get(attribute_num);

		doGACondition.doMorph(one, two);


		return other;
	}

	public ArrayList<Reinforcer>  doMitosis(Reinforcer other)
	{
		//debug ok//System.out.println("	L2 : reinforcer has	the attribute_num ("+a_reinforcer.size()+") ");
		//int attribute_num=(int)Math.ceil(Math.random()*a_reinforcer.size()) -1 ;
		//System.out.println("	L2 		in reinforcer,	the attribute_num ("+attribute_num+") (0-3)is to mitosis");

		//this.a_reinforcer.put(attribute_num, one);
		ArrayList<Reinforcer> result_reinforcers= new ArrayList<Reinforcer>();
		for(int attribute_index=0;attribute_index<a_reinforcer.size(); attribute_index++)
		{
			//debug ok//System.out.println("	L2 : 	processing attribute NO. ("+attribute_index+") ");
			ConditionValue one = this.a_reinforcer.get(attribute_index);
			ConditionValue two = other.a_reinforcer.get(attribute_index);
			ArrayList<ConditionValue> result_conditions =  one.doMitosis(two);
			//construct the new reinforcer by the result_condition
			// the number of new reinforcer to be constructed is  the number of ConditionValue in the arraylist
			if(!result_conditions.isEmpty())
			{
				for(int i=0; i< result_conditions.size();i++)
				{
					//debug ok//System.out.println("	L2 : 		doMitosis: resulting  generation of new reinforcer ("+i+"/"+result_conditions.size()+") in attribute_index("+attribute_index+")");
					Reinforcer a_reinforcer_generated_by_mitosis = new Reinforcer(this.a_reinforcer);
					a_reinforcer_generated_by_mitosis.a_reinforcer.put(attribute_index, result_conditions.get(i));
					result_reinforcers.add(a_reinforcer_generated_by_mitosis);
				}
			}
			else
			{
				//debug ok//System.out.println("	L2 : 		attribute NO. ("+attribute_index+")  is empty, case 4 )");
			}

		}
		//debug ok//System.out.println("	L2 : summary: 	doMitosis: result_reinforcers has number of ("+result_reinforcers.size()+")\n");

		return result_reinforcers;

	}
	/* replace by mutateThis()
	public void mutate(Reinforcer one)
	{

		ConditionValue doGACondition= new ConditionValue();
		for(int i = 0; i < one.a_reinforcer.size(); i++)
		{
			doGACondition.mutation(one.a_reinforcer.get(i));
		}

	}
	*/
	public void checkSwitchBoundary(ConditionValue oneCheck )
	{
		if(oneCheck.getConditionValuelowerCell()>oneCheck.getConditionValueupperCell())
		{
			oneCheck.switchConditionValue();
		}
	}

	public boolean checkBoundary(ConditionValue oneCheck )
	{
		if(oneCheck.getConditionValuelowerCell()<=oneCheck.getConditionValueupperCell())
		{
			return true;
		}
		return false;
	}
	public void mutateThis()
	{
		//mutate itself
		//ConditionValue doGACondition= new ConditionValue();
		//showInstanceValue();

		/*
		for(int i = 0; i < this.a_reinforcer.size(); i++)
		{
			//doGACondition.mutation(one.a_reinforcer.get(i));
			this.a_reinforcer.get(i).mutateThis(i); // go to ConditionValue.java
		}
		*/

		/**only mutation one attribute**/
		int attribute_seq= ((int)Math.ceil(Math.random()*this.a_reinforcer.size()))-1;
		this.a_reinforcer.get(attribute_seq).mutateThis(attribute_seq); // go to ConditionValue.java


	}
	public void mutateThis(double[][] boundary)
	{
		/**only mutation one attribute**/
		int attribute_seq= ((int)Math.ceil(Math.random()*this.a_reinforcer.size()))-1;
		this.a_reinforcer.get(attribute_seq).mutateThis(attribute_seq, boundary); // go to ConditionValue.java
	}
	/*
	public void crossoverConditionValue(ConditionValue one,ConditionValue two )
	{
		one.crossOverLOWER(one,two);
	}
	*/

	public void setScope(Reinforcer scopeReinforcer,Reinforcer other)
	{
		//check if scopeReinforcer is empty
		if (scopeReinforcer.a_reinforcer==null)
		{
			for(int i=0;i<other.a_reinforcer.size();i++)
			{
				ConditionValue oneInstance = other.a_reinforcer.get(i);
				scopeReinforcer.a_reinforcer.get(i).setConditionValueUPPER(oneInstance.getConditionValueUPPER());
				scopeReinforcer.a_reinforcer.get(i).setConditionValueLOWER(oneInstance.getConditionValueLOWER());
			}
			System.out.println(" 			the scopeReinforcer is null");
		}
		else {
			for (int i = 0; i < other.a_reinforcer.size(); i++) {
				ConditionValue scope = scopeReinforcer.a_reinforcer.get(i);
				ConditionValue oneInstance = other.a_reinforcer.get(i);

				//now
				System.out.println(" 			before checkScope:: ScopeConditionValue :upper ('" + scope.getConditionValueUPPER() + "')," +
						"	lower(" + scope.getConditionValueLOWER() + ") VS instance  :upper ('" + oneInstance.getConditionValueUPPER() + "')," +
						"	lower(" + oneInstance.getConditionValueLOWER() + ")");
				// in this case "this.a_reinforcer" actually represents the scope reinforcer
				// which contains the max Upper and min low

				if (oneInstance.getConditionValueUPPER() >= scope.getConditionValueUPPER()) {
					scopeReinforcer.a_reinforcer.get(i).setConditionValueUPPER(oneInstance.getConditionValueUPPER());
				}
				if (oneInstance.getConditionValueLOWER() <= scope.getConditionValueLOWER()) {
					scopeReinforcer.a_reinforcer.get(i).setConditionValueLOWER(oneInstance.getConditionValueLOWER());
				}
			}
		}
	}
	public String combineString()
	{
		String str="";
		for(int i=0;i< a_reinforcer.size();i++)
		{
			String one=a_reinforcer.get(i).getConditionValueTernaryAttribute();
			str += one;
		}
		return str;
	}

	public String showPIDMM()
	{
		String str = " ";
		for(int i=0;i< a_reinforcer.size();i++)
		{
			ConditionValue one=a_reinforcer.get(i);
			str += one.toStringValue();

		}
		return str;
	}

	public Reinforcer cloneReinforcer()throws CloneNotSupportedException
	{
		//this will return a clone reinforcer with three conditionValue,
		//  such as a location-reinforcer  with (x,y,theta)-triple conditionValue
		HashMap<Integer, ConditionValue> a_clone_reinforcer = new HashMap<Integer, ConditionValue>();
		for(int i=0; i<this.a_reinforcer.size();i++)
		{ 	//ConditionValue conditionValue= new ConditionValue();
			//conditionValue=(ConditionValue) a_reinforcer.get(i).clone();
			a_clone_reinforcer.put(i,a_reinforcer.get(i).cloneConditionValue());
		}
		Reinforcer clone_reinforcer= new Reinforcer(a_clone_reinforcer);

		return clone_reinforcer;
	}
	
	public String toString(){
		String s = "";
		for(ConditionValue c : a_reinforcer.values())
			s += c.toString() +" ";
		return s;
	}

	public String toString_csv(){
		String s = "";
		for(ConditionValue c : a_reinforcer.values())
			s += c.toString_csv() +"#";
		return s;
	}

	public String toHeaderString_csv( ){
		String s = "";
		int whichAttribute=1;
		for(ConditionValue c : a_reinforcer.values())
		{
			s += c.toHeaderString_csv(whichAttribute) +"#";
			whichAttribute++;
		}
		return s;
	}

	public String showReinforcer()
	{
		String s = "";
		for(ConditionValue c : a_reinforcer.values())
			s += c.showConditionValue() +" ";
		return s;

	}

}
