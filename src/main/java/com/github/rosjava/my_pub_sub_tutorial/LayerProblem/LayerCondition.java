package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

public class LayerCondition extends Condition<LayerCondition> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<Integer, Reinforcer> reinforcers = new HashMap<Integer, Reinforcer>();
	// for example reinforcers-> <0, location>,<1, lidarData>,<2, Collisions>,<3, >
	
	public LayerCondition(Reinforcer[] reinforcerSet)
	{
		for(int i = 0; i < reinforcerSet.length; i++)
		{
			this.reinforcers.put(i, reinforcerSet[i]);
			//System.out.println("			LayerCondition: i("+i+")--reinforcerSet("+reinforcerSet[i]+")");
			//			System.out.println("			LayerCondition: i("+i+")--reinforcerSet.get(0).getConditionValueupperCell" +"("+reinforcerSet[i].getAReinforcerCell().get(0).getConditionValueupperCell()+")");
		}
	}

	public LayerCondition(Reinforcer[] reinforcerSet, boolean doCell)
	{
		if(doCell)
		{
			for(int i = 0; i < reinforcerSet.length; i++)
			{
				this.reinforcers.put(i, reinforcerSet[i]);
				System.out.println("			LayerCondition: i("+i+")--reinforcerSet("+reinforcerSet[i]+")");
				//			System.out.println("			LayerCondition: i("+i+")--reinforcerSet.get(0).getConditionValueupperCell" +"("+reinforcerSet[i].getAReinforcerCell().get(0).getConditionValueupperCell()+")");
			}
		}
		else
			{
				for(int i = 0; i < reinforcerSet.length; i++)
				{
					this.reinforcers.put(i, reinforcerSet[i]);
					System.out.println("			LayerCondition: i("+i+")--reinforcerSet("+reinforcerSet[i]+")");
					//			System.out.println("			LayerCondition: i("+i+")--reinforcerSet.get(0).getConditionValueupperCell" +"("+reinforcerSet[i].getAReinforcerCell().get(0).getConditionValueupperCell()+")");
				}
			}

	}
	public LayerCondition(HashMap<Integer, Reinforcer> reinforcerSet)
	{
		this.reinforcers=reinforcerSet;
		/*
		for(int i = 0; i < reinforcer.size(); i++)
			this.reinforcers.put(i, new Reinforcer(reinforcer.get(i)));
		*/
	}
	public LayerCondition(Reinforcer[] reinforcerSet,String phenotype)
	{
		if(phenotype.equals("ternary"))
		{
			for(int i = 0; i < reinforcerSet.length; i++)
			{
				this.reinforcers.put(i, reinforcerSet[i]);
				//System.out.println("			LayerCondition: i("+i+")--reinforcerSet("+reinforcerSet[i]+")");
				//			System.out.println("			LayerCondition: i("+i+")--reinforcerSet.get(0).getConditionValueupperCell" +"("+reinforcerSet[i].getAReinforcerCell().get(0).getConditionValueupperCell()+")");
			}
		}
	}

	/*
	private LayerCondition(HashMap<Integer, Reinforcer> reinforcer)
	{
		for(int i = 0; i < reinforcer.size(); i++)
			this.reinforcers.put(i, new Reinforcer(reinforcer.get(i)));
	}
	*/



	public boolean checkAttribute(Integer i,String ternary, String att)
	{
		//compare an attribute of a_reinforcer in reinforcers
		Reinforcer one= reinforcers.get(i);
		String str=one.combineString();
		if(ternary.equals("ternary"))
		{
			System.out.println("		check ternary: check if the string("+str+" include 1");
			if(str.contains(att))
			{
				System.out.println(" collision happens at :index ("+str.indexOf(att)+")");
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public int whereAttribute(Integer i,String ternary, String att)
	{
		//compare an attribute of a_reinforcer in reinforcers
		Reinforcer one= reinforcers.get(i);
		String str=one.combineString();
		if(ternary.equals("ternary"))
		{
			System.out.println("		check ternary: check if the string("+str+" include 1");
			if(str.contains(att))
			{
				System.out.println(" collision happens at :index ("+str.indexOf(att)+") and (return 1+index)");
				return (1+str.indexOf(att)) ;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}


	@Override
	public void crossoverCondition(LayerCondition one,LayerCondition other,String mode)
	{
		for(int i=0; i< one.reinforcers.size(); i++)
		{
			Reinforcer oneRe=one.reinforcers.get(i);
			Reinforcer otherRe=other.reinforcers.get(i);
			if(mode.equals("only_one"))
			{
				otherRe	=	oneRe.crossover_oneAttribute(otherRe);
			}
			else
			{// default mode= "all"
				otherRe	=	oneRe.crossover(otherRe);

			}
			//otherRe	=	oneRe.crossover(otherRe);
			one.reinforcers.put(i,oneRe);
			other.reinforcers.put(i,otherRe);

		}
	}


	@Override
	public  void doMorph(LayerCondition one,LayerCondition other)
	{
		for(int i=0; i< one.reinforcers.size(); i++)
		{
			Reinforcer oneRe=one.reinforcers.get(i);
			Reinforcer otherRe=other.reinforcers.get(i);
			otherRe	=	oneRe.doMorph(otherRe);
			one.reinforcers.put(i,oneRe);
			other.reinforcers.put(i,otherRe);

		}
	}

	@Override
	public  ArrayList<LayerCondition> doMitosis(LayerCondition other)
	{
		ArrayList<LayerCondition> motisis_condition_set =  new ArrayList<LayerCondition>();
		for(int i=0; i< this.reinforcers.size(); i++)
		{

			Reinforcer oneRe=this.reinforcers.get(i);
			Reinforcer otherRe=other.reinforcers.get(i);

			//debug ok//System.out.println(" L1	Mitosis: in layerCondition, show reinforcers (size("+this.reinforcers.size()+")), classifier's condition("+oneRe.toString_csv()+")" + " and current representation of instance input("+otherRe.toString_csv()+")");

			ArrayList<Reinforcer> generated_reinforcer_list=oneRe.doMitosis(otherRe);

			if(!generated_reinforcer_list.isEmpty())
			{
				for(int reinforcer_index=0; reinforcer_index<generated_reinforcer_list.size(); reinforcer_index++)
				{
					//construct new reinforcerSet
					LayerCondition generated_reinforcerSet = new LayerCondition(this.reinforcers);
					//replaced a reinforcer in reforcerSet
					generated_reinforcerSet.reinforcers.put(i,generated_reinforcer_list.get(reinforcer_index));
					motisis_condition_set.add(generated_reinforcerSet);
				}

			}
		}
		return motisis_condition_set;
	}


	public int get_layer_condision_dim()
	{
		//dim_Level_1
		int length;
		length=this.reinforcers.size();
		return length;
	}

	public Reinforcer get_a_reinforcer(int index)
	{
		if(index < this.reinforcers.size())
		{
			return 	this.reinforcers.get(index);
		}
		else
		{
			System.out.println("ERROR: the targeted reinforcer is not in the reinforcers set!");
			return null;
		}
	}


	@Override
	public void mutateCondition(LayerCondition one, LayerCondition boundary)
	{
		// TODO Auto-generated method stub
		/* replace by mutateCondition()
		for(int i=0; i< one.reinforcers.size(); i++)
		{
			Reinforcer oneRe=one.reinforcers.get(i);
			Reinforcer boundaryRe=boundary.reinforcers.get(i);
			//oneRe.mutate(oneRe,boundaryRe);
			oneRe.mutate(oneRe);
			one.reinforcers.put(i,oneRe);
		}
		*/
	}
	@Override
	public void mutateCondition()
	{
		// TODO Auto-generated method stub
		for(int i=0; i< this.reinforcers.size(); i++)
		{
			/*
			Reinforcer oneRe=this.reinforcers.get(i);
			oneRe.mutate(oneRe);
			this.reinforcers.put(i,oneRe);
			*/
			this.reinforcers.get(i).mutateThis();
		}

	}
	@Override
	public void mutateCondition(double[][] boundary)
	{
		// TODO Auto-generated method stub
		for(int i=0; i< this.reinforcers.size(); i++)
		{
			/*
			Reinforcer oneRe=this.reinforcers.get(i);
			oneRe.mutate(oneRe);
			this.reinforcers.put(i,oneRe);
			*/
			this.reinforcers.get(i).mutateThis(boundary);
		}

	}

	/*
	@Override
	public void getConditionInstanceVALUE()
	{
		for(int i=0; i< this.reinforcers.size(); i++)
		{
			this.reinforcers.get(i).getInstanceValue();
		}
	}
	*/

	/*
	public void layerConditionCrossover(Reinforcer one, Reinforcer other)
	{

		other=one.crossover(other);
	}
	*/
	@Override
	public boolean isStateWithin(LayerCondition other) {

		for(int i = 0; i < reinforcers.size(); i++)
		{
			Reinforcer r1 = reinforcers.get(i);
			Reinforcer r2 = other.reinforcers.get(i);

			if(!r1.isWithin(r2))
				return false;
		}
		return true;
	}

	@Override
	public boolean isStateWithin(double[] other) {

		for(int i = 0; i < reinforcers.size(); i++)
		{
			Reinforcer r1 = reinforcers.get(i);
			//Reinforcer r2 = other.reinforcers.get(i);//todo: reinforcer.isStateWithin()

			//todo: maze
			//if(!r1.isWithin(r2))
			//	return false;
		}
		return true;
	}

	@Override
	public int compareTo(LayerCondition condition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public LayerCondition replicate(){
		//replace with cloneLayerCondition()
		// TODO Auto-generated method stub

		//LayerCondition layerCondition= new LayerCondition(this.reinforcers);
		//LayerCondition returnLayerCondition= (LayerCondition) layerCondition.cloneLayerCondition();
		return new LayerCondition(reinforcers);// henry's shallow copy
	}

	public LayerCondition cloneLayerCondition()throws CloneNotSupportedException
	{

		//reinforcers is an Set with reinforcers
		//reinforcers.size represent the number of different reinforcer in this set
		//
		HashMap<Integer, Reinforcer> a_clone_reinforcers= new HashMap<Integer, Reinforcer>();

		for(int i=0; i<this.reinforcers.size();i++)
		{
			a_clone_reinforcers.put(i, reinforcers.get(i).cloneReinforcer()) ;
		}

		LayerCondition clone_layerCondition= new LayerCondition(a_clone_reinforcers);
		return clone_layerCondition;
	}

	@Override
	public  LayerCondition transferToLayerCondition(double[] state)
	{
		HashMap<Integer, Reinforcer> a_transfer_reinforcers= new HashMap<Integer, Reinforcer>();

		a_transfer_reinforcers.put(0, new Reinforcer(state)) ;

		LayerCondition transfer_layerCondition= new LayerCondition(a_transfer_reinforcers);
		return transfer_layerCondition;
	}

	@Override
	public boolean isTheSameCondition(LayerCondition condition) {
		// is contained, not the same
		for(int i = 0; i < reinforcers.size(); i++)
		{
			Reinforcer r1 = reinforcers.get(i);
			Reinforcer r2 = condition.reinforcers.get(i);

			if(!r1.isWithin(r2))
			{return false;}
		}
		return true;
	}
	@Override
	public boolean isTheSameCondition_unique(LayerCondition condition) {
		// check it they are the same, not within or contain in
		for(int i = 0; i < reinforcers.size(); i++)
		{
			Reinforcer r1 = reinforcers.get(i);
			Reinforcer r2 = condition.reinforcers.get(i);

			if(!r1.isTheSame_unique(r2))
			{return false;}
		}
		return true;
	}

	public String showCondition()
	{
		String s = "";
		for(Reinforcer r : reinforcers.values())
			s += r.showReinforcer() + " ";
		return s;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String s = "";
		for(Reinforcer r : reinforcers.values())
			s += r.toString() ;
		//System.out.println("toString_version" +s);
		return s;
	}

	@Override
	public String toString_for_csv() {
		// TODO Auto-generated method stub
		String s = "";
		for(Reinforcer r : reinforcers.values())
			s += r.toString_csv()+"#" ;
		//System.out.println("toString_csv_version" +s);
		return s;
	}


	@Override
	public String toHeaderString_csv() {
		// TODO Auto-generated method stub
		String s = "";
		for(Reinforcer r : reinforcers.values())
			s += r.toHeaderString_csv()+"#" ;
		return s;
	}

	@Override
	public void display(Graphics2D g2d, int offset_x, int offset_y) {
		// TODO Auto-generated method stub

	}

}
