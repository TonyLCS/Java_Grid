package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javassist.bytecode.Descriptor;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ClassifierExpectation;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.FitnessCount;

import com.thoughtworks.xstream.XStream;

public class RobotPreformance
{
    public     RobotPreformance(){}

    /**1**/
    public int seqNumber_of_tasks=0;
    /**2**/
    public float number_of_failure=0;
    public float number_of_success=0;


    // public HashMap<Integer, LayerCondition> 		collision 		= new HashMap<Integer, LayerCondition>();
  //  public HashMap<Integer, Float64>             completeness= new HashMap<Integer, Float64>();
    //public float rate_of_completement=0;

    /***************************/
    /** performance in a trail**/
    /***************************/
    // Interget= the sequence number of tasks, taskPerformance    /**hash table comtains all the tasks' situation**/
    public HashMap<Integer, OneTrailPerformance> trailPerformanceSet= new HashMap<Integer, OneTrailPerformance>();
    public OneTrailPerformance a_TrailPerformance = new OneTrailPerformance();




    /*******************/
    /**a tempory state**/
    /*******************/
    /** 1.time_reward**/
    private double time_taken_reward=0;
    /** 2.Success Flag **/
    private boolean successFlag= false;
    /** 3.collision number**/
    private float time_of_collision=0;
    /**4. collision map/ location**/
    private boolean isCollision= false;
    private LayerCondition collisionLocation=null;
    /**5. record current location **/
    private  LayerCondition currentLocation=null;





    /*************************************************************************************/
    /**record from Robot_Generic to Set**********/
    /** 1.time_reward**/
    public void setTime_taken(double time_taken_reward)
    {
        this.time_taken_reward=time_taken_reward;
        //this.time_taken.put(seqNumber_of_tasks, time_taken_reward);
    }

    /** 2.Success Flag **/
    //todo:
    public void SetSuccessFlag(boolean successFlag)
    {
        this.successFlag=successFlag;
        if(successFlag)
        {
            this.setNumber_of_success();
        }
        else
        {
            this.setNumber_of_failure();
        }

    }


    /** 3.collision number**/
    public void setNumber_of_collision()
    {
        this.time_of_collision++;
    }

    /**4. collision map/ location**/
    //todo:
    public void setCollisionLocation(LayerCondition location)
    {
        this.collisionLocation= location;
    }

    /**5. record current location **/
    public  void setCurrentLocation(LayerCondition currentLocation)
    {
        this.currentLocation= currentLocation;
    }


   /*************************************************************************************/
   /**tasks statistics**/
   /****1.seqNumber_of_tasks **/
   public void setNumber_of_tasks()
   {
       this.seqNumber_of_tasks++;
   }
   /****2.number of success and fail ************/
   private void setNumber_of_failure()
   {
       this.number_of_failure++;
   }
    private void setNumber_of_success()
    {
        this.number_of_success++;
    }

   /****3.collision calculation******/



    /*************************************************************************************/
    /******set a trial********************************************************************/

    public void saveOneTrailPerformance(String file)
    {
        System.out.println("save performance 1");
        // put OneTrailPerformance entity into hashmap
        // save 4 things
        this.updateOneTrailPerformance();// pack the data into OneTrailPerformance type of class.
        this.trailPerformanceSet.put(seqNumber_of_tasks, this.a_TrailPerformance);// save OneTrailPerformance into hashmap
        this.cleanOneTrailPerformance();// the data has already been saved into hashmap.
        this.savePreformance(file);

    }

    private void updateOneTrailPerformance()
    {
        System.out.println("save performance 2: pass time_taken_reward("+time_taken_reward+"), " +
                "successFlag("+successFlag+"),time_of_collision("+time_of_collision+") and collisionLocation("+collisionLocation+")" +
                " down to  a_TrailPerformance structure ");
        /**pass 4 parameters form Set down to entity**/
        /** 1.time_reward**/
        this.setOneTrial_TimeTaken(this.time_taken_reward);
        /** 2.Success Flag **/
        this.setOneTrial_SuccessFlag(this.successFlag);
        /** 3.collision number**/
        this.setOneTrial_TimeOfCollison(this.time_of_collision);
        /**4. collision map/ location**/
        if(this.time_of_collision>0)
        {
            this.setOneTrial_CollisionMap(this.collisionLocation);
        }
        else
            {
                this.collisionLocation=null;
            }
        /** 5.current location**/
        this.setOneTrial_CurrentLocation(this.currentLocation);




    }
            /*******form Set to entity******/
            /** 1.time_reward**/
            private void setOneTrial_TimeTaken(double time_taken_reward)
            {
                this.a_TrailPerformance.toSetTimeConsumption(time_taken_reward);
            }

            /** 2.Success Flag **/
            private void setOneTrial_SuccessFlag(boolean successFlag)
            {
                this.a_TrailPerformance.toSetSuccessFlag(successFlag);
            }

            /** 3.collision number**/
            private void setOneTrial_TimeOfCollison(float time_of_collision)
            {
                this.a_TrailPerformance.toSetTimeOfCollision(time_of_collision);
            }

            /**4. collision map/ location**/
            private void setOneTrial_CollisionMap(LayerCondition location)
            {
                this.a_TrailPerformance.toSetCollisionMap(location);
            }
            /**5. current location**/
            private void setOneTrial_CurrentLocation(LayerCondition currentLocation)
            {
                this.a_TrailPerformance.toSetCurrentLocation(currentLocation);
            }






    public void cleanOneTrailPerformance()
    {
        this.time_taken_reward=0;
        this.successFlag=false;
        this.time_of_collision=0;
        this.isCollision=false;
        this.collisionLocation= null;
        this.currentLocation=null;
        this.a_TrailPerformance= new OneTrailPerformance();

    }

    private void savePreformance(String file)
    {
        System.out.println("save performance 3 : time_of_collision("+this.time_of_collision+"), number_of_tasks("+this.seqNumber_of_tasks+") ");
        //saveOneTrailPerformance();
        try
        {
            XStream xstream = new XStream();
            String xml = xstream.toXML(this);
            BufferedWriter xmlOut = new BufferedWriter(new FileWriter(file+".xml"));
            xmlOut.write(xml);
            xmlOut.close();
            //System.out.println("save Management: file("+file+")");


        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        //System.out.println("test Management: end of saving procedural ");

    }


}