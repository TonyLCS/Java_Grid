package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.Filters;

import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerAction;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerClassifier;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerCondition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters.Filter;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters.FilterMessage;

public class LayerExecutionFilter extends Filter<LayerProblem, LayerCondition, LayerAction>
{

	public LayerExecutionFilter() {
		super("LayerModifyFilter Update");
	}

	@Override
	public FilterMessage<LayerCondition, LayerAction> apply(LayerProblem agent, int iterationCount, FilterMessage<LayerCondition, LayerAction> message) {
		printMessage("LayerExecutionFilter.java:: Execute Emotion", agent);
		LayerAction layerAction= message.getCurrentAction();

		// check if the selected action could be applied or not
		// call next layer? get filters of next layer?
		//agent.takeAction(layerAction);
		agent.executeModifier(layerAction);
		
		printMessage("LayerExecutionFilter.java:: 'takeAction' Update compelete", agent);
		return message;
	}

	@Override
	protected void printMessage(String message, LayerProblem agent)
	{
		String s = NAME +" "+message;
		agent.printMessage(s);
	}
}
