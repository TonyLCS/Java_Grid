package main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem;

import java.io.*;
import java.util.*;

import com.opencsv.CSVReader;
import com.sun.org.apache.xpath.internal.operations.Mod;
import com.thoughtworks.xstream.io.xml.DomDriver;
import javassist.bytecode.Descriptor;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.*;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;

import com.thoughtworks.xstream.XStream;


//import Matrix
import main.java.com.github.rosjava.my_pub_sub_tutorial.Jama.*;
import com.opencsv.CSVWriter;


public class LayerPopulation extends Population<LayerClassifier, LayerCondition, LayerAction>
{

	/**
	 *
	 */

	private static final long serialVersionUID = 1L;
	private HashMap<LayerAction, ClassifierExpectation<LayerAction>> modifiers_expectations = new HashMap<LayerAction, ClassifierExpectation<LayerAction>>();
	private HashMap<Integer, Modifier> 		modifiers 		= new HashMap<Integer, Modifier>();
	protected int IDFromGenesis=0;
	protected int lastGenesis=0;
	protected Reinforcer ScopeReinforcer;
	protected LayerCondition conditionBoundary;


	public String agentFileName;
	public int the_diversity_of_actions;  // how many actions? preset parameter


	private HashMap<LayerAction, ArrayList<Integer>> expected_ids = new HashMap<LayerAction, ArrayList<Integer>>();
	private ArrayList<HashMap<LayerAction, Integer>> bowTieSet= new ArrayList<HashMap<LayerAction, Integer>>();

	private HashMap<Integer, Integer> infection =new HashMap<Integer, Integer>();//double is the number of parasite case and the number of host case
	private HashMap<HashMap<Integer, Integer>, Double> reward_map= new HashMap<HashMap<Integer, Integer>, Double>();// key is the infection, value is its predict reward.
	private double rewardAssign_buff_num1=0;
	private double rewardAssign_buff_num2=0;


	protected int MAX_NUM_MODIFIERS = 11;

	public HashMap<LayerAction, Double> anticipated_secheduled_reward_hashmap = new HashMap<LayerAction, Double>();

	LinkedList<Double> prd_distribution_vector_point= new LinkedList<Double>();
	LinkedList<Double> prd_distribution_vector_value= new LinkedList<Double>();

	public LayerPopulation()
	{
		//super();

		rewardAssign_buff_num1=0;
		rewardAssign_buff_num2=0;

	}

	public void setInfection(int parasiteNum, int hostNum)
	{
		this.infection.put(1, hostNum);
		this.infection.put(2, parasiteNum);

	}
	public void setInfectionRewardMap(int parasiteNum, int hostNum, double reward)
	{
		HashMap<Integer, Integer> a_infection=new HashMap<Integer, Integer>();
		a_infection.put(1, hostNum);
		a_infection.put(2, parasiteNum);
		double savedReward=0;

		if(reward_map.containsKey(a_infection))
		{
			savedReward= reward_map.get(a_infection);
		}

		savedReward = savedReward+ 0.2*(reward-savedReward);
		reward_map.put(a_infection, savedReward);
	}

	public void calculateRewardMatrix( ) //demo for the parasite and host classifiers
	{
		//parasite and host problem
		//System.out.println(" >>>>>>>>>>>>>>>length_of_matrix ("+length_of_matrix+")");
		/**
		 * keyset
		 * **/
		double sum_num_1=0;
		double sum_num_2=0;
		double sum_reward=0;

		double line_num1=0;
		double line_num2=0;
		double line_reward=0;

		double length_of_matrix= reward_map.size();
		double line= Math.ceil(Math.random()*length_of_matrix);
		double line_temp=1;
		if(length_of_matrix>=3)
		{
			for(HashMap<Integer, Integer> a_case: reward_map.keySet())
			{
				sum_num_1=sum_num_1+a_case.get(1);
				sum_num_2=sum_num_2+a_case.get(2);

				if(line_temp== line)
				{
					line_num1=a_case.get(1);
					line_num2=a_case.get(2);
				}
				line_temp++;
			}

			line_temp=1;
			for(Map.Entry<HashMap<Integer, Integer>, Double>a_reward_map: reward_map.entrySet())
			{
				sum_reward=sum_reward+a_reward_map.getValue();

				if(line_temp== line)
				{
					line_reward=a_reward_map.getValue();
				}
				line_temp++;
			}

			System.out.println("sum_num_1("+sum_num_1+"), sum_num_2("+sum_num_2+"), sum_reward("+sum_reward+"), sum_reward/sum_num_1("+(sum_reward/sum_num_1)+")");

			/**
			 * get another line for calculation
			 * **/

			double rewardAssign_num1=0;
			double rewardAssign_num2=0;

			rewardAssign_num2=((sum_reward/sum_num_1)-(line_reward/line_num1))/((sum_num_2/sum_num_1)-(line_num2/line_num1));
			rewardAssign_num1=(sum_reward/sum_num_1)-rewardAssign_num2*(sum_num_2/sum_num_1);

			/**
			 * calculate the host reward and the parasite reward
			 * **/

			//parasite and host problem
			rewardAssign_buff_num1=rewardAssign_buff_num1+0.2*(rewardAssign_num1-rewardAssign_buff_num1);
			rewardAssign_buff_num2=rewardAssign_buff_num2+0.2*(rewardAssign_num2-rewardAssign_buff_num2);

			System.out.println("\n");

			System.out.println("rewardAssign_buff_num1("+rewardAssign_buff_num1+"), rewardAssign_buff_num2("+rewardAssign_buff_num2+")" +
					" VS rewardAssign_num1("+rewardAssign_num1+"), rewardAssign_num2("+rewardAssign_num2+")");

		}

	}


	public double getRewardAssigned(String whichOne)
	{
		if(whichOne.equals("one"))
		{return this.rewardAssign_buff_num1;}
		if(whichOne.equals("two"))
		{return this.rewardAssign_buff_num2;}
		else return 500;
	}

	public void effectModifier(LayerAction layerAction, boolean exploit)
	{

		ArrayList<Integer> ids = new ArrayList<Integer>(modifiers.keySet());
		if(!modifiers_expectations.containsKey(layerAction))
			modifiers_expectations.put(layerAction, new ClassifierExpectation<LayerAction>(layerAction, ids));
		else
			modifiers_expectations.get(layerAction).updateIDs(ids);// put 0.5 as modifier's fitness value(classifierFitness) if this value is empty.
		System.out.println("effectModifier:: modifiers.keySet()='"+ids+"'<--, to exploit or explore= exploit='"+exploit+"'<--true/false 50%.");
		System.out.println("effectModifier:: layerAction='"+layerAction+"'; show mod.stat:" );
		modifiers_expectations.get(layerAction).showModStat();


		int id = 0;
		if(exploit)
		{
			//id = modifiers_expectations.get(emotion).getPrediction();
			//TODO:Tony: choose the most fitness modifier for an action
			id = modifiers_expectations.get(layerAction).getBestModifier();
		}
		else
			id = (int)(Math.random() * ids.size());
		//double debug_modifier_fitness= this.modifiers_expectations.get(emotion).getOneClassifierFitness(id);
		//System.out.println("effectModifier:: choose modifier from modifiers_set. the cho0sed modifier's fitness is '"+debug_modifier_fitness+"'  BOW-TIE (emotion-modifier)=('"+emotion+"'-'"+id+"')==> bowtie::emotion='"+emotion+"'----bowtie:Modifier='"+id+"', modifier.size= '"+modifiers.size()+"' ");

		double debug_modifier_fitness= this.modifiers_expectations.get(layerAction).getOneModifierFitness(id);
		System.out.println("effectModifier:: NEW!!! choose modifier from modifiers_set. the cho0sed modifier's fitness is '"+debug_modifier_fitness+"'  BOW-TIE (layerAction-modifier)=('"+layerAction+"'-'"+id+"')==> bowtie::layerAction='"+layerAction+"'----bowtie:Modifier='"+id+"', modifier.size= '"+modifiers.size()+"' ");

		if(id != -1)
		{
			if(!this.expected_ids.containsKey(layerAction))
				this.expected_ids.put(layerAction, new ArrayList<Integer>());
			this.expected_ids.get(layerAction).add(id);// add mod.id into bow-tie list !

			modifiers.get(id).applyModifier();

			//TODO:Tony add new bowTie
			HashMap<LayerAction, Integer> bowTie= new HashMap<LayerAction, Integer>();
			bowTie.put(layerAction,id);
			bowTieSet.add(bowTie);// establish new bowtie to store the mapping between emotion and modifier

		}
		/*
		{
			//show old bow_tie
			int listLength = 0;
			listLength = this.expected_ids.get(layerAction).size();
			System.out.println("================print old bowTie layerAction('"+layerAction+"') list (size ='"+listLength+"'):=====dim of this.expected_ids (bow-tie) ='"+this.expected_ids.size()+"'===============");
			for (Integer j:this.expected_ids.get(layerAction) )
				{System.out.println("=======show old bowTie set='"+j+"'<----this modifier id is in the bowtie list=====");}

		}*/
		{
			//show new bowTie

			//Iterator<LayerAction,Integer> itr= this.bowTieSet.iterator();
			System.out.println("		bowTie.size='"+this.bowTieSet.size()+"';");
			//Map.Entry<Emotion, Integer> a_bowTie= new HashMap<Emotion, Integer>();
			for(int j=0; j< this.bowTieSet.size(); j++)
			{
				HashMap<LayerAction, Integer> a_bowTie = this.bowTieSet.get(j);
				//Map.Entry<Emotion, Integer> entry= a_bowTie.entrySet().get(0);
				for(Map.Entry<LayerAction, Integer> entry: a_bowTie.entrySet())
				{
					System.out.println("		=======show new bowTie set("+(j+1)+"/"+this.bowTieSet.size()+"):" +
							"	 layerAction('"+entry.getKey()+"') ==" +
							"	 modifier.id('"+entry.getValue()+"')");
				}
			}

		}

	}
	public void executeModifier(LayerAction layerAction)
	{

		/*
			0. setup modifier in modifiers(set)
			1. translate layerAction into corresponding modifiers.ID
			2. apply corresponding modifier
			3. record into bow-tie structure
		*/

		//0. setup modifier in modifiers(set)
		// directly link modifier on emotion!!! change
		//initialize modifier Set
		int modID= layerAction.getMask();
		if(!modifiers.containsKey(modID))
		{
			Modifier modifier = new Modifier(modID);//create a modifier with the same ID with action
			System.out.println("	!!!		establish modifier form layerAction("+layerAction+"),layerAction's mask("+modID+")--> set  new modifier.id("+modifier.ID+")   ");
			//Modifier modifier = new Modifier();
			if(modifiers.values().size() < MAX_NUM_MODIFIERS)
			{
				modifiers.put(modID, modifier);
			}
		}


		//1. translate layerAction into corresponding modifiers.ID
		int id = layerAction.getMask();
		System.out.println("executeModifier:: !!! Directly apply layerAction("+layerAction+") --id("+id+")");

		//double debug_modifier_fitness= this.modifiers_expectations.get(layerAction).getOneModifierFitness(id);
		//System.out.println("effectModifier:: NEW!!! choose modifier from modifiers_set. the cho0sed modifier's fitness is '"+debug_modifier_fitness+"'  BOW-TIE (layerAction-modifier)=('"+layerAction+"'-'"+id+"')==> bowtie::layerAction='"+layerAction+"'----bowtie:Modifier='"+id+"', modifier.size= '"+modifiers.size()+"' ");


			/* 	Tony: no apply expected_ids anymore. the bow-tie will store in bowTieSet.*/
		if(!this.expected_ids.containsKey(layerAction))
			this.expected_ids.put(layerAction, new ArrayList<Integer>());
		this.expected_ids.get(layerAction).add(id);// add mod.id into bow-tie list !


		//2. apply corresponding modifier

		modifiers.get(id).applyModifier();

		//TODO:Tony add new bowTie
		//3. record into bow-tie structure

		HashMap<LayerAction, Integer> bowTie= new HashMap<LayerAction, Integer>();
		bowTie.put(layerAction,id);
		bowTieSet.add(bowTie);// establish new bowtie to store the mapping between emotion and modifier



		{
			//show new bowTie
			System.out.println("		bowTie.size='"+this.bowTieSet.size()+"';");
			for(int j=0; j< this.bowTieSet.size(); j++)
			{
				HashMap<LayerAction, Integer> a_bowTie = this.bowTieSet.get(j);
				for(Map.Entry<LayerAction, Integer> entry: a_bowTie.entrySet())
				{
					System.out.println("		=======show new bowTie set("+(j+1)+"/"+this.bowTieSet.size()+"):" +
							"	 layerAction('"+entry.getKey()+"') ==" +
							"	 modifier.id('"+entry.getValue()+"')");
				}
			}

		}

	}

	public Modifier getModifier(LayerAction layerAction)
	{
		int id = modifiers_expectations.get(layerAction).getPrediction();
		return modifiers.get(id);
	}

	public double getModifierFitness(LayerAction layerAction)
	{
		return modifiers_expectations.get(layerAction).getExpectationFitness();
	}

	public void updateExpectations(LayerAction layerAction, double reward)
	{
		System.out.println("update expectation part4 <----modifiers_expectation->classifierFitness, layerAction="+layerAction+"'. stack of bow-tie set is:'"+this.expected_ids.get(layerAction)+"'.");
		this.modifiers_expectations.get(layerAction).updatePredictionFitness(this.expected_ids.get(layerAction));

		//TODO:Tony: call new mod.fit update method here.
		// TODO:Tony: this method will contains statistics which will replace those fits in "modifiers_expectations"

		////update part 1: input rwd, bowTieSet, according to different emotion and account
		for(int i= 0; i< this.bowTieSet.size();i++)
		{
			//extract layerAction, mod.id, count from bowTieSet
			HashMap<LayerAction, Integer> a_bowTie = bowTieSet.get(i);

			for(Map.Entry<LayerAction, Integer> entry: a_bowTie.entrySet())
			{
				LayerAction a_layerAction			=	entry.getKey();
				Integer a_modifierID		=	entry.getValue();

				int discount = (i+1);

				System.out.println("			BOWTIE 1 :: to update modifier statistics :: pass discount='"+i+"';" +
						"	reward ('"+reward+"')" +
						"	 layerAction('"+entry.getKey()+"') ==" +
						"	 modifier.id('"+entry.getValue()+"')" +
						" to update in modifiers_expectations structure(classifierExpectation.java)");
				//update part 1: rwd,err,exp,niche
				this.modifiers_expectations.get(a_layerAction).updateModMappingStat1(a_modifierID,reward, discount);

			}
		}
		////update part 2: sum of acc
		double sum_bowTieSet_acc=0;
		for(int i= 0; i< this.bowTieSet.size();i++)
		{
			//extract acc from modifier_mapping
			double a_modifier_acc=0;
			HashMap<LayerAction, Integer> a_bowTie = bowTieSet.get(i);


			for(Map.Entry<LayerAction, Integer> entry: a_bowTie.entrySet())
			{
				LayerAction a_layerAction			=	entry.getKey();
				Integer a_modifierID		=	entry.getValue();
				a_modifier_acc=this.modifiers_expectations.get(a_layerAction).get_a_modifier_acc(a_modifierID);
				System.out.println("			BOWTIE 2.1 :: a_modifier_acc='"+a_modifier_acc+"'.");
			}
			sum_bowTieSet_acc += a_modifier_acc;


		}
		System.out.println("			BOWTIE 2.1 :: sum_bowTieSet_acc='"+sum_bowTieSet_acc+"'.");

		///update part 3: fitness
		for(int i= 0; i< this.bowTieSet.size();i++)
		{
			//extract acc from modifier_mapping
			double a_modifier_acc=0;
			HashMap<LayerAction, Integer> a_bowTie = bowTieSet.get(i);


			for(Map.Entry<LayerAction, Integer> entry: a_bowTie.entrySet())
			{
				LayerAction a_layerAction			=	entry.getKey();
				Integer a_modifierID		=	entry.getValue();

				this.modifiers_expectations.get(a_layerAction).updateModMappingStat2(a_modifierID,sum_bowTieSet_acc);

			}


		}

		// remove all the bowTieSet
		clearBowTieSet();

		this.expected_ids.put(layerAction, new ArrayList<Integer>());
	}
	public void clearBowTieSet()
	{
		// remove all the bowTieSet
		this.bowTieSet.clear();

		//this.expected_ids.put(layerAction, new ArrayList<Integer>());
	}


	@Override
	protected LayerClassifier createClassifier(LayerCondition currentState) {
		/* the modifier will be established in LayerExecutionFilter.java
		Modifier modifier = new Modifier();

		if(modifiers.values().size() < MAX_NUM_MODIFIERS)
			modifiers.put(modifier.ID, modifier);
		 */
		//int id =555;
		return new LayerClassifier(currentState);
	}

	@Override
	protected LayerClassifier createClassifier(int id, LayerCondition currentState, LayerAction action) { //todo: add statisitcs
		/* the modifier will be established in LayerExecutionFilter.java
		Modifier modifier = new Modifier();

		if(modifiers.values().size() < MAX_NUM_MODIFIERS)
			modifiers.put(modifier.ID, modifier);
		 */
		return new LayerClassifier(id, currentState, action); //todo: add statisitics
	}

	@Override
	protected LayerClassifier createClassifier(LayerCondition currentState,int actionMaskID) {
		/*
		Modifier modifier = new Modifier();
		if(modifiers.values().size() < MAX_NUM_MODIFIERS)
			modifiers.put(modifier.ID, modifier);
		*/
		//Only create a Michigan style classifier
		return new LayerClassifier(currentState,actionMaskID);
	}

	@Override
	protected LayerClassifier createClassifier(LayerCondition currentState,LayerAction layerAction) {
		//TODO:Change this into Michgan style!!! change
		//for test
		// directly link modifier on emotion!!! change
		//initialize modifier Set
		/* move this part to executeModifier()
		int modID= layerAction.getMask();
		if(!modifiers.containsKey(modID))
		{
			Modifier modifier = new Modifier(modID);//create a modifier with the same ID with action
			System.out.println(" establish modifier form layerAction("+layerAction+"),layerAction's mask("+modID+")--> set  new modifier.id("+modifier.ID+")   ");
			//Modifier modifier = new Modifier();
			if(modifiers.values().size() < MAX_NUM_MODIFIERS)
			{
				modifiers.put(modID, modifier);
			}
		}
		*/

		//Only create a Michigan style classifier
		return new LayerClassifier(currentState,layerAction);
	}

	public ArrayList<Integer> calculate_Mitosis_set(ArrayList<Integer> classifiers_ids,  double reward)
	{

		ArrayList<Integer> mitosis_ids = new ArrayList<Integer>();
		ArrayList<LayerClassifier> classifiers = getClassifiers(classifiers_ids);
		//ArrayList<LayerClassifier> accSet = new  ArrayList<LayerClassifier>();
		//ArrayList<LayerClassifier> accSet_2 = new  ArrayList<LayerClassifier>();
		//ArrayList<LayerClassifier> accSet_3 = new  ArrayList<LayerClassifier>();

		for(LayerClassifier c : classifiers)
		{
			if(c.getAccuracy()> 0.99)
			{
				//accSet.add(c);
				if(c.getPredictedReward()-reward > 10)
				{
					//accSet_2.add(c);
					mitosis_ids.add(c.getID());
				}
				if(c.getPredictedReward()-reward <= 10)
				{
					//accSet_3.add(c);
				}
			}

		}

		//check the size of accSet
		System.out.println("		calculate_Mitosis_set :  actionSet.size("+classifiers.size()+")," +
				" mitosis_ids.size("+mitosis_ids.size()+").");
		/*
		if(accSet_2.size()>0)
		{
			for(LayerClassifier c : accSet_2)
			{

				insertIntoPopulationDirectly(c.doMitosis());
			}

		}
		*/
		return mitosis_ids;
	}

	public void update(ArrayList<Integer> classifiers_ids,  double reward)
	{
		ArrayList<LayerClassifier> classifiers = getClassifiers(classifiers_ids);

		double sum_relative_accuracy = 0;
		double sum_num_for_niche =0;
		for(LayerClassifier c : classifiers)
		{
			sum_num_for_niche += c.getNumerosity();
		}
		for(LayerClassifier c : classifiers)
		{
			//System.out.println("	cl.id("+c.getID()+") update 4 statistics:");
			c.updateSet(reward, sum_num_for_niche);
			// replaced by the line above: c.updateSet(reward, classifiers.size());
			sum_relative_accuracy += c.getAccuracy()*c.getNumerosity();
		}

		for(LayerClassifier c : classifiers)
		{
			//System.out.println("	cl.id("+c.getID()+") condition("+c.showCondition()+") updateFitness:");
			c.updateFitness(sum_relative_accuracy);
		}
	}

	public void update_learning_rate(ArrayList<Integer> classifiers_ids, double reward,double learning_rate)
	{
		ArrayList<LayerClassifier> classifiers = getClassifiers(classifiers_ids);

		double sum_relative_accuracy = 0;
		double sum_num_for_niche =0;
		for(LayerClassifier c : classifiers)
		{
			sum_num_for_niche += c.getNumerosity();
		}
		for(LayerClassifier c : classifiers)
		{
			//System.out.println("	cl.id("+c.getID()+") update 4 statistics:");
			c.updateSet_learning_rate(reward, sum_num_for_niche, learning_rate);
			// replaced by the line above: c.updateSet(reward, classifiers.size());
			sum_relative_accuracy += c.getAccuracy()*c.getNumerosity();
		}

		for(LayerClassifier c : classifiers)
		{
			//System.out.println("	cl.id("+c.getID()+") condition("+c.showCondition()+") updateFitness:");
			c.updateFitness(sum_relative_accuracy);
		}
	}

	public void update_incremental_reward(ArrayList<Integer> classifiers_ids,  double incremental_reward)
	{
		ArrayList<LayerClassifier> classifiers = getClassifiers(classifiers_ids);

		double sum_relative_accuracy = 0;
		double sum_num_for_niche =0;
		for(LayerClassifier c : classifiers)
		{
			sum_num_for_niche += c.getNumerosity();
		}
		for(LayerClassifier c : classifiers)
		{
			//System.out.println("	cl.id("+c.getID()+") update 4 statistics:");
			c.updateSet_incremental(incremental_reward, sum_num_for_niche);
			// replaced by the line above: c.updateSet(reward, classifiers.size());
			sum_relative_accuracy += c.getAccuracy()*c.getNumerosity();
		}

		for(LayerClassifier c : classifiers)
		{
			//System.out.println("	cl.id("+c.getID()+") condition("+c.showCondition()+") updateFitness:");
			c.updateFitness(sum_relative_accuracy);
		}
	}



	public double getMaxFitInActionSet(ArrayList<Integer> classifiers_ids)
	{
		ArrayList<LayerClassifier> classifiers = getClassifiers(classifiers_ids);
		double MaxFitness=0;
		for(LayerClassifier c : classifiers)
		{
			if(c.getFitness()>= MaxFitness)
			{
				MaxFitness= c.getFitness();
			}
		}
		return MaxFitness;
	}

	public double getMaxFitXPrdInActionSet(ArrayList<Integer> classifiers_ids)
	{
		/**return maximun of  fitness times reward -> fitXprd**/
		ArrayList<LayerClassifier> classifiers = getClassifiers(classifiers_ids);

		double MaxFitXPrd=0;
		for(LayerClassifier c : classifiers)
		{
			if(c.getFitness()>= MaxFitXPrd)
			{
				MaxFitXPrd= c.getFitness()*c.getPredictedReward();
			}
		}
		return MaxFitXPrd;
	}
	public double getMaxFitWithPrdInActionSet(ArrayList<Integer> classifiers_ids)
	{
		/**return maximun of  fitness times reward -> fitXprd**/
		ArrayList<LayerClassifier> classifiers = getClassifiers(classifiers_ids);

		double MaxFitXPrd=0;
		double MaxFit=0;
		for(LayerClassifier c : classifiers)
		{
			if(c.getFitness()>= MaxFit)
			{
				MaxFit=c.getFitness();
				MaxFitXPrd= c.getFitness()*c.getPredictedReward();
			}
		}
		return MaxFitXPrd;
	}

	@Override
	public LayerAction getNextAction(LayerCondition condition, ArrayList<Integer> matchSet, ArrayList<Integer> actionSet, boolean exploit)
	{
		//Martin
		ArrayList<LayerClassifier> match_set = getClassifiers(matchSet);
		LayerAction action = LayerAction.empty;

		System.out.println(" getNextAction (without anticipated reward) ");

		if(exploit)
		{

			//action=exploitOneBestAction(match_set);// a single action,select action according to Maximun of (fit*prd)
			//action =exploitPAVote(match_set);//Tony's voting method,select action according to Maximun of (fit)
			action =exploit_fitXprd_Vote(match_set);//Martin's vote
			//action = exploit(match_set);
			System.out.println("	1. advocate action 1::exploit mode, select action("+action+")");

		}
		else
		{
			action = exploreLayerActions(match_set);
			//action = explore(match_set);
			System.out.println("	1. advocate action ::explore mode, select action("+action+")");
		}



		//Tony's standard Michgan method to establish action_set
		for(LayerClassifier c: match_set)
		{
			if(c.getLayerClassifierAction().equals(action))
			{
				actionSet.add(c.ID);
				//debug work//	System.out.println("	getNextAction and establish actionSet 2 : Put classifier.ID="+c.ID+"'<- in action_set, " + "which vote for action("+action+")");
			}
		}
		System.out.println("	2 generate ActionSet : size of action_set is "+actionSet.size()+"");

		// save anticipated_reward_hashmap
		//this.anticipated_reward_hashmap= anticipated_reward_hashmap;
		return action;
	}

	public void getActionSetFromChosenAction(ArrayList<Integer> matchSet, ArrayList<Integer> actionSet, LayerAction chosen_action)
	{
		ArrayList<LayerClassifier> match_set = getClassifiers(matchSet);

		for(LayerClassifier c: match_set)
		{
			if(c.getLayerClassifierAction().equals(chosen_action))
			{
				actionSet.add(c.ID);
				//debug work//
				System.out.println("	getActionSetFromChosenAction : Put classifier.ID="+c.ID+"'<- in action_set, " + "which vote for action("+chosen_action+")");
			}
		}
	}

	public HashMap<LayerAction, Double> get_actions_reward_hashmap(ArrayList<Integer> match_set)
	{

		HashMap<LayerAction, FitnessCount> votes = new HashMap<LayerAction, FitnessCount>();
		for(LayerClassifier c : getClassifiers(match_set))
		{
			LayerAction classifierAction= c.getLayerClassifierAction();
			if(!votes.containsKey(classifierAction))
			{
				votes.put(classifierAction, new FitnessCount(c.getFitness(),c.getPredictedReward()));
			}
			else
			{
				votes.get(classifierAction).addFitness(c.getFitness(), c.getPredictedReward());
			}
		}

		/***
		 * put vote into actions_reward_hashmap
		 * **/
		ArrayList<Map.Entry<LayerAction, FitnessCount>> entries = new ArrayList<Map.Entry<LayerAction, FitnessCount>>(votes.entrySet());

		HashMap<LayerAction, Double> actions_reward_hashmap = new HashMap<LayerAction, Double>();

		if(entries.size()>1)
		{
			for (int i = 0; i < entries.size(); i++) {
				//System.out.println("			(" + i + "/" + entries.size() + ") " + "action ('" + entries.get(i).getKey() + "').vote('" + entries.get(i).getValue().getVote() + "').");

				actions_reward_hashmap.put(entries.get(i).getKey(), entries.get(i).getValue().getVote());
			}
		}
		return actions_reward_hashmap;
	}

	@Override
	public LayerAction getNextAction(LayerCondition condition, ArrayList<Integer> matchSet, ArrayList<Integer> actionSet, boolean exploit, ArrayList<Double> anticipated_reward) {

		ArrayList<LayerClassifier> match_set = getClassifiers(matchSet);
		LayerAction action = LayerAction.empty;
		//System.out.println(" getNextAction and establish actionSet 1::begin exploit and explore mode");

//		if(exploit)
//		{
//
//
//			//this part is replaced by new exploit method below/
//			/*/
//			action=exploitOneBestAction(match_set);// a single action,select action according to Maximun of (fit*prd)
//
//			//action =exploitPAVote(match_set);//Tony's voting method,select action according to Maximun of (fit)
//			//action = exploit(match_set);
//			System.out.println("	getNextAction and establish actionSet 1::exploit mode, select action("+action+")");
//			*/
//		}
//		else
//		{
//			// the exploreLayerActions will randomly select an existing action,
//			// which belong to existed classifier/classifiers that have existed in current match_set.
//			// the new exploreLayerActions will search "LayerAction" (data niche) of classifiers,
//			// while the original explore will search "LayerActions" of classifiers.
//			// the different is caused by different data structure of classifier,
//			// and my data structure is based on Michigan Style.
//
//			action = exploreLayerActions(match_set);
//			//action = explore(match_set);
//			System.out.println("	getNextAction and establish actionSet 1::explore mode, select action("+action+")");
//		}


		/**
		 * new exploit
		 * **/
		/* this might cause prediction accuracy divergence, especially when the reward is vary...*/
		HashMap<LayerAction, Double> anticipated_reward_hashmap = new HashMap<LayerAction, Double>();
		//calculate the anticipated_reward for each action according to the best fitness classifiers of each action
		anticipated_reward_hashmap=calculate_anticipatedReward(match_set);
		double best_reward = -100000;


		if(exploit)
		{
			/****
			 *	does not do summary of (fit * prd)
			 *****/
			// scale_prd= prd*fit/ sum_fit
			action = exploit_scaled_prd_fitXprd(match_set);
			//action=getBestActionAccordingToMaxFitness(anticipated_reward_hashmap);
			System.out.println("	>>>>>> new exploit method :  getBestActionAccordingToMaxFitness: select action("+action+") with reward("+anticipated_reward_hashmap.get(action)+")");
			/************
			 * take the action of the most fitness classifier
			 * *****************/
			/*
			for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
			{
				if(a_anticipated_reward_map.getValue() > best_reward)
				{
					best_reward = a_anticipated_reward_map.getValue();
					action = a_anticipated_reward_map.getKey();
				}
				//System.out.println("		(Accuracy-based) calcuate anticipated reward :\taction("+a_anticipated_reward_map.getKey()+")" + "\t- reward("+a_anticipated_reward_map.getValue()+") ");
			}
			*/
		}
		else
		{
			// the exploreLayerActions will randomly select an existing action,
			// which belong to existed classifier/classifiers that have existed in current match_set.
			// the new exploreLayerActions will search "LayerAction" (data niche) of classifiers,
			// while the original explore will search "LayerActions" of classifiers.
			// the different is caused by different data structure of classifier,
			// and my data structure is based on Michigan Style.

			action = exploreLayerActions(match_set);
			//action = explore(match_set);
			System.out.println("	getNextAction and establish actionSet 1::explore mode, select action("+action+")");
		}



		anticipated_reward.add(anticipated_reward_hashmap.get(action));

		System.out.println("	>>>>>> new exploit/explore method :  getBestActionAccordingToMaxFitness: select action("+action+") with reward("+anticipated_reward+")");



		//System.out.println(" getNextAction and establish actionSet 2:: finish exploit and explore mode." + "choose next action:'"+action+"'.");


		//this part is establishing action_set according to the action it select.
		// Because Tony's layer is supposed to be standard Michgan style, it will be much easier to establish action_set.
		//and comment the following lines which are design for Henry's bow-tie structure.

		/* Henry's method
		ArrayList<LayerAction> viable_actions = LayerAction.getLayerAction();
		// establish the actionSet from the matchSet
		for(LayerClassifier c : match_set)
		{
			System.out.println(" getNextAction and establish actionSet 3: classifier.ID="+c.ID+"'<- is in match_set");
			if (c.getBestAction(viable_actions).equals(action)) // this line is for Henry's bow-tie structure.
			{
				actionSet.add(c.ID);
				System.out.println(" getNextAction and establish actionSet 4: Put classifier.ID="+c.ID+"'<- in action_set");

			}
		}
		*/

		//Tony's standard Michgan method to establish action_set
		for(LayerClassifier c: match_set)
		{
			if(c.getLayerClassifierAction().equals(action))
			{
				actionSet.add(c.ID);
				//debug work//	System.out.println("	getNextAction and establish actionSet 2 : Put classifier.ID="+c.ID+"'<- in action_set, " + "which vote for action("+action+")");
			}
		}
		System.out.println("	getNextAction and establish actionSet 3 : size of action_set is "+actionSet.size()+"");

		// save anticipated_reward_hashmap
		//this.anticipated_reward_hashmap= anticipated_reward_hashmap;
		return action;
	}


	public HashMap<LayerAction, Double> get_anticipated_reward_hashmap(ArrayList<Integer> matchSet)
	{
		ArrayList<LayerClassifier> match_set = getClassifiers(matchSet);
		HashMap<LayerAction, Double> anticipated_reward_hashmap = new HashMap<LayerAction, Double>();
		anticipated_reward_hashmap=calculate_anticipatedReward2(match_set);
		return anticipated_reward_hashmap;
	}


	public HashMap<LayerAction, LayerClassifier> get_instance_classifier_hashmap(ArrayList<Integer> matchSet)
	{
		ArrayList<LayerClassifier> match_set = getClassifiers(matchSet);
		HashMap<LayerAction, LayerClassifier> instance_classifier_hashmap = new HashMap<LayerAction, LayerClassifier>();
		instance_classifier_hashmap = calculate_niche_instance_classifier(match_set);
		return instance_classifier_hashmap;
	}

	public HashMap<Integer, Matrix> calculate_niche_strength_distribution(ArrayList<Integer> match_niche_set)
	{
		LayerCondition a_condition=null;
		ArrayList<LayerClassifier> match_set = getClassifiers(match_niche_set);
		//return a niche strength distribution
		//sum_calculate_niche_strength_distribution(match_set);
		HashMap<Integer, Matrix> modifier_niches_set=sum_calculate_niche_strength_distribution_matrix(match_set);
		return modifier_niches_set;
	}

	public HashMap<Integer, Modifier_ContinuouslyProjected> calculate_niche_strength_distribution_continuous(ArrayList<Integer> match_niche_set)
	{
		LayerCondition a_condition=null;
		ArrayList<LayerClassifier> match_set = getClassifiers(match_niche_set);
		//return a niche strength distribution
		//sum_calculate_niche_strength_distribution(match_set);

		HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches_set=sum_calculate_niche_strength_distribution_matrix_continuous(match_set);
		return modifier_niches_set;
	}

	public HashMap<Integer, Modifier_ContinuouslyProjected> transform_modifier_niches_set_into_matrix_inline(HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches)
	{

		int how_many_modifier= modifier_niches.size();
		//
		for(int i=0; i< how_many_modifier; i++)
		{
			Modifier_ContinuouslyProjected a_modifier= modifier_niches.get(i);
			//todo: transform into inline method
			a_modifier.translate_attributes_into_matrix();
			a_modifier.show_attributes();
		}

 		return  modifier_niches;
	}


	public void save_modifiers(String s, HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches)
	{
		for(int i =0; i<modifier_niches.size();i++)
		{
			Modifier_ContinuouslyProjected a_modifier= modifier_niches.get(i);
			a_modifier.save_niche_distributions(s);
		}

	}

	public HashMap<Integer, Matrix>  transform_modifier_niches_set_into_matrix(HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches)
	{
		int max_length_of_attr=0;
		int total_att_num= 0;
		int how_many_modifier= modifier_niches.size();
		//
		for(int i=0; i< how_many_modifier; i++)
		{
			Modifier_ContinuouslyProjected a_modifier= modifier_niches.get(i);
			int how_many_attribute= a_modifier.get_size();

			for(int j=0; j< how_many_attribute;j++)
			{
				Modifier_Niche_Attribute_Continuous an_attribute= a_modifier.get_attribute(j);
				if(max_length_of_attr< an_attribute.prd_distribution_vector_point.size())
				{
					max_length_of_attr = an_attribute.prd_distribution_vector_point.size();
					total_att_num++;
				}
				System.out.println("an_attribute.prd_distribution_vector_point, size("+an_attribute.prd_distribution_vector_point.size()+"): "+an_attribute.prd_distribution_vector_point.toString());

			}
		}
		//initiate HashMap<Integer, Matrix>  modifier_niches_set
		HashMap<Integer, Matrix>  modifier_niches_set_o = new HashMap<Integer, Matrix>();
		for(int i=0; i< how_many_modifier; i++)
		{
			Modifier_ContinuouslyProjected a_modifier= modifier_niches.get(i);
			int how_many_attribute= a_modifier.get_size();
			Matrix pair_rows = new Matrix(2*how_many_attribute,max_length_of_attr);// contains all attributes, each attribute has two rows (a pair).
			modifier_niches_set_o.put(i,pair_rows);
		}

		//begin to transform
		for(int i=0; i< how_many_modifier; i++)
		{
			Modifier_ContinuouslyProjected a_modifier_i= modifier_niches.get(i);
			int how_many_attribute= a_modifier_i.get_size();
			Matrix a_modifier_o = modifier_niches_set_o.get(i);

			for(int j=0; j< how_many_attribute;j++)
			{
				LinkedList<Double>  a_vector_point = a_modifier_i.get_attribute(j).prd_distribution_vector_point;
				LinkedList<Double>  a_vector_value = a_modifier_i.get_attribute(j).prd_distribution_vector_value;

				Double[] loc_m= a_vector_point.toArray(new Double[a_vector_point.size()]);
				Double[] prd_m= a_vector_value.toArray(new Double[a_vector_value.size()]);
				//1. put into modifier matrix
				for(int col=0; col< loc_m.length; col++)
				{
					a_modifier_o.set(2*j,col, loc_m[col]);
				}
				for(int col=0; col< prd_m.length; col++)
				{
					a_modifier_o.set(2*j+1,col, prd_m[col]);
				}
				//2. show as string
				String loc_m_s="location_continuous	#Mod."+ i+"#Att." +j+"#"; // which modifier
				for(int i_row =0; i_row<loc_m.length;i_row++)
				{
					loc_m_s+= loc_m[i_row]+ "#";
				}
				String prd_m_s="niche_strength	#Mod." + i+"#Att."+j+"#";
				for(int i_row =0; i_row<prd_m.length;i_row++)
				{
					prd_m_s+= prd_m[i_row]+ "#";
				}
				System.out.println("\n>> loc_m ("+loc_m.length+"):"+ loc_m_s);
				System.out.println(">> prd_m ("+ prd_m.length+"):"+ prd_m_s);


			}//end of saving all attributes into a modifier
			modifier_niches_set_o.put(i, a_modifier_o);// save  NO.i modifier into modifiers niches set
		}

		return  modifier_niches_set_o;
	}

	public void select_a_discrete_value_for_modifier(HashMap<Integer, Matrix> modifier_set, String s)
	{
		int how_many_modifier= modifier_set.size();
		for(int i=0; i< how_many_modifier;i++)
		{
			Matrix a_modifier= modifier_set.get(i);
			select_best_discrete_value_for_a_modifier(a_modifier, s);
		}

	}
	@Override
	public void save_anticipated_reward_hashmap(int iteration, int instance_id)
	{
		String file = "check_anticipated_reward_hashmap";
		List<String[]> data;
		//1. read csv
		try
		{
			FileReader f= new FileReader("./Results/"+file+".csv");
			CSVReader csv_reader= new CSVReader(f);
			data= csv_reader.readAll();

			//2.add current data //transform to hashmap
			String data_to_add= Integer.toString(iteration)+ "#"+ Integer.toString(instance_id)+ "#"+ transform_anticipated_reward_hashmap(this.anticipated_secheduled_reward_hashmap);
			String[] s_data_to_add= data_to_add.split("#");
			data.add(s_data_to_add);
			//3. write csv Data

			try
			{
				System.out.println("CSV version:  the classifiers");

				CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/"+file+".csv"));
				csv_writer.writeAll(data);
				csv_reader.close();
				csv_writer.close();

			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}


	}

	public String transform_anticipated_reward_hashmap(HashMap<LayerAction, Double> anticipated_reward_hashmap)
	{
		String data_to_add ="";
		/*
		for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
		{


			System.out.println("				print 		 anticipated_reward_hashma :\taction("+a_anticipated_reward_map.getKey()+")" +
					"\t- reward("+a_anticipated_reward_map.getValue()+") " );

		}
		*/

		for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
		{
			data_to_add += a_anticipated_reward_map.getKey().toString();
			data_to_add +="#";
			data_to_add += a_anticipated_reward_map.getValue().toString();
			data_to_add +="#";
		}
		System.out.println("				print 		 anticipated_reward_hashmap"+data_to_add);

		return data_to_add;
	}

	private LayerAction explore(ArrayList<LayerClassifier> match_set)
	{
		LayerAction action = LayerAction.empty;
		int index = (int)(Math.random() * match_set.size());
		LayerClassifier random_c = match_set.get(index);
		action = random_c.getRandomAction();

		System.out.println(" selection action->explore()." +
				"choose random classifer id ='"+random_c.getID()+"';" +
				" and random action ='"+ action+ "';" +
				" from match_set.size()='"+match_set.size()+"'." );
		return action;
	}

	private LayerAction exploreLayerActions(ArrayList<LayerClassifier> match_set)
	{
		// this method will randomly select an action from all optional actions, disregard of vote.
		// the vote here keep the number of action explore in the match_set.
		// the vote, representing experience, is  different from exploit mode which represents fitness.
		// the value of the vote, the experience of a specific action, is just calculated for academic interesting (fun)
		//  and not real applied in algorithm yet.


		HashMap<LayerAction, FitnessCount> votes = new HashMap<LayerAction, FitnessCount>();

		//add all the cl.fit in matchSet
		for(LayerClassifier c : match_set)
		{

			//1. return action of this classifier (my definition of layer's action)
			LayerAction classifierAction= c.getLayerClassifierAction();
			//LayerAction a = c.getBestAction(LayerAction.getLayerAction());// Henry's method
			//2. calculate vote through FitnessCount class
			// input cl.fit cl.rwd cl.num
			//System.out.println("	exploreLayerActions::	active for the explore mode");
			if(!votes.containsKey(classifierAction))
			{
				votes.put(classifierAction, new FitnessCount(true));// this will initiall exploreCount=1;
			}
			else
			{
				votes.get(classifierAction).addExploreCount();
			}

		}

		//show votes
		for(Map.Entry<LayerAction, FitnessCount> entry: votes.entrySet()) {
			LayerAction a_layerAction = entry.getKey();
			int exploreCount= entry.getValue().getExploreCount();
			System.out.println("	exploreLayerActions::	action("+a_layerAction+")  count("+exploreCount+")	 in votes.size("+votes.size()+").");
		}

		//select a random layerAction

		int selectedRamdon= (int)Math.floor(Math.random()*votes.size());
		ArrayList<Map.Entry<LayerAction, FitnessCount>> entries = new ArrayList<Map.Entry<LayerAction, FitnessCount>>(votes.entrySet());

		LayerAction selectedLayerAction=entries.get(selectedRamdon).getKey();
		System.out.println("	exploreLayerActions::	selectedLayerAction("+selectedLayerAction+")  selectedRamdon("+selectedRamdon+")	");
		return selectedLayerAction;
	}

	private LayerAction exploit(ArrayList<LayerClassifier> match_set)
	{
		//"actions" is a hashMap which contains the calculation of each action.
		// the calculation is the vote which will pick the best action
		HashMap<LayerAction, FitnessCount> actions = new HashMap<LayerAction, FitnessCount>();



		//add all the cl.fit in matchSet
		for(LayerClassifier c : match_set)
		{
			//Henry use this line to get best action for the actionSet, which links to each classifier.
			//LayerAction a = c.getBestAction(LayerAction.getLayerAction());
			LayerAction a = c.getBestAction(LayerAction.getLayerActionSet(getLengthOfLayerActionSet()));//change for multiple agent.

			if(!actions.containsKey(a))
			{
				actions.put(a, new FitnessCount(c.getFitness()));
			}
			else
			{
				actions.get(a).addFitness(c.getFitness());
			}

		}

		//screen print the matchset
		int i=0;
		for(LayerClassifier c : match_set)
		{
			System.out.println("	in exploit mode for selecting action:: 1 ::" +
					" show match_set:("+i+"/" +match_set.size()+") "+
					" classifier.id in MatchSet='"+c.getID()+"'," +
					" classifier.action='"+c.getLayerAction()+"'"+
					" classifier's fitness ='"+c.getFitness()+"'**" );
			//" actions.fits:** "+c.getActions().ActionFitnesstoString()+"" +
			//"**" );
			i++;
		}
		i=0;

		//System.out.println("	in exploit mode for selecting action:: 1.2 :: end");


		ArrayList<Map.Entry<LayerAction, FitnessCount>> entries = new ArrayList<Map.Entry<LayerAction, FitnessCount>>(actions.entrySet());

		if(entries.size()>1)
		{
			System.out.println("\n");
			System.out.println("	in exploit mode for selecting action:: 2.1 :: entries.size('"+entries.size()+"') ");

			System.out.println("	in exploit mode for selecting action:: 2.2 ::" +
					"sum of fit of two actions :before sort: " +
					"action 1("+entries.get(0).getKey()+").fit("+entries.get(0).getValue().getFitness()+") VS " +
					"action 2("+entries.get(1).getKey()+").fit("+entries.get(1).getValue().getFitness()+") <--=========");

			Collections.sort(entries, new Comparator<Map.Entry<LayerAction, FitnessCount>>() {
				@Override
				public int compare(Map.Entry<LayerAction, FitnessCount> entry1, Map.Entry<LayerAction, FitnessCount> entry2)
				{

					//System.out.println("		in exploit mode for selecting action:: 1.3 ::" + "calculate two action of all the fitness of classifiers in MatchSet -> " + "entry1.getValue()='"+entry1.getValue().getFitness()+"' VS  " + "entry2.getValue()='"+entry2.getValue().getFitness()+"'.");

					double fitnessDiff= entry1.getValue().getFitness()-entry2.getValue().getFitness();//descend mode
					int fitFlag=0;
					if(Math.abs(fitnessDiff)<=0.000001)
					{

						if(Math.random() >= 0.5)
						{
							fitFlag=1;
						}else
						{
							fitFlag=-1;
						}
						return (fitFlag);
					}
					//TODO: ways define best action:1. greater fitness win
					//TODO: 2.including reward in this process.
					if(fitnessDiff>0.000001)
					{
						fitFlag=-1;// descent mode
					}
					if(fitnessDiff<-0.000001)
					{
						fitFlag=1;
					}

					return (fitFlag);

					//return entry2.getValue().compareTo(entry1.getValue());// descent order
				}
			});
			System.out.println("	in exploit mode for selecting action:: 2.2 :: end");

			System.out.println("	in exploit mode for selecting action:: 3 ::" +
					"sum of fit of two actions :after sort : " +
					"action 1("+entries.get(0).getKey()+").fit("+entries.get(0).getValue().getFitness()+") VS " +
					"action 2("+entries.get(1).getKey()+").fit("+entries.get(1).getValue().getFitness()+") <--=========");

			LayerAction action = entries.get(0).getKey();
			LayerAction action2 = entries.get(1).getKey();
			System.out.println("in exploit mode for selecting action:: 3 ::" +
					"best action is = '" + action+ "'VS other action ='"+action2+"'; " +
					"best action's fitness is = '"+ entries.get(0).getValue().getFitness()+"' vs others:'"+entries.get(1).getValue().getFitness()+"'.");

		}else{
			System.out.println("	in exploit mode for selecting action:: 2.1 :: entries.size('"+entries.size()+"')" +
					"<---1 only one action ");

		}

		LayerAction action = entries.get(0).getKey();
		return action;

//		return null;
	}

	private ArrayList<LayerClassifier> get_accurate_set(ArrayList<LayerClassifier> match_set)
	{
		//form a accurate set
		ArrayList<LayerClassifier> accurate_set = new ArrayList<LayerClassifier>();
		for(LayerClassifier c : match_set)
		{
			if(c.getAccuracy() == 1)
			{
				if(c.getPredictedReward()>=990 )
				{
					accurate_set.add(c);
				}
			}
		}
 		return accurate_set;
	}

	private LayerAction exploit_fitXprd_Vote(ArrayList<LayerClassifier> match_set)
	{
		//Martin Butz's method
		/*****
		 * vote= PA/FSA = sum(prd*fit)/ sum(fit)
		 * *******/

		HashMap<LayerAction, FitnessCount> votes = new HashMap<LayerAction, FitnessCount>();
		for(LayerClassifier c : match_set)
		{
			LayerAction classifierAction= c.getLayerClassifierAction();
			//2. calculate vote through FitnessCount class
			// input cl.fit cl.rwd ,PA= prd*fit FSA= fit, vote= PA/FSA
			if(!votes.containsKey(classifierAction))
			{
				votes.put(classifierAction, new FitnessCount(c.getFitness(),c.getPredictedReward()));
			}
			else
			{
				votes.get(classifierAction).addFitness(c.getFitness(), c.getPredictedReward());
			}
		}
		/*****
		 * this part bias to accurate set
		 * **/
		/*
		ArrayList<LayerClassifier> accurate_set = get_accurate_set(match_set);
		if (accurate_set.size()>=1)
		{
			System.out.println("get accurate set: length is "+accurate_set.size());
			//vote based on acc rules:
			for(LayerClassifier c : accurate_set)
			{

				LayerAction classifierAction= c.getLayerClassifierAction();

				if(!votes.containsKey(classifierAction))
				{
					votes.put(classifierAction, new FitnessCount(c.getFitness(),c.getPredictedReward()));
				}
				else
				{
					votes.get(classifierAction).addFitness(c.getFitness(), c.getPredictedReward());
				}
			}
		}
		else{

			System.out.println("not accurate_set yet! ");
			for(LayerClassifier c : match_set)
			{
				LayerAction classifierAction= c.getLayerClassifierAction();
				//2. calculate vote through FitnessCount class
				// input cl.fit cl.rwd ,PA= prd*fit FSA= fit, vote= PA/FSA
				if(!votes.containsKey(classifierAction))
				{
					votes.put(classifierAction, new FitnessCount(c.getFitness(),c.getPredictedReward()));
				}
				else
				{
					votes.get(classifierAction).addFitness(c.getFitness(), c.getPredictedReward());
				}
			}
		}
		*/
		/**
		 * get vote hashmap now, go to sort
		 * **/


		ArrayList<Map.Entry<LayerAction, FitnessCount>> entries = new ArrayList<Map.Entry<LayerAction, FitnessCount>>(votes.entrySet());

		if(entries.size()>1)
		{
			System.out.println("		exploit_fitXprd_Vote : in exploit mode for selecting action:: 2.1 :: entries.size('"+entries.size()+"') ");
			System.out.println("		exploit_fitXprd_Vote : in exploit mode for selecting action:: 2.2 :: votes :before sort: ");

			for(int i=0; i<entries.size(); i++)
			{
				//entries.get(i).getValue().getVote_PA_FSA();
				System.out.println("			("+i+"/"+entries.size()+") " +
						"action ('"+entries.get(i).getKey()+"').vote('"+entries.get(i).getValue().getVote()+"').");

			}
			//System.out.println("\n");
			/**
			 *  sort by vote_acc, which is acc*prd
			 * ******************/
			Collections.sort(entries, new Comparator<Map.Entry<LayerAction, FitnessCount>>() {
				@Override
				public int compare(Map.Entry<LayerAction, FitnessCount> entry1, Map.Entry<LayerAction, FitnessCount> entry2)
				{

					//System.out.println("in exploit mode for selecting action:: 1.3 ::" + "calculate two action of all the fitness of classifiers in MatchSet -> " + "entry1.getValue()='"+entry1.getValue().getFitness()+"' VS  " + "entry2.getValue()='"+entry2.getValue().getFitness()+"'.");

					//double fitnessDiff= entry1.getValue().getVote()-entry2.getValue().getVote();//descend mode//getVote!
					double VoteDiff= entry1.getValue().getVote()-entry2.getValue().getVote();//descend mode//getVote, Martin's method
					int fitFlag=0;
					if(Math.abs(VoteDiff)<=0.0000001)
					{
						if(Math.random() >= 0.5)
						{
							fitFlag=1;
						}else
						{
							fitFlag=-1;
						}
						return (fitFlag);
					}
					//TODO: ways define best action:1. greater fitness win
					//TODO: 2.including reward in this process.
					if(VoteDiff>0.000001)
					{
						fitFlag=-1;// descent mode
					}
					if(VoteDiff<-0.000001)
					{
						fitFlag=1;
					}

					return (fitFlag);

					//return entry2.getValue().compareTo(entry1.getValue());// descent order
				}
			});



			System.out.println("		exploit_fitXprd_Vote : in exploit mode for selecting action:: 2.3 :: votes :After sort: ");

			for(int i=0; i<entries.size(); i++)
			{
				System.out.println("			("+i+"/"+entries.size()+") " +
						"action ('"+entries.get(i).getKey()+"').vote('"+entries.get(i).getValue().getVote()+"').");
			}
			//System.out.println("\n");

			/*
			LayerAction action = entries.get(0).getKey();
			LayerAction action2 = entries.get(1).getKey();
			System.out.println("	in exploit mode for selecting action:: 3 ::" +
					"best action is = '" + action+ "'VS other action ='"+action2+"'; " +
					"best action's fitness is = '"+ entries.get(0).getValue().getFitness()+"' vs others:'"+entries.get(1).getValue().getFitness()+"'.");

			*/
		}else{
			System.out.println("		in exploit mode for selecting action:: 3 :: entries.size('"+entries.size()+"')" +
					"<---1 only one action advocated in MatchSet.");

		}

		LayerAction action = entries.get(0).getKey();
		return action;
	}
	private LayerAction exploitPAVote(ArrayList<LayerClassifier> match_set)
	{
		//"votes" is a hashMap which contains the calculation of each action.
		// the calculation is the vote which will pick the best action
		// the vote calculate  cl.fit*cl.rwd for each classifier.
		HashMap<LayerAction, FitnessCount> votes = new HashMap<LayerAction, FitnessCount>();



		//add all the cl.fit in matchSet
		for(LayerClassifier c : match_set)
		{
			//Henry use this line to get best action for the actionSet, which links to each classifier.
			//1. return action of this classifier (my definition of layer's action)
			LayerAction classifierAction= c.getLayerClassifierAction();
			//LayerAction a = c.getBestAction(LayerAction.getLayerAction());// Henry's method
			//2. calculate vote through FitnessCount class
			// input cl.fit cl.rwd cl.num
			if(!votes.containsKey(classifierAction))
			{
				votes.put(classifierAction, new FitnessCount(c.getFitness(),c.getNumerosity(), c.getPredictedReward(), c.getAccuracy()));
			}
			else
			{
				votes.get(classifierAction).addFitness(c.getFitness(),c.getNumerosity(), c.getPredictedReward(), c.getAccuracy());
			}

		}

		//screen print the matchset
		/*
		int j=0;
		for(LayerClassifier c : match_set)
		{
			System.out.println("		in exploit mode for selecting action:: 1 ::" +
					" show match_set:("+j+"/" +match_set.size()+") "+
					" cl.id("+c.getID()+")," +
					" act("+c.getLayerAction()+"),"+
					" fit("+c.getFitness()+")," +
					" rwd("+c.getPredictedReward()+")," +
					" num("+c.getNumerosity()+")," +
					" acc("+c.getAccuracy()+")," +
					" exp("+c.getExperience()+") for vote.");

			j++;
		}
		j=0;
		*/

		//System.out.println("	in exploit mode for selecting action:: 1 :: end");
		//System.out.println("\n");

		ArrayList<Map.Entry<LayerAction, FitnessCount>> entries = new ArrayList<Map.Entry<LayerAction, FitnessCount>>(votes.entrySet());

		if(entries.size()>1)
		{
			System.out.println("		exploitPAVote in exploit mode for selecting action:: 2.1 :: entries.size('"+entries.size()+"') ");
			System.out.println("		exploitPAVote in exploit mode for selecting action:: 2.2 :: votes :before sort: ");

			for(int i=0; i<entries.size(); i++)
			{
				System.out.println("			("+i+"/"+entries.size()+") " +
						"action ('"+entries.get(i).getKey()+"').voteFit('"+entries.get(i).getValue().getVote()+"')," +
						" voteAcc('"+entries.get(i).getValue().getVoteAcc()+"') ");
			}
			//System.out.println("\n");
			/**
			 *  sort by vote_acc, which is acc*prd
			 * ******************/
			Collections.sort(entries, new Comparator<Map.Entry<LayerAction, FitnessCount>>() {
				@Override
				public int compare(Map.Entry<LayerAction, FitnessCount> entry1, Map.Entry<LayerAction, FitnessCount> entry2)
				{

					//System.out.println("in exploit mode for selecting action:: 1.3 ::" + "calculate two action of all the fitness of classifiers in MatchSet -> " + "entry1.getValue()='"+entry1.getValue().getFitness()+"' VS  " + "entry2.getValue()='"+entry2.getValue().getFitness()+"'.");

					//double fitnessDiff= entry1.getValue().getVote()-entry2.getValue().getVote();//descend mode//getVote!
					double fitnessDiff= entry1.getValue().getVoteAcc()-entry2.getValue().getVoteAcc();//descend mode//getVoteAcc!
					int fitFlag=0;
					if(Math.abs(fitnessDiff)<=0.0000001)
					{
						if(Math.random() >= 0.5)
						{
							fitFlag=1;
						}else
						{
							fitFlag=-1;
						}
						return (fitFlag);
					}
					//TODO: ways define best action:1. greater fitness win
					//TODO: 2.including reward in this process.
					if(fitnessDiff>0.000001)
					{
						fitFlag=-1;// descent mode
					}
					if(fitnessDiff<-0.000001)
					{
						fitFlag=1;
					}

					return (fitFlag);

					//return entry2.getValue().compareTo(entry1.getValue());// descent order
				}
			});



			System.out.println("		exploitPAVote in exploit mode for selecting action:: 2.3 :: votes :After sort: ");

			for(int i=0; i<entries.size(); i++)
			{
				System.out.println("			("+i+"/"+entries.size()+") " +
						"action ('"+entries.get(i).getKey()+"').vote('"+entries.get(i).getValue().getVote()+"')," +
						" voteAcc('"+entries.get(i).getValue().getVoteAcc()+"') ");
			}
			//System.out.println("\n");

			/*
			LayerAction action = entries.get(0).getKey();
			LayerAction action2 = entries.get(1).getKey();
			System.out.println("	in exploit mode for selecting action:: 3 ::" +
					"best action is = '" + action+ "'VS other action ='"+action2+"'; " +
					"best action's fitness is = '"+ entries.get(0).getValue().getFitness()+"' vs others:'"+entries.get(1).getValue().getFitness()+"'.");

			*/
		}else{
			System.out.println("		in exploit mode for selecting action:: 3 :: entries.size('"+entries.size()+"')" +
					"<---1 only one action advocated in MatchSet.");

		}

		LayerAction action = entries.get(0).getKey();
		return action;
	}
	private LayerAction exploitOneBestAction(ArrayList<LayerClassifier> match_set)
	{
		//"votes" is a hashMap which contains the calculation of each action.
		// the calculation is the vote which will pick the best action
		// the vote calculate  cl.fit*cl.rwd for each classifier.
		HashMap<LayerAction, FitnessCount> votes = new HashMap<LayerAction, FitnessCount>();

		double best_vote=0;
		double a_vote=0;
		LayerAction best_action=LayerAction.empty;
		for(LayerClassifier c : match_set)
		{

			a_vote=c.getAccuracy()*c.getPredictedReward();
			if(a_vote>best_vote)
			{
				best_vote= a_vote;
				best_action=c.getClassifierAction();
			}

		}

		return best_action;

	}

	private LayerAction exploit_scaled_prd_fitXprd(ArrayList<LayerClassifier> match_set)
	{

		LayerAction best_action=LayerAction.empty;
		HashMap<LayerAction, Double> anticipated_reward_hashmap = new HashMap<LayerAction, Double>();
		HashMap<LayerAction, Double> fitness_sum_hashmap = new HashMap<LayerAction, Double>();

		double anticipated_reward_of_an_action_sum=0;
		double fitness_sum=0;

		for(LayerClassifier c : match_set)
		{
			//if anticipated_reward hashmap does not contain current action, add action and anticipated into hashmap
			if(!anticipated_reward_hashmap.containsKey(c.getLayerAction()))
			{
				//fit
				fitness_sum= c.getFitness();
				fitness_sum_hashmap.put(c.getLayerAction(),fitness_sum);
				//fit*prd
				anticipated_reward_of_an_action_sum=c.getFitness()*c.getPredictedReward();
				anticipated_reward_hashmap.put(c.getLayerAction(),anticipated_reward_of_an_action_sum);
			}
			else
			{
				//fit
				fitness_sum= 	c.getFitness() + fitness_sum_hashmap.get(c.getLayerAction());
				fitness_sum_hashmap.put(c.getLayerAction(),fitness_sum);
				//fit*prd
				anticipated_reward_of_an_action_sum=c.getFitness()*c.getPredictedReward() +
						anticipated_reward_hashmap.get(c.getLayerAction());
				anticipated_reward_hashmap.put(c.getLayerAction(),anticipated_reward_of_an_action_sum);
			}

		}

		double max_scaled_prd=-3000000;// max negative reward
		for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
		{

			// scale the anticipated_reward_of_an_action_sum according to fitness_sum
			double a_scaled_prd=a_anticipated_reward_map.getValue()/fitness_sum_hashmap.get(a_anticipated_reward_map.getKey());

			System.out.println("		XCS_exploit_standard: calcuate anticipated reward :\taction("+a_anticipated_reward_map.getKey()+")" +
					"\t- a_scaled_prd("+a_scaled_prd+")"+
					"\t- reward("+a_anticipated_reward_map.getValue()+") " +
					"\t- fit("+fitness_sum_hashmap.get(a_anticipated_reward_map.getKey())+")");
 			this.anticipated_secheduled_reward_hashmap.put(a_anticipated_reward_map.getKey(),a_scaled_prd);
			if(a_scaled_prd> max_scaled_prd)
			{
				max_scaled_prd=a_scaled_prd;
				best_action= a_anticipated_reward_map.getKey();
			}
		}
		// todo:return anticipated_reward_hashmap
		return  best_action;
	}

	/* this modifier version is the same structure of reinforcers. they take hashmap as data structure. a new version replace this hashmap with Matrix data structure.
	private LayerCondition sum_calculate_niche_strength_distribution(ArrayList<LayerClassifier> match_set)
	{

		//modifier_set contains the summary of all niche strength distrubution
		HashMap<Integer, ModifierProjected> modifier_set= new HashMap<Integer, ModifierProjected>();


		//initiate modifier_set, a modifier set store niche strength distribution of all rules in the niche match set
		LayerClassifier a_classifier_demo = match_set.get(0);
		LayerCondition a_classifierCondition_demo= a_classifier_demo.getCondition();
		int howmany_reinforcer_demo=  a_classifierCondition_demo.get_layer_condision_dim();
		int howmany_conditionValue_demo=0;
		for(int j=0; j<howmany_reinforcer_demo;j++)
		{
			ModifierProjected a_modifier= new ModifierProjected(){};
			Reinforcer a_reinforcer_demo= a_classifierCondition_demo.get_a_reinforcer(j);
			howmany_conditionValue_demo= a_reinforcer_demo.get_a_reinforcer_size();

			for(int k=0; k< howmany_conditionValue_demo; k++)
			{
				ModifierValue a_modifierValue = new ModifierValue(0, 80, 0.1);// initate each attribute of a modifier
				//store a_modifierValue into modifier(Projected)
				a_modifier.store_modifier_value(k, a_modifierValue);//save various attributes into a modifier
			}

			modifier_set.put(j,a_modifier);// save a modifier into set
			//
		}
		Matrix modifer_set_matrix =new Matrix(howmany_reinforcer_demo,howmany_conditionValue_demo);



		//modifier set go through niche match set
		for(LayerClassifier a_classifier : match_set)
		{
			double prd= a_classifier.getPredictedReward();

			//dim_L1: LayerCondition. dimension_of_reinforcers_set
			LayerCondition a_classifierCondition= a_classifier.getCondition();
			int howmany_reinforcer=  a_classifierCondition.get_layer_condision_dim();
			System.out.println("dim Level 1, LayerConditionL:: it contain ("+howmany_reinforcer+") reinforcers");

			//dim_L2: Reinforcer
			for(int j=0; j<howmany_reinforcer;j++)
			{
				//ModifierProjected a_modifier= new ModifierProjected(){};
				ModifierProjected a_modifier= modifier_set.get(j);
				Reinforcer a_reinforcer= a_classifierCondition.get_a_reinforcer(j);
				int howmany_conditionValue= a_reinforcer.get_a_reinforcer_size();
				System.out.println("dim Level 2, Reinforcer :: it contain ("+howmany_conditionValue+") ConditionValue");


				for(int k=0; k< howmany_conditionValue; k++)
				{
					//ModifierValue a_modifierValue = new ModifierValue(0,80,0.1);
					ModifierValue a_modifierValue = a_modifier.get_modifier_attribute_value(k);
					//dim_L3: ConditionValue
					ConditionValue a_condition_value= a_reinforcer.get_a_condition_value(k);

					System.out.println("dim Level 3, ConditionValue :: it contains value low ("+a_condition_value.getConditionValuelowerCell()+")" +
							" and value high("+a_condition_value.getConditionValueupperCell()+")");

					//dim_L3: project ConditionValue to Modifier value
					//prd=-5555;
					a_modifierValue = project_condition_value_to_modifier_value_L3(a_condition_value, a_modifierValue, prd);

					System.out.println("a modifier_value:" +a_modifierValue.to_string());

					//store a_modifierValue into modifier(Projected)
					a_modifier.store_modifier_value(k, a_modifierValue);

				}// end of modifier_value/modifier_attribute


				//store a modifier into modifier_set

				modifier_set.put(j,a_modifier);
			}//end of modifier_set

		}



		// todo:return anticipated_reward_hashmap
		return  null;
	}
	*/

	private  HashMap<Integer, Modifier_ContinuouslyProjected> sum_calculate_niche_strength_distribution_matrix_continuous(ArrayList<LayerClassifier> match_set)
	{
		//initiate modifier_set, a modifier set store niche strength distribution of all rules in the niche match set
		LayerClassifier a_classifier_demo = match_set.get(0);
		LayerCondition a_classifierCondition_demo= a_classifier_demo.getCondition();
		int how_many_modifier=  a_classifierCondition_demo.get_layer_condision_dim();

		Reinforcer a_reinforcer_demo= a_classifierCondition_demo.get_a_reinforcer(0);
		int how_many_modifier_attribute= a_reinforcer_demo.get_a_reinforcer_size();
		System.out.println("how_many_modifier("+how_many_modifier+"), how_many_modifier_attribute("+ how_many_modifier_attribute+")" );

		//initiate modifier_niches_set

		HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches_set= new HashMap<Integer, Modifier_ContinuouslyProjected>();

		for(int i=0;i<how_many_modifier;i++)
		{
			modifier_niches_set.put(i, new Modifier_ContinuouslyProjected( how_many_modifier_attribute) );
		}
		// end of initiation of modifier_niches_set

		double sum_fit=0;
		for(LayerClassifier a_classifier : match_set)
		{


			sum_fit += a_classifier.getFitness();
		}

		for(LayerClassifier a_classifier : match_set)
		{

			//TODO:check prd = prd*num??? prd_mode : scaled prd
			//todo: this is niche prd = prd*fit/sum(fit)
			//double prd= a_classifier.getPredictedReward() * a_classifier.getNumerosity();
			double prd= a_classifier.getPredictedReward() * a_classifier.getFitness()/sum_fit;

			//dim_L1: LayerCondition. dimension_of_reinforcers_set
			LayerCondition a_classifierCondition= a_classifier.getCondition();
				//int howmany_reinforcer=  a_classifierCondition.get_layer_condision_dim();
				//System.out.println("dim Level 1, LayerConditionL:: it contain ("+how_many_modifier+") how_many_modifier");

			//dim_L2: Reinforcer/ modifier opeartion
			for(int j=0; j<how_many_modifier;j++)
			{
				Reinforcer a_reinforcer= a_classifierCondition.get_a_reinforcer(j);
					//int howmany_conditionValue= a_reinforcer.get_a_reinforcer_size();
					//System.out.println("dim Level 2, Reinforcer :: it contain ("+how_many_modifier_attribute+") ConditionValue");

				// get an modifier
				Modifier_ContinuouslyProjected a_modifier= modifier_niches_set.get(j);


				// goto each attribute
				for(int k=0; k< how_many_modifier_attribute; k++)
				{
					// get an attribute
					Modifier_Niche_Attribute_Continuous an_attribute = a_modifier.get_attribute(k);
					//get condition attribute(condition value)
					ConditionValue a_condition_value= a_reinforcer.get_a_condition_value(k);
					// do project
					an_attribute=project_condition_value_to_modifier_attribute_L3_continuous(a_condition_value,an_attribute,prd);
					// save an attribute
					a_modifier.set_attribute(k,an_attribute);// save attribute into each modifier

				}// end of modifier_value/modifier_attribute
				// save modifier into modifiers_set
				modifier_niches_set.put(j, a_modifier);

			}//end of modifier_niches_set

		}// end of go through entire classifiers' niche match set
		//System.out.println("after projection, an attribute.point.size("+modifier_niches_set.get(0).get_attribute(0).prd_distribution_vector_point.size()+"):"+ modifier_niches_set.get(0).get_attribute(0).prd_distribution_vector_point.toString());
		//System.out.println("after projection, an attribute.value.size("+modifier_niches_set.get(0).get_attribute(0).prd_distribution_vector_value.size()+"):"+modifier_niches_set.get(0).get_attribute(0).prd_distribution_vector_value.toString());
		System.out.println("modifier_niches_set.get(0).get_attribute(0).prd_distribution_vector_point.size("+modifier_niches_set.get(0).get_attribute(0).prd_distribution_vector_point.size() +") :"+modifier_niches_set.get(0).get_attribute(0).prd_distribution_vector_point);

		return  modifier_niches_set;
	}


	private HashMap<Integer, Matrix> sum_calculate_niche_strength_distribution_matrix(ArrayList<LayerClassifier> match_set)
	{

		//modifier_set contains the summary of all niche strength distrubution


		//initiate modifier_set, a modifier set store niche strength distribution of all rules in the niche match set
		LayerClassifier a_classifier_demo = match_set.get(0);
		LayerCondition a_classifierCondition_demo= a_classifier_demo.getCondition();
		int how_many_modifier=  a_classifierCondition_demo.get_layer_condision_dim();

		Reinforcer a_reinforcer_demo= a_classifierCondition_demo.get_a_reinforcer(0);
		int how_many_modifier_attribute= a_reinforcer_demo.get_a_reinforcer_size();
		double boundary=81;
		double resolution =0.1;

		int how_many_modifier_attribute_representation= (int)(boundary/resolution);
		System.out.println("how_many_modifier("+how_many_modifier+"), how_many_modifier_attribute("+ how_many_modifier_attribute+"),  how_many_modifier_attribute_representation("+how_many_modifier_attribute_representation+")");
		Matrix an_init_attribute =new Matrix(how_many_modifier_attribute,how_many_modifier_attribute_representation, 0);
		HashMap<Integer, Matrix> modifier_niches_set= new HashMap<Integer, Matrix>();

		//clean prd_distribution_vector_point and prd_distribution_vector_value
		this.prd_distribution_vector_point.clear();
		this.prd_distribution_vector_value.clear();

		for(int i=0; i<how_many_modifier; i++)
		{
			modifier_niches_set.put(i, an_init_attribute);
		}// finish initiation


		//modifier set go through niche match set
		for(LayerClassifier a_classifier : match_set)
		{
			double prd= a_classifier.getPredictedReward();

			//dim_L1: LayerCondition. dimension_of_reinforcers_set
			LayerCondition a_classifierCondition= a_classifier.getCondition();
			//int howmany_reinforcer=  a_classifierCondition.get_layer_condision_dim();
			//System.out.println("dim Level 1, LayerConditionL:: it contain ("+how_many_modifier+") how_many_modifier");

			//dim_L2: Reinforcer/ modifier opeartion
			for(int j=0; j<how_many_modifier;j++)
			{
				//ModifierProjected a_modifier= new ModifierProjected(){};
				//ModifierProjected a_modifier= modifier_set.get(j);
				Reinforcer a_reinforcer= a_classifierCondition.get_a_reinforcer(j);
				//int howmany_conditionValue= a_reinforcer.get_a_reinforcer_size();
				//System.out.println("dim Level 2, Reinforcer :: it contain ("+how_many_modifier_attribute+") ConditionValue");

				Matrix a_modifier= modifier_niches_set.get(j);
				// goto each attribute
				for(int k=0; k< how_many_modifier_attribute; k++)
				{
					//ModifierValue a_modifierValue = new ModifierValue(0,80,0.1);
					//ModifierValue a_modifierValue = a_modifier.get_modifier_attribute_value(k);
					//dim_L3: ConditionValue
					ConditionValue a_condition_value= a_reinforcer.get_a_condition_value(k);

					//System.out.println("dim Level 3, ConditionValue :: it contains value low ("+a_condition_value.getConditionValuelowerCell()+")" + " and value high("+a_condition_value.getConditionValueupperCell()+")");

					//dim_L3: project ConditionValue to Modifier value
					//prd=-5555;
					//a_modifierValue = project_condition_value_to_modifier_value_L3(a_condition_value, a_modifierValue, prd);
					a_modifier=project_condition_value_to_modifier_attribute_L3(a_condition_value,a_modifier, k,prd);


					//store a_modifierValue into modifier(Projected)
					//a_modifier.store_modifier_value(k, a_modifierValue);

				}// end of modifier_value/modifier_attribute
				modifier_niches_set.put(j, a_modifier);// save a_modifier into modifier_niches_set

			}//end of modifier_niches_set

		}// end of go through entire classifiers' niche match set
		//System.out.println("after projection, a_modifier.size("+modifier_niches_set.get(0).getRowDimension()+"  by"+modifier_niches_set.get(0).getColumnDimension()+")");

		return  modifier_niches_set;
	}

	private void select_best_discrete_value_for_a_modifier(Matrix a_modifier, String csv_prefix)
	{
		//go through modifier's each attribute
		int how_many_attribute= a_modifier.getRowDimension();
		// find best advocated attribute value in each row
		for(int i=0; i< how_many_attribute;i++)
		{
			find_maximun_items_in_a_row(a_modifier, i, csv_prefix);
		}

	}

	public void find_maximun_items_in_a_row(Matrix a_matrix, int row_index, String csv_prefix)
	{
		ArrayList<Integer> peak= new ArrayList<>();
		double maximun_value =a_matrix.get(row_index,0) ;
		String best_niche_string = csv_prefix;
		String niche_prd_distribution_string =csv_prefix;
		//iterate through a row and identify the maximun value
		for(int i=0; i< a_matrix.getColumnDimension();i++)
		{
			if(a_matrix.get(row_index,i)> maximun_value)
			{
				maximun_value=a_matrix.get(row_index,i);
			}
			niche_prd_distribution_string += Double.toString(a_matrix.get(row_index,i))+"#";
		}

		for(int i=0; i< a_matrix.getColumnDimension();i++)
		{
			if(a_matrix.get(row_index,i)== maximun_value)
			{
				peak.add(i);
				best_niche_string += Integer.toString(i)+"#";
			}
		}
		//System.out.println("maximun_value("+maximun_value+") happens at following locations:"+ peak);

		// prepare to save csv string

		String[] best_niche_array= best_niche_string.split("#");
		save_niche_to_csv("best_niche_points", best_niche_array);

		String[] niche_distribution_array= niche_prd_distribution_string.split("#");
		save_niche_to_csv("niche_distribution", niche_distribution_array);

	}

	public 	ModifierValue project_condition_value_to_modifier_value_L3(ConditionValue src, ModifierValue dst, double prd)
	{
		// dim: level 3 operation project a condition value into the modifier value

		dst.update_modifier_prd_distribution(src.getConditionValuelowerCell(), src.getConditionValueupperCell(), prd);
		return  dst;
	}

	public Modifier_Niche_Attribute_Continuous project_condition_value_to_modifier_attribute_L3_continuous(ConditionValue a_condition_value_attribute,Modifier_Niche_Attribute_Continuous an_attribute, double prd)
	{
		// this is a continuous version of modifier representation

		double low = a_condition_value_attribute.getConditionValuelowerCell();
		double high = a_condition_value_attribute.getConditionValueupperCell();

		//LinkedList<Double> a_prd_distribution_vector_point= new LinkedList<Double>();
		//LinkedList<Double> a_prd_distribution_vector_value= new LinkedList<Double>();


		//todo : pass through linkedList
		niche_strength_distribution_linkedList(an_attribute, low, -1*prd);// todo: return a matrix of attribute
		niche_strength_distribution_linkedList(an_attribute, high, prd);

		//System.out.println("\n>> prd_distribution_vector_point (size="+an_attribute.prd_distribution_vector_point.size()+" ):"+an_attribute.prd_distribution_vector_point.toString());
		//System.out.println(">> prd_distribution_vector_value (size="+an_attribute.prd_distribution_vector_value.size()+" ):"+an_attribute.prd_distribution_vector_value.toString());
		return an_attribute;

	}

	public Matrix project_condition_value_to_modifier_attribute_L3(ConditionValue a_condition_value_attribute,Matrix a_modifier,int which_attribute_index, double prd)
	{
		double low = a_condition_value_attribute.getConditionValuelowerCell();
		double high = a_condition_value_attribute.getConditionValueupperCell();
		double resolution =0.1;
		// input: a condition value's 1. lowcell, 2. highcell; 3, scheduled_prd
		int low_resoluted= normalize_conditionValue_input(low, resolution);
		int high_resoluted= normalize_conditionValue_input(high, resolution);
		//System.out.println("low_resoluted("+low_resoluted+")  high_resoluted("+high_resoluted+")");

		//String s= "";
		// resulution method
		for(int i=low_resoluted; i<= high_resoluted; i++)
		{
			//double pre_prd = this.prd_distribution.get(i);
			double pre_prd = a_modifier.get(which_attribute_index,i);
			double crr_prd = pre_prd+ prd;
			//this.prd_distribution.put(i, crr_prd);
			a_modifier.set(which_attribute_index,i, crr_prd);
			//s += a_modifier.get(which_attribute_index,i) + "  ";
		}// finish updating a modifier according to one rule.

		//int low_index = add_point_into_sequential_linkList(low);
		//add_prd_into_modifier_linkedList(low_index, -1*prd);
		//int high_index = add_point_into_sequential_linkList(high);
		//add_prd_into_modifier_linkedList(high_index, prd);

		niche_strength_distribution_linkedList(low, -1*prd);
		niche_strength_distribution_linkedList(high, prd);

		//System.out.println("\n>> prd_distribution_vector_point (size="+prd_distribution_vector_point.size()+" ):"+prd_distribution_vector_point.toString());
		//System.out.println(">> prd_distribution_vector_value (size="+prd_distribution_vector_value.size()+" ):"+prd_distribution_vector_value.toString());

		/***
		 * trans
		 * ***********/

		//System.out.println("modifer_set_matrix["+which_attribute_index+"]: "+s);
		return a_modifier;

	}

	//merge this function into "prd_distribution_vector_point" procedure
	public void add_prd_into_modifier_linkedList(int low_index,double prd)
	{
		System.out.println("add_prd_into_modifier_linkedList :: index("+low_index+"), prd("+prd+")");
		if(this.prd_distribution_vector_value.size()<=low_index)
		{
			this.prd_distribution_vector_value.add(low_index,prd);
			if(low_index+1!=this.prd_distribution_vector_value.size() )
			{
				//error
				System.out.println("prd_distribution_vector_value error!!!!!!!!!! length is no match...");
			}
			low_index= low_index-1;
		}

		// iterate through all the rest of  prd_distribution_vector_value
		for(int i=0; i<=low_index;i++)
		{
			double a_previous_value= this.prd_distribution_vector_value.get(i);
			double a_updated_value= a_previous_value+ prd;
			this.prd_distribution_vector_value.set(i,a_updated_value);
			System.out.println("prd_distribution_vector_value:: update new value("+a_updated_value+") in location("+i+")" );

		}
		System.out.println("prd_distribution_vector_value:" + prd_distribution_vector_value.toString());


	}

	public void niche_strength_distribution_linkedList(Modifier_Niche_Attribute_Continuous an_attribute,double location, double prd_value)
	{

		LinkedList<Double> prd_distribution_vector_point= an_attribute.prd_distribution_vector_point;
		LinkedList<Double> prd_distribution_vector_value= an_attribute.prd_distribution_vector_value;
		int point_index=0;
		boolean has_equal= false;
		if(prd_distribution_vector_point.isEmpty())
		{
			//initiate the linkedList
			prd_distribution_vector_point.add(location);
			point_index=0;
			// also establish value linkedList
			prd_distribution_vector_value.add(prd_value);
		}
		else
		{
			if(prd_distribution_vector_point.contains(location))
			{	//if the linkedList already has this location, point_index will record its location,
				// and set the has_equal flag to avoid adding the same location into the linkedList.
				point_index= prd_distribution_vector_point.indexOf(location);
				has_equal= true;
				update_prd_distribution_vector_values(prd_distribution_vector_value,point_index, prd_value);

			}
			else
			{
				// it does not contain the point, find its location in an ascending sequence.
				//calculate index through iterate all the linkedlist
				Boolean no_find_location_yet= true;
				for (Iterator<Double> iter = prd_distribution_vector_point.iterator(); iter.hasNext(); )
				{
					Double a_point= iter.next();
					if(location<=a_point)
					{
						if(no_find_location_yet)
						{
							no_find_location_yet= false;
							point_index= prd_distribution_vector_point.indexOf(a_point);
						}
					}
				}
				//current value is the biggest item among the existing list
				if(no_find_location_yet)
				{
					point_index= prd_distribution_vector_point.size();
				}

			}

			if(!has_equal)
			{
				//System.out.println("add point location("+location+") into location("+point_index+")");
				prd_distribution_vector_point.add(point_index,location);
				//System.out.println("prd_distribution_vector_point :"+prd_distribution_vector_point.toString());

				//1. add current prd_value at the end of the value linkedList
				prd_distribution_vector_value.add(prd_value);
				//2. update all prd values whose index number is less than point_index
				update_prd_distribution_vector_values(prd_distribution_vector_value,(point_index-1), prd_value);



			}
			else
			{
				//System.out.println(" already recorded same location ("+location+") at location("+point_index+")");
				//update all prd values whose index number is less than point_index

				update_prd_distribution_vector_values(prd_distribution_vector_value,point_index, prd_value);

			}

		}

		//save to an_attribute
		an_attribute.save_prd_distribution_vector_point(prd_distribution_vector_point);
		an_attribute.save_prd_distribution_vector_value(prd_distribution_vector_value);


		/**
		 *  transform linkedList into matrix
		 * ***/
		/* keep data as hashmap structure until go through all match set and finish projecting, then transform into Matrix structure
		Double[] loc_m= prd_distribution_vector_point.toArray(new Double[prd_distribution_vector_point.size()]);
		Double[] prd_m= prd_distribution_vector_value.toArray(new Double[prd_distribution_vector_value.size()]);

		String loc_m_s= "";
		for(int i=0; i<loc_m.length;i++)
		{
			loc_m_s+= loc_m[i]+ "#";
		}
		String prd_m_s= "";
		for(int i=0; i<prd_m.length;i++)
		{
			prd_m_s+= prd_m[i]+ "#";
		}

		System.out.println("\n>> loc_m:"+ loc_m_s);
		System.out.println(">> prd_m:"+ prd_m_s);

		//save into a new matrix
		int length_matrix= prd_distribution_vector_point.size();
		Matrix an_attribute= new Matrix(2,length_matrix);
		for(int i=0; i<length_matrix;i++)
		{
			an_attribute.set(0,i,loc_m[i]);
			an_attribute.set(1,i,prd_m[i]);
		}

		*/

	}


	public void niche_strength_distribution_linkedList(double location, double prd_value)
	{

		int point_index=0;
		boolean has_equal= false;
		if(prd_distribution_vector_point.isEmpty())
		{
			//initiate the linkedList
			prd_distribution_vector_point.add(location);
			point_index=0;
			// also establish value linkedList
			prd_distribution_vector_value.add(prd_value);
		}
		else
		{
			if(prd_distribution_vector_point.contains(location))
			{	//if the linkedList already has this location, point_index will record its location,
				// and set the has_equal flag to avoid adding the same location into the linkedList.
				point_index= prd_distribution_vector_point.indexOf(location);
				has_equal= true;
				update_prd_distribution_vector_values(point_index, prd_value);

			}
			else
			{
				// it does not contain the point, find its location in an ascending sequence.
				//calculate index through iterate all the linkedlist
				Boolean no_find_location_yet= true;
				for (Iterator<Double> iter = prd_distribution_vector_point.iterator(); iter.hasNext(); )
				{
					Double a_point= iter.next();
					if(location<=a_point)
					{
						if(no_find_location_yet)
						{
							no_find_location_yet= false;
							point_index= prd_distribution_vector_point.indexOf(a_point);
						}
					}
				}
				//current value is the biggest item among the existing list
				if(no_find_location_yet)
				{
					point_index= prd_distribution_vector_point.size();
				}

			}

			if(!has_equal)
			{
				//System.out.println("add point location("+location+") into location("+point_index+")");
				prd_distribution_vector_point.add(point_index,location);
				//System.out.println("prd_distribution_vector_point :"+prd_distribution_vector_point.toString());

				//1. add current prd_value at the end of the value linkedList
				prd_distribution_vector_value.add(prd_value);
				//2. update all prd values whose index number is less than point_index
				update_prd_distribution_vector_values((point_index-1), prd_value);


			}
			else
			{
				//System.out.println(" already recorded same location ("+location+") at location("+point_index+")");
				//update all prd values whose index number is less than point_index
				update_prd_distribution_vector_values(point_index, prd_value);
			}

		}
		/**
		 *  transform linkedList into matrix
		 * ***/
		/* keep data as hashmap structure until go through all match set and finish projecting, then transform into Matrix structure
		Double[] loc_m= prd_distribution_vector_point.toArray(new Double[prd_distribution_vector_point.size()]);
		Double[] prd_m= prd_distribution_vector_value.toArray(new Double[prd_distribution_vector_value.size()]);

		String loc_m_s= "";
		for(int i=0; i<loc_m.length;i++)
		{
			loc_m_s+= loc_m[i]+ "#";
		}
		String prd_m_s= "";
		for(int i=0; i<prd_m.length;i++)
		{
			prd_m_s+= prd_m[i]+ "#";
		}

		System.out.println("\n>> loc_m:"+ loc_m_s);
		System.out.println(">> prd_m:"+ prd_m_s);

		//save into a new matrix
		int length_matrix= prd_distribution_vector_point.size();
		Matrix an_attribute= new Matrix(2,length_matrix);
		for(int i=0; i<length_matrix;i++)
		{
			an_attribute.set(0,i,loc_m[i]);
			an_attribute.set(1,i,prd_m[i]);
		}

		*/

	}
	public void update_prd_distribution_vector_values(LinkedList<Double> prd_distribution_vector_value,int point_index, double prd_value)
	{
		//check if the current prd_distribution_vector_value's length is larger than the point_index
		if(prd_distribution_vector_value.size()<=point_index)
		{
			System.out.println("error!!! prd_distribution_vector_value.size()<=point_index ");
		}
		else
		{
			//update all the prd values whose index is less than point_index
			for(int i=0; i<= point_index;i++)
			{
				double a_pre_prd = prd_distribution_vector_value.get(i);
				double a_cur_prd = a_pre_prd+ prd_value;
				prd_distribution_vector_value.set(i, a_cur_prd);
			}
		}

	}

	public void update_prd_distribution_vector_values(int point_index, double prd_value)
	{
		//check if the current prd_distribution_vector_value's length is larger than the point_index
		if(prd_distribution_vector_value.size()<=point_index)
		{
			System.out.println("error!!! prd_distribution_vector_value.size()<=point_index ");
		}
		else
		{
			//update all the prd values whose index is less than point_index
			for(int i=0; i<= point_index;i++)
			{
				double a_pre_prd = prd_distribution_vector_value.get(i);
				double a_cur_prd = a_pre_prd+ prd_value;
				prd_distribution_vector_value.set(i, a_cur_prd);
			}
		}

	}

	//todo: add prd procedure here
	public int add_point_into_sequential_linkList(Double low)
	{

		int point_index=0;
		boolean has_equal= false;
		if(prd_distribution_vector_point.isEmpty())
		{
			prd_distribution_vector_point.add(low);
			point_index=0;
		}
		else
		{
			if(prd_distribution_vector_point.contains(low))
			{
				point_index= prd_distribution_vector_point.indexOf(low);
				has_equal= true;
			}
			else
			{
				//calculate index through iterate all the linkedlist
				Boolean no_find_location_yet= true;
				for (Iterator<Double> iter = prd_distribution_vector_point.iterator(); iter.hasNext(); )
				{
					Double a_point= iter.next();
					if(low<=a_point)
					{
						if(no_find_location_yet)
						{
							no_find_location_yet= false;
							point_index= prd_distribution_vector_point.indexOf(a_point);
						}
					}
				}
				//low is the biggest item among the existing list
				if(no_find_location_yet)
				{
					point_index= prd_distribution_vector_point.size();
				}

			}

			if(!has_equal)
			{
				System.out.println("add low("+low+") into location("+point_index+")");
				prd_distribution_vector_point.add(point_index,low);
				//System.out.println("prd_distribution_vector_point :"+prd_distribution_vector_point.toString());
			}
			else
			{
				System.out.println("has equal value("+low+") at location("+point_index+")");

			}

		}

		return point_index;

	}

	public int normalize_conditionValue_input(double conditionValue,double resolution)
	{
		int modifier_value;
		if(conditionValue<0)
		{
			return 0;
		}
		else
		{
			modifier_value= (int) (conditionValue/resolution);
			return modifier_value;
		}
	}

	public void reverse_causality(LayerAction a_trageted_action)
	{
		/**
		 * generated a modifier with the real value for a_targeted_action based on clossifiers
		 * 1. generated niche_set: classifiers rules which advocated the "a_targeted_action"
		 * 2. calculate niche_predicted_reward for each rules in niche_set
		 * 3. summarize niche_predicted_reward for the action.
		 * 4. select most strongest niche as advocated modifier
		 * ************/
		ArrayList<LayerClassifier> niche_set = get_niche_set_of_a_trageted_action(a_trageted_action);


	}

	public ArrayList<LayerClassifier> get_niche_set_of_a_trageted_action(LayerAction a_trageted_action)
	{
		/**
		 * go through entire classifiers population.
		 * if a classifier's advocated_action equals to a targeted_action,
		 * 		add this classifier into the niche_set
		 * */
		return null;
	}


	private HashMap<LayerAction, Double> calculate_anticipatedReward(ArrayList<LayerClassifier> match_set)
	{
		/**
		 * exploit best action and also calculate the anticipated reward
		 * because (1) an accuracy classifier is not necessary the most fitness classifier.
		 * (2)  the fitness is a compared accuracy among the same action,
		 * therefore the exploit mode should select an action which has the most anticipated reward.
		 * and the anticipated reward is calculated and compared in the same action
		 *
		 * go through each classifiers in the match_set
		 * 1. according to action, go to different divisions
		 * 2. in each division, find the classifier with maximun fitness
		 * 3. the anticipated reward of this action is cl.acc*cl,prd
		 * guarentee the fitness, accuarcy and prd.
		 * ***/
		// System.out.println("calculate_anticipatedReward:: matchset.size("+match_set.size()+").");

		//"votes" is a hashMap which contains the calculation of each action.
		// the calculation is the vote which will pick the best action
		// the vote calculate  cl.acc*cl.rwd for each classifier.
		HashMap<LayerAction, Double> anticipated_reward_hashmap = new HashMap<LayerAction, Double>();
		HashMap<LayerAction, Double> fitness_max_hashmap = new HashMap<LayerAction, Double>();
		HashMap<LayerAction, Double> acc_max_hashmap = new HashMap<LayerAction, Double>();// for debug purpose


		//LayerAction best_action=LayerAction.empty;

		//double best_anticipated_reward_of_an_action=0;
		double anticipated_reward_of_an_action=0;
		double fitness_max=0;
		for(LayerClassifier c : match_set)
		{
			//if anticipated_reward hashmap does not contain current action, add action and anticipated into hashmap
			if(!anticipated_reward_hashmap.containsKey(c.getLayerAction()))
			{
				//fit
				fitness_max_hashmap.put(c.getLayerAction(),c.getFitness());
				//prd
				anticipated_reward_of_an_action=c.getAccuracy()*c.getPredictedReward();
				anticipated_reward_hashmap.put(c.getLayerAction(),anticipated_reward_of_an_action);
				//acc
				acc_max_hashmap.put(c.getLayerAction(), c.getAccuracy());
			}

			//get the most fitness information
			fitness_max=fitness_max_hashmap.get(c.getLayerAction());
			if(c.getFitness()>fitness_max)
			{
				//fit
				fitness_max_hashmap.put(c.getLayerAction(),c.getFitness());
				//prd
				anticipated_reward_of_an_action=c.getAccuracy()*c.getPredictedReward();
				anticipated_reward_hashmap.put(c.getLayerAction(),anticipated_reward_of_an_action);
				//acc
				acc_max_hashmap.put(c.getLayerAction(), c.getAccuracy());
			}

			/*
			best_anticipated_reward_of_an_action=anticipated_reward_hashmap.get(c.getLayerAction());
			anticipated_reward_of_an_action=c.getAccuracy()*c.getPredictedReward();
			if(best_anticipated_reward_of_an_action<anticipated_reward_of_an_action)
			{
				best_anticipated_reward_of_an_action=anticipated_reward_of_an_action;
				anticipated_reward_hashmap.put(c.getLayerAction(),anticipated_reward_of_an_action);
			}
			*/
		}

		//check the anticipated_reward_hashmap

		for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
		{
			this.anticipated_secheduled_reward_hashmap.put(a_anticipated_reward_map.getKey(),a_anticipated_reward_map.getValue());
			System.out.println("		(Accuracy-based) calcuate anticipated reward :\taction("+a_anticipated_reward_map.getKey()+")" +
					"\t- reward("+a_anticipated_reward_map.getValue()+") " +
					"\t- acc("+acc_max_hashmap.get(a_anticipated_reward_map.getKey())+") " +
					"\t- fit("+fitness_max_hashmap.get(a_anticipated_reward_map.getKey())+")");

		}
		return anticipated_reward_hashmap;

	}


	private HashMap<LayerAction, LayerClassifier> calculate_niche_instance_classifier(ArrayList<LayerClassifier> match_set)
	{


		//System.out.println("calculate_anticipatedReward:: matchset.size("+match_set.size()+").");

		HashMap<LayerAction, Double> anticipated_reward_hashmap = new HashMap<LayerAction, Double>();
		HashMap<LayerAction, Double> fitness_max_hashmap = new HashMap<LayerAction, Double>();
		HashMap<LayerAction, Double> acc_max_hashmap = new HashMap<LayerAction, Double>();// for debug purpose

		// save the classifier which best represent this niche
		HashMap<LayerAction, LayerClassifier> best_represented_classifier_hashmap = new HashMap<LayerAction, LayerClassifier>();// classifier version
		HashMap<LayerAction, String[]> best_represented_classifier_string_hashmap = new HashMap<LayerAction, String[]>();		//save to string for csv// string version

		//LayerAction best_action=LayerAction.empty;

		//double best_anticipated_reward_of_an_action=0;
		double anticipated_reward_of_an_action=0;
		double fitness_max=0;
		for(LayerClassifier c : match_set)
		{
			//if anticipated_reward hashmap does not contain current action, add action and anticipated into hashmap
			if(!anticipated_reward_hashmap.containsKey(c.getLayerAction()))
			{
				//fit
				fitness_max_hashmap.put(c.getLayerAction(),c.getFitness());
				//prd
				anticipated_reward_of_an_action=c.getAccuracy()*c.getPredictedReward();
				anticipated_reward_hashmap.put(c.getLayerAction(),anticipated_reward_of_an_action);
				//acc
				acc_max_hashmap.put(c.getLayerAction(), c.getAccuracy());
				//id
				best_represented_classifier_hashmap.put(c.getLayerAction(), c);
				//csv
				//best_represented_classifier_string_hashmap.put(c.getLayerAction(), c.toStringArray_for_csv());
			}

			//get the most fitness information
			fitness_max=fitness_max_hashmap.get(c.getLayerAction());
			//show up equal fitness
			//if(c.getFitness()==fitness_max)
			//{
			//	System.out.println("		equal fitness("+c.getFitness()+") happens  most fitness classifier id(" + c.getID() + ") " );
			//}

			if(c.getFitness()>fitness_max)
			{
				//fit
				fitness_max_hashmap.put(c.getLayerAction(),c.getFitness());
				//prd
				anticipated_reward_of_an_action=c.getAccuracy()*c.getPredictedReward();
				anticipated_reward_hashmap.put(c.getLayerAction(),anticipated_reward_of_an_action);
				//acc
				acc_max_hashmap.put(c.getLayerAction(), c.getAccuracy());
				//id
				best_represented_classifier_hashmap.put(c.getLayerAction(), c);
				//csv string
				//best_represented_classifier_string_hashmap.put(c.getLayerAction(), c.toStringArray_for_csv());

			}


		}

		/*
		//check the anticipated_reward_hashmap
		for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
		{
			System.out.println("		(Accuracy-based) calcuate anticipated reward :\taction("+a_anticipated_reward_map.getKey()+")" +
					"\t- reward("+a_anticipated_reward_map.getValue()+") " +
					"\t- acc("+acc_max_hashmap.get(a_anticipated_reward_map.getKey())+") " +
					"\t- fit("+fitness_max_hashmap.get(a_anticipated_reward_map.getKey())+")");

		}
		*/

		//check the classifier
		for(Map.Entry<LayerAction, LayerClassifier> a_classifier_in_hashmap: best_represented_classifier_hashmap.entrySet()) {
		System.out.println("	kernel:	(Accuracy-based) most fitness classifier :\t s-action(" + a_classifier_in_hashmap.getKey() + ")" +
				"\t- id(" + a_classifier_in_hashmap.getValue().getID() + ")," +
				" condition (" + a_classifier_in_hashmap.getValue().getCondition() + " )," +
				" action(" + a_classifier_in_hashmap.getValue().getLayerAction() + ")," +
				" acc("+ a_classifier_in_hashmap.getValue().getAccuracy()+" ), " +
				" prd("+ a_classifier_in_hashmap.getValue().getPredictedReward()+")," +
				" fit("+ a_classifier_in_hashmap.getValue().getFitness()+")");
		}


		return best_represented_classifier_hashmap;
	}

	private HashMap<LayerAction, Double> calculate_anticipatedReward2(ArrayList<LayerClassifier> match_set)
	{


		System.out.println("calculate_anticipatedReward:: matchset.size("+match_set.size()+").");

		HashMap<LayerAction, Double> anticipated_reward_hashmap = new HashMap<LayerAction, Double>();
		HashMap<LayerAction, Double> fitness_max_hashmap = new HashMap<LayerAction, Double>();
		HashMap<LayerAction, Double> acc_max_hashmap = new HashMap<LayerAction, Double>();// for debug purpose

		// save the classifier which best represent this niche
		HashMap<LayerAction, LayerClassifier> best_represented_classifier_hashmap = new HashMap<LayerAction, LayerClassifier>();

		//LayerAction best_action=LayerAction.empty;

		//double best_anticipated_reward_of_an_action=0;
		double anticipated_reward_of_an_action=0;
		double fitness_max=0;
		for(LayerClassifier c : match_set)
		{
			//if anticipated_reward hashmap does not contain current action, add action and anticipated into hashmap
			if(!anticipated_reward_hashmap.containsKey(c.getLayerAction()))
			{
				//fit
				fitness_max_hashmap.put(c.getLayerAction(),c.getFitness());
				//prd
				anticipated_reward_of_an_action=c.getAccuracy()*c.getPredictedReward();
				anticipated_reward_hashmap.put(c.getLayerAction(),anticipated_reward_of_an_action);
				//acc
				acc_max_hashmap.put(c.getLayerAction(), c.getAccuracy());
				//id
				best_represented_classifier_hashmap.put(c.getLayerAction(), c);
			}

			//get the most fitness information
			fitness_max=fitness_max_hashmap.get(c.getLayerAction());
			if(c.getFitness()>fitness_max)
			{
				//fit
				fitness_max_hashmap.put(c.getLayerAction(),c.getFitness());
				//prd
				anticipated_reward_of_an_action=c.getAccuracy()*c.getPredictedReward();
				anticipated_reward_hashmap.put(c.getLayerAction(),anticipated_reward_of_an_action);
				//acc
				acc_max_hashmap.put(c.getLayerAction(), c.getAccuracy());
				//id
				best_represented_classifier_hashmap.put(c.getLayerAction(), c);
			}


		}

		//check the anticipated_reward_hashmap
		for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
		{
			System.out.println("		(Accuracy-based) calcuate anticipated reward :\taction("+a_anticipated_reward_map.getKey()+")" +
					"\t- reward("+a_anticipated_reward_map.getValue()+") " +
					"\t- acc("+acc_max_hashmap.get(a_anticipated_reward_map.getKey())+") " +
					"\t- fit("+fitness_max_hashmap.get(a_anticipated_reward_map.getKey())+")");

		}
		/*
		//check the classifier
		for(Map.Entry<LayerAction, LayerClassifier> a_classifier_in_hashmap: best_represented_classifier_hashmap.entrySet()) {
			System.out.println("		(Accuracy-based) most fitness classifier :\t s-action(" + a_classifier_in_hashmap.getKey() + ")" +
					"\t- id(" + a_classifier_in_hashmap.getValue().getID() + ")," +
					" condition (" + a_classifier_in_hashmap.getValue().getCondition() + " )," +
					" action(" + a_classifier_in_hashmap.getValue().getLayerAction() + ")");


		}
		*/
		return anticipated_reward_hashmap;
	}

	private LayerAction getBestActionAccordingToMaxFitness(HashMap<LayerAction, Double> anticipated_reward_hashmap )
	{
		LayerAction best_action=LayerAction.empty;
		double anticipated_reward_max=-30000;

		for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
		{
			if(anticipated_reward_max<a_anticipated_reward_map.getValue())
			{
				anticipated_reward_max = a_anticipated_reward_map.getValue();
				best_action = a_anticipated_reward_map.getKey();
			}
		}

		System.out.println("		 anticipated best_action ("+best_action+")" +
				"-anticipated_reward_max("+anticipated_reward_max+")");

		return best_action;

	}



	public void saveThePopulation(String file)
	{

		IDFromGenesis= this.getAndShowNextID();
		try
		{
			XStream xstream = new XStream();
			String xml = xstream.toXML(this);
			BufferedWriter xmlOut = new BufferedWriter(new FileWriter(file+".xml"));
			xmlOut.write(xml);
			xmlOut.close();
			System.out.println(" layerpopulation saveThePopulation: write buffer in :'"+file+".xml'," +
					" IDFromGenesis('"+IDFromGenesis+"')");

			/*
			if((IDFromGenesis-lastGenesis)>=20)
			{
				XStream xstreamBackup = new XStream();
				String xmlBackup = xstreamBackup.toXML(this);
				BufferedWriter xmlOutBackup = new BufferedWriter(new FileWriter(file+"20.xml"));
				xmlOutBackup.write(xmlBackup);
				xmlOutBackup.close();
				System.out.println(" in LayerProblem/EmotionPopulation.java->saveThePopulation() write buffer in :'"+file+"20.xml'<==backup," +
						" IDFromGenesis('"+IDFromGenesis+"')");
				lastGenesis=IDFromGenesis;
			}
			*/


		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public void save_classifiers(String file)
	{

		try
		{
			System.out.println("save the classifiers alone");
			XStream xstream = new XStream(new DomDriver());
			String xml = xstream.toXML(super.getClassifiers());
			BufferedWriter xmlOut = new BufferedWriter(new FileWriter(file+".xml"));
			xmlOut.write(xml);
			xmlOut.close();

//			FileOutputStream fileOut = new FileOutputStream("population.ser");
//			ObjectOutputStream out = new ObjectOutputStream(fileOut);
//			out.writeObject(classifiers);
//			out.close();
//			fileOut.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public void save_txt_classifiers( )
	{
		try
		{
			System.out.println("show the classifiers first");

			String s = super.classifiersToString();
			System.out.println(s);
			PrintWriter out =new PrintWriter("classifier.txt");
			out.println(s);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public void save_csv_classifiers(String file)
	{
		IDFromGenesis= this.getAndShowNextID();

		try
		{
			System.out.println("CSV version:  the classifiers");

			CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/"+file+".csv"));

			List<String[]> data=  classifiersToCSV();
			csv_writer.writeAll(data);
			csv_writer.close();
			/*
			if((IDFromGenesis-lastGenesis)>=100)
			{
				System.out.println("CSV version:  the classifiers, classifiers_csv_backup_file_100.");

				CSVWriter csv_writer_backup= new CSVWriter(new FileWriter("./Results/"+file+"_backup.csv"));
				csv_writer_backup.writeAll(data);
				csv_writer_backup.close();

				System.out.println(" classifiers_csv_file_100.csv backup" +
						" IDFromGenesis('"+IDFromGenesis+"')");
				lastGenesis=IDFromGenesis;
			}
			*/

		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public static List<String[]> load_csv_classifiers(String file)
	{
		List<String[]> data;
		try
		{
			FileReader f= new FileReader("./Results/"+file+".csv");
			CSVReader csv_reader= new CSVReader(f);
			data= csv_reader.readAll();
			return data;
		}
		catch(Exception e)
		{
			System.out.println("No previous population!: " + e.getMessage());
			return null;
		}
	}

	public void	save_niche_to_csv(String file_name, String[] a_row_data)
	{
		List<String[]> data =new ArrayList<String[]>() ;
		//1. read csv
		try
		{
			FileReader f= new FileReader("./Results/"+file_name+".csv");
			CSVReader csv_reader= new CSVReader(f);
			data= csv_reader.readAll();
			// add a row of data into data
			data.add(a_row_data);

			//3. write csv Data
			try
			{
				System.out.println("save_csv_string to filename("+file_name+")");

				CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/"+file_name+".csv"));
				csv_writer.writeAll(data);
				csv_reader.close();
				csv_writer.close();

			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

	}

	public static LayerPopulation readThePopulation(String file)
	{
		System.out.println("LayerProblem::LayerPopulation.java->readThePopulation 1 try to read populatiopn from file:'"+file+"'.........");


//		FileInputStream fileIn;
//		ObjectInputStream in;
		try
		{
//			fileIn = new FileInputStream(new File("population.ser"));
//			in = new ObjectInputStream(fileIn);
//			this.classifiers = (HashMap<Integer, Classifier>)in.readObject();
//			for(int id : classifiers.keySet())
//				if(id >= NEXT_ID)
//					NEXT_ID = id + 1;
//			System.out.println(this.classifiers.size());
			System.out.println("LayerProblem::LayerPopulation.java->LayerPopulation.java-> : 2.1 try to read populatiopn from file:'"+file+".xml' .........");
			XStream xstream = new XStream();
			LayerPopulation population = (LayerPopulation)xstream.fromXML(new File(file+".xml"));

			//population.agentFileName=file;// set agentFileName for different agent's population
			population.setNextID(population.IDFromGenesis);//pass this parameter to classifiers in population

			//population.setNextID(gesis_ID);
			System.out.println("LayerProblem::LayerPopulation.java-> LayerPopulation.java-> : 2.2 read populatiopn from file:'"+file+".xml' successfully." +
					" population.IDFromGenesis('"+population.IDFromGenesis+"')........");
			return population;
		}
		catch(Exception e)
		{
			System.out.println("LayerProblem::LayerPopulation.java->readThePopulation 3 fail to read populatiopn from file:'"+file+".xml' fail.........");
			System.out.println("No previous population!: " + e.getMessage());
//			e.printStackTrace();
			return new LayerPopulation();
		}
	}

	public static LayerPopulation readThePopulationfromCSV(String file)
	{
		/*
		System.out.println("readThePopulationfromCSV 1 try to read populatiopn from csv file:'"+file+"'.........");

		// 1. load csv data

		List<String[]> csv_data;
		csv_data = load_csv_classifiers(file);
		boolean hasCSVData= false;
		if(csv_data.size()>1)
		{
			hasCSVData = true;
		}
		*/
		//2. translate csv_data into classifiers
		LayerPopulation population = new LayerPopulation();
		/*
		if(hasCSVData)
		{
			population.CSVToClassifeirs(csv_data);

		}
		*/
		return population;

	}

	public static LayerPopulation getEmptyPopulation()
	{
		return new LayerPopulation();
	}

	public void set_The_DIVERSITY_of_actions(int the_diversity_of_actions)
	{
		this.the_diversity_of_actions= the_diversity_of_actions;
	}

	@Override
	public int getLengthOfLayerActionSet()
	{
		System.out.println(" 		show agentFileName("+agentFileName+"), show the_diversity_of_actions("+the_diversity_of_actions+")");

		return the_diversity_of_actions;

		/*
		//here define the length of layerActionSet
		System.out.println(" 		show agentFileName("+agentFileName+")");

		if(this.agentFileName.equals("pidmm"))
		{
			return 2;
		}else
		{
			return 3;///////set here the length of action set !!! the number of iris species types
		}
		*/
	}
}