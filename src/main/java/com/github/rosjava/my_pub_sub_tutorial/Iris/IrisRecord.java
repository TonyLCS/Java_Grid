package main.java.com.github.rosjava.my_pub_sub_tutorial.Iris;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;


import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


//import main.java.com.github.rosjava.my_pub_sub_tutorial.EmotionProblem.EmotionProblem;
//import main.java.com.github.rosjava.my_pub_sub_tutorial.EmotionProblem.EmotionalCondition;
//import main.java.com.github.rosjava.my_pub_sub_tutorial.EmotionProblem.Reinforcer_Factory;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.*;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerCondition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerProblem;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;


import org.hibernate.hql.internal.ast.tree.BooleanLiteralNode;





import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.*;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javassist.bytecode.Descriptor;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ClassifierExpectation;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.FitnessCount;

import com.thoughtworks.xstream.XStream;



public class IrisRecord
{

    private HashMap<Integer, Integer> recordLog = new HashMap<Integer, Integer>();

    private Integer iter=0;
    private double successTimeChechPoint=0;
    private double failureTimeChechPoint=0;

    private HashMap<Integer, Double> CheckPointTrajectory = new HashMap<Integer, Double>();
    public IrisRecord(){}


    public void saveRecord(boolean predictResult, int iteration )
    {
        int reward;
        iter++;
        if(predictResult)
        {
            reward=1;
            successTimeChechPoint++;
        }
        else
        {
            reward =0;
            failureTimeChechPoint++;
        }
        System.out.println("iris save record iteration("+iteration+"), reward("+reward+")");
        this.recordLog.put((Integer)iteration, (Integer)reward);

        if(iter%1500==0)
        {
            System.out.println("iris save record iteration("+iteration+"), success rate("+(successTimeChechPoint/iter)+")");
            this.CheckPointTrajectory.put(iter,(Double)(successTimeChechPoint/1500));
            //this.CheckPointTrajectory.put(iter,(Double)(successTimeChechPoint/iter));

            successTimeChechPoint=0;
            failureTimeChechPoint=0;
        }

        savePreformance("irisRecord");

    }

    private void savePreformance(String file)
    {
        System.out.println("iris save record ");

        try
        {
            XStream xstream = new XStream();
            String xml = xstream.toXML(this);
            BufferedWriter xmlOut = new BufferedWriter(new FileWriter(file+".xml"));
            xmlOut.write(xml);
            xmlOut.close();
            //System.out.println("save Management: file("+file+")");


        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        //System.out.println("test Management: end of saving procedural ");

    }
}

