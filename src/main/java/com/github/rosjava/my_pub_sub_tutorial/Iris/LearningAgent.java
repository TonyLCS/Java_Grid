package main.java.com.github.rosjava.my_pub_sub_tutorial.Iris;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;


import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


//import main.java.com.github.rosjava.my_pub_sub_tutorial.EmotionProblem.EmotionProblem;
//import main.java.com.github.rosjava.my_pub_sub_tutorial.EmotionProblem.EmotionalCondition;
//import main.java.com.github.rosjava.my_pub_sub_tutorial.EmotionProblem.Reinforcer_Factory;
import main.java.GridSim.EnvironmentManager.Instance;
import main.java.com.github.rosjava.my_pub_sub_tutorial.Jama.Matrix;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.*;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerCondition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerProblem;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;



import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.*;

import main.java.GridSim.EnvironmentManager.SingleTask;
import main.java.GridSim.EnvironmentManager.Instance;

public class LearningAgent
{

    public LearningAgent(){}
    public LearningAgent(String which_task)
    {
        this.which_task = which_task;
        //load population
        iris_xcs = new LayerProblem(which_task); //still has an empty population
    }
    String which_task= "instance";//"wine???"
    private LayerProblem iris_xcs = new LayerProblem(which_task);//this will initialise a XCS population
    private LayerCondition current_iris_state; //current iris instance
    //private double[] current_rat_state;
    private Reinforcer_Factory reinforcer_factory=new Reinforcer_Factory();
    private double speciesType=0;
    public int iteration=0;
    public boolean predictResult;

    public void set_xcs_population(LayerPopulation population)
    {
        this.iris_xcs.setPopulation(population);
        //this.iris_xcs = xcs;
    }
    public LayerProblem get_xcs()
    {
        return  this.iris_xcs;
    }
    public void set_the_DIVERSITY_of_actions(int the_DIVERSITY_of_actions)
    {
        this.iris_xcs.set_the_DIVERSITY_of_actions(the_DIVERSITY_of_actions);
    }

    public void setLearningMode(int exploreRate,int iterationCount_threshold, int GATheshold)
    {
        // this mode is applied to switch between various training mode, such as learning/training, testing and mcts.
        this.iris_xcs.setExploreRate(exploreRate);
        this.iris_xcs.setIterationCount_threshold(iterationCount_threshold);
        this.iris_xcs.setGATheshold(GATheshold);//150*10 ?? effect is suspective
        this.iris_xcs.replace_select_filter(exploreRate, iterationCount_threshold);
        this.iris_xcs.replace_GA_filter(GATheshold);

    }
    public void setTestingMode(int exploreRate,int iterationCount_threshold, int GATheshold)
    {
        // this mode is applied to switch between various training mode, such as learning/training, testing and mcts.
        this.iris_xcs.setExploreRate(exploreRate);
        this.iris_xcs.setIterationCount_threshold(iterationCount_threshold);
        this.iris_xcs.setGATheshold(GATheshold);//150*10
        this.iris_xcs.replace_select_filter(exploreRate, iterationCount_threshold);
        //NOT GA this.iris_xcs.replace_GA_filter(GATheshold);
    }

    public void setLearningMode(int exploreRate,int iterationCount_threshold, int GATheshold, String fiters_portfolio)
    {
        // this function is discard.
        this.iris_xcs.setExploreRate(exploreRate);
        this.iris_xcs.setIterationCount_threshold(iterationCount_threshold);
        //NOT GA this.iris_xcs.setGATheshold(GATheshold);//150*10

        this.iris_xcs.replace_select_filter(exploreRate, iterationCount_threshold, fiters_portfolio);
        //NOT GA this.iris_xcs.replace_GA_filter(GATheshold);
    }

//    public boolean irisTrain(SingleTask singleTask,int iteration)
//    {
//        this.iteration=iteration;
//        System.out.println("irisTrain : a_singleTask: species("+singleTask.species+"): ("+singleTask.sepal_length+"/"+ singleTask.sepal_width+"/"+singleTask.petal_length+"/"+singleTask.petal_width+")");
//
//        //todo:change perception in cell wrold and crystal eyes
//        reinforcer_factory.setIrisTask_cellWorld(singleTask);//crystal eyes //training 1
//        //reinforcer_factory.setIrisTask(singleTask);
//        this.current_iris_state= getPerceptionState("iris");
//
//        System.out.println("current_iris_state("+this.current_iris_state+")");
//
//        this.speciesType=singleTask.species;// result, for reward
//
//        this.iris_xcs.setExploreRate(20);
//        this.iris_xcs.setIterationCount_threshold(1500);
//        this.iris_xcs.setGATheshold(20);//150*10
//
//        this.iris_xcs.iterateIris(this.current_iris_state, this.iteration );
//
//        this.predictResult= this.iris_xcs.irisReward(this.speciesType);
//
//        //this.iris_xcs.rewardLayer1(1000);
//        this.iris_xcs.deletionLayer1();
//        // todo: check this write extension class
//        this.iris_xcs.save();
//
//        return  this.predictResult;
//
//    }

//    public void irislearningMultiStep(SingleTask singleTask,int iteration)//for the parasite and host
//    {
//        this.iteration= iteration;
//        //reinforcer_factory.setIrisTask_testCellWorld(singleTask);
//        reinforcer_factory.setIrisTask_cellWorld(singleTask);//crystal eyes //training 2
//        this.current_iris_state= getPerceptionState("iris");
//        this.speciesType=singleTask.species;// result, for reward
//        this.iris_xcs.iterateIrisLearningMultiStep(this.current_iris_state, this.iteration );
//    }

//    public void iris_learning_multiStep_4fliters(SingleTask singleTask,int iteration)
//    {
//        this.iteration= iteration;
//        //reinforcer_factory.setIrisTask_testCellWorld(singleTask);
//        reinforcer_factory.setIrisTask_cellWorld(singleTask);//crystal eyes //training 3
//        this.current_iris_state= getPerceptionState("iris");
//        this.iris_xcs.iterateIrisLearningMultiStep_4Fliters(this.current_iris_state, this.iteration );
//    }


    public void iris_learning_multiStep_groundTruth(SingleTask singleTask)
    {
        reinforcer_factory.setIrisTask_cellWorld(singleTask);//training 4 crystal eyes ,iris_learning_multiStep_groundTruth
        this.current_iris_state= getPerceptionState("iris");           // iris_learning_multiStep_groundTruth
        this.speciesType=singleTask.species;// result, for reward                   // iris_learning_multiStep_groundTruth
        //todo: save this ground truth for future reward
        this.iris_xcs.saveAReward(this.speciesType);

    }
    public void set_multiStep_groundTruth_for_Instance(double ground_truth)
    {
        this.iris_xcs.saveAReward(ground_truth); //instance
    }

    public double reward_summary()
    {
        /**calculate total reward
         * totalRewardGenerate
         * **/
       return this.iris_xcs.reward_summary();

    }
    public boolean reward_assign(double taskReward, String method, int O, int T)
    {
       // this.iris_xcs.reward_assign(taskReward, "even");
        //this.iris_xcs.reward_assign(taskReward, "fitnessWeight");
        //this.iris_xcs.reward_assign(taskReward,"fitXprd");
      return  this.iris_xcs.reward_assign(taskReward, method, O,T);

    }

    public void set_task_seq(int task_seq)
    {
        this.iris_xcs.set_task_seq(task_seq);
    }

    public void calculateMatrix_example(double TaskReward,int scenario_seq)
    {
        //this.iris_xcs.calculateMatrix_example();


//        this.iris_xcs.calculate_step_reward_Matrix_flexible(TaskReward, scenario_seq);

        this.iris_xcs.calculate_step_reward_Matrix(TaskReward, scenario_seq);

       // this.iris_xcs.calculate_step_reward_Matrix_dichotomy(TaskReward, scenario_seq);// this should replace "        this.iris_xcs.calculate_step_reward_Matrix(TaskReward, scenario_seq);"

    }


    public void span_orthogonal_reward_space(double TaskReward, int scenario_seq, int multistep)
    {
        this.iris_xcs.span_orthogonal_reward_space(TaskReward,scenario_seq, multistep);
    }

    public void span_span_matrix_steps_rewards_vectors()
    {
        this.iris_xcs.span_matrix_steps_rewards_vectors();

    }


//    public Matrix calculate_reward_all_steps_matrix(int O, int S, int T)
//    {
//        // O= scenario_seq
//        // S= multistep
//        // t= action
//       return this.iris_xcs.calculate_reward_all_steps_matrix(O, S, T);
//    }


//    public void extract_reward_S_step_from_all( int S, Matrix reward_all_steps_matrix)
//    {
//        this.iris_xcs.extract_reward_S_step_from_all(S, reward_all_steps_matrix);
//    }

    public void reward_apply()
    {
        this.iris_xcs.reward_update_multiple_step();
    }

    public void reward_dynamic_apply(double learning_rate)
    {
        this.iris_xcs.reward_update_multiple_step_dynamic(learning_rate);
    }

    public void mcts_reward_apply()
    {
        this.iris_xcs.reward_update_mcts_multiple_step();
    }

    public void mcts_reward_apply_dynamic_learning_rate(double learning_rate)
    {
        this.iris_xcs.reward_update_mcts_multiple_step_learning_rate(learning_rate);
    }
    public void mcts_reward_clear()
    {
        this.iris_xcs.reward_update_mcts_multiple_step_clear();
    }
    public void donot_update_reward()
    {
        this.iris_xcs.donot_update_reward();
    }

    public void irisMultistepReward(int parasiteNum, int hostNum)
    {

        this.iris_xcs.irisRewardMultistep(parasiteNum, hostNum, this.speciesType);
    }

    public void irislearningMultistep_del(int pop_size)
    {
        this.iris_xcs.deletionLayer1(pop_size);
        //this.iris_xcs.save();
    }

    public void subsumption()
    {
        this.iris_xcs.subsumption();
    }

    public void save_csv_file(int episode)
    {
        this.iris_xcs.save(episode);
    }
    public void save_csv_file(int episode,String folder_name)
    {
        this.iris_xcs.save(episode, folder_name);
    }

    public boolean iris_testMode(SingleTask singleTask,int test_exploreRate, int test_iterationCount_threshold,int test_GA_threshold)
    {
        System.out.println("Testing : a_singleTask: species("+singleTask.species+"): ("+singleTask.sepal_length+"/"+ singleTask.sepal_width+"/"+singleTask.petal_length+"/"+singleTask.petal_width+")");
        reinforcer_factory.setIrisTask_testCellWorld(singleTask);//testEyes// single eye, not crystal eyes. no boundary
        this.current_iris_state= getPerceptionState("iris");
        System.out.println("current_iris_state("+this.current_iris_state+")");

        this.speciesType=singleTask.species;// result, for reward

        //todo: set exploit mode
        //test_exploreRate=0
        //test_iterationCount_threshold=0
        //test_GA_threshold=1500000000
        this.iris_xcs.setExploreRate(test_exploreRate);
        this.iris_xcs.setIterationCount_threshold(test_iterationCount_threshold);
        this.iris_xcs.setGATheshold(test_GA_threshold);
        this.iris_xcs.iterateIrisTest(this.current_iris_state, this.iteration );
        return this.iris_xcs.irisMeasure(this.speciesType);
        //this.iris_xcs.irisReward(this.speciesType);


    }

    public boolean instance_testMode(Instance singleTask,int test_exploreRate, int test_iterationCount_threshold,int test_GA_threshold)
    {
        System.out.println("Testing : a_singleTask: species("+singleTask.ground_truth+"): attributes: ("+singleTask.attribute+")");
        //reinforcer_factory.setIrisTask_testCellWorld(singleTask);//testEyes// single eye, not crystal eyes. no boundary
        //this.current_iris_state= getPerceptionState("iris");
        reinforcer_factory.set_perception_state_for_instance(which_task,"no_boundary",singleTask.attribute); //no_boundary for the test mode
        this.current_iris_state= getPerceptionState(which_task);
        System.out.println("current_iris_state("+this.current_iris_state+")");

        this.speciesType=singleTask.ground_truth;// result, for reward

        //todo: set exploit mode
        //test_exploreRate=0
        //test_iterationCount_threshold=0
        //test_GA_threshold=1500000000
        this.iris_xcs.setExploreRate(test_exploreRate);
        this.iris_xcs.setIterationCount_threshold(test_iterationCount_threshold);
        this.iris_xcs.setGATheshold(test_GA_threshold);
        this.iris_xcs.iterateIrisTest(this.current_iris_state, this.iteration );// instance_testMode // todo: return action confidence
        return this.iris_xcs.irisMeasure(this.speciesType);
        //this.iris_xcs.irisReward(this.speciesType);
    }

    public boolean instance_testMode_check_instance_action_anticipatedPRD(int instance_ID,Instance singleTask,int test_exploreRate, int test_iterationCount_threshold,int test_GA_threshold)
{
    System.out.println("Testing : a_singleTask: species("+singleTask.ground_truth+"): attributes: ("+singleTask.attribute+")");
    //reinforcer_factory.setIrisTask_testCellWorld(singleTask);//testEyes// single eye, not crystal eyes. no boundary
    //this.current_iris_state= getPerceptionState("iris");
    reinforcer_factory.set_perception_state_for_instance(which_task,"no_boundary",singleTask.attribute); //no_boundary for the test mode
    this.current_iris_state= getPerceptionState(which_task);
    System.out.println("current_iris_state("+this.current_iris_state+")");

    this.speciesType=singleTask.ground_truth;// result, for reward

    //todo: set exploit mode
    //test_exploreRate=0
    //test_iterationCount_threshold=0
    //test_GA_threshold=1500000000
    this.iris_xcs.setExploreRate(test_exploreRate);
    this.iris_xcs.setIterationCount_threshold(test_iterationCount_threshold);
    this.iris_xcs.setGATheshold(test_GA_threshold);
    this.iris_xcs.iterateIrisTest_check_instance_action_anticipatedPRD(this.iteration, instance_ID, this.current_iris_state);// instance_testMode // todo: return action confidence
    return this.iris_xcs.irisMeasure(this.speciesType);
    //this.iris_xcs.irisReward(this.speciesType);
}

    public HashMap<LayerAction, LayerClassifier> get_instance_classifier(Instance singleTask,int test_exploreRate, int test_iterationCount_threshold,int test_GA_threshold)
    {

        reinforcer_factory.set_perception_state_for_instance(which_task,"no_boundary",singleTask.extractValueInstance()); //extract instance value from old attribute as a new attribute!!!
        this.current_iris_state= getPerceptionState(which_task);
        //System.out.println("current_iris_state("+this.current_iris_state+")");

        this.iris_xcs.setExploreRate(test_exploreRate);
        this.iris_xcs.setIterationCount_threshold(test_iterationCount_threshold);
        this.iris_xcs.setGATheshold(test_GA_threshold);
        //todo: return anticipated reward here
        HashMap<LayerAction, LayerClassifier> instance_classifier_hashmap = this.iris_xcs.get_instance_classifier(this.current_iris_state);// instance_testMode // todo: return action anticipated reward
        //return this.iris_xcs.irisMeasure(this.speciesType);
        //this.iris_xcs.irisReward(this.speciesType);
        return instance_classifier_hashmap;
    }


    public HashMap<LayerAction, Double> get_instance_action_anticipatedPRD(Instance singleTask,int test_exploreRate, int test_iterationCount_threshold,int test_GA_threshold)
    {

        reinforcer_factory.set_perception_state_for_instance(which_task,"no_boundary",singleTask.attribute); //no_boundary for the test mode
        this.current_iris_state= getPerceptionState(which_task);
        System.out.println("current_iris_state("+this.current_iris_state+")");

        this.iris_xcs.setExploreRate(test_exploreRate);
        this.iris_xcs.setIterationCount_threshold(test_iterationCount_threshold);
        this.iris_xcs.setGATheshold(test_GA_threshold);
        //todo: return anticipated reward here
        HashMap<LayerAction, Double> anticipated_reward_hashmap = this.iris_xcs.get_instance_action_anticipatedPRD_GO_filters(this.current_iris_state);// instance_testMode // todo: return action anticipated reward
        //return this.iris_xcs.irisMeasure(this.speciesType);
        //this.iris_xcs.irisReward(this.speciesType);
        return anticipated_reward_hashmap;
    }

    public void niche_strength_advocate(int test_exploreRate, int test_iterationCount_threshold,int test_GA_threshold)
    {
        //System.out.println("Testing : a_singleTask: species("+singleTask.ground_truth+"): attributes: ("+singleTask.attribute+")");
        //reinforcer_factory.setIrisTask_testCellWorld(singleTask);//testEyes// single eye, not crystal eyes. no boundary
        //this.current_iris_state= getPerceptionState("iris");
        //reinforcer_factory.set_perception_state_for_instance(which_task,"no_boundary",singleTask.attribute); //no_boundary for the test mode
        //this.current_iris_state= getPerceptionState(which_task);
        //System.out.println("current_iris_state("+this.current_iris_state+")");

        //this.speciesType=singleTask.ground_truth;// result, for reward

        //todo: set exploit mode
        //test_exploreRate=0
        //test_iterationCount_threshold=0
        //test_GA_threshold=1500000000
        this.iris_xcs.setExploreRate(test_exploreRate);
        this.iris_xcs.setIterationCount_threshold(test_iterationCount_threshold);
        this.iris_xcs.setGATheshold(test_GA_threshold);
        this.iris_xcs.niche_strength_advocate(this.iteration, this.current_iris_state); // niche strength

        //return this.iris_xcs.irisMeasure(this.speciesType);
        //this.iris_xcs.irisReward(this.speciesType);
    }

    public void set_singleStep_groundTruth_for_Instance(double groundTruth)
    {
        this.speciesType= groundTruth;
    }


    /****  set perception and go to 4 filters for an iteration*************************/
    public void train_4filters_for_instance(double[] attributes,int iteration)
    {
        //this is the iteration training process 4 filter.
        this.iteration= iteration;
        //System.out.println("Training : ("+attributes[0]+"/"+attributes[1]+"...");

        //1. get instance before perception, no boundary
        reinforcer_factory.set_perception_state_for_instance(which_task, "no_boundary",attributes);// no_boundary
        this.current_iris_state= getPerceptionState(which_task);
        //debug ok//System.out.println("current_iris_state("+this.current_iris_state+")  <--no boundary");


        //2. get agent's perception, with boundary

        LayerCondition agent_current_state_representation;
        reinforcer_factory.set_perception_state_for_instance(which_task, "yes_boundary",attributes);// yes_boundary, maze perception.
        agent_current_state_representation= getPerceptionState(which_task);
        //debug ok//
        System.out.println("agent_current_state_representation("+agent_current_state_representation+")  <--yes boundary");

        //3. go to iterations
       this.iris_xcs.iterateIris_Learning(this.current_iris_state, agent_current_state_representation, this.iteration );//single, crystalEyes


    }

    /****  maze ************/
    public void train_4filters(double[] perception, double[][] perception_boundary, int iteration)
    {
        //this is the iteration training process 4 filter.
        this.iteration= iteration;
        //debug ok //System.out.println("Training : ("+perception[0]+"/"+perception[1]+"... whichtask:"+ which_task);

        //1. get instance before perception, no boundary
        reinforcer_factory.set_perception_state_with_boundary(which_task, "no_boundary",perception, perception_boundary);// no_boundary, maze
        this.current_iris_state= getPerceptionState(which_task);
        //debug ok//
        System.out.println("current_iris_state("+this.current_iris_state+")  <--no boundary");

        //2. get agent's perception, with boundary

        LayerCondition agent_current_state_representation;
        reinforcer_factory.set_perception_state_with_boundary(which_task, "yes_boundary",perception, perception_boundary);// yes_boundary, maze perception.
        agent_current_state_representation= getPerceptionState(which_task);
        //debug ok//
        System.out.println("agent_current_state_representation("+agent_current_state_representation+")  <--yes boundary");

        //3. go to iterations
        this.iris_xcs.iterate_Learning(this.current_iris_state, agent_current_state_representation, this.iteration, perception_boundary);// maze

    }

    public int test_mode_filters(double[] perception, double[][] perception_boundary, int iteration)
    {
        //1. get instance before perception, no boundary
        reinforcer_factory.set_perception_state_with_boundary(which_task, "no_boundary",perception, perception_boundary);// no_boundary, maze
        this.current_iris_state= getPerceptionState(which_task);
        //debug ok//
        System.out.println("current_iris_state("+this.current_iris_state+")  <--no boundary");

        //2. get agent's perception, with boundary

        LayerCondition agent_current_state_representation;
        reinforcer_factory.set_perception_state_with_boundary(which_task, "yes_boundary",perception, perception_boundary);// yes_boundary, maze perception.
        agent_current_state_representation= getPerceptionState(which_task);
        //debug ok//
        System.out.println("agent_current_state_representation("+agent_current_state_representation+")  <--yes boundary");

        //3. go to iterations
        return this.iris_xcs.iterate_testing(this.current_iris_state, agent_current_state_representation, this.iteration, perception_boundary);// maze test mode

    }

    public int mcts_mode_filters(double[] perception, double[][] perception_boundary, int iteration)
    {
        //1. get instance before perception, no boundary
        reinforcer_factory.set_perception_state_with_boundary(which_task, "no_boundary",perception, perception_boundary);// no_boundary, maze
        this.current_iris_state= getPerceptionState(which_task);
        //debug ok//
        System.out.println("MCTS : current_iris_state("+this.current_iris_state+")  <--no boundary");

        //2. get agent's perception, with boundary

        LayerCondition agent_current_state_representation;
        reinforcer_factory.set_perception_state_with_boundary(which_task, "yes_boundary",perception, perception_boundary);// yes_boundary, maze perception.
        agent_current_state_representation= getPerceptionState(which_task);
        //debug ok//System.out.println("MCTS : agent_current_state_representation("+agent_current_state_representation+")  <--yes boundary");

        //3. go to iterations
       return this.iris_xcs.iterate_mcts(this.current_iris_state, agent_current_state_representation, this.iteration, perception_boundary);// maze MCTS mode
    }

    public void mcts_establish_MacthSet_ActionSet_filters(double[] perception, double[][] perception_boundary, int action)
    {
        //1. get instance before perception, no boundary
        reinforcer_factory.set_perception_state_with_boundary(which_task, "no_boundary",perception, perception_boundary);// no_boundary, maze
        this.current_iris_state= getPerceptionState(which_task);
        //debug ok//System.out.println("MCTS : current_iris_state("+this.current_iris_state+")  <--no boundary");

        //2. get agent's perception, with boundary

        LayerCondition agent_current_state_representation;
        reinforcer_factory.set_perception_state_with_boundary(which_task, "yes_boundary",perception, perception_boundary);// yes_boundary, maze perception.
        agent_current_state_representation= getPerceptionState(which_task);
        //debug ok//System.out.println("MCTS : agent_current_state_representation("+agent_current_state_representation+")  <--yes boundary");

        //3. establish->MacthSet->ActionSet-> mcts_messages
        this.iris_xcs.mcts_establish_MacthSet_ActionSet_filters(this.current_iris_state, agent_current_state_representation, this.iteration, perception_boundary, action);// maze MCTS mode
    }

    public int get_action()
    {
        return this.iris_xcs.get_action();
    }
    public HashMap<LayerAction, Double> get_actions_reward_hashmap(String training_mode)
    {
        return this.iris_xcs.get_actions_reward_hashmap(training_mode);
    }
    /** maze problem, initial_filters_portfolio**/
    public void initial_filters_portfolio()
    {
        this.iris_xcs.initial_filters_portfolio(); // set 4 filters as portfolio
    }

    public void initial_test_filters_portfolio()
    {
        this.iris_xcs.initial_test_filters_portfolio();  //set match filter and selction filter
    }

    public void initial_mcts_filters_portfolio()
    {
        this.iris_xcs.initial_mcts_filters_portfolio();  //set match filter and selction filter
    }

    public void create_classifier_through_learningAgent(int id, double[] condition_attributes_arr, String action_mask, double[] statistic) // todo: set_condition_filter, set_action_filter, set_statistic_filter
    {
        // generate condition
        LayerCondition condition_established=  new LayerCondition(reinforcer_factory.get_perception_state_for_classifier(condition_attributes_arr)); //got a layercondition

        //goto layerProblem to establish a classifier
        this.iris_xcs.create_a_classifier_through_layerProblem(id, condition_established, action_mask, statistic);
    }

    public boolean getExploreMode()
    {
        return this.iris_xcs.getExploreMode();
    }


    private LayerCondition getPerceptionState(String perceptionType)
    {
        LayerCondition perceptionState;

        if(perceptionType.equals("iris"))
        {
            perceptionState= new LayerCondition(reinforcer_factory.getIrisTask());//location's cell work.
        }else
        {
            perceptionState= null;
        }

        if(perceptionType.equals("instance"))
        {
            perceptionState= new LayerCondition(reinforcer_factory.get_perception_state_for_instance());//location's cell work.
        }

        return perceptionState;
    }

}

