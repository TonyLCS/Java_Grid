package main.java.com.github.rosjava.my_pub_sub_tutorial.Iris;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


import com.opencsv.CSVWriter;

import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.*;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerCondition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerProblem;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;

import org.hibernate.hql.internal.ast.tree.BooleanLiteralNode;

import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.*;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javassist.bytecode.Descriptor;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ClassifierExpectation;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.FitnessCount;

import com.thoughtworks.xstream.XStream;



public class IrisCheckPoints
{

    private HashMap<Integer, Double> checkPointLog = new HashMap<Integer, Double>();
    private HashMap<Integer, Double> fail_task_log = new HashMap<Integer, Double>();
    private HashMap<Integer, Double> checkPointLog_SuccessNum = new HashMap<Integer, Double>();
    private List<String[]> checkPointCSV = new ArrayList<String[] >();
    private CSVWriter csv_writer ;

    private double successRateChechPoint=0;

    public IrisCheckPoints(){
        String[] s = new String[2];
        s[0]= "iteration";
        s[1]= "performanceRate";

        this.checkPointCSV.add(s);
    }

    public void cleanFailureTaskLog()
    {
        fail_task_log=new HashMap<Integer, Double>();

    }

    public void saveFailureTaskSeq(int seq)
    {
        Integer seqNum =(Integer)seq;
        if(this.fail_task_log.containsKey(seqNum) )
        {
            double logValue=1+this.fail_task_log.get(seqNum);
            this.fail_task_log.put(seqNum,logValue );
        }
        else
        {
            double logValue=1;
            this.fail_task_log.put(seqNum, logValue);
        }
    }

    public void saveCheckPoints(double performanceRate, int iteration )
    {

        this.checkPointLog.put((Integer)iteration,performanceRate);

        savePreformance("./Results/ChechPoint");

    }

    public void saveCheckPoints_csv_file(double performanceRate, int iteration )
    {
        String[] s = new String[2];
        s[0]= Integer.toString(iteration);
        s[1]= Double.toString(performanceRate);

        this.checkPointCSV.add(s);
        try
        {
            System.out.println("CSV version: CheckP");
            Path path = Paths.get("");
            String s_path =path.toAbsolutePath().toString();
            //System.out.println("current working path is:"+s_path);
            String checkPoint_filename=s_path+"/Results/CheckP.csv";

            //CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/CheckP.csv"));
            //csv_writer= new CSVWriter(new FileWriter("./Results/CheckP.csv"));
            csv_writer= new CSVWriter(new FileWriter(checkPoint_filename));
            csv_writer.writeAll(this.checkPointCSV);
            csv_writer.close();

        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public void saveCheckPointsSuccessNum(double successNum, int iteration )
    {

        this.checkPointLog_SuccessNum.put((Integer)iteration,successNum);

        //savePreformance("irisChechPointFailure");
    }


    private void savePreformance(String file)
    {
        System.out.println("iris save record ");

        try
        {
            XStream xstream = new XStream();
            String xml = xstream.toXML(this);
            BufferedWriter xmlOut = new BufferedWriter(new FileWriter(file+".xml"));
            xmlOut.write(xml);
            xmlOut.close();
            //System.out.println("save Management: file("+file+")");


        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        //System.out.println("test Management: end of saving procedural ");

    }
}

