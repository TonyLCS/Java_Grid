package main.java.com.github.rosjava.my_pub_sub_tutorial.LinearRegression;

//copy all import from LayerProblem.java
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Stack;

import javax.swing.JPanel;

import main.java.com.github.rosjava.my_pub_sub_tutorial.Jama.Matrix;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.Filters.LayerModifyFilter;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Debug;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters.*;

import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.Filters.LayerExecutionFilter;// add for new layer1's execution.

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

//for Matrix
import java.util.Arrays;



public class LinearRegression {

    public LinearRegression() {
    }


    public Matrix create_matrix_A(int num_step, int num_action)
    {
        //System.out.println("create_matrix_A for step("+num_step+") and action("+num_action+") ");
        // set action_matrix as dialog matrix, each row of this matrix represents an possible action applied
        Matrix actions_matrix= new Matrix(num_action,num_action,0);
        for(int i=0; i<num_action;i++)
        {
            for(int j=0; j<num_action;j++)
            {
                if(i==j)
                {
                    actions_matrix.set(i,j,1);
                }
                else
                {
                    actions_matrix.set(i,j,0);
                }
            }
        }
        //check
        /*
        for(int i=0; i<actions_matrix.getRowDimension();i++)
        {
            for(int j=0;j<actions_matrix.getColumnDimension();j++ )
            {
                System.out.println("	show actions_matrix["+i+"]["+j+"] ("+actions_matrix.get(i,j)+") ");
            }
            System.out.println("	");
            //System.out.println("	show actions_matrix ("+actions_matrix.get(i,0)+", "+actions_matrix.get(i,1)+")" + ", "+actions_matrix.get(i,2)+"), "+actions_matrix.get(i,3)+")");
        }
        */


        // matrix_A contains all situations that actions combination in steps
        // column of matrix_A is num_step*num_action
        // row of matrix_A is num_action power with index num_step
        // setup a list, action_sequence, before the matrix_A can be established,

        int num_of_colunm   = num_step*num_action;
        int num_of_row      = (int)Math.pow((double)num_action,num_step);
        Matrix matrix_A     = new Matrix(num_of_row,num_of_colunm,0);
        // organise the action into sequence. the sequence for matrix_A. the sequence is in an order of steps.
        Matrix action_sequence = new Matrix(num_of_row*num_step,num_action);


        int whichrow=0;
        for(int j=0; j<num_step;j++)
        {// for each step
            int repeat_action_time= (int)Math.pow((double)num_action,(num_step-j-1));
            int action_group_repeat_time= (int)( (num_of_row/repeat_action_time )/num_action);
            for(int act_group_index=0; act_group_index<action_group_repeat_time; act_group_index++)
            {

                for(int a=0; a<num_action; a++)
                {
                    //for each action
                    for(int repeat_index=0; repeat_index<repeat_action_time; repeat_index++)
                    {
                        //put action(a row with id=a ) in action_matrix) representation in to this list sequencially.
                        // see diary Jan.18
                        // todo:  chech if this is necessary ? action_sequence=
                        //action_sequence=
                        fill_matrix_row_with_block(action_sequence, actions_matrix, whichrow,0,a,0,num_action);
                        whichrow++;
                    }

                }
            }


        }

        //check
        /*
        for(int i=0; i<action_sequence.getRowDimension();i++)
        {
            for(int j=0;j<action_sequence.getColumnDimension();j++ )
            {
                System.out.println("	show action_sequence["+i+"]["+j+"] ("+action_sequence.get(i,j)+") ");
            }
            System.out.println("	");
            //System.out.println("	show actions_matrix ("+actions_matrix.get(i,0)+", "+actions_matrix.get(i,1)+")" + ", "+actions_matrix.get(i,2)+"), "+actions_matrix.get(i,3)+")");
        }
        */


        //fill the matrix_a step by step
        // start filling step 1,then step 2...
        int action_sequence_index=0;
        for(int j=0; j<num_step;j++)
        {
            for(int i=0; i< num_of_row;i++)
            {
                //fill matrix_A with action block, which is one specific row in actions_matrix
                int location_row= i;
                int location_column= j*num_action;

                //fill_matrix_with_block(matrix_A, action_sequence[action_sequence_index], location_row,location_column);
                //matrix_A=
                fill_matrix_row_with_block(matrix_A, action_sequence, location_row, location_column,action_sequence_index,0,num_action);
                action_sequence_index++;
            }

        }

        /* check
        for(int i=0; i<matrix_A.getRowDimension();i++)
        {
            for(int j=0;j<matrix_A.getColumnDimension();j++ )
            {
                System.out.println("	show matrix_A["+i+"]["+j+"] ("+matrix_A.get(i,j)+") ");
            }
            System.out.println("	");
            //System.out.println("	show actions_matrix ("+actions_matrix.get(i,0)+", "+actions_matrix.get(i,1)+")" + ", "+actions_matrix.get(i,2)+"), "+actions_matrix.get(i,3)+")");
        }
        */

        return matrix_A;
    }


//    public Matrix span_orthogonal_reward_space(double batch_reward,int O, int S,int T, Matrix orthogonal_behaviours_matrix, Stack<Integer> step_action_applied_stack)
//    {
//
//      //get parameter r, o, s, t
//
//        //1 set up a orthogonal_behaviours_matrix
//        //2. put  batch_reward_matrix according to the behaviour_matrix
//        //3. orthogonal_reward_space is a hashmap < <"orthogonal_behaviours_matrix", matrix >,<"batch_reward_matrix", matrix>>
//
//
//
//        orthogonal_behaviours_matrix = establish_orthogonal_behaviours_matrix(S,T, orthogonal_behaviours_matrix, step_action_applied_stack);
//
//       return orthogonal_behaviours_matrix;
//    }

    public Matrix initate_orthogonal_behaviours_matrix(int S, int T)
    {
        Matrix orthogonal_behaviours_matrix= new Matrix(S*T, S*T,0);

        /**
         * additional row is (S-1). but here set it as S, by increasing a line, to make thing easy.
         * Does not need to consider which one step has the full rank space
         * **/
        int additional_row = S;

        for(int i=0; i< additional_row; i++)
        {
            // try to fill the representation, which has representation like [111,000, 000, ...], with a row_representation
            for(int j=0; j<T; j++)
            {
                // set all the actions in a step as 1, this is a wild case
                //orthogonal_behaviours_matrix.set(i,j+((i+1)*T),1);
                orthogonal_behaviours_matrix.set(i,j+(i*T),1);

            }

        }

        System.out.println( "   initate_orthogonal_behaviours_matrix >>>>   initial rank("+orthogonal_behaviours_matrix.rank()+") ");

        return  orthogonal_behaviours_matrix;
    }

    /**
     *   establish a reward space for fexible number of scenarios, steps and actions in a dynamic process.
     *   the reward space is constructed by orthogonal vectors
     *   these orthoganal vects comes from the representation of cases of behaviours patterns.
     *
     */

    public Matrix initate_reward_received_matrix(int S, int T)
    {
        Matrix reward_received_matrix =new Matrix(S*T,1,0);

        double rare_predefined_reward=-1000*(T-2);// 1000+ -1000+ -1000
        //for(int i=0; i<(S-1); i++)
        for(int i=0; i<S; i++)
        {
            reward_received_matrix.set(i,0, rare_predefined_reward);
        }
        
        return reward_received_matrix;
    }


    /**
     *
     * **/
    public Matrix establish_orthogonal_behaviours_matrix(int S, int T, Matrix orthogonal_behaviours_matrix, Stack<Integer> step_action_applied_stack)
    {
        //int orthogonal_behaviours_matrix_rank= T+ (S-1)*(T-1);
        int orthogonal_behaviours_matrix_rank= S*T;
        int current_rank= orthogonal_behaviours_matrix.rank();
        int row_index=current_rank;

        if(orthogonal_behaviours_matrix_rank == current_rank)
        {
            System.out.println( " establish_orthogonal_behaviours_matrix   reach full rank >>>>>> current_rank("+current_rank+")");
            return orthogonal_behaviours_matrix;
        }
        // get what actions are applied in the last OST
        Matrix current_row_representataion= get_row_representation_from_step_action_applied_stack(S, T, step_action_applied_stack);


        fill_matrix_row_with_block(orthogonal_behaviours_matrix, current_row_representataion, row_index,0,0,0,S*T);
        int new_rank= orthogonal_behaviours_matrix.rank();

        System.out.println( " establish_orthogonal_behaviours_matrix    >>>>>> current_rank("+current_rank+") =>new_rank("+new_rank+")      ");
        return orthogonal_behaviours_matrix;
    }


    public Matrix establish_reward_received_matrix(int row_num, Matrix reward_received_matrix,double batch_reward)
    {

        reward_received_matrix.set(row_num,0,batch_reward);
        return reward_received_matrix;
    }


    public Matrix get_row_representation_from_step_action_applied_stack(int S,int T, Stack<Integer> step_action_applied_stack)
    {
        int dimension_column= S*T;
        Matrix a_row_representation= new Matrix(1,dimension_column,0);


        int step_action_applied_stack_index=0;
        for(Integer an_action_mask:step_action_applied_stack)
        {
            // the first one
            int a_step_action_index= T*step_action_applied_stack_index+ an_action_mask.intValue()-1;
            //System.out.println(">>>>  get_row_representation_from_step_action_applied_stack : action mask("+an_action_mask+")," + " a_step_action_index("+a_step_action_index+")");

            a_row_representation.set(0,a_step_action_index,1);
            step_action_applied_stack_index++;
        }

        return a_row_representation;
    }

    public Matrix fill_matrix_row_with_block(Matrix des_matrix, Matrix src_matrix, int des_location_row, int des_location_column, int src_location_row, int src_location_column, int length)
    {
        // move the number of length data from src_matrix to des_matrix
        for(int k=0; k<length;k++)
        {
            des_matrix.set(des_location_row,des_location_column+k,src_matrix.get(src_location_row,k+src_location_column));
        }
        return des_matrix;
    }



    public Matrix calculate_reward_all_steps_matrix(int S, int T, Matrix behaviours_matrix, Matrix reward_received_matrix)
    {
        System.out.println(" >>>> calculate_reward_all_steps_matrix");

        int full_rank=S*T;// number of step * number of action


        //A * x = b;
        // A = compressed_matrix  = behaviours_matrix
        // b = matrix_compressed_received_reward = reward_received_matrix
        // x = reward_steps_Matrix = reward_step_Matrix

        //initate reward_steps_matrix
        Matrix reward_steps_Matrix = new Matrix(full_rank,1,-1); //x

        if(behaviours_matrix.rank()==full_rank)
        {

            System.out.println(" ");
            reward_steps_Matrix= behaviours_matrix.solve(reward_received_matrix);

            //System.out.println("  matric result:( "+reward_steps_Matrix.get(0,0)+")," + " ( "+reward_steps_Matrix.get(1,0)+"), ( "+reward_steps_Matrix.get(2,0)+")");
            //System.out.println("  matric result:( "+reward_steps_Matrix.get(3,0)+")," + " ( "+reward_steps_Matrix.get(4,0)+"), ( "+reward_steps_Matrix.get(5,0)+")");
            //System.out.println("  matric result:( "+reward_steps_Matrix.get(6,0)+")," + " ( "+reward_steps_Matrix.get(7,0)+"), ( "+reward_steps_Matrix.get(8,0)+")");
            String s = ">>>>>>>>>>>>";
            for(int i=0; i< reward_steps_Matrix.getRowDimension();i++)
            {
                s = s + "reward_steps_Matrix["+i+"]"+"("+ reward_steps_Matrix.get(i,0)+") \t  \t" ;
            }
            System.out.println(s);
        }
        else
        {
            System.out.println(" not yet!!!, try even method.");


        }


        return reward_steps_Matrix;
    }

    public boolean debug_check_reward_calculation(Matrix reward_steps_Matrix)
    {
        boolean pass_diff_check_flag=true;
        for(int i=0; i< reward_steps_Matrix.getRowDimension();i++)
        {
            double a_calculation_result =  reward_steps_Matrix.get(i,0);

            if(a_calculation_result>=0)
            {
                double diff=Math.abs(a_calculation_result-1000);
                if (diff>10)
                {
                    pass_diff_check_flag=false;
                }
            }

            if(a_calculation_result<0)
            {
                double diff=Math.abs(a_calculation_result+1000);
                if (diff>10)
                {
                    pass_diff_check_flag=false;
                }
            }
        }
        return pass_diff_check_flag;
    }

    public Matrix extract_reward_S_step_from_all(int S, Matrix  reward_all_steps_matrix,int[] step_lineIndex_column)
    {

        Matrix reward_S_matrix= new Matrix(S,1,0);
        //int[] step_lineIndex_column =matrix_get_applied_column_from_messages();
        //update rewardmap
        //Matrix matrix_steps_rewards= new Matrix(3,1);

        for(int i=0; i< S; i++)
        {
            int update_index = step_lineIndex_column[i];
            double update_value = reward_all_steps_matrix.get(update_index,0);
            reward_S_matrix.set(i,0,update_value);
            System.out.println("  extract_reward_S_step_from_all: matrix_steps_rewards :( "+update_value+") for step("+i+")");
        }


        return reward_S_matrix;
    }

}