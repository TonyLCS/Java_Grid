package main.java.com.github.rosjava.my_pub_sub_tutorial.LinearRegression;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import com.thoughtworks.xstream.XStream;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.*;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerCondition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerProblem;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;


import org.hibernate.hql.internal.ast.tree.BooleanLiteralNode;


import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.*;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;



public class RewardBoundaryCheck
{

    private HashMap<Integer, Integer> error_log   =  new HashMap<Integer, Integer>();
    //<iteration, O>

    public RewardBoundaryCheck(){}

    public void save_reward_error_log(int task_seq, int O )
    {

        this.error_log.put((Integer)task_seq, (Integer)O);

        savePreformance("reward_boundary_error");

    }




    private void savePreformance(String file)
    {
        System.out.println("reward boundary error record ");

        try
        {
            XStream xstream = new XStream();
            String xml = xstream.toXML(this);
            BufferedWriter xmlOut = new BufferedWriter(new FileWriter(file+".xml"));
            xmlOut.write(xml);
            xmlOut.close();
            //System.out.println("save Management: file("+file+")");


        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        //System.out.println("test Management: end of saving procedural ");

    }
}

