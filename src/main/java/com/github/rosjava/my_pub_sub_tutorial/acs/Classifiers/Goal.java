package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;

public abstract class Goal implements Comparable<Goal>
{
	public abstract boolean isComplete(Condition nextState);	
}
