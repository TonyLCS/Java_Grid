package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.awt.Graphics2D;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Modifier_ContinuouslyProjected implements Cloneable
{
	/**
	 * 
	 */

	private HashMap<Integer, Modifier_Niche_Attribute_Continuous> modifier = new HashMap<Integer, Modifier_Niche_Attribute_Continuous>();


	public Modifier_ContinuouslyProjected(){}

	public Modifier_ContinuouslyProjected(int how_many_attributes)
	{
		for(int i=0;i<how_many_attributes;i++)
		{
			this.modifier.put(i, new Modifier_Niche_Attribute_Continuous());
		}
	}
	public Modifier_Niche_Attribute_Continuous get_attribute(int index)
	{
		return  this.modifier.get(index);
	}

	public void set_attribute(int index, Modifier_Niche_Attribute_Continuous attribute)
	{
		this.modifier.put(index, attribute);
	}
	public int get_size()
	{
		return modifier.size();
	}

	public void translate_attributes_into_matrix()
	{
		for(int i=0; i< this.modifier.size();i++)
		{
			//translate attribute from LinkedList into two double array
			this.modifier.get(i).translate_into_matrix();
			//show best
			/*
			Integer[] indexs=this.modifier.get(i).search_best_niche_index();
			this.modifier.get(i).best_niche_upperAndLower(indexs);
			*/
		}
	}
	public void show_attributes()
	{
		for(int i=0; i< this.modifier.size();i++)
		{
			//translate attribute from LinkedList into two double array
			this.modifier.get(i).show_loc_points();
			this.modifier.get(i).show_prd_values();

		}
	}

	public void save_niche_distributions(String s)
	{
		String loc_points_filename= "loc_points";
		String prd_values_filename= "prd_values";

		save_loc_points(s);
		save_prd_values(s);

	}

	public void save_prd_values(String s)
	{
		String file_name= "prd_values";

		for(int i=0; i<modifier.size();i++)
		{
			// add header
			String a_row_data_s=s;

			// add attributes
			a_row_data_s += "Attribute No."+Integer.toString(i)+ "#";
			a_row_data_s += modifier.get(i).prd_values_toString_stream();

			save_a_row_string_to_csv( file_name,  a_row_data_s);
		}
	}

	public void save_loc_points(String s)
	{
		String file_name= "loc_points";

		for(int i=0; i<modifier.size();i++)
		{
			// add header
			String a_row_data_s=s;

			// add attributes
			a_row_data_s += "Attribute No."+Integer.toString(i)+ "#";
			a_row_data_s += modifier.get(i).loc_points_to_String_stream();

			save_a_row_string_to_csv( file_name,  a_row_data_s);
		}
	}

	public void save_a_row_string_to_csv(String file_name, String a_row_data_s)
	{

		List<String[]> data;
		//1. read csv
		try
		{
			FileReader f= new FileReader("./Results/"+file_name+".csv");
			CSVReader csv_reader= new CSVReader(f);
			data= csv_reader.readAll();

			//2.add current data //transform to hashmap

			String[] s_data_to_add= a_row_data_s.split("#");
			data.add(s_data_to_add);
			//3. write csv Data

			try
			{
				System.out.println("CSV version:  the Modifier_Continuous");

				CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/"+file_name+".csv"));
				csv_writer.writeAll(data);
				csv_reader.close();
				csv_writer.close();

			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}


	}


	public void	save_a_row_to_csv(String file_name, String[] a_row_data)
	{
		List<String[]> data =new ArrayList<String[]>() ;
		//1. read csv
		try
		{
			FileReader f= new FileReader("./Results/"+file_name+".csv");
			CSVReader csv_reader= new CSVReader(f);
			data= csv_reader.readAll();
			// add a row of data into data
			data.add(a_row_data);

			//3. write csv Data
			try
			{
				System.out.println("save_csv_string to filename("+file_name+")");

				CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/"+file_name+".csv"));
				csv_writer.writeAll(data);
				csv_reader.close();
				csv_writer.close();

			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

	}

}
