package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;


public class FitnessCount implements Comparable<FitnessCount>
{
	private int count;
	
	private double fitness;




	public double Sum_FitNumPrd=0;
	public double Sum_FitNum=0;
	public double vote=0;


	public double Sum_AccNumPrd=0;
	public double Sum_Acc=0;
	public double vote_acc=0;


	public int exploreCount;

	//Martin's explore method
	public double PA = 0;
	public double FSA = 0;
	
	public FitnessCount(double fitness)
	{
		this.fitness = fitness;
		this.count = 1;
	}
	public FitnessCount(boolean explore){
		if(explore)
		{
			exploreCount=1;
		}
	}

	public FitnessCount(double fitness, double numerous, double predictedReward, double accuracy)
	{
		//Tony: for Vote method
		//this.vote = fitness*numerous*predictedReward;// ask Will, should I consider cl.num in this procedural????
		//this.vote_acc= accuracy*numerous*predictedReward;
		this.vote = fitness*predictedReward;// ask Will, should I consider cl.num in this procedural????
		this.vote_acc= accuracy*predictedReward;
	}

	public FitnessCount(double fitness,double predictedReward)
	{
		//Martin : for Vote method
		//1. initiate
		this.PA = fitness*predictedReward;//
		this.FSA = fitness;
	}

	public void addFitness(double fitness,double predictedReward)
	{
		//Martin : for Vote method
		//2. go through entire set
		this.PA += fitness*predictedReward;//
		this.FSA += fitness;
	}

	public double getVote_PA_FSA()
	{
		this.vote= this.PA/this.FSA;
		return this.vote;
	}

	public void addFitness(double fitness)
	{
		this.fitness = this.fitness * count;
		this.count++;
		this.fitness += fitness;
		this.fitness /= this.count;
	}

	public void addFitness(double fitness, double numerous, double predictedReward, double accuracy)
	{
		//Tony: for Vote method
		this.Sum_FitNumPrd= this.Sum_FitNumPrd+fitness*predictedReward;// ask Will, should I consider cl.num in this procedural????
		this.Sum_FitNum =this.Sum_FitNum+ fitness;
		this.vote=this.Sum_FitNumPrd/this.Sum_FitNum;

		this.Sum_AccNumPrd= this.Sum_AccNumPrd+accuracy*predictedReward;
		this.Sum_Acc = this.Sum_Acc+ accuracy;
		this.vote_acc= this.Sum_AccNumPrd/this.Sum_Acc;
		/*
		this.Sum_FitNumPrd= this.Sum_FitNumPrd+fitness*numerous*predictedReward;// ask Will, should I consider cl.num in this procedural????
		this.Sum_FitNum =this.Sum_FitNum+ fitness*numerous;
		this.vote=this.Sum_FitNumPrd/this.Sum_FitNum;

		this.Sum_AccNumPrd= this.Sum_AccNumPrd+accuracy*numerous*predictedReward;
		this.Sum_Acc = this.Sum_Acc+ accuracy*numerous;
		this.vote_acc= this.Sum_AccNumPrd/this.Sum_Acc;
		* */
	}

	public void addExploreCount()
	{
		this.exploreCount++;
	}

	public double getVote()
	{
		this.vote= this.PA/this.FSA;
		return this.vote;
	}
	public double getVoteAcc()
	{
		return this.vote_acc;
	}

	public int getExploreCount(){ return this.exploreCount;}

	//TODO:Tony
	public double getFitness()
	{
		return this.fitness;
	}

	@Override
	public int compareTo(FitnessCount other) 
	{
		return (int)(this.fitness - other.fitness);
	}
}