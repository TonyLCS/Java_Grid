package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

public class SelectFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{
	private boolean exploit = false;
	private double explore_rate;

	public static final String NAME = "Select Filter";
	
	public SelectFilter()
	{
		super(NAME);
	}
	
	public SelectFilter(double explore_rate)
	{
		super(NAME);
		this.explore_rate = explore_rate;
	}

	@Override
	public FilterMessage<C,A> apply(P agent, int iterationCount, FilterMessage<C,A> message) 
	{
		ArrayList<Integer> matchSet = message.getMatchSet();
		ArrayList<Integer> action_set = new ArrayList<Integer>();
		//I move the explore procedural ahead to make this decision match this action
		double r = Math.random() * 100;
		if(r > explore_rate)
			exploit = true;
		else
			exploit = false;
		A action = agent.getNextAction(message.getCurrentState(), matchSet, action_set, exploit);

		/*move ahead
		double r = Math.random() * 100;
		if(r > explore_rate)
			exploit = true;
		else
			exploit = false;
		*/

		printMessage("(r ('"+r+"') VS explore_rate('"+ explore_rate+"'), exploit= '"+exploit+"' ", agent);
		printMessage("action_set.size: " + action_set.size()+"; Selected action: " + action, agent);
		printMessage("action_set: '" + action_set+"'<-", agent);
		//printMessage("Selected Action: " + action, agent);
		
		message.setCurrentAction(action);
		message.setActionSet(action_set);
		//population.getPredictionFromAction(action);
		return message;
	}
	
	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +":::: "+message;
		agent.printMessage(s);
	}	
}
