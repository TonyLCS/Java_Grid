package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;
import java.util.HashMap;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

public class FilterMessage<C extends Condition<C>, A extends ActionInterface> 
{	
//	private Goal goal; 
//	
//	public Goal getGoal() {
//		return goal;
//	}
//
//	public void setGoal(Goal goal) {
//		this.goal = goal;
//	}

	private C lastState;
	private C currentState;
	private C nextState;


	private C currentREState;
	private C currentState_representation;

	private double[] currentRatState;

//	private int predictedNextState = -1;

	private ArrayList<Integer> matchSet;
	private ArrayList<Integer> actionSet;
	private ArrayList<Integer> matchActionSet;
	private ArrayList<Integer> mitosisSet;

	private A lastAction;
	private A currentAction;

	//anticipated_reward
	private double anticipated_reward;
	private boolean explore_mode;

	//record  actions_reward_hashmap
	private HashMap<A, Double> current_actions_reward_hashmap = new HashMap<A, Double>();
	private HashMap<A, Double> last_actions_reward_hashmap = new HashMap<A, Double>();

	private boolean complete = false;
	public void setComplete(){
		this.complete = true;
	}

	public double[][] boundary;

	public FilterMessage(){
	}
	
	public FilterMessage(FilterMessage<C, A> message)
	{
		this.lastState = message.lastState;
		this.currentState = message.currentState;
		
		this.matchSet = message.matchSet;
		this.actionSet = message.actionSet;
		
		this.currentAction = message.currentAction;
		this.lastAction = message.lastAction;
		
		this.complete = message.complete;
		
		this.currentREState = message.currentREState;
		this.currentREState =message.currentState_representation;

		//anticipated_reward
		this.anticipated_reward= message.anticipated_reward;
		this.explore_mode= message.explore_mode;

		//match action set
		this.matchActionSet= message.matchActionSet;
	}

	public ArrayList<Integer> getMatchSet() {
		return matchSet;
	}

	public void setMatchSet(ArrayList<Integer> matchSet) {
		this.matchSet = matchSet;
	}

	public void setMatchActionSet(ArrayList<Integer> matchActionSet) {
		this.matchActionSet = matchActionSet;
	}

	public ArrayList<Integer> getMatchActionSet() {
		return this.matchActionSet;
	}

	public ArrayList<Integer> getActionSet() {
		return actionSet;
	}
	public ArrayList<Integer> getMitosisSet() {
		return this.mitosisSet;
	}

	public void setActionSet(ArrayList<Integer> actionSet) {
		this.actionSet = actionSet;
	}
	public void setMitosisSet(ArrayList<Integer> mitosisSet) {
		this.mitosisSet = mitosisSet;
	}

	public A getCurrentAction() {
		return currentAction;
	}

	public void setCurrentAction(A currentAction) {
		this.currentAction = currentAction;
	}

	public A getLastAction() {
		return lastAction;
	}

	public void set_current_actions_reward_hashmap(HashMap<A, Double> actions_reward_hashmap)
	{
		this.current_actions_reward_hashmap = actions_reward_hashmap;
	}
	public HashMap<A, Double> get_current_actions_reward_hashmap()
	{
		return this.current_actions_reward_hashmap;
	}
	public void set_last_actions_reward_hashmap(HashMap<A, Double> actions_reward_hashmap)
	{
		this.last_actions_reward_hashmap = actions_reward_hashmap;
	}
	public HashMap<A, Double> get_last_actions_reward_hashmap()
	{
		return this.last_actions_reward_hashmap;
	}

	public void setLastAction(A lastAction) {
		this.lastAction = lastAction;
	}

	public C getCurrentState() {
		return currentState;
	}
	public C getCurrentReState() {
		return currentREState;
	}

	public double[] getCurrentRatState(){return  currentRatState;}

	public void setCurrentState(C currentState) {
		this.currentState = currentState;
	}
	public void setCurrentReState(C currentREState) {
		this.currentREState = currentREState;
	}
	public void setCurrentRatState(double[] currentRatState){this.currentRatState = currentRatState;}

	public void setReState(C currentREState) {
		this.currentREState = currentREState;
	}

	public C getCurrentState_representation()
	{
		return this.currentState_representation;
	}
	public void setCurrentState_representation(C currentState_representation)
	{
		 this.currentState_representation=currentState_representation;
	}

	public C getLastState() {
		return lastState;
	}

	public void setLastState(C condition) {
		this.lastState = condition;
	}
	
	public C getNextState(){
		return this.nextState;
	}

	public void setNextState(C nextState) {
		this.nextState = nextState;
		//goal.isComplete(this.nextState);
	}

	public void set_boundary(double[][] perception_boundary)
	{
		this.boundary = perception_boundary;
	}
	public double[][] get_boundary()
	{
		return  this.boundary;
	}
	public boolean isComplete() {
		return complete;
	}
	
	public FilterMessage<C, A> clone()
	{
		FilterMessage<C, A> message = new FilterMessage<C, A>(this);
		return message;
	}

	public void record_anticipated_reward(double anticipated_reward)
	{
		this.anticipated_reward= anticipated_reward;
	}

	public double get_anticipated_reward()
	{
		return this.anticipated_reward;
	}

	public void record_explore_mode(boolean explore_mode)
	{
		this.explore_mode= explore_mode;
		System.out.println("trace 2 : explore_mode("+this.explore_mode+")");

	}

	public boolean get_explore_mode()
	{
		//System.out.println("trace 3 : explore_mode("+this.explore_mode+")");
		return this.explore_mode;
	}
}
