package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;
import java.util.HashMap;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

public class SelectFSAFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{
	private boolean exploit = false;
	private double explore_rate;// will be override by (" SelectPAFilter(double explore_rate)"), which pass explore_chance to here
	private double iterationCount_threshold;
	public static final String NAME = "Select FSA Filter";
	
	public SelectFSAFilter()
	{
		super(NAME);
	}

	public void set_explore_rate(double explore_rate)
	{
		this.explore_rate = explore_rate;
		System.out.println(" set explore rate as: "+ this.explore_rate);
	}
	public SelectFSAFilter(double explore_rate, double iterationCount_threshold)
	{
		super(NAME);
		this.explore_rate = explore_rate;
		this.iterationCount_threshold= iterationCount_threshold;
	}
	public SelectFSAFilter(double explore_rate)
	{
		super(NAME);
		this.explore_rate = explore_rate;
	}
	
	@Override
	public FilterMessage<C,A> apply(P agent, int iteration, FilterMessage<C,A> message)
	{
		// remove anticipated reward from SelectPAFilter
		//System.out.println("SelectFSAFilter:: begin ");
		ArrayList<Integer> matchSet = message.getMatchSet();
		ArrayList<Integer> action_set = new ArrayList<Integer>();
		//pass the address of anticipated_reward down to getNextAction().
		// 	In getNextAction() will calculate the anticipated_reward, which will be applied in reward.
		ArrayList<Double> anticipated_reward=new ArrayList<Double>();// redundancy
		double an_anticipated_reward=-0.031415926;// redundancy


		//I move the explore procedural ahead to make this decision match this action
		double r = Math.random() * 100;
		if(r >= explore_rate) {exploit = true;}
		else{exploit = false;}
			// exploit habitation. explore protect. no exploit under first 1500 time

		System.out.println("SelectFSAFilter:  iterationCount("+iteration+") " +
				"< iterationCount_threshold("+iterationCount_threshold+"), exploit("+exploit+")");
		if(iteration<iterationCount_threshold) //iterationCount_threshold
		{
			//todo:to test the classifier mask the following line
			//todo: test mode
			exploit = false;
		}
		//1.calculate vote for actions.
		//2.select best action from vote if it is an exploit mode.
		//3. return the action and also set the action_set!
		//4. new exploit mode will also calculate anticipated reward, also return it.
		//A action = agent.getNextAction(message.getCurrentState(), matchSet, action_set, exploit, anticipated_reward);
		A action = agent.getNextAction(message.getCurrentState(),matchSet,action_set,exploit);
		/**get actions-reward-hashmap from selection and save it into message**/
		HashMap<A, Double> actions_reward_hashmap = agent.get_actions_reward_hashmap(matchSet);
		if(actions_reward_hashmap.isEmpty())
		{
			System.out.println("actions_reward_hashmap is empty");
		}
		else
		{
			//save it into message...
			if(actions_reward_hashmap.size()>1)
			{
				message.set_current_actions_reward_hashmap(actions_reward_hashmap);
				for (A key: actions_reward_hashmap.keySet()) {
					System.out.println(" 	show actions_reward_hashmap : action ('" + key + "').vote('" + actions_reward_hashmap.get(key) + "').");
				}

			}
		}

		//check the action_set is setted in the sub-procedural of the above line.
		System.out.println("SelectFSAFilter:: action_set.size() is "+action_set.size()+", and advocate action("+action+")");
		/*debug work
		for(int i=0; i< action_set.size();i++)
		{
			System.out.println("	("+i+"/"+action_set.size()+") classifier.Id("+action_set.get(i)+") in action_set().");
		}
		*/

		//4. pass the action selected and action_set into the message
		message.setCurrentAction(action);
		message.setActionSet(action_set);

		//5. record informations for anticipated reward, such as explore mode(keyword "exploit"), anticipated reward
		//an_anticipated_reward=anticipated_reward.get(0);
		message.record_anticipated_reward(an_anticipated_reward);
		boolean exploreMode;
		if(exploit) {exploreMode=false;}
		else {exploreMode=true;}
		//System.out.println("trace 1 : SelectPAFilter:: exploit("+exploit+") - exploreMode("+exploreMode+")");
		message.record_explore_mode(exploreMode);
		//message.get_explore_mode();

		System.out.println("SelectFSAFilter:: select for action("+action+")\n");

		return message;
	}
	
	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +":::: "+message;
		agent.printMessage(s);
	}	
}
