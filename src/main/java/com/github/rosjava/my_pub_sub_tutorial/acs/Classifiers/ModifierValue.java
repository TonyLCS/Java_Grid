package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;


import java.text.DecimalFormat;
import java.lang.Object.*;
import java.util.HashMap;


public class ModifierValue implements Cloneable
{
	public int boundary_low;
	public int boundary_high;
	public double resolution;
	public HashMap<Integer, Double> prd_distribution= new HashMap<Integer, Double>();

	public ModifierValue(){}

	public ModifierValue(double boundary_low, double boundary_high, double resolution)
	{

		this.resolution = resolution;

		this.boundary_low = normalize_conditionValue_input(boundary_low);
		this.boundary_high =normalize_conditionValue_input(boundary_high);


		initate_modifier_prd_distribution();
	}

	public void  initate_modifier_prd_distribution()
	{
		
		for(int i= this.boundary_low; i<= this.boundary_high; i++)
		{
			this.prd_distribution.put(i, 0.0);
		}

		//System.out.println(" initiate a modifier_value:" +to_string());
	}

	public void update_modifier_prd_distribution(double low, double high, double prd)
	{
		// input: a condition value's 1. lowcell, 2. highcell; 3, scheduled_prd
		int low_resoluted= normalize_conditionValue_input(low);
		int high_resoluted= normalize_conditionValue_input(high);
		for(int i=low_resoluted; i<= high_resoluted; i++)
		{
			double pre_prd = this.prd_distribution.get(i);
			double crr_prd = pre_prd+ prd;
			this.prd_distribution.put(i, crr_prd);
		}

	}
	public int normalize_conditionValue_input(double conditionValue)
	{
		int modifier_value;
		if(conditionValue<0)
		{
			return 0;
		}
		else
		{
			modifier_value= (int) (conditionValue/this.resolution);
			return modifier_value;
		}
	}

	public String to_string()
	{
		String s= "a modifierValue to string: ";

		for(int i=this.boundary_low; i< this.boundary_high; i++)
		{
			s += "  <"+i+", "+prd_distribution.get(i) +" >  ";
		}

		return s;
	}

}
