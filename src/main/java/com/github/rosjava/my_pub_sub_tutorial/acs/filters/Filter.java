package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Debug;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

public abstract class Filter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> 
{
	public final String NAME;
	
	public Filter(String name)
	{
		this.NAME = name;
	}
	
	public abstract FilterMessage<C, A> apply(P agent, int iterationCount, FilterMessage<C, A> message);
	
	public String getName() 
	{
		return NAME;
	}
	
	protected abstract void printMessage(String message, P a);
}
