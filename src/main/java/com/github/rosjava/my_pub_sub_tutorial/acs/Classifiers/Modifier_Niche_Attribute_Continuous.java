package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;


import java.text.DecimalFormat;
import java.lang.Object.*;
import java.util.HashMap;
import java.util.LinkedList;


public class Modifier_Niche_Attribute_Continuous implements Cloneable
{
	public LinkedList<Double> prd_distribution_vector_point= new LinkedList<Double>();
	public LinkedList<Double> prd_distribution_vector_value= new LinkedList<Double>();
	public Double[] loc_points;
	public Double[] prd_values;

	public Modifier_Niche_Attribute_Continuous()
	{

	}

	public void save_prd_distribution_vector_point(LinkedList<Double> list)
	{
		this.prd_distribution_vector_point= (LinkedList<Double>)list.clone();
	}
	public void save_prd_distribution_vector_value(LinkedList<Double> list)
	{
		this.prd_distribution_vector_value= (LinkedList<Double>)list.clone();
	}

	public void translate_into_matrix()
	{
		this.loc_points= this.prd_distribution_vector_point.toArray(new Double[this.prd_distribution_vector_point.size()]);
		this.prd_values= this.prd_distribution_vector_value.toArray(new Double[this.prd_distribution_vector_value.size()]);
	}
	public void show_loc_points()
	{
		this.loc_points= this.prd_distribution_vector_point.toArray(new Double[this.prd_distribution_vector_point.size()]);
		String loc_m_s="location_continuous	#"; //
		for(int i_row =0; i_row<loc_points.length;i_row++)
		{
			loc_m_s+= loc_points[i_row]+ "#";
		}


		System.out.println("\n>> loc_m ("+loc_points.length+"):"+ loc_m_s);


	}

	public void show_prd_values()
	{
		this.prd_values= this.prd_distribution_vector_value.toArray(new Double[this.prd_distribution_vector_value.size()]);
		String prd_m_s="niche_strength	#";
		for(int i_row =0; i_row<this.prd_values.length;i_row++)
		{
			prd_m_s+= this.prd_values[i_row]+ "#";
		}

		System.out.println(">> prd_m ("+ this.prd_values.length+"):"+ prd_m_s);
	}

	public double search_max_prd_values()
	{
		// the first value serving as a header...
		double max_prd_values= prd_values[1];
		for(int i=1; i<prd_values.length;i++)
		{
			if(max_prd_values< prd_values[i])
			{
				max_prd_values = prd_values[i];
			}
		}

		return max_prd_values;
	}

	public Integer[] search_best_niche_index()
	{
		//Integer[] best_niche_index;
		LinkedList<Integer> best_niche_index= new LinkedList<Integer>();
		double max_prd_values= search_max_prd_values()*0.95;
		for(int i=0; i<prd_values.length;i++)
		{
			if(max_prd_values <= prd_values[i])
			{
				best_niche_index.add(i);
			}
		}

		if(best_niche_index.size()!=0)
		{
			Integer[] best_niche_index_m= best_niche_index.toArray(new Integer[best_niche_index.size()]);
			// show index
			String s = "";
			for(int i=0;i< best_niche_index.size(); i++)
			{
				s += best_niche_index_m[i].toString() +"#";
			}

			System.out.println("max_prd("+max_prd_values+") at best niche index:"+s);

			return best_niche_index_m;
		}
		else
		{
			return null;
		}

	}

	public Double[] best_niche_upperAndLower(Integer[] index)
	{
		int len = 2*index.length;
		Double[] best_niche_boundary= new Double[len];

		for(int i = 0; i< index.length;i++)
		{
			double high = this.prd_distribution_vector_point.get(index[i]);
			double low = this.prd_distribution_vector_point.get(index[i]-1);
			best_niche_boundary[2*i] = low;
			best_niche_boundary[2*i+1] = high;
			System.out.println(">> best_niche_boundary ("+low+" - "+high+" )");
		}
		return  best_niche_boundary;
	}

	public String[] loc_points_toString()
	{
		String[] s = new String[loc_points.length];
		for(int i=0; i<loc_points.length;i++)
		{
			s[i]=loc_points[i].toString();
		}
		return s;
	}

	public String loc_points_to_String_stream()
	{
		String s = "";
		for(int i=0; i<loc_points.length;i++)
		{
			s += loc_points[i].toString() + "#";
		}
		return s;
	}

	public String[] prd_values_toString()
	{
		String[] s = new String[prd_values.length];
		for(int i=0; i<prd_values.length;i++)
		{
			s[i]=prd_values[i].toString();
		}
		return s;
	}

	public String prd_values_toString_stream()
	{
		String s = "";
		for(int i=0; i<prd_values.length;i++)
		{
			s +=prd_values[i].toString() +"#";
		}
		return s;
	}


}
