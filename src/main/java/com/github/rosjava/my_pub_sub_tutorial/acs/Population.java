package main.java.com.github.rosjava.my_pub_sub_tutorial.acs;

import java.awt.Graphics2D;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerAction;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Classifier;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters.FilterMessage;

import com.thoughtworks.xstream.XStream;


public abstract class Population<C extends Classifier<O, A>, O extends Condition<O>, A extends ActionInterface> implements Serializable 
{

	private HashMap<Integer, C> classifiers = new HashMap<Integer, C>();



	private static int NEXT_ID = 0;
	private final double expTheshold =3;
	private final double accSubsumption=0.99;
	private final int popSize_threshold=900;//popsize threshold
	private final int popSize_threshold_active=905;// deletion active threshold, to reduce deletion frequency, popsize threshold

	private final double  theta=0.1;// apply for deletion method
	private final double  theta_ruleDiscovery=0.2;// apply for deletion method

	
	public Population()
	{
		//super();
	}

	public void save_csv_classifiers()
	{}

	public void readPopulation(String file)
	{
		System.out.println("ACS::Population->readPopulation : 1 try to read populatiopn from file:'"+file+"'.........");
//		FileInputStream fileIn;
//		ObjectInputStream in;
		try
		{
//			fileIn = new FileInputStream(new File("population.ser"));
//			in = new ObjectInputStream(fileIn);
//			this.classifiers = (HashMap<Integer, Classifier>)in.readObject();
//			for(int id : classifiers.keySet())
//				if(id >= NEXT_ID)
//					NEXT_ID = id + 1;
//			System.out.println(this.classifiers.size());

			XStream xstream = new XStream();
			HashMap<Integer, C> hashMap = (HashMap<Integer, C>)xstream.fromXML(new File(file+".xml"));
			if(hashMap != null)
				this.classifiers = hashMap;

			for(int id : hashMap.keySet()) {
				if (id >= NEXT_ID) {
					NEXT_ID = id + 1;
					System.out.println("ACS::Population->readPopulation :  NEXT_ID("+NEXT_ID+").......");

				}
			}

			System.out.println("ACS::Population->readPopulation : 2 read populatiopn from file:'"+file+".xml' successfully. NEXT_ID("+NEXT_ID+").......");

		}
		catch(Exception e)
		{
			System.out.println("ACS::Population->readPopulation : 3 fail to read populatiopn from file:'"+file+".xml' fail.........");
			System.out.println("No previous population!: " + e.getMessage());
//			e.printStackTrace();
		}
	}

	@Override
	public String toString()
	{
		String s = "*********************************\n";
		
		s += "Population\n";
		
		for(C c : classifiers.values())
			s += c.toString() +"\n";
		
		s += "*********************************\n";
		
		return s;
	}

	public String classifiersToString()
	{
		String s = "*********************************\n";

		s += "Population\n";

		for(C c : classifiers.values())
			s += c.toString() +"\n";

		s += "*********************************\n";

		return s;
	}

	/*
	public void CSVToClassifeirs(List<String[]> csv_data)
	{
		Iterator<String[]> iter = csv_data.iterator();
		String[] a_classifier_string;
		HashMap<Integer, C> classifiers_from_csv = new HashMap<Integer, C>();
		// read header
		String[] csv_header_str_arr = iter.next();

		int length_of_attribute = csv_header_str_arr.length - 9;
		System.out.println("reading header from the csv file:"+csv_header_str_arr.toString());
		while(iter.hasNext())
		{
			csv_header_str_arr=iter.next();
			// set an individual classifier.

			//createClassifierFromCSV(csv_header_str_arr);


		}
		this.classifiers = classifiers_from_csv;
	}
	*/

	public List<String[]> classifiersToCSV()
	{
		List<String[]> classifier_array_csv= new ArrayList<String[]>();
		//adding header
		boolean has_header= false;
		for(C c : classifiers.values())
		{
			if(!has_header)
			{
				classifier_array_csv.add(c.toHeaderString_for_csv());
				has_header=true;
			}

			classifier_array_csv.add(c.toStringArray_for_csv());
		}
		return classifier_array_csv;

	}

	public boolean checkIsTheSame_unique(C classifier)
	{
		//check if the classifier is unique in the population
		for(C c:classifiers.values())
		{
			if(c.checkIsTheSame_unique(classifier))
			{
				return false;
			}
		}

		return true;
	}
	
	public void insertIntoPopulation(C classifier) {

		boolean hasSubsumption= false;
		ArrayList<Integer> subsumptionList= new ArrayList<Integer>();
		ArrayList<C> subsumption_Classifiers_List= new ArrayList<C>();

		System.out.println("		InsertIntoPopulation::	1. try to insert classifier(" + classifier.getID() + ")into population. ");
		for (C c : classifiers.values())
		{
			if (c.equals(classifier)) {
				//debug work// System.out.println("		InsertIntoPopulation:	2.1 getAccuracy("+c.getAccuracy()+");  getExperience("+c.getExperience()+").");
				//check if the subsumer classifier is accurate and experienced.
				if (c.getAccuracy() > accSubsumption && c.getExperience() > expTheshold) {
					//c.increaseNumerosity();
					//System.out.println("		InsertIntoPopulation:	2.1 subsumption,increase cl.id('" + c.ID + "') Numerosity(" + c.getNumerosity() + "),  ");
					hasSubsumption= true;
					subsumptionList.add((Integer)c.getID());
					subsumption_Classifiers_List.add(c);
					//return;
				}
			}
		}
		if(hasSubsumption)
		{
			//todo: select one classifier from list and increase numerosity
			System.out.println("		InsertIntoPopulation:	3 subsumptionList.length("+subsumptionList.size()+")");
			/**
			 * if the list is larger than 3, randomly increase the selected classifier's numerous,
			 * otherwise, increase their numerosity.
			 * ***/
			if(subsumption_Classifiers_List.size()<=3)
			{
				for(C c:subsumption_Classifiers_List)
				{
					c.increaseNumerosity();
					System.out.println("		InsertIntoPopulation:	2.1 subsumption,increase cl.id('" + c.ID + "') Numerosity(" + c.getNumerosity() + "),  ");
				}
			}
			else
			{
				for(C c:subsumption_Classifiers_List)
				{
					if(Math.random()<=0.5)
					{
						c.increaseNumerosity();
						System.out.println("		InsertIntoPopulation(random):	2.1 subsumption,increase cl.id('" + c.ID + "') Numerosity(" + c.getNumerosity() + "),  ");
					}
				}
			}


		}
		else
		{
			int classID = classifier.ID;
			classifiers.put(classID, classifier);
			//increaseNextID();
			System.out.println("		InsertIntoPopulation:  2.2.New cl='"+classID+"', cl.id("+classID+") ");
		}

	}
	public void insertIntoPopulationDirectly(C classifier)
	{
		//debug ok//System.out.println("		InsertIntoPopulation::	1. try to insert classifier("+classifier.getID()+")into population. ");

		int classID = classifier.ID;
		classifiers.put(classID, classifier);
		//increaseNextID();

		//debug ok//System.out.println("		InsertIntoPopulation:  2.2.New cl='"+classID+"', cl.id("+classID+") ");
	}
	/*
	public void deleteFromPopulation(C classifier)
	{
		System.out.println("		deleteFromPopulation:: before: PopulationSize("+classifiers.size()+"), delete cl.id("+classifier.getID()+")");
		//classifiers.remove(classifier.getID());
		System.out.println("		deleteFromPopulation:: after:  PopulationSize("+classifiers.size()+"), delete cl.id("+classifier.getID()+")");

	}
	*/
	public void deleteFromPopulation(int classifierID)
	{
		C classifierToDel= getClassifier(classifierID);
		/*
		System.out.println("		deleteFromPopulation:: before: PopulationSize("+classifiers.size()+")," +
				" delete cl.id("+classifierID+")," +
				" cl.num("+classifierToDel.getNumerosity()+")," +
				" cl.fit("+classifierToDel.getFitness()+")," +
				" cl.rwd("+classifierToDel.getPredictedReward()+")," +
				" cl.niche("+classifierToDel.getNichSize()+")," +
				" cl.acc("+classifierToDel.getAccuracy()+"). " +
				" cl.condition("+classifierToDel.showCondition()+")");
		*/
		if(classifierToDel.getNumerosity()==1)
		{
			classifiers.remove(classifierID);
			//System.out.println("		deleteFromPopulation:: after:  PopulationSize("+classifiers.size()+"), delete cl.id("+classifierID+")");

		}
		else
		{
			classifierToDel.decreaseNumerosity();
			/*
			System.out.println("		deleteFromPopulation:: after: PopulationSize("+classifiers.size()+")," +
					" delete cl.id("+classifierID+")," +
					" cl.num("+classifierToDel.getNumerosity()+")," +
					" cl.fit("+classifierToDel.getFitness()+")," +
					" cl.rwd("+classifierToDel.getPredictedReward()+")," +
					" cl.niche("+classifierToDel.getNichSize()+")," +
					" cl.acc("+classifierToDel.getAccuracy()+"). " +
					" cl.condition("+classifierToDel.showCondition()+")");
			*/
		}

	}
	public double deletionVote(C classifier, double avFitPop)
	{
		//calculate the vote for deletion
		//realise niching as, as well as removal the lowest fitness classifiers.
		double vote=0;
		vote = ((double)classifier.getNumerosity())*classifier.getNichSize();

		//boolean flagExp=false;
		boolean flagFit=false;
		/** get rid of the expTheshold!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!**/
		//if(classifier.getExperience()> expTheshold){flagExp=true;}
		if(classifier.getFitness()/classifier.getNumerosity()<avFitPop*theta){flagFit=true;}
		//if(flagExp&&flagFit)
		if(flagFit)
		{
			vote= vote*avFitPop*classifier.getNumerosity()/classifier.getFitness();
			if(classifier.getFitness()==0)
			{// Tony set the initial fitness above 0, so this part just in case
				vote=vote*avFitPop*classifier.getNumerosity()/(0.0000000001);
			}
		}
		return vote;
	}
	public double GArulediscovery_Vote(C classifier, double avFitPop)
	{
		//todo: to understand del vote!!!
		//calculate the vote for deletion
		//realise niching as, as well as removal the lowest fitness classifiers.
		double vote=0;
		vote = ((double)classifier.getNumerosity())*classifier.getNichSize();

		boolean flagExp=false;
		boolean flagFit=false;
		/** need the expTheshold to be parent!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!**/
		if(classifier.getExperience()> expTheshold){flagExp=true;}
		if(classifier.getFitness()/classifier.getNumerosity()>avFitPop*theta_ruleDiscovery){flagFit=true;}
		if(flagExp&&flagFit)
		{
			vote= vote*avFitPop*classifier.getNumerosity()/classifier.getFitness();
			if(classifier.getFitness()==0)
			{// Tony set the initial fitness above 0, so this part just in case
				vote=vote*avFitPop*classifier.getNumerosity()/(0.0000000001);
			}
		}

		return vote;
	}


	public ArrayList<C> getClassifiers(ArrayList<Integer> IDs)
	{
		ArrayList<C> classifiers_ids = new ArrayList<C>();
		for(Integer id : IDs)
		{
			C c = classifiers.get(id);
			if(c != null)
				classifiers_ids.add(c);
		}
		return classifiers_ids;
	}
	
	public static int getNextID()
	{
		return Population.NEXT_ID++;
	}
	public void setNextID(int gensis_ID)
	{
		NEXT_ID= gensis_ID;
	}
	public void increaseNextID()
	{
		NEXT_ID++;
	}
	public int getAndShowNextID()
	{
		return Population.NEXT_ID;
	}

	public C getClassifier(int classifierID)
	{
		return classifiers.get(classifierID);
	}

	public ArrayList<Integer> getMatchingClassifiers(O state) 
	{
		System.out.println("Match Filter::::::c current condition("+state+").  classifiers.size =("+classifiers.size()+")");
		//System.out.println("Match Filter::::::classifiers.size =("+classifiers.size()+").");

		ArrayList<Integer> matchingClassifierIDs = new ArrayList<Integer>();
		for(C classifier : classifiers.values())
		{
			if(classifier.isStateWithin(state))
			{
				matchingClassifierIDs.add(classifier.ID);
				//System.out.println("Match Filter::::::classifiers.size ="+classifiers.size()+", a matching classifier.ID='"+classifier.ID+ "'.");
			}
		}

		//System.out.println("Match Filter::::::classifiers.size("+classifiers.size()+") matchingClassifierIDs ='"+matchingClassifierIDs+"'.");
		return matchingClassifierIDs;
	}

	public ArrayList<Integer> getMatchingClassifiers(double[] state)
	{
		System.out.println("Match Filter::::::c current condition("+state+").  classifiers.size =("+classifiers.size()+")");
		//System.out.println("Match Filter::::::classifiers.size =("+classifiers.size()+").");

		ArrayList<Integer> matchingClassifierIDs = new ArrayList<Integer>();
		for(C classifier : classifiers.values())
		{
			//todo : maze
			// if(classifier.isStateWithin(state))
			//{
			//	matchingClassifierIDs.add(classifier.ID);
			//	//System.out.println("Match Filter::::::classifiers.size ="+classifiers.size()+", a matching classifier.ID='"+classifier.ID+ "'.");
			//}
		}

		//System.out.println("Match Filter::::::classifiers.size("+classifiers.size()+") matchingClassifierIDs ='"+matchingClassifierIDs+"'.");
		return matchingClassifierIDs;
	}

	public ArrayList<Integer> getMatchingClassifiers_for_action(A action)
	{
		//System.out.println("Match Action Filter::::::c current  conditional action("+action+").");
		//System.out.println("Match Action Filter::::::classifiers.size =("+classifiers.size()+").");

		ArrayList<Integer> matchingClassifierIDs = new ArrayList<Integer>();
		for(C classifier : classifiers.values())
		{
			if(classifier.isSameAction(action)) // is same action
			{
				matchingClassifierIDs.add(classifier.ID);
				//todo: set match set need to be numerostiy>1
				//if( classifier.getNumerosity() >=1)
				//{
				//	matchingClassifierIDs.add(classifier.ID);
				//}

				//System.out.println("Match Action Filter::::::classifiers.size ="+classifiers.size()+", a matching classifier.ID='"+classifier.ID+ "'.");
			}
		}

		//System.out.println("Match Action Filter::::::classifiers.size("+classifiers.size()+") matchingClassifierIDs ='"+matchingClassifierIDs+"'.");
		return matchingClassifierIDs;
	}
	
	protected abstract C createClassifier(O currentState);
	protected abstract C createClassifier(O currentState,int actionMaskID);
	protected abstract C createClassifier(O currentState,A action);
	protected abstract C createClassifier(int id, O currentState, A action); //todo:add statistics

	public int createClassifierFromState(O currentState)
	{
		C classifier = createClassifier(currentState);
		if(classifier == null)
		{
			System.out.println("createClassifierFromState::	Fail to createClassifierFromState!!!!");
			return -1;
		}
		System.out.println("createClassifierFromState:: classifier.ID ('"+classifier.ID+"'), and then insertIntoPopulation(classifier)");
		insertIntoPopulation(classifier);

		return classifier.ID;
	}

	public int createClassifierFromStateWithAction(O currentState,ArrayList<Integer> MatchSetIDs)
	{
		//TODO: add cover condition here?
		//ArrayList<A> existingActionSetList= getClassifiersActionSetList();// this will iterate through all population
		ArrayList<A> existingActionSetList= getClassifiersActionSetListFromMatchSet(MatchSetIDs); //actions listed in the match set
		ArrayList<A> possibleActionSetList= getClassifiersPossibleActionSetList();		// actions available and preset
		ArrayList<A> deficientActionSetList= getActionSetDeficientList(MatchSetIDs);	//action that no yet cover by existing list

		System.out.println("		createClassifierFromState: before:	existingActionSetList("+existingActionSetList+")");
		System.out.println("		createClassifierFromState: before:	deficientActionSetList("+deficientActionSetList+")");

		if(existingActionSetList.size()==0)
		{
			A possibleAction = possibleActionSetList.get((int)Math.floor(Math.random()*possibleActionSetList.size()));
			//A possibleAction = deficientActionSetList.get((int)Math.floor(Math.random()*deficientActionSetList.size()));
			System.out.println("		createClassifierFromState: 	from possibleActionSetList:	randomly select possibleAction("+possibleAction+")");
			C classifier = createClassifier(currentState,possibleAction);


			if(classifier == null)
			{
				System.out.println("createClassifierFromState::	Fail to createClassifierFromState!!!!");
				return -1;
			}
			System.out.println("createClassifierFromState:: classifier.ID ('"+classifier.ID+"'), and then insertIntoPopulation(classifier)");
			insertIntoPopulationDirectly(classifier);

			return classifier.ID;
		}
		else
		{
			/*
			for(int i=0;i< existingActionSetList.size();i++)
			{
				//check if possibleActionSetList contain elements in existingActionSetList
				// if it contains that element, remove the element from list.
				// after operation, the possibleActionSetList will only contain the action that is not applied yet.
				for(Iterator<A> iter= possibleActionSetList.iterator(); iter.hasNext();)
				{
					A an_action= iter.next();
					if(an_action.equals(existingActionSetList.get(i)))
					{
						iter.remove();
					}
				}
				//System.out.println("		createClassifierFromState: after:	possibleActionSetList("+possibleActionSetList+")");

			}
			*/
			if(deficientActionSetList.size()>0)
			{
				//A possibleAction= possibleActionSetList.get((int)Math.floor(Math.random()*possibleActionSetList.size()));
				A deficientAction = deficientActionSetList.get((int)Math.floor(Math.random()*deficientActionSetList.size()));

				System.out.println("		createClassifierFromState:	from deficientActionSetList: randomly select deficientAction("+deficientAction+")");
				//this create a classifier with specific actionMaskID
				C classifier = createClassifier(currentState,deficientAction);


				if(classifier == null)
				{
					System.out.println("createClassifierFromState::	Fail to createClassifierFromState!!!!");
					return -1;
				}
				System.out.println("createClassifierFromState:: classifier.ID ('"+classifier.ID+"'), and then insertIntoPopulation(classifier)");
				insertIntoPopulationDirectly(classifier);

				return classifier.ID;
			}
			else
			{
				A possibleAction = possibleActionSetList.get((int)Math.floor(Math.random()*possibleActionSetList.size()));
				System.out.println("		createClassifierFromState: 	from existingActionSetList:	randomly select possibleAction("+possibleAction+")");
				C classifier = createClassifier(currentState,possibleAction);


				if(classifier == null)
				{
					System.out.println("createClassifierFromState::	Fail to createClassifierFromState!!!!");
					return -1;
				}
				System.out.println("createClassifierFromState:: classifier.ID ('"+classifier.ID+"'), and then insertIntoPopulation(classifier)");
				insertIntoPopulationDirectly(classifier);

				return classifier.ID;
			}


		}

	}

	public void  create_classifier_by_mitosis(O currentState,ArrayList<Integer> MitosisSetIDs)
	{
		for(int id :MitosisSetIDs)
		{
			//get ids of classifiers whose accuracy =1 but currently has error.
			C original_classifier =getClassifier(id);
			// get the mitosis classifier set which are generated according to mitosis procedure
			ArrayList<Classifier> mitosis_classifier_set = original_classifier.generate_Mitosis_Classifiers(currentState);
			if(!mitosis_classifier_set.isEmpty())
			{
				//insert into population
				for(int cl_index=0; cl_index < mitosis_classifier_set.size();cl_index++)
				{
					C classifier_to_insert = (C) mitosis_classifier_set.get(cl_index);

					//debug ok//System.out.println("create_classifier_by_mitosis:: classifier.ID ('"+classifier_to_insert.ID+"'), and then insertIntoPopulation");
					insertIntoPopulationDirectly(classifier_to_insert);
				}
			}
		}
	/*
		if(MitosisSetIDs.size()>0)
		{
			C classifier = createClassifier(currentState,possibleAction);


			if(classifier == null)
			{
				System.out.println("createClassifierFromState::	Fail to createClassifierFromState!!!!");
			}
			System.out.println("create_classifier_by_mitosis:: classifier.ID ('"+classifier.ID+"'), and then insertIntoPopulation");
			insertIntoPopulationDirectly(classifier);

		}
		*/



	}

	public void createClassifierFromCSV(int id, O condition_input,String action_mask, double[] statistic)
	{
		//int id =777;

		/** create condition **/

		/** create action **/
		A action_input = (A) LayerAction.getLayerAction(action_mask);


		/** create a classifier **/
		C classifier = createClassifier(id, condition_input, action_input); // todo:add statistics
		/** set statistics **/
		classifier.setStatistics(statistic);
		/** insert into population **/
		insertIntoPopulationDirectly(classifier);

		//check classifier
		System.out.println("createClassifierFromCSV ,classifiers size("+classifiers.size()+") , check action ("+classifier.getLayerAction()+")" );


	}
	public boolean actionSetDiversity(ArrayList<Integer> MatchSetIDs) {

		ArrayList<A> existingActionSetList = getClassifiersActionSetListFromMatchSet(MatchSetIDs);
		ArrayList<A> possibleActionSetList = getClassifiersPossibleActionSetList();
		//System.out.println("		createClassifierFromState: before:	existingActionSetList(" + existingActionSetList + ")");
		//System.out.println("		createClassifierFromState: before:	possibleActionSetList(" + possibleActionSetList + ")");

		if (existingActionSetList.size() == 0) {
			//System.out.println("		covery:actionSetDiversity: existingActionSetList.size(" + existingActionSetList.size() + ")");
			return false;
		}
		else
		{
			for (int i = 0; i < existingActionSetList.size(); i++) {
				//check if possibleActionSetList contain elements in existingActionSetList
				// if it contains that element, remove the element from list.
				// after operation, the possibleActionSetList will only contain the action that is not applied yet.
				for (Iterator<A> iter = possibleActionSetList.iterator(); iter.hasNext(); ) {
					A an_action = iter.next();
					if (an_action.equals(existingActionSetList.get(i))) {
						iter.remove();
					}
				}
				//System.out.println("		covery:actionSetDiversity: 	possibleActionSetList.size(" + possibleActionSetList.size() + ")");
			}

			if (possibleActionSetList.size() > 0) {
				System.out.println("		covery:actionSetDiversity.size(" + possibleActionSetList.size() + "): 	possibleActionSetList(" + possibleActionSetList + ")");
				return false;
			}
			else
			{
				return true;
			}

		}
	}

	public ArrayList<A> getActionSetDeficientList(ArrayList<Integer> MatchSetIDs) {
		ArrayList<A> deficientList = new ArrayList<A>();
		ArrayList<A> existingActionSetList = getClassifiersActionSetListFromMatchSet(MatchSetIDs);
		ArrayList<A> possibleActionSetList = getClassifiersPossibleActionSetList();
//		System.out.println("		createClassifierFromState: before:	existingActionSetList(" + existingActionSetList + ")");
//		System.out.println("		createClassifierFromState: before:	possibleActionSetList(" + possibleActionSetList + ")");

		if (existingActionSetList.size() == 0) {
			//System.out.println("		covery:actionSetDiversity: existingActionSetList.size(" + existingActionSetList.size() + ")");
			return possibleActionSetList;
		}
		else
		{
			for (int i = 0; i < existingActionSetList.size(); i++) {
				//check if possibleActionSetList contain elements in existingActionSetList
				// if it contains that element, remove the element from list.
				// after operation, the possibleActionSetList will only contain the action that is not applied yet.
				for (Iterator<A> iter = possibleActionSetList.iterator(); iter.hasNext(); ) {
					A an_action = iter.next();
					if (an_action.equals(existingActionSetList.get(i))) {
						iter.remove();
					}
				}
				//System.out.println("		covery:actionSetDiversity: 	possibleActionSetList.size(" + possibleActionSetList.size() + ")");
			}

			deficientList=possibleActionSetList;
			if (deficientList.size() > 0) {
				System.out.println("	return deficientList("+deficientList+").");
				return deficientList;
			}
			else
			{
				return deficientList;
			}

		}
	}

	public int getActionSetSizeInMatchSet(ArrayList<Integer> MatchSetIDs)
	{
		//how many type of actions existing in the match set
		//put type of action into a arrayList
		ArrayList<A> existingActionSetList= getClassifiersActionSetListFromMatchSet(MatchSetIDs);
		//check how many types of actions in the list

		return existingActionSetList.size();
	}



	public int getActionSetSizePossibility()
	{
		ArrayList<A> possibleActionSetList= getClassifiersPossibleActionSetList();
		return possibleActionSetList.size();
	}

	public ArrayList<A> getClassifiersActionSetList()
	{
		ArrayList<A> existingActionSetList= new ArrayList<A>();
		//ArrayList<Integer> matchingClassifierIDs = new ArrayList<Integer>();
		for(C classifier : classifiers.values())
		{
			existingActionSetList.add(classifier.getClassifierAction()) ;
			//get classifiers' existing Action Set
			//classifier.getAction()
		}
		return existingActionSetList;
	}

	public ArrayList<A> getClassifiersActionSetListFromMatchSet(ArrayList<Integer> MatchSetIDs)
	{
		//how many type of actions existing in the match set
		//put type of action into a arrayList

		ArrayList<A> existingActionSetListFromMatchSet= new ArrayList<A>();
		ArrayList<C> classifierSubset=getClassifierSubset(MatchSetIDs);// all classifiers (set) in Match set

		for(int i=0; i< classifierSubset.size();i++)
		{
			C classifier= classifierSubset.get(i);
			existingActionSetListFromMatchSet.add(classifier.getClassifierAction()) ;
		}
		return existingActionSetListFromMatchSet;
	}

	protected abstract int getLengthOfLayerActionSet();

	public ArrayList<A> getClassifiersPossibleActionSetList()
	{	//put all possible actions into arraylist
		int lengthOfLayerActionSet=getLengthOfLayerActionSet();
		ArrayList<A> possibleActionSetList= new ArrayList<A>();
		ArrayList<LayerAction> layerActionArrayList=LayerAction.getLayerActionSet(lengthOfLayerActionSet);
		for(int i=0;i<layerActionArrayList.size();i++)
		{
			possibleActionSetList.add((A)layerActionArrayList.get(i));
		}

		return possibleActionSetList;
	}



	public void updateClassifiersAction(FilterMessage<O, A> message, double reward)
	{


		ArrayList<Integer> action_set_ids = message.getActionSet();
		ArrayList<C> action_set = getClassifiers(action_set_ids);


		A taken_action = message.getCurrentAction();

		System.out.println("updateClassifiersAction <== begin," +
				" reward='"+reward+"'," +
				" action_set='"+action_set.size()+"';" +
				" taken_action(emotion)= '"+taken_action+"'" +
				" FilterMessage<O, A> message='"+message+"'");

		//
		//modified by Tony
		//
		// 1. get sum of accuracy
		double sum_actionSet_acc=0;
		int sum_cal_time=0;
		int niche_size=action_set.size();
		//1 update exp, rwd, err, acc, niche of each classifier
		for(C c : action_set)
		{
			//c.updateAction(taken_action, reward);
			c.updateAction(taken_action, reward, niche_size);
		}
		//2 calculate sum of acc in action set
		for(C c:action_set)
		{
			//it should sum up the accuracy of action(statisitcs in "action_mapping")
			//sum_actionSet_acc += c.getAccuracy();
			double action_accuracy=c.getActionAccuarcy(taken_action);
			sum_actionSet_acc += action_accuracy;
			sum_cal_time++;
			System.out.println("debug(add sum of acc for the update of ActionSet 3), " +
					"accuarcy of each action of each classifier is='"+action_accuracy+"'. " +
					"sum_actionSet_acc :'"+sum_actionSet_acc +". sum_cal_time='"+sum_cal_time+"' :: niche_size='"
					+niche_size+
					"'. acc("+action_accuracy+")/sum_acc("+sum_actionSet_acc+")='"+(action_accuracy/sum_actionSet_acc)
			);

		}
		//3. calculate fitness for each classifier
		for(C c:action_set)
		{
			c.updateActionFitness(taken_action, sum_actionSet_acc);
		}




		System.out.println("updateClassifiersAction <== end ");

	}
	
	public abstract ActionInterface getNextAction(O condition, ArrayList<Integer> matchSet, ArrayList<Integer> actionSet, boolean exploit);
	public abstract ActionInterface getNextAction(O condition, ArrayList<Integer> matchSet, ArrayList<Integer> actionSet, boolean exploit, ArrayList<Double>  anticipated_reward);
	//public abstract HashMap<LayerAction, Double> get_anticipated_reward_hashmap(ArrayList<Integer> matchSet);


	public abstract void save_anticipated_reward_hashmap(int iteration, int instance_id);
	
	public void applyGA(ArrayList<Integer> action_set_ids, int iteration, int ga_threshold)
	{
		double doMutationChange=0.04;
		double doCrossoverChange=0.8;
		ArrayList<C> action_set = getClassifiers(action_set_ids);
		
		double average_time_since_ga = 0;
		double action_set_num = 0;
		for(C c : action_set)
			action_set_num += c.getNumerosity();
		for(C c : action_set)
			average_time_since_ga += c.getIterations_since_ga() * c.getNumerosity() / action_set_num;


		//System.out.println("Applying GA:average_time_since_ga ('"+average_time_since_ga+"') VS ga_threshold ('"+ga_threshold+"')");
		if(average_time_since_ga > ga_threshold)
		{
			System.out.println("Applying GA: 1. average_time_since_ga ('"+average_time_since_ga+"')> ga_threshold ('"+ga_threshold+"')");
			Debug.DebugMessage("Applying GA");

			//2. select parent using to roulette wheel selection according to the fitness of the classifiers
			//C parent_one = selectClassifier(action_set);	//roulette wheel method
			//C parent_two = selectClassifier(action_set);	//roulette wheel method

			/** replaced by a clone of list
			ArrayList<C> parentList= selectClassifierT(action_set);//this is a deep copy which will change the content of actionSet

			//todo::clone or deep copy!!!!!!!!!!!!!!!!!!!!!!!!
			//todo:: realize the clone of action! actionSet.


			C parent_one;
			C parent_two;
			C child_one;
			C child_two;

			parent_one	= parentList.get(0);
			parent_two = parentList.get(1);

			*/
			//clone or deep copy!!!!!!!!!!!!!!!!!!!!!!!!
			// realize the clone of action! actionSet.

			ArrayList<Integer> parentListID= new ArrayList<Integer>(selectClassifierT(action_set_ids,true));//clone method

			//try have diversity parents
			if(!parentListID.isEmpty())
			{
				//try to get new parents again if they are the same one
				if(parentListID.get(0) == parentListID.get(1))
				{
					parentListID= new ArrayList<Integer>(selectClassifierT(action_set_ids,true));//clone method
				}
			}

			if(!parentListID.isEmpty())
			{
				C parent_one;
				C parent_two;
				C child_one;
				C child_two;

				parent_one	= getClassifier(parentListID.get(0));
				parent_two = getClassifier(parentListID.get(1));

				System.out.println("Applying GA: 2. Select parent_one ("+parent_one.getID()+") .fit("+parent_one.getFitness()+")" +
						"and parent_two("+parent_two.getID()+").fit("+parent_two.getFitness()+") from actionSet.size("+action_set.size()+"). (Tournament Method)");

				//if there are the same parent, increase the mutation rate
				if(parent_one.getID() == parent_two.getID())
				{
					doMutationChange = 0.5;
				}
				//3. spawn two children
				//notice that the statistics have changed
				child_one = (C) parent_one.createSameChild();
				child_two = (C) parent_two.createSameChild();

				//C child_one = (C) parent_one.createMutantChild();
				//C child_two = (C) parent_two.createMutantChild();
				System.out.println("Applying GA: 3. spawn two children ("+child_one.getID()+") and ("+child_two.getID()+")" +
						" from parent_one ("+parent_one.getID()+") and parent_two("+parent_two.getID()+") ");

				//	System.out.println("Applying GA: 3. result:\n" + "	parent-one("+parent_one.toString()+"\n" + "	child-one("+child_one.toString()+") \n " + "	parent_two("+parent_two.toString()+"\n"+ "   child two("+child_two.toString()+")" + ". ");


				//5. CROSSOVER operation
				if(Math.random()<doCrossoverChange)
				{
					System.out.println("Applying GA: 4. crossover");
					//debug deep clone //System.out.println("Applying GA: 5. crossover before:" + "	child-one("+child_one.toString()+") " + "	child-two("+child_two.toString()+") " + "");

					child_one.crossoverClassifier(child_two);
					//debug deep clone //System.out.println("Applying GA: 5. crossover after:"+ "	child-one("+child_one.toString()+") " + "	child-two("+child_two.toString()+") " + "");


				}


				//4. MUTATION operation
				if(Math.random()<doMutationChange)
				{
					System.out.println("Applying GA: 5. mutation");
					//System.out.println("Applying GA: 4. mutation before:" + "	child-one("+child_one.toString()+")  and " + "child two("+child_two.toString()+"). ");

					//if(Math.random()<=doMutationChange)
					//System.out.println("Applying GA: 4. mutation before:" + "	child-one("+child_one.toString()+") ");

					//debug deep clone //System.out.println("Applying GA: 4. before mutation:\n" + "	parent-one("+parent_one.toString()+"\n" + "	child-one("+child_one.toString()+") \n ");
					child_one.mutateClassifier();
					//debug deep clone //System.out.println("Applying GA: 4. after mutation:\n" + "	parent-one("+parent_one.toString()+"\n" + "	child-one("+child_one.toString()+") \n ");
					//System.out.println("Applying GA: 4. mutation after:" + "	child-one("+child_one.toString()+") ");

					//if(Math.random()<=doMutationChange)

					//debug deep clone //System.out.println("Applying GA: 4. mutation before:" + "	parent-two("+parent_two.toString()+"\n" +  "	child_two("+child_two.toString()+") ");
					child_two.mutateClassifier();
					//System.out.println("Applying GA: 4. mutation after:" + "	child_two("+child_two.toString()+") ");
					//debug deep clone //System.out.println("Applying GA: 4. after mutation:\n" + "	parent-two("+parent_two.toString()+"\n" + "	child-two("+child_two.toString()+") \n ");
					//System.out.println("Applying GA: 4. mutation after:" + "	child-one("+child_one.toString()+")  and " + "child two("+child_two.toString()+"). ");

				}





				System.out.println("Applying GA: 6. try to insert  ("+child_one.getID()+") and ("+child_two.getID()+")" +
						" into population(size:"+size()+")");
				//6. add offspring to population
				insertIntoPopulation(child_one);
				insertIntoPopulation(child_two);

				for(C c: action_set)
					c.setIterations_since_ga(0);


			}else
				{
					System.out.println("Applying GA 2: empty parent");
				}

		}
		System.out.println("Applying GA: 7. finish GA. pop size:("+size()+")");
	}

	public void applyGA_boundary(ArrayList<Integer> action_set_ids, int iteration, int ga_threshold, double[][] boundary)
	{
		double doMutationChange=0.05;//1;
		double doCrossoverChange=0.3;//0.8
		ArrayList<C> action_set = getClassifiers(action_set_ids);

		double average_time_since_ga = 0;
		double action_set_num = 0;
		for(C c : action_set)
			action_set_num += c.getNumerosity();
		for(C c : action_set)
			average_time_since_ga += c.getIterations_since_ga() * c.getNumerosity() / action_set_num;


		//System.out.println("Applying GA:average_time_since_ga ('"+average_time_since_ga+"') VS ga_threshold ('"+ga_threshold+"')");
		if(average_time_since_ga > ga_threshold)
		{
			System.out.println("applyGA_boundary : 1. average_time_since_ga ('"+average_time_since_ga+"')> ga_threshold ('"+ga_threshold+"')");
			Debug.DebugMessage("applyGA_boundary");

			ArrayList<Integer> parentListID= new ArrayList<Integer>(selectClassifierT(action_set_ids,true));//clone method

			if(!parentListID.isEmpty())
			{
				C parent_one;
				C parent_two;
				C child_one;
				C child_two;

				parent_one	= getClassifier(parentListID.get(0));
				parent_two = getClassifier(parentListID.get(1));

				System.out.println("applyGA_boundary GA: 2. Select parent_one ("+parent_one.getID()+") .fit("+parent_one.getFitness()+")" +
						"and parent_two("+parent_two.getID()+").fit("+parent_two.getFitness()+") from actionSet.size("+action_set.size()+"). (Tournament Method)");

				//3. spawn two children
				//notice that the statistics have changed
				child_one = (C) parent_one.createSameChild();
				child_two = (C) parent_two.createSameChild();

				//C child_one = (C) parent_one.createMutantChild();
				//C child_two = (C) parent_two.createMutantChild();
				System.out.println("Applying GA: 3. spawn two children ("+child_one.getID()+") and ("+child_two.getID()+")" +
						" from parent_one ("+parent_one.getID()+") and parent_two("+parent_two.getID()+") ");

				//	System.out.println("Applying GA: 3. result:\n" + "	parent-one("+parent_one.toString()+"\n" + "	child-one("+child_one.toString()+") \n " + "	parent_two("+parent_two.toString()+"\n"+ "   child two("+child_two.toString()+")" + ". ");

				if(parent_one.getID() == parent_two.getID())
				{
					System.out.println(" if parents are the same, increase the mutation rate dynamically.");
					doMutationChange = 0.8;
				}

				//5. CROSSOVER operation
				if(Math.random()<doCrossoverChange)
				{
					System.out.println("Applying GA: 4. crossover");
					//debug deep clone //System.out.println("Applying GA: 5. crossover before:" + "	child-one("+child_one.toString()+") " + "	child-two("+child_two.toString()+") " + "");

					child_one.crossoverClassifier(child_two);
					//debug deep clone //System.out.println("Applying GA: 5. crossover after:"+ "	child-one("+child_one.toString()+") " + "	child-two("+child_two.toString()+") " + "");


				}


				//4. MUTATION operation
				if(Math.random()<doMutationChange)
				{
					System.out.println("Applying GA: 5. mutation");
					//System.out.println("Applying GA: 4. mutation before:" + "	child-one("+child_one.toString()+")  and " + "child two("+child_two.toString()+"). ");

					//if(Math.random()<=doMutationChange)
					//System.out.println("Applying GA: 4. mutation before:" + "	child-one("+child_one.toString()+") ");

					//debug deep clone //System.out.println("Applying GA: 4. before mutation:\n" + "	parent-one("+parent_one.toString()+"\n" + "	child-one("+child_one.toString()+") \n ");
					child_one.mutateClassifier(boundary);
					//debug deep clone //System.out.println("Applying GA: 4. after mutation:\n" + "	parent-one("+parent_one.toString()+"\n" + "	child-one("+child_one.toString()+") \n ");
					//System.out.println("Applying GA: 4. mutation after:" + "	child-one("+child_one.toString()+") ");

					//if(Math.random()<=doMutationChange)

					//debug deep clone //System.out.println("Applying GA: 4. mutation before:" + "	parent-two("+parent_two.toString()+"\n" +  "	child_two("+child_two.toString()+") ");
					child_two.mutateClassifier(boundary);
					//System.out.println("Applying GA: 4. mutation after:" + "	child_two("+child_two.toString()+") ");
					//debug deep clone //System.out.println("Applying GA: 4. after mutation:\n" + "	parent-two("+parent_two.toString()+"\n" + "	child-two("+child_two.toString()+") \n ");
					//System.out.println("Applying GA: 4. mutation after:" + "	child-one("+child_one.toString()+")  and " + "child two("+child_two.toString()+"). ");

				}





				System.out.println("Applying GA: 6. try to insert  ("+child_one.getID()+") and ("+child_two.getID()+")" +
						" into population(size:"+size()+")");
				//6. add offspring to population
				insertIntoPopulation(child_one);
				//insertIntoPopulation(child_two);

				for(C c: action_set)
					c.setIterations_since_ga(0);


			}else
			{
				System.out.println("Applying GA 2: empty parent");
			}

		}
		System.out.println("Applying GA: 7. finish GA. pop size:("+size()+")");
	}
	public void applyRuleDiscovery()
	{
		/**
		 *
		 * rule discovery will apply on the entire population.
		 * select best fitness classifiers (with the same Action and the same Reward(1000/0)) as parents.
		 * children's condition and action will apply crossover and mutation
		 * crossover on condition -> bring isolated instance together,
		 * 		to avoid over specific classifier which only contains a instance
		 * mutation on action -> generate Reward=1000 classifier according to R=0 classifiers
		 *
		 * **************/
		System.out.println("\n");
		System.out.println("applyRuleDiscovery:		");
		/**calculate each action's fitness summary**/
		HashMap<A, Double> action_fitness_summary= new HashMap<A, Double>();
		action_fitness_summary= applyRuleDiscovery_1_fitnessSummary();

		/** select parent list,tournament method**/
		/** reward model: high reward parent!!!**/
		String reward_Option="high";
		HashMap<A, ArrayList<Integer>> parentList_all_actions= applyRuleDiscovery_2_selectQualifiedParent(action_fitness_summary,reward_Option);

		/** GA: morph high************/
		/** generate a general classifier and a specific one, operating on condition, directly insert into the population**/

		for(ArrayList<Integer> one_action_parents: parentList_all_actions.values())
		{
			System.out.println("	action(i) one_action_parents("+one_action_parents+")");

			if(one_action_parents == null ||one_action_parents.isEmpty())
			{
				System.out.println("		Empty: not yet: one_action_parents is null");
			}
			else
			{
				System.out.println("		 one_action_parents.length("+one_action_parents.size()+")");

			}

		}
		//System.out.println("		parentList_all_actions.size("+parentList_all_actions.size()+")");


		applyRuleDiscovery_3_morph(parentList_all_actions,reward_Option);

	}
	public void applyRuleDiscovery_3_morph(HashMap<A, ArrayList<Integer>> parentList_all_actions, String reward_Option)
	{
		/**if reward_Option is high, GA will operate on condition, which is CrossOver(Morph)
		 * else GA will operate on action, which is Mutation.
		 * **/
		if(reward_Option.equals("high"))
		{
			applyRuleDiscovery_3_1_morph_crossOver_condition(parentList_all_actions);
		}
		else
		{
			applyRuleDiscovery_3_2_morph_muation_action(parentList_all_actions);
		}
	}
	public void applyRuleDiscovery_3_1_morph_crossOver_condition(HashMap<A, ArrayList<Integer>> parentList_all_actions)
	{
		/**
		 * randomly choose an attribute and do morph
		 * **/
		/**
		 * 1. clone two classifiers
		 * 2. randomly select attribute
		 * 3. do morph
		 * 4. insert into population
		 * **/
		for(ArrayList<Integer> parentListID:parentList_all_actions.values())
		{
			if( parentListID != null )
			{
				if(parentListID.size()>1)
				{
					C parent_one;
					C parent_two;
					C child_one;
					C child_two;

					parent_one	= getClassifier(parentListID.get(0));
					parent_two = getClassifier(parentListID.get(1));

					System.out.println("applyRuleDiscovery_3_1_morph_crossOver_condition. Select parent_one ("+parent_one.getID()+") .fit("+parent_one.getFitness()+")" +
							"and parent_two("+parent_two.getID()+").fit("+parent_two.getFitness()+") ");

					//3. spawn two children, and do morph
					child_one = (C) parent_one.createSameChild();
					child_two = (C) parent_two.createSameChild();
					//int attribute_num=(int)Math.ceil(Math.random()*getClassifiersPossibleActionSetList().size());
					child_one.doMorph(child_two);

					//4. insert into population
					/**
					 *  check if the generated classifiers unique
					 *  general child should go through the subsumption precedual,
					 * in contrast, the specific children should directly insert into population.
					 * **/

					if(	checkIsTheSame_unique(child_one))
					{
						insertIntoPopulationDirectly(child_one);
					}
					else
					{
						System.out.println("applyRuleDiscovery_3_1_morph :: children_one already in the population!!!");
					}
					if( checkIsTheSame_unique(child_two))
					{
						insertIntoPopulationDirectly(child_two);
					}
					else
					{
						System.out.println("applyRuleDiscovery_3_1_morph :: children_two already in the population!!!");
					}
					//insertIntoPopulation(child_one);
						//insertIntoPopulationDirectly(child_one);
					//insertIntoPopulationDirectly(child_two);
				}
			}

		}
		System.out.println("		applyRuleDiscovery_3_morph_crossOver_condition");
	}

	public void applyRuleDiscovery_3_2_morph_muation_action(HashMap<A, ArrayList<Integer>> parentList_all_actions)
	{
		System.out.println("		applyRuleDiscovery_3_morph_muation_action");
	}

	public HashMap<A, ArrayList<Integer>> applyRuleDiscovery_2_selectQualifiedParent(HashMap<A, Double> action_fitness_summary, String reward_Option)
	{
		HashMap<A, ArrayList<Integer>> classifiedParentList= new HashMap<A, ArrayList<Integer>>();
		ArrayList<Integer> qualifiedParentList_an_action= new ArrayList<Integer>();
		double threshold=0;
		double threshold_lower=0.1; // this parameter will guarantee the elite parents selected.
		double reward_threshold_low=600;
		double reward_threshold_high=1000;

		/******************************************
		 * 1 calculate the threshold for each action
		 ****************************************** */
		HashMap<A, Double> action_fitness_threshold= new HashMap<A, Double>();

		for(Entry<A, Double> an_entry:action_fitness_summary.entrySet())
		{

			/**
			 * threshold_lower set the lower boundary for an action's threshold
			 * therefore, the new threshold will varify between threshold%- a random percentage, such as 20% to 80%*random
			 * **/
			double an_action_threshold= threshold_lower* an_entry.getValue()+ (1-threshold_lower)*an_entry.getValue()*Math.random();

			action_fitness_threshold.put(an_entry.getKey(), an_action_threshold);

		}



		//check action_fitness_threshold hashmap
		for(Entry<A, Double> an_entry:action_fitness_threshold.entrySet())
		{

			System.out.println("		check action_fitness_threshold ("+an_entry.getKey()+", "+an_entry.getValue()+").");
		}

		/** end of action_fitness threshold calculation**/


		/***************************************************************
		 * 2.classifier current parents list according to their actions
		 **************************************************************/

		for(C c:classifiers.values())
		{
			A an_LayerAction= c.getLayerAction();
			Integer id = c.getID();
			ArrayList<Integer> qualifiedParentList_an_action_temp= new ArrayList<Integer>();
			if(classifiedParentList.containsKey(an_LayerAction))
			{
				qualifiedParentList_an_action_temp=classifiedParentList.get(an_LayerAction);
				//System.out.println("	an_LayerAction("+an_LayerAction+") is contained in classifiedParentList ");
			}
			else
			{
				//System.out.println("	an_LayerAction("+an_LayerAction+") is not contained in classifiedParentList ");
			}
			qualifiedParentList_an_action_temp.add(id);


			//debug qualifiedParentList_an_action_temp
			/*
			for(Integer an_id: qualifiedParentList_an_action_temp)
			{
				System.out.println("	an_LayerAction("+an_LayerAction+")-id("+an_id+") in qualifiedParentList_an_action_temp ");
			}
			*/

			//save the classified result
			classifiedParentList.put(an_LayerAction,qualifiedParentList_an_action_temp);


		}

		/*******************************
		 * 3. select parents- touranment
		 * select parents for each action
		 ******************************/



		for(A an_action_iter: classifiedParentList.keySet())
		{
			ArrayList<Integer> action_set_ids= classifiedParentList.get(an_action_iter);
			ArrayList<Integer> parentListID = new ArrayList<Integer>();

			if(reward_Option.equals("high"))
			{
				//parentListID= new ArrayList<Integer>(selectClassifierT(action_set_ids,true)); //rule discovery.
				parentListID= selectClassifierTReward(action_set_ids,reward_threshold_low,reward_threshold_high); //rule discovery.
				//System.out.println("	Tournament Selection with Reward filter for action("+an_action_iter+") (selectClassifierTReward):: reward option: high ");

			}else
			{
				parentListID= new ArrayList<Integer>(selectClassifierT(action_set_ids,true)); //rule discovery.
				//parentListID= new ArrayList<Integer>(selectClassifierTReward(action_set_ids,0,10)); //rule discovery.

			}


			classifiedParentList.put(an_action_iter,parentListID);
		}

			return classifiedParentList;
	}


	public HashMap<A, Double> applyRuleDiscovery_1_fitnessSummary()
	{
		/**iteration through each action**/
		ArrayList<A> possibleActionSetList= getClassifiersPossibleActionSetList();
		HashMap<A, Double> action_fitness_summary= new HashMap<A, Double>();
		A an_action;
		double a_fitness=0.0;
		double a_summary=0.0;
		/**generate a reference list, from which parent will be choosen. the generation is according to the fitness **/
		/**opposite to deletion method***/

		for(A actionIter: possibleActionSetList)
		{
			//initial the action_fitness_summary, the hashmap storing summary of fitness
			action_fitness_summary.put(actionIter,a_fitness);
		}

		for(C c:classifiers.values())
		{
			an_action=c.getLayerAction();
			a_fitness=c.getFitness();
			a_summary= action_fitness_summary.get(an_action)+a_fitness;
			action_fitness_summary.put(an_action,a_summary);
		}

		//print out to check
		for(A actionIter: possibleActionSetList)
		{
			System.out.println("		applyRuleDiscovery	actionIter("+actionIter+")-" +
					"fitnessSummary("+action_fitness_summary.get(actionIter)+") <-- just check");
		}
		return action_fitness_summary;
	}

	public boolean applyDeletionRW(int popSize_threshold)
	{
		//Prepare for Roulette-Wheel Deletion
		// 1.check pop.size to decide whether to initialize deletion operation.
		// 2.calculate avFitPoP
		// 3.voteSum

		// 1.check pop.size to decide whether to initialize deletion operation.
		//Todo: calculate the number of micro classifiers
		int microPopSize=0;
		for(C c:classifiers.values())
		{
			microPopSize += c.getNumerosity();
		}

		if(microPopSize<= popSize_threshold + 10)
		{
			return false;
		}

		while(microPopSize>popSize_threshold)
		{


			// 2.calculate avFitPoP
			double avFitPop=0;
			double fitSum=0;
			//double numSum=0;

			for(C c: this.classifiers.values())
			{
				//numSum += c.getNumerosity();
				fitSum += c.getFitness()*c.getNumerosity();
			}
			avFitPop= fitSum/microPopSize;

			//System.out.println("\n");
			//System.out.println("	applyDeletionRW 1  avFitPop("+avFitPop+")= fitSum("+fitSum+")/microPopSize("+microPopSize+")");

			//3. calculate voteSum and checkPoint for RW
			double voteSum=0;
			for(C c: this.classifiers.values())
			{

				voteSum += deletionVote(c,avFitPop);
			}

			//Do Roulette-Wheel Deletion
			double choicePoint = Math.random()*voteSum;
			//System.out.println("	applyDeletionRW 1  choicePoint("+choicePoint+"), voteSum("+voteSum+")");

			voteSum=0;

			for(C c: this.classifiers.values())
			{

				voteSum += deletionVote(c,avFitPop);
				if(voteSum>choicePoint)
				{
					//System.out.println("\n");
					//System.out.println("delete cl("+c.getID()+")from Population, when voteSum("+voteSum+") > (choicePoint("+choicePoint+"))");
					deleteFromPopulation(c.getID());
					break;
				}

			}




			//prepare for next loop if microPopulationSize is larger than setting threshold.
			microPopSize=0;
			for(C c:classifiers.values())
			{
				microPopSize += c.getNumerosity();
			}
		}
		/*
		// 2.calculate avFitPoP
		double avFitPop=0;
		double fitSum=0;
		//double numSum=0;

		for(C c: this.classifiers.values())
		{
			//numSum += c.getNumerosity();
			fitSum += c.getFitness()*c.getNumerosity();
		}
		avFitPop= fitSum/microPopSize;
		System.out.println("	applyDeletionRW 1  avFitPop("+avFitPop+")= fitSum("+fitSum+")/numSum("+microPopSize+")");

		//3. calculate voteSum and checkPoint for RW
		double voteSum=0;
		for(C c: this.classifiers.values())
		{

			voteSum += deletionVote(c,avFitPop);
		}

		//Do Roulette-Wheel Deletion
		double choicePoint = Math.random()*voteSum;
		System.out.println("	applyDeletionRW 1  choicePoint("+choicePoint+"), voteSum("+voteSum+")");

		voteSum=0;

		for(C c: this.classifiers.values())
		{

			voteSum += deletionVote(c,avFitPop);
			if(voteSum>choicePoint)
			{
				deleteFromPopulation(c.getID());
				return true;
			}

		}
		return false;
		*/
		return true;

	}

	public void subsumption()
	{
		System.out.println("subsumption of the accuracy classifiers in the population according to actions");
		//this subsumption will subsumpt all the same classifiers in the population
		//1. only subsumpt between accurate classifiers,
		//sum up: fit, num, max: exp, remain: acc, niche
		// then dele the one been subsumpted,
		ArrayList<Integer> del_list = new ArrayList<Integer>();


		// establis a hashmap to tempare store classifiers that have been examed and be qualified as the one to subsume
		HashMap<A, ArrayList<Integer>> listSet = new HashMap<A, ArrayList<Integer>>();
		ArrayList<A> actionSetList =getClassifiersPossibleActionSetList();
		for(A act:actionSetList)
		{
			ArrayList<Integer> same_action_list = new ArrayList<Integer>();
			listSet.put(act, same_action_list);
		}// this has an empty listset

		// go through entire classifier popluation
		for(C c:classifiers.values())
		{
			//1. must be accurate classifier
			if(c.getAccuracy() == 1)
			{
				//2. classify according to actions
				A current_act= c.getLayerAction();
				ArrayList<Integer> same_action_list = listSet.get(current_act);

				if(same_action_list.isEmpty())
				{
					//add the first item into list as qualified one
					System.out.println("add classifier (ID."+c.getID()+") as a first qualified eator.");
					same_action_list.add(c.getID());
					//restore the listSet
					listSet.put(current_act, same_action_list);
				}
				else{
					//go through entire list for subsumption
					ArrayList<Integer> to_add_same_action_list = new ArrayList<Integer>();
					boolean is_unique_among_the_same_action_list = true;
					for(Integer a_id:same_action_list)
					{
						//check out whether they have same attributes
						C classifier_to_eat =  this.classifiers.get(a_id);
						if(classifier_to_eat.checkIsTheSame_unique(c))
						{
							//if there are the same classifiers, do subsume
							double [] statistics_group_one = classifier_to_eat.get_statistics();
							double [] statistics_group_two = c.get_statistics();
							//add fit
							statistics_group_one[3] = statistics_group_one[3] +statistics_group_two[3];
							//add num
							statistics_group_one[6] = statistics_group_one[6] +statistics_group_two[6];
							// max exp
							statistics_group_one[4] = java.lang.Math.max(statistics_group_one[4], statistics_group_two[4]);
							// reset statistics
							classifier_to_eat.setStatistics(statistics_group_one);

							//del current one
							System.out.println("  classifier_to_eat ID."+classifier_to_eat.getID()+" is about to eat ID."+ c.getID() );
							System.out.println(" add (ID ="+ c.getID()+")into del list " );
							//classifiers.remove(c.getID());  // subsumption method
							del_list.add(c.getID());
							is_unique_among_the_same_action_list = false;
						}
					}

					if(is_unique_among_the_same_action_list)
					{
						System.out.println("add classifier (ID."+c.getID()+") as a qualified eator.");
						to_add_same_action_list.add(c.getID());

					}

					if(!to_add_same_action_list.isEmpty())
					{
						for(int classifier_to_add_as_eator:to_add_same_action_list)
						{
							//add the first item into list as qualified one
							same_action_list.add(classifier_to_add_as_eator);
							//restore the listSet
							listSet.put(current_act, same_action_list);
						}
					}

				}

			}

		}
		if(!del_list.isEmpty())
		{
			for(int classifier_to_del:del_list)
			{
				System.out.println("del classifier (ID."+classifier_to_del+") from population ");
				classifiers.remove(classifier_to_del);  // subsumption method
			}

		}
	}

	public C selectClassifier(ArrayList<C> classifiers)
	{
		//this is Roulette wheel method
		double fitness_sum = 0;
		for(C c : classifiers)
		{
			fitness_sum +=c.getFitness();	//The pow method might be too bias to fitness
			//fitness_sum += Math.pow(c.getFitness(), 3);
		}
		double choice_point = Math.random() * fitness_sum;
		fitness_sum = 0;
		for(C c : classifiers)
		{
			fitness_sum += c.getFitness();
			//fitness_sum += Math.pow(c.getFitness(), 3);
			if(fitness_sum > choice_point)
				return c;
		}
		return null;
	}

	/*replaced by "selectClassifierT(ArrayList<Integer> action_set_ids,boolean IDlist)"
	public ArrayList<C> selectClassifierT(ArrayList<C> classifiers)
	{
		//todo:: realize the clone of action! actionSet.or shallow copy???

		//this applies Tournament Selection. It will return two parents.
		ArrayList<C> parents=new ArrayList<C>();
		ArrayList<C> refParents=new ArrayList<C>();
		refParents=classifiers;
		if(classifiers.size()<2)
		{
			parents.add(classifiers.get(0)) ;// parent one
			parents.add(classifiers.get(0)) ;// parent two
			return parents;
		}
		if(classifiers.size()==2)
		{
			parents=classifiers;
			return parents;
		}


		for(int j=0; j<2;j++)
		{

			double theta_sel=0.5;

			// selectdListSize is the number of reference parents, from which will select the best fitness parents
			int selectdListSize= (int)theta_sel*classifiers.size();
			if(selectdListSize<2)
			{
				selectdListSize=2; //in case not enough reference parents
			}

			//randomly sample reference parent from Action set
			//select selectdListSize in classifiers.size()
			//instead of adding, the following procedural will remove certain number of classifier from refParents.
			refParents=classifiers;
			while(refParents.size()>selectdListSize)
			{
				int removeParentsNO= (int) Math.random()*classifiers.size();
				refParents.remove(removeParentsNO);
				System.out.println("	Tournament Selection: selectdListSize("+selectdListSize+")/refParents.size("+refParents.size()+")  ");
			}
			C bestParent=refParents.get(0);
			C tempBestParent;
			double bestFitness=0;
			//select a best fitness parent from refParents
			for(int i=0;i<selectdListSize;i++)
			{
				tempBestParent=refParents.get(i);
				if(bestFitness<tempBestParent.getFitness())
				{
					bestFitness=tempBestParent.getFitness();
					bestParent=refParents.get(i);
				}
			}
			parents.add(bestParent);
		}

		return parents;
	}*/

	public ArrayList<Integer> selectClassifierT(ArrayList<Integer> action_set_ids,boolean IDlist) {

		//this applies Tournament Selection. It will return two parents.
		//todo:: realize the clone of action! actionSet.or shallow copy???
		//clone list// deep copy
		ArrayList<Integer> refParents=(ArrayList) action_set_ids.clone();
		//check reward is the same

		/**
		 * make sure parents are with the same reward ! (remove...because they should be the same in the action set)
		 * */
		/*
		double reward_lower;
		double reward_upper;

		if(Math.random()>0.5)
		{
			System.out.println("	Tournament Selection: Reward=1000");
			//refParents=selectClassifierTReward(refParents,990,1000);
			 reward_lower=990;
			 reward_upper=1000;
		}
		else
		{
			System.out.println("	Tournament Selection: Reward=0");
			reward_lower=0;
			reward_upper=10;
		}


		for(Iterator<Integer> itRefP= refParents.iterator(); itRefP.hasNext();)
		{
			Integer refP_iter = itRefP.next();
			if( getClassifier((int)refP_iter).getPredictedReward() >= reward_upper)
			{
				itRefP.remove();
			}
			else
			{

				if( getClassifier((int)refP_iter).getPredictedReward() <= reward_lower)
				{
					itRefP.remove();
				}

			}
		}
		*/

		ArrayList<Integer> selectedParents= new ArrayList<Integer>();
		if(refParents.isEmpty())
		{
			System.out.println("		in parent Tournament Selection: refParents is empty, and action_set_ids is empty");
			return refParents;// return refParents <-isEmpty()
		}
		if(refParents.size()==2)
		{
			selectedParents= (ArrayList) refParents.clone();
			return selectedParents;
		}
		if(refParents.size()==1)
		{
			//choose a only parent twice
			selectedParents.add(refParents.get(0));
			selectedParents.add(refParents.get(0));
			return selectedParents;
		}

		//now begin the Tournament selection method procedural
		ArrayList<Integer> subsetRefParents;
		for(int j=0; j<2;j++)
		{
			// each loop represents a round of tournament selection procedual
			double theta_sel = 0.5;

			// selectdListSize is the number of subset of reference parents to select from, from which will select the best fitness parents
			int selectdListSize = (int) (theta_sel*refParents.size());
			//int selectdListSize = (int) (theta_sel*action_set_ids.size());
			//System.out.println(" selectdListSize("+selectdListSize+")= refParents.size("+refParents.size()+") VS action_set_ids.size("+action_set_ids.size()+")");

			//to show
			/*debug ok
			for(Integer k: refParents)
			{System.out.println(" cl.id("+k+") in refParents");}
			*/
			if (selectdListSize < 2)
			{
				selectdListSize = 2; //in case not enough reference parents
			}
			subsetRefParents= (ArrayList)  refParents.clone();

			//now ramdonly delete id from subsetRefParents until subsetRefParents.size() equal to selectdListSize.
			System.out.println("in tournament selection: randomly select parent set (size ="+subsetRefParents.size()+" ) from (size ="+selectdListSize+")");
			if(subsetRefParents.size() <= 25)
			{	System.out.println("remove method of subsetRefParents");
				while(subsetRefParents.size()>selectdListSize)
				{
					int removeParentsNO= (int) (Math.random()*subsetRefParents.size());
					subsetRefParents.remove(removeParentsNO);
				}

			}
			else{
				//  this is to optimise program
				System.out.println("adding method of subsetRefParents");
				ArrayList<Integer> subsetRefParents_adding = new ArrayList<Integer>();
				int adding_index = (int)(Math.random()*subsetRefParents.size());
				while(subsetRefParents_adding.size()<selectdListSize)
				{
					subsetRefParents_adding.add(subsetRefParents.get(adding_index));
				}
				subsetRefParents.clear();
				subsetRefParents = (ArrayList)  subsetRefParents_adding.clone();
			}

			//System.out.println("	Tournament Selection Round("+j+"): delete element in subsetRefParent. " + "make its size converge from subsetRefParents.size("+subsetRefParents.size()+")to  selectdListSize("+selectdListSize+")");

			//to show
			/* debug ok
			for(Integer k: subsetRefParents)
			{
				System.out.println(" cl.id("+k+") in subsetRefParents");
			}
			*/
			//System.out.println("begin of one round of tournament selection");

			//now select a best parent from subsetRefParents.
			C toCompareParent;
			C bestParent;
			int bestParentID;
			double bestParentFitness;

			bestParentID=subsetRefParents.get(0);
			//bestParent= getClassifier(subsetRefParents.get(0));
			//bestParentFitness= bestParent.getFitness();
			bestParentFitness= getClassifier(subsetRefParents.get(0)).getFitness();
			for(int i=0;i<selectdListSize;i++)
			{
				toCompareParent=getClassifier(subsetRefParents.get(i));

				if(bestParentFitness<toCompareParent.getFitness())
				{
					//System.out.println("	Tournament Selection: " +"replace old.bestfit("+bestParentFitness+") " +"with new.Bestfit("+toCompareParent.getFitness()+") with prw("+toCompareParent.getPredictedReward()+")," + " bestParentID("+subsetRefParents.get(i)+")");
					bestParentFitness=toCompareParent.getFitness();
					//bestParent=getClassifier(subsetRefParents.get(i));
					bestParentID=subsetRefParents.get(i);
				}
				if(bestParentFitness==toCompareParent.getFitness())
				{
					if(Math.random()<0.5)
					{// the change to replace the best parent with a equal fitness one is 50%
						//System.out.println("	Tournament Selection: " +"replace old.bestfit("+bestParentFitness+") " +"with new.Bestfit("+toCompareParent.getFitness()+")," +" bestParentID("+subsetRefParents.get(i)+")");

						bestParentFitness=toCompareParent.getFitness();
						//bestParent=getClassifier(subsetRefParents.get(i));
						bestParentID=subsetRefParents.get(i);
					}
				}
			}//end of one round of tournament selection
			//System.out.println("end of one round of tournament selection");

			//save a best parent from one tournament selection as selected parent
			selectedParents.add(bestParentID);
		}

		return selectedParents;
	}

	public ArrayList<Integer> selectClassifierTReward(ArrayList<Integer> id_set, double reward_lower,double reward_upper)
	{
		/** a reward filter before selection classifier***/
		ArrayList<Integer> refParents=(ArrayList) id_set.clone();
		//differentiate the reward
		// remove those whose reward do not match


		for(Iterator<Integer> itRefP= refParents.iterator(); itRefP.hasNext();)
		{
			/*
			if( getClassifier(itRefP.next()).getPredictedReward() >= reward_upper  )
			{
				itRefP.remove();
			}
			else
			{
				if( getClassifier(itRefP.next()).getPredictedReward() <= reward_lower)
				{
					itRefP.remove();
				}
			}
			*/
			int iter=itRefP.next();
			if( getClassifier(iter).getPredictedReward() > reward_upper ||  getClassifier(iter).getPredictedReward() < reward_lower )
			{
				//System.out.println("	Tournament Selection with Reward filter:: remove with a reward("+getClassifier(iter).getPredictedReward() +")");
				itRefP.remove();
			}

		}
		//check if refParent is empty;
		if(refParents.isEmpty())
		{
			System.out.println("	Tournament Selection with Reward filter (selectClassifierTReward):: empty parent ");
			return null;
		}
		refParents = selectClassifierT(refParents,true);
		return refParents;
	}

	public void increaseTimeSinceGA()
	{
		for(C c: this.classifiers.values())
			c.setIterations_since_ga(1+c.getIterations_since_ga());
	}

	public ArrayList<C> getClassifiers() 
	{
		return new ArrayList<C>(this.classifiers.values());
	}

	public  ArrayList<C> getClassifierSubset(ArrayList<Integer> IDs)
	{
		//this will return classifierSet according to the IDs
		ArrayList<C> classifierSubset= new ArrayList<C>();

		for(int i=0; i< IDs.size();i++)
		{
			C a_classifier=this.classifiers.get((int)IDs.get(i));
			classifierSubset.add(a_classifier);
		}
		return classifierSubset;
	}
	
	public void display(Graphics2D g, int offset_x, int offset_y) 
	{
		for(C c : this.classifiers.values())
			c.display(g, offset_x, offset_y);
	}
	
	public int size() {
		return this.classifiers.size();
	}
}
