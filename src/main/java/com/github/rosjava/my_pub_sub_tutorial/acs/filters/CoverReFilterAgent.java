package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerAction;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerClassifier;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerPopulation;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.FitnessCount;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Statistics;

public class CoverReFilterAgent<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{
	public static final String NAME = "Cover Reinforcers Filter";
	public static  String file;
	public CoverReFilterAgent(String file)
	{
		super(NAME);
		this.file= file;
	}
	
	@Override
	public FilterMessage<C, A> apply(P agent, int iterationCount, FilterMessage<C,A> message) 
	{
		boolean str=file.equals("pidmm");
		System.out.println("+++ CoverReFilterAgent: file("+this.file+")/("+str+")");
		if(str)
		{
			System.out.println("+++ CoverReFilterAgent: file("+this.file+")/("+str+") +++");
		}
		//the operation COVER will be called if matchSet is below Minmun limit and the diversity of actions are not enough.
		//Cover will create a new classifier and insert into population
		//the new classifier will have the condition, which match the current state,
		//	 and an action, which try to meet action's diversity requirement.

		System.out.println("apply CoverReFilterAgent: message.getMatchSet().size() = "+message.getMatchSet().size() + "; agent = "+ agent );
		//1.calculate the MatchSet
		boolean matchSetSizeCondition=false;
		if(message.getMatchSet().size() < Statistics.MINIMUM_SIZE)
		{
			matchSetSizeCondition=true;
		}
		//2. calculate the diversity of actions in MatchSet
		boolean actionSetSizeCondition=false;

		int ActionSetSizeInMatchSet=agent.getActionSetSizeInMatchSet(message.getMatchSet());
		int ActionSetSizePossibility=agent.getActionSetSizePossibility();
		if(ActionSetSizeInMatchSet<ActionSetSizePossibility)
		{
			actionSetSizeCondition=true;
		}
		////3. active COVER operation
		//TODO: condition mutaion
		System.out.println("apply CoverReFilterAgent: matchSetSizeCondition("+matchSetSizeCondition+")" +
				" || actionSetSizeCondition("+actionSetSizeCondition+").");
		if(matchSetSizeCondition||actionSetSizeCondition)
		{
			System.out.println("apply CoverReFilterAgent: createClassifierFromStateWithAction active" +
					" to cover current state"+message.getCurrentReState()+".");
			//3. active COVER operation
			C currentState = message.getCurrentReState();
			int newClass = agent.createClassifierFromStateWithAction(currentState, message.getMatchSet());
			if(newClass != -1)
			{
				message.getMatchSet().add(newClass);
				printMessage("add a new classifier into MatchSet, adding classifier's id(newClass)="+newClass,agent);
			}

		}

		return message;
		/*
		printMessage("message.getMatchSet().size() ('"+message.getMatchSet().size()+"') VS(<) Statistics.MINIMUM_SIZE("+Statistics.MINIMUM_SIZE+")", agent);
		//if(message.getMatchSet().size() < Statistics.MINIMUM_SIZE) // replace this by actions diversity
		{
			printMessage("Less than minimun size ->	apply covering", agent);
			C currentState = message.getCurrentState();
			//int newClass = cover(agent, currentState, message.getMatchSet());
			int newClass = agent.createClassifierFromStateWithAction(currentState, message.getMatchSet());
			if(newClass != -1)
			{
				message.getMatchSet().add(newClass);
				printMessage("add a new classifier into MatchSet, adding classifier's id(newClass)="+newClass,agent);
			}
			printMessage("message.getMatchSet().size(): " + message.getMatchSet().size(), agent);
		}

		return message;
		*/
	}



	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +"::::"+message;
		agent.printMessage(s);
	}
}
