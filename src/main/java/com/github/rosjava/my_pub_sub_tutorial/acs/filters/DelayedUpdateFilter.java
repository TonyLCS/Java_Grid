package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.LinkedList;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Statistics;

public class DelayedUpdateFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{	
	private LinkedList<FilterMessage<C, A>> previous_messages = new LinkedList<FilterMessage<C, A>>();
	//private final double discount = 0.5;
	private final double discount = 1;

	public static final String NAME = "Delayed Update Filter";
	
	public DelayedUpdateFilter()
	{
		super(NAME);
	}
	
	@Override
	public FilterMessage<C, A> apply(P agent, int iterationCount, FilterMessage<C, A> message)
	{
System.out.println("apply DelayedUpdateFilter:  message.isComplete()= '"+message.isComplete()+ "'; agent = '"+ agent +"'" );

		printMessage("apply DelayedUpdateFilter: Updating: "+message.isComplete(), agent);
		previous_messages.add(message.clone());
		boolean completed_goal = message.isComplete();
		if(completed_goal)
		{
System.out.println("apply DelayedUpdateFilter: completed_goal='"+completed_goal+"'<---is true, so updateActionSet!!! agent = '"+ agent +"'" );
			printMessage("Goal Complete!", agent);
			updateActionSet(agent);

		}
		return message;
	}
	
	private void updateActionSet(P population)
	{
		//population.updateClassifiers(message, Statistics.MAX_REWARD); 
		int count = 0;
		printMessage("DelayedUpdateFilter: Update Size: "+previous_messages.size(), population);
		while(!previous_messages.isEmpty())
		{
			FilterMessage<C, A> message = previous_messages.poll();
			double reward = Statistics.MAX_REWARD * Math.pow(discount, count);
			printMessage("DelayedUpdateFilter: Updating Classifiers!: Reward: " + reward + " Classifiers: " + message.getActionSet().size(), population);
			count++;		
			population.updateClassifiersAction(message, reward);
		}
	}
	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +" "+message;
		agent.printMessage(s);
	}
	
}
