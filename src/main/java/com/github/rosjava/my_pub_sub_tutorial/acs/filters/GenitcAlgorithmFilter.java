package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

public class GenitcAlgorithmFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{
	private final int GA_THRESHOLD;
	
	public static final String NAME = "Genitc Algorithm Filter";
	
	
	public GenitcAlgorithmFilter(int ga_threshold)
	{
		super(NAME);
		this.GA_THRESHOLD = ga_threshold;
	}

	@Override
	public FilterMessage<C, A> apply(P agent, int iterationCount, FilterMessage<C, A> message) 
	{
		printMessage("GA Filter", agent);

		//Henry actually apply GA in match set.
		ArrayList<Integer> match_set_ids = message.getMatchSet();//?? should GA apply to action_set!!!
		agent.applyGA(match_set_ids, iterationCount, GA_THRESHOLD);

		//ArrayList<Integer> action_set_ids = message.getActionSet();//?? should GA apply to action_set!!!
		//agent.applyGA(action_set_ids, iterationCount, GA_THRESHOLD);
		agent.increaseTimeSinceGA();
		return message;
	}
	
	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +" "+message;
		agent.printMessage(s);
	}
}
