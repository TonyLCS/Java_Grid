package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Statistics;

public class MitosisFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{
	public static final String NAME = "Mitosis Filter";
	
	public MitosisFilter()
	{
		super(NAME);
	}
	
	@Override
	public FilterMessage<C, A> apply(P agent, int iterationCount, FilterMessage<C,A> message) 
	{
		//System.out.println("in Mitosis Filter! ");

		if(!message.getMitosisSet().isEmpty())
		{
			System.out.println("apply Mitosis Filter : message.getMitosisSet().size( "+message.getMitosisSet().size()+")");

			if(message.getMitosisSet().size()>=1)
			{
				/**when XCS cover the state, it should produce the representation(crystal eye),
				 *  which comes from a current states(single eye).**/
				C currentState = message.getCurrentReState();
				System.out.println("apply Mitosis Filter:" +
								"message.getCurrentReState("+message.getCurrentReState()+")\t"+
								" getCurrentState_representation ("+currentState+"). \t" +
								" getCurrentState("+message.getCurrentState()+")");

				//int newClass = agent.createClassifierFromStateWithAction(currentState, message.getMitosisSet());
				agent.create_Classifier_by_mitosis(currentState, message.getMitosisSet());
				//int newClass = cover(agent, currentState);

			}
		}
		//System.out.println("in Mitosis Filter, end ");

		return message;
	}

	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +"::::"+message;
		agent.printMessage(s);
	}
}
