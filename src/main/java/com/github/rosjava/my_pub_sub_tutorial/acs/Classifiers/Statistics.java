package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;

import java.io.Serializable;

public class Statistics implements Comparable<Statistics>, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final double MAX_REWARD = 1000;
	public static final double org_error = MAX_REWARD * 0.05;// err threshold
	public static final double HASH_CHANCE = 10;
	
	public static final  int MINIMUM_SIZE = 1;
	
	public double BETA = 0.2; //learning rate
	public static final double ALPHA = 0.1;
	public static final double V = 3;
	public static final double MUTATE_CHANCE = 0.05;
	
	private double predicted_reward = 0.1;
	private double accuracy = 0.000000000001;
	private double fitness 	= 0.000000000001;
	
	private int experience = 0;//aka age????!!!! should set as 0
	private double niche_size = 1;
	private double prediction_error = MAX_REWARD-predicted_reward;
	private int numerosity = 1;

	private double niche_predicted_reward;
	//boolean flag_first_blood=true;
	
	public Statistics() { }

	public void inherit(Statistics parent)
	{
		this.predicted_reward = parent.predicted_reward;
		this.accuracy = parent.accuracy;
		this.fitness = parent.fitness;
		this.niche_size = parent.niche_size;
		this.numerosity = 1;
		this.prediction_error = parent.prediction_error;
		this.niche_predicted_reward = parent.niche_predicted_reward;
		this.experience = 0;
	}

	public double getAccuracy()
	{
		return accuracy;
	}
	
	public int getNumerosity()
	{
		return this.numerosity;
	}
	public double getNichSize()
	{
		return this.niche_size;
	}

	public void decreaseNumerosity()
	{
		 this.numerosity=(this.numerosity-1);
	}

	public void updateSet(double reward, double action_size_sum_num)
	{
		/*
		System.out.println("				Statistics calculation 1 real reward : " +reward+
				" update 4 statistics: \n" +
				"				before==>" +
				" prd='"+predicted_reward+"';" +
				" exp='"+experience+"';" +
				" err='"+prediction_error+"';" +
				" acc='" +accuracy+"';"+
				" niche='"+niche_size+"';  /" +
				" num("+numerosity+"), fit("+fitness+") .");
		*/
		experience++;//increase experience as it has been used again
		
		double reward_difference = reward - predicted_reward;
		//update prediction value
		//update prediction error
		if(experience < (1/BETA))
		{
			/*System.out.println("			Statistics calculation 1 :	" +
					"experience"+experience+" < 1/BETA (5): " +
					"predicted_reward = " +
					"predicted_reward("+predicted_reward+")" +
					" + reward_difference("+reward_difference+")/ experience("+experience+")");*/


			predicted_reward = predicted_reward + reward_difference / experience;
			/*System.out.println("		Statistics calculation 1 : " +
					"prediction_error = " +
					"prediction_error (" +prediction_error+"')"+
					"+ (Math.abs(reward_difference) ("+Math.abs(reward_difference)+") " +
					"- prediction_error) / experience");*/

			prediction_error = prediction_error + (Math.abs(reward_difference) - prediction_error) / experience;
			niche_size = niche_size + (action_size_sum_num - niche_size) / experience;

			/*
			if(experience<=3)
			{
				//the reason that choose 3 rather than 2 is that: the third is for those classifiers is children. therefore, their err can be large than it supposed to be.
				//predicted_reward=reward;
				prediction_error=Math.abs(reward_difference) ;// because the err is relied on prd,therefore, the first initial value should based on the experience 1
				//niche_size=action_size_sum_num;
			}
			*/

		}
		else
		{
			/*System.out.println("	" +
					"predicted_reward =" +
					"predicted_reward("+predicted_reward+")" +
					" + BETA *  reward_difference("+reward_difference+")");*/
			predicted_reward = predicted_reward + BETA * reward_difference;
			/*System.out.println("		Statistics calculation 1 :" +
					"prediction_error =" +
					"prediction_error (" +prediction_error+"')"+
					"+ BETA * (Math.abs(reward_difference) ("+Math.abs(reward_difference)+") " +
					" - prediction_error)");*/
			prediction_error = prediction_error + BETA * (Math.abs(reward_difference) - prediction_error);
			niche_size = niche_size + BETA * (action_size_sum_num - niche_size);
		}
		
		if(prediction_error < org_error )
		{
			this.accuracy = 1;

			/*
			//this.BETA=0.02;
			if(flag_first_blood)
			{
				prediction_error=Math.abs(reward_difference);

				flag_first_blood=false;
			}
			*/
			/*
			System.out.println("				Statistics calculation 1 : prediction_error(prediction_error)< org_error " +
					"=> accuracy = 1 <--acc");
					*/
		}
		else
		{
			this.accuracy = ALPHA * Math.pow((prediction_error / org_error), -V);
			//this.BETA=0.2;
			/*
			System.out.println("						Statistics calculation 1 : this.accuracy ("+this.accuracy+") <--acc-- = " +
					"ALPHA (0.1) * " +
					"Math.pow((prediction_error("+prediction_error+") / org_error(100)), -V) " +
					"("+Math.pow((prediction_error / org_error), -V)+");");
			*/
		}
		//System.out.println("		Statistics calculation 1 : this.accuracy ("+this.accuracy+")");
		/*
		System.out.println("				after ==>" +
				" prd='"+predicted_reward+"';" +
				" exp='"+experience+"';" +
				" err='"+prediction_error+"';" +
				" acc='" +accuracy+"';"+
				" niche='"+niche_size+"';  /" +
				" num("+numerosity+"), fit("+fitness+") .");
		*/
		//System.out.println("  \n");

		String s1 = "Statistics:\n";
		s1 += "Experience      : " + experience +"\n";
		s1 += "Predicted Reward: " + predicted_reward +"\n";
		s1 += "Prediction Error: " + prediction_error +"\n";
		s1 += "Niche Size      : " + niche_size +"\n";
		s1 += "Accuracy        : " + accuracy +"\n";
		s1 += "others statistics without update\n";
		s1 += "Fitness         : " + fitness +"\n";
		s1 += "Numerosity      : " + numerosity +"\n";
		//System.out.println("level 2.1: reward='"+reward+"', the action_map update! statisitics:"+s1+"<----exp,reward,reward_err,niche_size,accuracy--------");
		/*System.out.println("Statistics calculation 1 : reward='"+reward+"';" +
				" predicted_reward='"+predicted_reward+"';" +
				" prediction_error='"+prediction_error+"'; " +
				" experience='"+experience+"'; " +
				" niche_size='"+niche_size+"';" +
				" accuracy='"+accuracy+"'; <-----predicted_reward,prediction_error,experience,niche_size,accuracy-----");*/
	}

	public void updateSet_learning_rate(double reward, double action_size_sum_num, double learning_rate)
	{
		double beta_default = BETA;
		BETA = learning_rate;

		experience++;//increase experience as it has been used again

		double reward_difference = reward - predicted_reward;

		//predicted_reward = predicted_reward + BETA * reward_difference;
		//prediction_error = prediction_error + BETA * (Math.abs(reward_difference) - prediction_error);
		//niche_size = niche_size + BETA * (action_size_sum_num - niche_size);
		/*
		if(experience < (1/BETA))
		{
			predicted_reward = predicted_reward + reward_difference / experience;
			prediction_error = prediction_error + (Math.abs(reward_difference) - prediction_error) / experience;
			niche_size = niche_size + (action_size_sum_num - niche_size) / experience;
		}
		*/
		predicted_reward = predicted_reward + BETA * reward_difference;
		prediction_error = prediction_error + BETA * (Math.abs(reward_difference) - prediction_error);
		niche_size = niche_size + BETA * (action_size_sum_num - niche_size);


		if(prediction_error < org_error )
		{
			this.accuracy = 1;
		}
		else
		{
			this.accuracy = ALPHA * Math.pow((prediction_error / org_error), -V);
		}

		BETA = beta_default;
	}

	public void updateSet_incremental(double incremental_reward, double action_size_sum_num)
	{

		experience++;//increase experience as it has been used again

		double reward_difference = incremental_reward;
		//update prediction value
		//update prediction error
		if(experience < (1/BETA))
		{

			predicted_reward = predicted_reward + reward_difference / experience;
			prediction_error = prediction_error + (Math.abs(reward_difference) - prediction_error) / experience;
			niche_size = niche_size + (action_size_sum_num - niche_size) / experience;

		}
		else
		{
			predicted_reward = predicted_reward + BETA * reward_difference;
			prediction_error = prediction_error + BETA * (Math.abs(reward_difference) - prediction_error);
			niche_size = niche_size + BETA * (action_size_sum_num - niche_size);
		}

		if(prediction_error < org_error )
		{
			this.accuracy = 1;

			/*
			//this.BETA=0.02;
			if(flag_first_blood)
			{
				prediction_error=Math.abs(reward_difference);

				flag_first_blood=false;
			}
			*/
			/*
			System.out.println("				Statistics calculation 1 : prediction_error(prediction_error)< org_error " +
					"=> accuracy = 1 <--acc");
					*/
		}
		else
		{
			this.accuracy = ALPHA * Math.pow((prediction_error / org_error), -V);
			//this.BETA=0.2;
			/*
			System.out.println("						Statistics calculation 1 : this.accuracy ("+this.accuracy+") <--acc-- = " +
					"ALPHA (0.1) * " +
					"Math.pow((prediction_error("+prediction_error+") / org_error(100)), -V) " +
					"("+Math.pow((prediction_error / org_error), -V)+");");
			*/
		}
		//System.out.println("		Statistics calculation 1 : this.accuracy ("+this.accuracy+")");
		/*
		System.out.println("				after ==>" +
				" prd='"+predicted_reward+"';" +
				" exp='"+experience+"';" +
				" err='"+prediction_error+"';" +
				" acc='" +accuracy+"';"+
				" niche='"+niche_size+"';  /" +
				" num("+numerosity+"), fit("+fitness+") .");
		*/
		//System.out.println("  \n");

		String s1 = "Statistics:\n";
		s1 += "Experience      : " + experience +"\n";
		s1 += "Predicted Reward: " + predicted_reward +"\n";
		s1 += "Prediction Error: " + prediction_error +"\n";
		s1 += "Niche Size      : " + niche_size +"\n";
		s1 += "Accuracy        : " + accuracy +"\n";
		s1 += "others statistics without update\n";
		s1 += "Fitness         : " + fitness +"\n";
		s1 += "Numerosity      : " + numerosity +"\n";
		//System.out.println("level 2.1: reward='"+reward+"', the action_map update! statisitics:"+s1+"<----exp,reward,reward_err,niche_size,accuracy--------");
		/*System.out.println("Statistics calculation 1 : reward='"+reward+"';" +
				" predicted_reward='"+predicted_reward+"';" +
				" prediction_error='"+prediction_error+"'; " +
				" experience='"+experience+"'; " +
				" niche_size='"+niche_size+"';" +
				" accuracy='"+accuracy+"'; <-----predicted_reward,prediction_error,experience,niche_size,accuracy-----");*/
	}

	public void updateFitness(double sum_relative_accuracy)
	{
		//System.out.println("						Statistics calculation 3 :  'fitness = fitness('"+fitness+"') + BETA * ((accuracy('"+accuracy+"') * numerosity ('"+numerosity+"')) / sum_relative_accuracy('"+sum_relative_accuracy+"') - fitness('"+fitness+"')); '<====the action_map update! update fitness");
		//fitness = fitness + BETA * ((accuracy * numerosity) / sum_relative_accuracy - fitness);
		fitness = fitness + BETA * (accuracy*numerosity  / sum_relative_accuracy - fitness);//get rid of numerosity. because the numerosity will make fitness method bias to marcoclassifiers.
		//however, the fitness should stick to accuracy. The numerosity may have imappropiate influence to the fitness.
		// General classifier will tent to have more numerosity than specific classifiers.
		// as a result, the fitness method will bias to the general one rather than the specific one.
		// In some case, the influence of accuracy will be decrease because of numerostiy, which is undesirable for accuracy based classifiers.
		//System.out.println("				Statistics calculation 3:  update fitness:"+fitness+"---[num("+this.numerosity+"),acc("+this.accuracy+"),exp("+this.experience+")]---");
	}

	public double getFitness() 
	{
		return this.fitness;
	}

	@Override
	public int compareTo(Statistics value) 
	{
		return (int)(this.fitness - value.fitness);
	}

	public void mutateStatistics(Statistics statistics) 
	{
		this.experience = 0;
		this.numerosity = 1;
		this.accuracy = 0.000000000001;// statistics.accuracy * 0.5;
		this.fitness = 0.000000000001;// statistics.fitness * 0.5;
		this.predicted_reward = statistics.predicted_reward / 2;
		this.prediction_error = statistics.prediction_error + statistics.predicted_reward / 2;
		//this.prediction_error = statistics.prediction_error * 2;
	}

	public void increaseNumerosity() 
	{
		this.numerosity++;
		if(this.numerosity>=30)
		{
			this.numerosity =30;
		}
	}
	
	public String toString()
	{
		String s = "Statistics: ";
		s += "		Predicted Reward: " + predicted_reward +"\t";
		s += "		Prediction Error: " + prediction_error +"\t";
		s += "		Accuracy: " + accuracy +"\t";
		s += "		Fitness: " + fitness +"\t";
		s += "		Experience: " + experience +"\t";
		s += "		Niche Size: " + niche_size +"\t";
		s += "		Numerosity: " + numerosity +"\t";

		/*
		double [] statistics_csv=statistics_toCSVSet();
		for(int i=0; i<statistics_csv.length;i++)
		{
			System.out.println(statistics_csv[i]);
		}
		*/
		return s;
	}

	public String[] statistics_toCSVSet()
	{
		String [] statistics_csv= new String[7];

		statistics_csv[0]=  Double.toString( predicted_reward);
		statistics_csv[1]=  Double.toString( prediction_error) ;
		statistics_csv[2]=  Double.toString( accuracy) ;
		statistics_csv[3]=  Double.toString( fitness) ;
		statistics_csv[4]=  Double.toString( experience) ;
		statistics_csv[5]=  Double.toString( niche_size) ;
		statistics_csv[6]=  Double.toString( numerosity) ;


		return statistics_csv;
	}
	public void set_statistics(double [] statistics_csv)
	{
		//String [] statistics_csv= new String[7];

		this.predicted_reward = statistics_csv[0];
		this.prediction_error = statistics_csv[1];
		this.accuracy = statistics_csv[2]; //=  Double.toString( accuracy) ;
		this.fitness = statistics_csv[3]; //=  Double.toString( fitness) ;
		this.experience = (int)statistics_csv[4]; //=  Double.toString( experience) ;
		this.niche_size = statistics_csv[5]; ///=  Double.toString( niche_size) ;
		this.numerosity = (int)statistics_csv[6]; //=  Double.toString( numerosity) ;

	}
	public double[] get_statistics()
	{
		double [] statistics_csv= new double[7];

		statistics_csv[0]= predicted_reward;
		statistics_csv[1]=  prediction_error;
		statistics_csv[2]=  accuracy;
		statistics_csv[3]=  fitness;
		statistics_csv[4]=  experience;
		statistics_csv[5]=  niche_size;
		statistics_csv[6]=  numerosity;

		return statistics_csv;
	}

	public String[] statistics_HeadertoCSVSet()
	{
		String [] statistics_csv= new String[7];

		statistics_csv[0]=  "PRD";
		statistics_csv[1]=  "ERR";
		statistics_csv[2]=  "ACC";
		statistics_csv[3]=  "FIT";
		statistics_csv[4]=  "EXP";
		statistics_csv[5]=  "NICHE";
		statistics_csv[6]=  "NUM";


		return statistics_csv;
	}

	public double getPredictedReward()
	{
		return this.predicted_reward;
	}
	public double getPredictedRewardError()
	{
		return this.prediction_error;
	}
	public double getExperience()
	{
		return this.experience;
	}

	public void  set_niche_predicted_reward(double niche_predicted_reward)
	{
		this.niche_predicted_reward = niche_predicted_reward;
	}

	public double get_niche_predicted_reward()
	{
		return  this.niche_predicted_reward;
	}

}
