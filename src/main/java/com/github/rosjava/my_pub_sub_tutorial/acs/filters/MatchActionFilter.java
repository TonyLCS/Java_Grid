package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

/**
 *  generate match_action_set according to an action
 *
 */

public class MatchActionFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{	
	public static final String NAME = "Match Filter for actions";
		
	public MatchActionFilter()
	{
		super(NAME);
	}
	
	@Override
	public FilterMessage<C,A> apply(P agent, int iterationCount, FilterMessage<C,A> message) 
	{
		//find match set according to current reinforcer state
		ArrayList<Integer> match_action_set = find_match_action_set(agent, message.getCurrentAction());
		
		//printMessage("Set size: " + matchSet.size(), agent);
		//printMessage("Set size: '" + matchSet+"'<-", agent);
		//printMessage("	apply MatchReFilter: matchSet.size= '" + matchSet.size()+"'; matchSet='" + matchSet+"'<-", agent);

		message.setMatchActionSet(match_action_set);
//System.out.println("apply MatchFilter: matchSet.size()= "+message.getMatchSet().size() + "; agent = "+ agent );

		return message;
	}

	public ArrayList<Integer> find_match_action_set(P population, A getCurrentAction)
	{
		ArrayList<Integer> matchSet = population.getMatchingClassifiers_for_action(getCurrentAction);
		return matchSet;
	}

	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +"::::"+message;
		agent.printMessage(s);
	}
}
