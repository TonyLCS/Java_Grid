package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;


public class ClassifierExpectation<A extends ActionInterface> implements Serializable
{

	private static final long serialVersionUID = 1L;
	//Classifier ID //Fitness
	private HashMap<Integer, Double> classifierFitness = new HashMap<Integer, Double>();
	private final A ACTION;
	private final double BETA = 0.5;

	//TODO:Tony: add statistics here
	protected HashMap<Integer, Statistics> modifier_mapping = new HashMap<Integer, Statistics>();



	public double getOneClassifierFitness(Integer choosed_modifier_id)
	{
		return this.classifierFitness.get(choosed_modifier_id);
	}
	public double getOneModifierFitness(Integer choosed_modifier_id)
	{
		return this.modifier_mapping.get(choosed_modifier_id).getFitness();
	}

	public ClassifierExpectation(A action, ArrayList<Integer> ids)
	{
		this.ACTION = action;
		for(int i : ids)
		{
			classifierFitness.put(i, 0.5);
			modifier_mapping.put(i, new Statistics());
			//check mod.stat is establish
			System.out.println("	mod.stat 1:		mod.stat is establish: " +
					" action ='"+action+"';" +
					" mod.id ='"+i+"';" +
					" mod.stat.fit='"+modifier_mapping.get(i).getFitness()+"';" +
					" mod.stat.acc='"+modifier_mapping.get(i).getAccuracy()+"'.");

		}
	}
	
	public void updateIDs(ArrayList<Integer> ids)
	{
		for(Integer id : ids)
		{
			if(!classifierFitness.containsKey(id))
			{
				classifierFitness.put(id, 0.5);
			}
			//TODO (Tony): initialise Statistics
			if(!modifier_mapping.containsKey(id))
			{
				modifier_mapping.put(id, new Statistics());
				System.out.println("	mod.stat 1.2:		mod.stat is establish: " +
						" action ='???';" +
						" mod.id ='"+id+"';" +
						" mod.stat.fit='"+modifier_mapping.get(id).getFitness()+"';" +
						" mod.stat.acc='"+modifier_mapping.get(id).getAccuracy()+"'.");

			}
		}
	}

	public void showModStat()
	{// show the statics in new mod.stat ->modifier_mapping = new HashMap<Integer, Statistics>()
		System.out.println("			show all fitness in all exsisting mod: mod.stat.size()='"+modifier_mapping.size()+"'.");
		ArrayList<Map.Entry<Integer, Statistics>> entries = new ArrayList<Map.Entry<Integer, Statistics>>(modifier_mapping.entrySet());

		for(Map.Entry<Integer, Statistics> entry : entries)
		{
			System.out.println("					mod.stat.id='"+entry.getKey()+"';" +
					" mod.stat.fit='"+entry.getValue().getFitness()+"';");
		}

	}

	public int getPrediction()
	{
		ArrayList<Map.Entry<Integer, Double>> entries = new ArrayList<Map.Entry<Integer, Double>>(classifierFitness.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<Integer, Double>>() 
		{
			@Override
			public int compare(Map.Entry<Integer, Double> entry1, Map.Entry<Integer, Double> entry2) 
			{
				return entry2.getValue().compareTo(entry1.getValue());//descend order
				//return entry1.getValue().compareTo(entry2.getValue());

			}
		});
		System.out.println("					check the descend order of modifiers_expectations.classifierExpectation.ClassifierFitness.(int,double)");
		for(Map.Entry<Integer, Double> entry : entries)
		{
			System.out.println("					mod.old.fit.id='"+entry.getKey()+"'; mod.old.fit.value='"+entry.getValue()+"';");
		}
		if(!entries.isEmpty())
			return entries.get(0).getKey();
		
	    return -1;
	}

	public int getBestModifier()
	{

		ArrayList<Map.Entry<Integer, Statistics>> entries = new ArrayList<Map.Entry<Integer, Statistics>>(modifier_mapping.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<Integer, Statistics>>()
		{
			@Override
			public int compare(Map.Entry<Integer, Statistics> entry1, Map.Entry<Integer, Statistics> entry2)
			{
				////entry1.getValue()== Stat
				double fit1= entry1.getValue().getFitness();
				double fit2= entry2.getValue().getFitness();
				//return entry2.getValue().compareTo(entry1.getValue());//descend order


				int randomFlag=0;
				double fitnessDiff=fit1-fit2;
				//double fitnessDiff=entry1.getValue().getFitness() - entry2.getValue().getFitness();
				if(Math.abs(fitnessDiff)<=0.000001)
				{
					if(Math.random() >= 0.5)
					{
						randomFlag=1;
					}else
					{
						randomFlag=-1;
					}
					return (randomFlag);
				}
				//TODO: ways define best action:1. greater fitness win
				//TODO: 2.including reward in this process.
				if(fitnessDiff>0.000001)
				{
					randomFlag=-1;// descent mode
				}
				if(fitnessDiff<-0.000001)
				{
					randomFlag=1;
				}

				return (randomFlag);
			}
		});

		for(Map.Entry<Integer, Statistics> entry:entries)
		{
			int 	testModifier_ID = (int)entry.getKey();
			double 	testModifier_fit= entry.getValue().getFitness();

			System.out.println(" 					in sort mod.fit chose BestModifier::" +
					" 	mod.id='"+testModifier_ID+"'; mod.fit='"+testModifier_fit+"'.");
		}

		int BestModifier_ID = (int)entries.get(0).getKey();

		return BestModifier_ID;
	}
	
	public double getExpectationFitness()
	{
		ArrayList<Map.Entry<Integer, Double>> entries = new ArrayList<Map.Entry<Integer, Double>>(classifierFitness.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<Integer, Double>>() 
		{
			@Override
			public int compare(Map.Entry<Integer, Double> entry1, Map.Entry<Integer, Double> entry2) 
			{
				return entry1.getValue().compareTo(entry2.getValue());
			}
		});
		if(!entries.isEmpty())
			return entries.get(0).getValue();
		return 0;
	}

	public void updateModMappingStat1(Integer a_modifierID, double reward,int discount)
	{
		//update stat in this.modifier_mapping HashMap<Integer, Statistics>()
		//	private ArrayList<HashMap<Emotion, Integer>> bowTieSet= new ArrayList<HashMap<Emotion, Integer>>();

		//1. update exp, rwd, err, acc, niche for each bowTieSet
		// input a single bowTie,rwd
		int niche_size= modifier_mapping.size();
		Statistics statistics = modifier_mapping.get(a_modifierID);
		statistics.updateSet(reward, niche_size);

	}
	public double get_a_modifier_acc(Integer a_modifierID)
	{
		double a_modifier_acc=0;
		a_modifier_acc= modifier_mapping.get(a_modifierID).getAccuracy();
		return a_modifier_acc;
	}

	public void  updateModMappingStat2(Integer a_modifierID, double sum_bowTieSet_acc)
	{
		Statistics statistics = modifier_mapping.get(a_modifierID);
		statistics.updateFitness(sum_bowTieSet_acc);
	}

	/*
	public void updateExpectStat1(A action, double reward,int niche_size)
	{
		Statistics statistics = action_mapping.get(action);
		statistics.updateSet(reward, niche_size);
		//statistics.updateFitness(statistics.getAccuracy());
		//statistics.updateFitness(sum_actionSet_acc);
		String s2= statistics.toString();
		System.out.println("level2.3: statistics of action('"+action+"') (part2) :"+s2+"<----but it don's save into population and niche_size is set to approach to 1");
		//save statistics into action_mapping
		//action_mapping.put(action, statistics);

	}
	*/
	public boolean updatePredictionFitness(ArrayList<Integer> ng_clamatching_classifiersssifiers)
	{	//this function is called by this line:
		// "this.modifiers_expectations.get(emotion).updatePredictionFitness(this.expected_ids.get(emotion));"
		// this.expected_ids.get(emotion)==>ArrayList<Integer> ng_clamatching_classifiersssifiers
		// therefore, ArrayList<Integer> ng_clamatching_classifiersssifiers  is the bow-tie list

		boolean correct_prediction = false;
		int prediction = getPrediction();
		
		if(prediction != -1)
		{	
			for(Integer i : ng_clamatching_classifiersssifiers)
				correct_prediction = correct_prediction || (i == prediction);
			//update prediction
			for(Integer i : ng_clamatching_classifiersssifiers)
			{
				if(classifierFitness.containsKey(i))
				{
					double probability = classifierFitness.remove(i);
					probability = probability + BETA * (1 - probability);
					classifierFitness.put(i, probability);

					System.out.println("update expectation part4 <----modifiers_expectation->classifierFitness='"+probability+"',modifier_id='"+i+"'");

				}
				else
				{
					classifierFitness.put(i, 0.5);
					System.out.println("update expectation part4 <----modifiers_expectation->classifierFitness='0.5'<---put ,modifier_id='"+i+"'");

				}

			}
//			if(correct_prediction)//correct prediction
//			{
//				//reward
//				double probability = classifierFitness.remove(prediction);
//				probability = probability + BETA * (1 - probability);
//				classifierFitness.put(prediction, probability);
//			}
//			else if(!correct_prediction)//wrong prediction
//			{
//				//punish (BAD PREDICTION! BAD!)
//				double probability = classifierFitness.remove(prediction);
//				probability = probability - BETA * (1 - probability);
//				classifierFitness.put(prediction, probability);
//			}
		}
		else
		{
			//Add prediction with average level fitness as startingmatching_classifiers
			for(Integer cid : ng_clamatching_classifiersssifiers)
				classifierFitness.put(cid, 0.5);
		}
		return correct_prediction;
	}

	public int getRandom() 
	{
		ArrayList<Integer> ids = new ArrayList<Integer>(classifierFitness.keySet());
		int id = (int)(Math.random() * ids.size());
		return id;
	}
}