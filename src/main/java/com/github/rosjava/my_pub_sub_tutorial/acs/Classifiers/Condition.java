package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;

import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerCondition;

import java.awt.Graphics2D;
import java.io.Serializable;
import java.util.ArrayList;

public abstract class Condition<C extends Condition<C>> implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract boolean isStateWithin(C state);

	public abstract boolean isStateWithin(double[] state);

	public abstract int compareTo(C condition);

	public abstract C replicate();

	public abstract C cloneLayerCondition()throws CloneNotSupportedException;

	public abstract C transferToLayerCondition(double[] state);

	public abstract void mutateCondition();
	public abstract void mutateCondition(double[][] boundary);
	public abstract void mutateCondition(C ConditionOne, C ConditionOther);

	public abstract void crossoverCondition(C ConditionOne, C ConditionOther, String mode);
	public abstract void doMorph(C ConditionOne, C ConditionOther);
	public abstract ArrayList<C> doMitosis(C Condition);
	public boolean equals(Object other)
	{
		if(!(other instanceof Condition))
			return false;
		return isTheSameCondition((C)other);
	}


	//public abstract boolean isTheSameCondition(C condition, C otherCondition);

	public abstract boolean isTheSameCondition(C condition);

	public abstract boolean isTheSameCondition_unique(C condition);

	public abstract String toString();

	public abstract String toString_for_csv();

	public abstract String toHeaderString_csv();

	public abstract void display(Graphics2D g2d, int offset_x, int offset_y);

}
