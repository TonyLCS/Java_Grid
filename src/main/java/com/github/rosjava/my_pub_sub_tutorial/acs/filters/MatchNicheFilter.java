package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerAction;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

/**
 * Generic Class that will find a match set, within the population, for the current state.
 * @author pwnbot
 *
 */

public class MatchNicheFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{	
	public static final String NAME = "MatchNicheFilter:: Match Filter for Niche ";
		
	public MatchNicheFilter()
	{
		super(NAME);
	}
	
	@Override
	public FilterMessage<C,A> apply(P agent, int iterationCount, FilterMessage<C,A> message) 
	{
		//find match set according to current reinforcer state
		ArrayList<Integer> match_Set = calculate_matchSet(agent, message.getCurrentReState());
		/*
		HashMap<LayerAction, Double> anticipated_reward_hashmap = agent.get_anticipated_reward_hashmap(matchSet);
		//show anticipated_reward_hashmap
		for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
		{
			System.out.println("		MatchNicheFilter :: (Accuracy-based) calcuate anticipated reward :\taction("+a_anticipated_reward_map.getKey()+")" +
					"\t- reward("+a_anticipated_reward_map.getValue()+"). ");
		}
		*/
		message.setMatchSet(match_Set);

//System.out.println("apply MatchFilter: matchSet.size()= "+message.getMatchSet().size() + "; agent = "+ agent );

		return message;
	}

	public ArrayList<Integer> calculate_matchSet(P population, C getCurrentReState)
	{
		ArrayList<Integer> matchSet = population.getMatchingClassifiers(getCurrentReState);
		return matchSet;
	}

	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +"::::"+message;
		agent.printMessage(s);
	}
}
