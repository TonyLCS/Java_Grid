package main.java.com.github.rosjava.my_pub_sub_tutorial.acs;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import main.java.com.github.rosjava.my_pub_sub_tutorial.Jama.Matrix;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerAction;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerClassifier;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerCondition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Modifier_ContinuouslyProjected;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters.FilterMessage;

public interface LCSProblem<Co extends Condition<Co>, A extends ActionInterface> {

//	public FilterMessage<Co, A> getMessage();

	public boolean takeAction(A action);

	public boolean executeModifier(A action);// execute modifier.
	
//	public Condition<Co> getState();

	public void render(Graphics2D g, int offset_x, int offset_y);

	public JPanel getTabs();

	public void  save();

	public void save(int episode);
	public void save(int episode,String folder_name);
	public void display();

	void iterate();

	public boolean isComplete();
	public void getActionSetFromChosenAction(ArrayList<Integer> matchSet, ArrayList<Integer> action_set, A chosen_action);
	public A getNextAction(Co currentState, ArrayList<Integer> match_set, ArrayList<Integer> action_set, boolean exploit);
	public A getNextAction(Co currentState, ArrayList<Integer> match_set, ArrayList<Integer> action_set, boolean exploit, ArrayList<Double> anticipated_reward); //for anticipated_reward method
	public HashMap<A, Double> get_actions_reward_hashmap(ArrayList<Integer> match_set);

	//public HashMap<LayerAction, Double> get_anticipated_reward_hashmap(ArrayList<Integer> matchSet);


	public HashMap<Integer, Matrix> calculate_niche_strength_distribution(ArrayList<Integer> match_niche_set);

	public HashMap<Integer, Modifier_ContinuouslyProjected> calculate_niche_strength_distribution_continuous(ArrayList<Integer> match_niche_set);

	public HashMap<Integer, Matrix>  transform_modifier_niches_set_into_matrix(HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches);

	public HashMap<Integer, Modifier_ContinuouslyProjected> transform_modifier_niches_set_into_matrix_inline(HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches);


	public void select_a_discrete_value_for_modifier(HashMap<Integer, Matrix> modifier_set, String s);

	public void save_modifiers(String s, HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches);

	public void save_anticipated_reward_hashmap(int iteration, int instance_id);

	public void updateClassifiersAction(FilterMessage<Co, A> message, double reward);
	
	public void increaseTimeSinceGA();
	
	public void applyGA(ArrayList<Integer> action_set, int iteration, int threshold);

	public void applyGA_boundary(ArrayList<Integer> action_set, int iteration, int threshold, double[][] boundary);

	public ArrayList<Integer> getMatchingClassifiers(Co condition);

	public ArrayList<Integer> getMatchingClassifiers(double[] state);

	public ArrayList<Integer> getMatchingClassifiers_for_action(A condition);

	
	public int createClassifierFromState(Co currentState);

	public int createClassifierFromStateWithAction(Co currentState,ArrayList<Integer> MatchSetIDs);

	public void create_Classifier_by_mitosis(Co currentState,ArrayList<Integer> MitosisSetIDs);

	public void createClassifierFromCSV(int id, Co currentState, String action_mask,double[] statistics);

	public boolean actionSetDiversity(ArrayList<Integer> MatchSetIDs);

	public  ArrayList<A> getClassifiersPossibleActionSetList();

	public int getActionSetSizeInMatchSet(ArrayList<Integer> MatchSetIDs);

	public int getActionSetSizePossibility();

	public void printMessage(String message);

	public Population getPopulation();
}
