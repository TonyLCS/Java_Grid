package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Statistics;

public class CoverFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A> 
{
	public static final String NAME = "Cover Filter";
	
	public CoverFilter()
	{
		super(NAME);
	}
	
	@Override
	public FilterMessage<C, A> apply(P agent, int iterationCount, FilterMessage<C,A> message) 
	{

System.out.println("apply CoverFilter: message.getMatchSet().size() = "+message.getMatchSet().size() + "; agent = "+ agent );

		printMessage("message.getMatchSet().size() ('"+message.getMatchSet().size()+"') VS(<) Statistics.MINIMUM_SIZE("+Statistics.MINIMUM_SIZE+")", agent);
		if(message.getMatchSet().size() < Statistics.MINIMUM_SIZE)
		{
			printMessage("Less than minimun size ->	apply covering", agent);
			C currentState = message.getCurrentState();
			int newClass = cover(agent, currentState);
			if(newClass != -1)
			{
				message.getMatchSet().add(newClass);
				printMessage("add a new classifier into MatchSet, adding classifier's id(newClass)="+newClass,agent);
			}
			printMessage("message.getMatchSet().size(): " + message.getMatchSet().size(), agent);
//System.out.println("apply CoverFilter: CoverFilter works, and covers a new classifier");
		}

//System.out.println("apply CoverFilter: message.getMatchSet().size() = "+message.getMatchSet().size() + "; agent = "+ agent );
		return message;
	}
	
	public int cover(P population, C currentState)
	{
		return population.createClassifierFromState(currentState);
	}
	
	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +"::::"+message;
		agent.printMessage(s);
	}
}
