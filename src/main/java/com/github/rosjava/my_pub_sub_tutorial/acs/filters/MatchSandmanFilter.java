package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

/**
 * Generic Class that will find a match set, within the population, for the current state.
 * @author pwnbot
 *
 */

public class MatchSandmanFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{	
	public static final String NAME = "Match Filter for Sandman";
		
	public MatchSandmanFilter()
	{
		super(NAME);
	}
	
	@Override
	public FilterMessage<C,A> apply(P agent, int iterationCount, FilterMessage<C,A> message) 
	{
		//find match set according to current reinforcer state
		ArrayList<Integer> matchSet = matchSet(agent, message.getCurrentReState());
		
		//printMessage("Set size: " + matchSet.size(), agent);
		//printMessage("Set size: '" + matchSet+"'<-", agent);
		printMessage("	apply MatchSandmanFilter: matchSet.size= '" + matchSet.size()+"'; matchSet='" + matchSet+"'<-", agent);

		if(matchSet.size()>0)
		{
			System.out.println("			Sandman is awake  !!!!!!!!!!!!!!!!");
			System.out.println("			Sandman is awake  !!!!!!!!!!!!!!!!");
			System.out.println("			Sandman is awake  !!!!!!!!!!!!!!!!");
			//todo: set the message which will put robot into sleep
			//sandman publish message to awake the moving obstacle.

		}

		message.setMatchSet(matchSet);

//System.out.println("apply MatchFilter: matchSet.size()= "+message.getMatchSet().size() + "; agent = "+ agent );


		return message;
	}

	public ArrayList<Integer> matchSet(P population, C getCurrentReState)
	{
		ArrayList<Integer> matchSet = population.getMatchingClassifiers(getCurrentReState);
		return matchSet;
	}

	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +"::::"+message;
		agent.printMessage(s);
	}
}
