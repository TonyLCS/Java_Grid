package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;


import java.text.DecimalFormat;
import java.lang.Object.*;
import java.util.ArrayList;


public class ConditionValue implements Cloneable
{
	private  double VALUE=0;
	private  double UPPER=0;
	private  double LOWER=0;
	private  boolean IS_HASH=false;
	private double BOUNDARY=1;

	//private final boolean IS_DONOTCARE= false;
	private  boolean IS_CELL=false;

	private double upperCell=0;
	private double lowerCell=0;

	private String ternaryAttribute="?";

	public void  setConditionValueUPPER(double upper)
	{
		this.UPPER=upper;
	}
	public double getConditionValueUPPER()
	{
		return this.UPPER;
	}
	public void setConditionValueLOWER(double lower)
	{
		this.LOWER =lower;
	}
	public double getConditionValueLOWER()
	{
		return this.LOWER;
	}

	public double getConditionValuelowerCell()
	{
		return this.lowerCell;
	}
	public void switchConditionValue()
	{
		double temp;

		if(IS_CELL)
		{
			temp= this.upperCell;
			this.upperCell= this.lowerCell;
			this.lowerCell= temp;

		}
		else
		{
			temp = this.UPPER;
			this.UPPER= this.LOWER;
			this.LOWER= temp;
		}
	}
	public double getConditionValueupperCell()
	{
		return this.upperCell;
	}

	public double getConditionVALUE()
	{
		return this.VALUE;
	}
	public boolean getIsCell(){return IS_CELL;}


	/**ternary phenotype***/
	public ConditionValue(double value, String phenotype)
	{
		if(phenotype.equals("ternary"))
		{
			switch((int)value)
			{
				case 0:
					this.ternaryAttribute = "0";
					break;
				case 1:
					this.ternaryAttribute = "1";
					break;
				case 2:
					this.ternaryAttribute = "#";
					break;
				default:
					this.ternaryAttribute = "?";
			}
		}

	}

	public String  getConditionValueTernaryAttribute()
	{
		return this.ternaryAttribute;
	}

	public ConditionValue(double value) 
	{
		super();
		if(!Double.isNaN(value))
		{
			VALUE = value;
			double percent = Math.random() * 0.5;
			double range = Math.abs(VALUE * percent);
			UPPER = VALUE + range;
			LOWER = VALUE - range;
			IS_HASH = Math.random() < 0.1 ? true : false;
			IS_CELL=false;
		}
		else
		{
			VALUE = 0;
			IS_HASH = true;
			UPPER = 0;
			LOWER = 0;
			IS_CELL=false;
		}
	}

	public ConditionValue(String str, String eyes, double value, double boundary) {
		super();

		//VALUE = value * 10;
		VALUE = value;
		if(boundary == 0)
		{
			boundary = 0.001;
		}else{
			this.BOUNDARY= boundary;
		}

		if (str.equals("instance")) {
			if (eyes.equals("yes_boundary")) {
				IS_CELL = true;
				if (!Double.isNaN(value)) {

					double percent = Math.random() * 0.05;
					//todo: set up boundary according to different dataset
					double range = Math.abs(BOUNDARY * percent);// range 0-4 with boundary is 80

					//double range = Math.abs(VALUE * 0.75 * Math.random());
					upperCell = VALUE + range;
					lowerCell = VALUE - range;

					IS_HASH = false;
					UPPER = upperCell;
					LOWER = lowerCell;

				}
			}
			if (eyes.equals("no_boundary")) {
				IS_CELL = true;
				if (!Double.isNaN(value)) {
					upperCell = VALUE;
					lowerCell = VALUE;
					IS_HASH = false;
					UPPER = upperCell;
					LOWER = lowerCell;
				}
			}

		}
	}

	public ConditionValue(double[] attributes, double boundary) {
		super();
		this.lowerCell = attributes[0];
		this.VALUE =  attributes[1];
		this.upperCell = attributes[2];
		this.IS_HASH = false;
		this.UPPER = upperCell;
		this.LOWER = lowerCell;

		this.BOUNDARY = boundary;

	}

	public ConditionValue(String str, String eyes, double value)
	{
		super();

		VALUE= value*10;


		if(str.equals("iris"))
		{
			if(eyes.equals("testEyes"))
			{
				IS_CELL=true;
				if(!Double.isNaN(value))
				{
					//upperCell=Math.ceil(VALUE );
					//lowerCell=Math.floor(VALUE );

					upperCell=VALUE;
					lowerCell=VALUE;
					IS_HASH = false;
					UPPER = upperCell;
					LOWER = lowerCell;
					//IS_CELL=true;
				}
			}

			if(eyes.equals("crystalEyes"))
			{
				IS_CELL=true;
				if(!Double.isNaN(value))
				{

					/*
					double percent = Math.random() * 0.5;
					double range = Math.abs(VALUE * percent);
					upperCell = VALUE + range;
					lowerCell = VALUE - range;
					*/
					double percent = Math.random() * 0.05;
					//todo: set up boundary according to different dataset
					double range =  Math.abs(BOUNDARY * percent);// range 0-4 with boundary is 80
					//double range = Math.abs(VALUE * percent);

					upperCell=VALUE + range;
					lowerCell=VALUE - range;
					/*
					double percent = Math.random() * 0.2;
					double range = Math.abs(VALUE * percent);

					upperCell=Math.ceil(VALUE + range);
					lowerCell=Math.floor(VALUE - range);
					*/
					/*
					upperCell=Math.ceil(VALUE);
					lowerCell=Math.floor(VALUE);
					*/
					//debug ok// System.out.println("			 LowerCell("+lowerCell+") < value("+value+") < UpperCell("+upperCell+")");
					//VALUE = value;
					IS_HASH = false;
					UPPER = upperCell;
					LOWER = lowerCell;
					//IS_CELL=true;
				}
			}
		}
	}

	public ConditionValue(double value, boolean doCell,int serial)
	{
		super();
		if(doCell)
		{
			IS_CELL=true;
			if(!Double.isNaN(value))
			{
				upperCell=Math.ceil(value);
				lowerCell=Math.floor(value);
				if(2==serial)
				{
					if((upperCell%360)>=180)
					{
						upperCell=2;
						lowerCell=1;
					}
					else
					{
						upperCell=1;
						lowerCell=0;
					}
				}
				//debug ok//System.out.println("			serial("+serial+"): LowerCell("+lowerCell+") < value("+value+") < UpperCell("+upperCell+")");

				// unite the upper and upperCell!!!!!!!!!
				VALUE = value;
				IS_HASH = false;
				UPPER = upperCell;
				LOWER = lowerCell;
				IS_CELL=true;
			}




		}
	}

	public ConditionValue()
	{	//this one just apply for GA operations such as crossOver and mutation.
		super();
		VALUE = 0;
		IS_HASH = true;
		UPPER = 0;
		LOWER = 0;
	}

	public ConditionValue(double value, double upper, double lower, boolean is_hash) 
	{
		super();
		VALUE = value;
		UPPER = upper;
		LOWER = lower;
		IS_HASH = is_hash;
	}
	
	public ConditionValue(ConditionValue conditionValue) 
	{
		super();
		this.VALUE = conditionValue.VALUE;
		this.UPPER = conditionValue.UPPER;
		this.LOWER = conditionValue.LOWER;
		this.IS_HASH = conditionValue.IS_HASH;
	}

	public boolean isWithin(ConditionValue other)
	{
		/*
		System.out.println("VALUE : this.VALUE ('"+this.VALUE+"') VS other.VALUE ('"+other.VALUE+"');" +
				" UPPER: ('"+this.UPPER+"') VS ('"+other.UPPER+"')" +
				" LOWER: ('"+this.LOWER+"') VS ('"+other.UPPER+"') ");
		*/
		if(other.IS_CELL)
		{
			//todo: notice a special case, in which a new ConditionValue object without setting upperCell and lowerCell yet.
			//todo: it should not use parameter VALUE to judege
			return ((other.getConditionValuelowerCell() >= lowerCell) && (other.getConditionValueupperCell() <= upperCell));
		}
		else
			{
				return other.VALUE >= LOWER && other.VALUE <= UPPER;

			}
		//return other.VALUE >= LOWER && other.VALUE <= UPPER;
	}
	
	public boolean compareTo(ConditionValue other)
	{
//		System.out.println(LOWER+" "+other.VALUE+" "+UPPER+" "+IS_HASH +" "+other.IS_HASH);
		if(other.IS_CELL)
		{
			return isWithin(other);
		}
		else
			{
				return IS_HASH || other.IS_HASH || isWithin(other);

			}
		//return IS_HASH || other.IS_HASH || isWithin(other);
	}


	public boolean isEqualTo_unique(ConditionValue other)
	{

		if(IS_CELL)
		{
			if(this.upperCell!=other.upperCell ||this.lowerCell!=other.lowerCell)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			if(this.UPPER!=other.UPPER ||this.LOWER!=other.LOWER)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}


	public String toString(){
		if(IS_CELL)
		{
			return roundTwoDecimals(lowerCell)+"\t"+roundTwoDecimals(VALUE)+"\t"+roundTwoDecimals(upperCell)+"\t"+IS_CELL+"\t";
		}
		else{
			return roundTwoDecimals(LOWER)+"\t"+roundTwoDecimals(VALUE)+"\t"+roundTwoDecimals(UPPER)+"\t"+IS_HASH+"\t";
		}
	}

	public String toString_csv(){

		return roundTwoDecimals(lowerCell)+"#"+roundTwoDecimals(VALUE)+"#"+roundTwoDecimals(upperCell);

	}

	public String toHeaderString_csv(int which_attribute){

		String s= "C"+which_attribute+"L"+"#"+"C"+which_attribute+"V"+"#"+"C"+which_attribute+"H";
		return s;

	}

	public String showConditionValue()
	{
		if(IS_CELL)
		{
			return "[ "+roundTwoDecimals(lowerCell)+", "+roundTwoDecimals(upperCell)+" ]";
		}
		else{
			return roundTwoDecimals(LOWER)+"\t"+roundTwoDecimals(VALUE)+"\t"+roundTwoDecimals(UPPER)+"\t"+IS_HASH+"\t";
		}
	}

	public String toStringValue(){
		return roundTwoDecimals(this.VALUE)+"\t";
	}
	
	public double roundTwoDecimals(double d) 
	{
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
	}

	public ConditionValue crossover(ConditionValue other)
	{
		//multiple points crossover
		ConditionValue temp= new ConditionValue();
		//temp.UPPER	=other.UPPER;
		//other.UPPER	= this.UPPER;
		//other.UPPER	= temp.UPPER;

		temp.LOWER	=other.LOWER;
		other.LOWER	= this.LOWER;
		this.LOWER	= temp.LOWER;

		return other;
	}
	public void crossOverLOWER(ConditionValue one,ConditionValue other)
	{
		ConditionValue temp= new ConditionValue();

		if(one.IS_CELL)
		{
			temp.lowerCell = other.lowerCell;
			other.lowerCell	= one.lowerCell;
			one.lowerCell	= temp.lowerCell;
		}
		else
		{
			temp.LOWER	=other.LOWER;
			other.LOWER	= one.LOWER;
			one.LOWER	= temp.LOWER;
		}

	}
	public void crossOverUPPER(ConditionValue one,ConditionValue other)
	{
		ConditionValue temp= new ConditionValue();
		if(one.IS_CELL)
		{
			temp.upperCell	=other.upperCell;
			other.upperCell	= one.upperCell;
			one.upperCell	= temp.upperCell;
		}
		else
		{
			temp.UPPER	=other.UPPER;
			other.UPPER	= one.UPPER;
			one.UPPER	= temp.UPPER;
		}



	}
	public void doMorph(ConditionValue one,ConditionValue other)
	{
		ConditionValue temp= new ConditionValue();
		//one is the general classifier; other is the specific classifier
		if(one.IS_CELL)
		{
			//System.out.println("		doMorph:  before one.lowerCell("+one.lowerCell+")-upperCell("+one.upperCell+")  ");
			//System.out.println("		doMorph:  before other.lowerCell("+other.lowerCell+")-upperCell("+other.upperCell+")  ");

			if(one.upperCell<other.upperCell)
			{
				temp.upperCell	=other.upperCell;
				other.upperCell	= one.upperCell;
				one.upperCell	= temp.upperCell;
			}
			if(one.lowerCell> other.lowerCell)
			{
				temp.lowerCell=other.lowerCell;
				other.lowerCell=one.lowerCell;
				one.lowerCell=temp.lowerCell;
			}
			//check lower<upper, only the specific one might have this problem
			if(other.lowerCell>other.upperCell)
			{
				temp.lowerCell=other.lowerCell;
				other.lowerCell=other.upperCell;
				other.upperCell=temp.lowerCell;
			}

			//System.out.println("		doMorph:  after one.lowerCell("+one.lowerCell+")-upperCell("+one.upperCell+")  ");
			//System.out.println("		doMorph:  after other.lowerCell("+other.lowerCell+")-upperCell("+other.upperCell+")  ");

		}
		else
		{
			if(one.UPPER<other.UPPER)
			{
				temp.UPPER	=other.UPPER;
				other.UPPER	= one.UPPER;
				one.UPPER	= temp.UPPER;
			}
			if(one.LOWER> other.LOWER)
			{
				temp.LOWER=other.LOWER;
				other.LOWER=one.LOWER;
				one.LOWER=temp.LOWER;
			}
		}



	}

	public ArrayList<ConditionValue> doMitosis(ConditionValue other)
	{
		//debug ok//System.out.println("	 		L3:		doMitosis:  before this. condition:\t lowerCell("+this.lowerCell+")	upperCell("+this.upperCell+") VALUE ('"+this.VALUE+"') ");
		//debug ok//System.out.println("	 		L3:		doMitosis:  before instance: 	\t lowerCell("+other.lowerCell+")	upperCell("+other.upperCell+") VALUE ('"+other.VALUE+"') ");

		ArrayList<double[]> result = new ArrayList<double[]>();
		ArrayList<ConditionValue> result_conditions = new ArrayList<ConditionValue>();
		//double percent = Math.random() * 0.05;
		//double range = Math.abs(BOUNDARY * percent);//
		//double lower_motisis = other.lowerCell - range;
		//double upper_motisis = other.upperCell + range;
		double lower_motisis = other.lowerCell;
		double upper_motisis = other.upperCell;
		//check 4 cases in Jun 10
		//case 1: 1 piece
		if(lower_motisis<this.lowerCell && upper_motisis < this.upperCell)
		{
			//this.lowerCell = upper_motisis;
			if( check_lower_and_upper(upper_motisis, this.upperCell))
			{
				double[] a_pair =  new double[3];
				a_pair[0] = upper_motisis;
				a_pair[1] =  this.VALUE;
				a_pair[2] = this.upperCell;
				result_conditions.add( new ConditionValue(a_pair, this.BOUNDARY));
				//debug ok//System.out.println("	 		L3:		doMitosis:  after: case 1: lowerCell("+upper_motisis+")	upperCell("+this.upperCell+") replaced lower ");

			}
		}

		//case 2: 1 piece
		if(this.lowerCell <lower_motisis && this.upperCell < upper_motisis )
		{
			//this.upperCell = lower_motisis;
			if( check_lower_and_upper(this.lowerCell, lower_motisis))
			{
				double[] a_pair =  new double[3];
				a_pair[0] = this.lowerCell;
				a_pair[1] =  this.VALUE;
				a_pair[2] = lower_motisis;
				result_conditions.add( new ConditionValue(a_pair, this.BOUNDARY));
				//debug ok//System.out.println("	 		L3:		doMitosis:  after: case 2: lowerCell("+this.lowerCell+")	upperCell("+lower_motisis+") replaced upper ");
			}
		}

		//case 3: 2 piece
		if( this.lowerCell <lower_motisis && this.upperCell < upper_motisis )
		{
			if( check_lower_and_upper(this.lowerCell, lower_motisis))
			{
				double[] a_pair =  new double[3];
				a_pair[0] = this.lowerCell;
				a_pair[1] =  this.VALUE;
				a_pair[2] = lower_motisis;
				result_conditions.add( new ConditionValue(a_pair, this.BOUNDARY));
				//debug ok//System.out.println("	 		L3:		doMitosis:  after: case 3.1: lowerCell("+this.lowerCell+")	upperCell("+lower_motisis+") replaced upper ");

			}

			if( check_lower_and_upper(upper_motisis, this.upperCell))
			{
				double[] a_pair =  new double[3];
				a_pair[0] = upper_motisis;
				a_pair[1] =  this.VALUE;
				a_pair[2] = this.upperCell;
				result_conditions.add( new ConditionValue(a_pair, this.BOUNDARY));
				//debug ok//System.out.println("	 		L3:		doMitosis:  after: case 3.2: lowerCell("+upper_motisis+")	upperCell("+this.upperCell+") replaced lower ");

			}


		}
		//case 4: annihilation

		return result_conditions;
	}

	public boolean check_lower_and_upper(double lowerCell, double upperCell)
	{
		if(lowerCell<= upperCell)
		{
			return true;
		}
		else return false;
	}
	/*replaced by mutateThis()
	public void mutation2(ConditionValue one,ConditionValue boundary)
	{
		double scope=boundary.UPPER-boundary.LOWER;
		double repair;
		System.out.println("		mutation:  before LOWER("+one.LOWER+")	UPPER("+one.UPPER+")		VALUE ('"+one.VALUE+"') ");
		do{
			//mutate LOWER
			repair = scope* Math.random();
			if(Math.random()<=0.5)
			{
				one.LOWER= one.LOWER +repair;
			}
			else
			{
				one.LOWER= one.LOWER -repair;
			}
			if(one.LOWER<boundary.LOWER)
			{
				one.LOWER=boundary.LOWER;
			}

			//mutate UPPER
			repair = scope* Math.random();
			if(Math.random()<=0.5)
			{
				one.UPPER= one.UPPER +repair;
			}
			else
			{
				one.UPPER= one.UPPER -repair;
			}
			if(one.UPPER>boundary.UPPER)
			{
				one.UPPER=boundary.UPPER;
			}

		}while(one.LOWER>=one.UPPER);
		//end of mutation
		System.out.println("		mutation:  after LOWER("+one.LOWER+")	UPPER("+one.UPPER+")		VALUE ('"+one.VALUE+"') ");
	}
	public void mutation(ConditionValue one)
	{
		if(one.IS_CELL)
		{//apply cell world
			//test
			double scope=one.upperCell-one.lowerCell;// repair method! should change if boundary apply
			if(scope==0)
			{
				scope=Math.ceil(2*Math.random());// for the case that upper == lower
			}
			double repair;
			if(one.IS_CELL)
			{
				repair= Math.ceil(scope* Math.random());
			}
			else
			{
				repair = scope* Math.random();
			}
			System.out.println("		mutation cell("+one.getIsCell()+"):   before:" +
					" lowerCell("+one.lowerCell+")	upperCell("+one.upperCell+")		VALUE ('"+one.VALUE+"') ");
			//do muatation
			do{
				if(Math.random()<= 0.5)
				{
					//mutate LOWER
					//scope=one.upperCell-one.lowerCell;
					repair= Math.ceil(scope* Math.random());// IS_CELL
					if(Math.random()<=0.5)
					{
						one.lowerCell= one.lowerCell +repair;
					}
					else
					{
						one.lowerCell= one.lowerCell -repair;
					}
				}
				else
				{//mutate UPPER
					repair= Math.ceil(scope* Math.random());// IS_CELL
					if(Math.random()<=0.5)
					{
						one.upperCell= one.upperCell +repair;
					}
					else
					{
						one.upperCell= one.upperCell -repair;
					}
				}
			}while(one.lowerCell>=one.upperCell);


			//end of mutation
			System.out.println("		mutation cell("+one.getIsCell()+"):  after:" +
					" lowerCell("+one.lowerCell+")	upperCell("+one.upperCell+")		VALUE ('"+one.VALUE+"') ");
		}
		else
		{//apply continuous boundary

			//test
			double scope=one.UPPER-one.LOWER;// repair method! should change if boundary apply
			if(scope==0)
			{
				scope=Math.random();// for the case that upper == lower
			}
			double repair = scope* Math.random();

			System.out.println("		mutation (continuous):  before LOWER("+one.LOWER+")	UPPER("+one.UPPER+")		VALUE ('"+one.VALUE+"') ");
			//do muatation
			do{
				if(Math.random()<= 0.5)
				{
					//mutate LOWER
					//scope=one.UPPER-one.LOWER;
					repair = scope* Math.random();
					if(Math.random()<=0.5)
					{
						one.LOWER= one.LOWER +repair;
					}
					else
					{
						one.LOWER= one.LOWER -repair;
					}
				}
				else
				{//mutate UPPER
					repair = scope* Math.random();
					if(Math.random()<=0.5)
					{
						one.UPPER= one.UPPER +repair;
					}
					else
					{
						one.UPPER= one.UPPER -repair;
					}
				}
			}while(one.LOWER>=one.UPPER);


			//end of mutation
			System.out.println("		mutation:  after LOWER("+one.LOWER+")	UPPER("+one.UPPER+")		VALUE ('"+one.VALUE+"') ");
		}
	}
	*/
	public double calculateRepair()
	{
		double scope=this.upperCell-this.lowerCell;// repair method! should change if boundary apply
		if(scope<=0)
		{scope=-scope;}
		if(scope==0)
		{
			scope=Math.ceil(3*Math.random());// for the case that upper == lower
		}
		/*
		if(scope<=3)
		{
			scope=Math.ceil(3*Math.random());// make scope bigger
		}
		*/
		double repair;
		if(this.IS_CELL)
		{
			repair= Math.ceil(scope* (1-0.5*Math.random()));
		}
		else
		{
			repair = scope* Math.random();
		}
		return repair;
	}
	public double calculateRepair(int seq, double[][] boundary)
	{
		//double scope=this.upperCell-this.lowerCell;// repair method! should change if boundary apply
		double scope= boundary[seq][1] - boundary[seq][0];
		if(scope<=0)
		{scope=-scope;}
		if(scope==0)
		{
			scope=Math.ceil(3*Math.random());// for the case that upper == lower
		}
		/*
		if(scope<=3)
		{
			scope=Math.ceil(3*Math.random());// make scope bigger
		}
		*/
		double repair;
		if(this.IS_CELL)
		{
			repair= Math.ceil( scope * 0.1 * Math.random() );
		}
		else
		{
			repair = scope* Math.random();
		}
		return repair;
	}
	public void mutateThis(int seq)
	{
		//temparily stop mutation

		if(this.IS_CELL)
		{//apply cell world
			//test
			double repair;
				//debug clone//System.out.println("		mutation cell("+this.getIsCell()+"):   before:" + " lowerCell("+this.lowerCell+")	upperCell("+this.upperCell+")		VALUE ('"+this.VALUE+"') ");
			//do muatation
			do{
				repair= calculateRepair();
				//do mutation: 1. mutate upperCell or lowerCell
				if(Math.random()<= 0.5)
				{
					//mutate LOWER
					//scope=one.upperCell-one.lowerCell;
					//repair= Math.ceil(scope* Math.random());// IS_CELL
					if(Math.random()<=0.5)
					{
						this.lowerCell= this.lowerCell +repair;
					}
					else
					{
						this.lowerCell= this.lowerCell -repair;
					}
				}
				else
				{//mutate UPPER
					//repair= Math.ceil(scope* Math.random());// IS_CELL
					if(Math.random()<=0.5)
					{
						this.upperCell= this.upperCell +repair;
					}
					else
					{
						this.upperCell= this.upperCell -repair;
					}
				}

				//do mutation: 2 check lower< upper
				if(this.lowerCell>this.upperCell)
				{
					switchConditionValue();
				}

				//do mutation: 3 make sure cell is limited in the boundary
				  checkInBoundary(seq);

				//make sure it contains the current instance
				//show current VALUE
				//System.out.println("  this value("+this.VALUE+")");
				if(this.lowerCell>this.VALUE)
				{
					this.lowerCell=this.VALUE;
				}
				if(this.upperCell<this.VALUE)
				{
					this.upperCell= this.VALUE;
				}

				/*
				//do mutation: 2 decrease the case that upper == lower
				if(this.lowerCell==this.upperCell)
				{
					if(Math.random()<=0.5)
					{
						this.upperCell= this.upperCell +repair;
					}
					else
					{
						this.lowerCell= this.lowerCell -repair;
					}
				}

				//here set up boundary of upperCell and lowerCell
				if( seq ==0){
					if(this.upperCell>0)
					{
						this.upperCell=0;
						if(this.lowerCell==this.upperCell)
						{
							this.lowerCell= this.lowerCell-1;
						}
					}
					if(this.lowerCell<-5)
					{
						this.lowerCell=-5;
						if(this.upperCell==this.lowerCell)
						{
							this.upperCell=this.upperCell+1;
						}
					}
				}
				if( seq ==1) {
					if (this.upperCell > 12)
					{
						this.upperCell = 12;
						if(this.lowerCell==this.upperCell)
						{
							this.lowerCell= this.lowerCell-1;
						}
					}
					if (this.lowerCell < 0)
					{
						this.lowerCell = 0;
						if(this.upperCell==this.lowerCell)
						{
							this.upperCell=this.upperCell+1;
						}
					}
				}
				if( seq ==2){

					if (this.upperCell > 2)
					{
						if(this.lowerCell==this.upperCell)
						{
							this.upperCell = 2;
							this.lowerCell= this.lowerCell-1;
						}
						this.upperCell = 2;
					}
					if (this.lowerCell < 0)
					{
						this.lowerCell = 0;
						if(this.upperCell==this.lowerCell)
						{
							this.upperCell=this.upperCell+1;
						}
					}
				}
				*/

				//System.out.println("in mutation loop for attribute("+seq+") lowerCell ("+this.lowerCell+")- upperCell("+this.upperCell+") ");
			}while(this.lowerCell>=this.upperCell);


			//end of mutation
			//debug clone//System.out.println("		mutation cell("+this.getIsCell()+"):  after:" + " lowerCell("+this.lowerCell+")	upperCell("+this.upperCell+")		VALUE ('"+this.VALUE+"') ");
		}
		else
		{//apply continuous boundary

			//test
			double scope=this.UPPER-this.LOWER;// repair method! should change if boundary apply
			if(scope==0)
			{
				scope=Math.random();// for the case that upper == lower
			}
			double repair = scope* Math.random();

			//System.out.println("		mutation (continuous):  before LOWER("+this.LOWER+")	UPPER("+this.UPPER+")		VALUE ('"+this.VALUE+"') ");
			//do muatation
			do{
				if(Math.random()<= 0.5)
				{
					//mutate LOWER
					//scope=one.UPPER-one.LOWER;
					repair = scope* Math.random();
					if(Math.random()<=0.5)
					{
						this.LOWER= this.LOWER +repair;
					}
					else
					{
						this.LOWER= this.LOWER -repair;
					}
				}
				else
				{//mutate UPPER
					repair = scope* Math.random();
					if(Math.random()<=0.5)
					{
						this.UPPER= this.UPPER +repair;
					}
					else
					{
						this.UPPER= this.UPPER -repair;
					}
				}

			}while(this.LOWER>=this.UPPER);


			//end of mutation
			//System.out.println("		mutation:  after LOWER("+this.LOWER+")	UPPER("+this.UPPER+")		VALUE ('"+this.VALUE+"') ");
		}

	}
	public void mutateThis(int seq, double[][] boundary)
	{
		//temparily stop mutation
		int loop_watchdog = 0;
		double restored_lowerCell = this.lowerCell;
		double restored_upperCell = this.upperCell;

		if(this.IS_CELL)
		{
			double repair;
			//do muatation
			repair= calculateRepair(seq, boundary);

			do{
				//restore lowerCell and upperCell
				this.lowerCell = restored_lowerCell;
				this.upperCell = restored_upperCell;
				//do mutation: 1. mutate upperCell or lowerCell
				if(Math.random()<= 0.5)
				{
					//mutate LOWER
					//scope=one.upperCell-one.lowerCell;
					//repair= Math.ceil(scope* Math.random());// IS_CELL
					if(Math.random()<=0.5)
					{
						this.lowerCell= this.lowerCell +repair;
					}
					else
					{
						this.lowerCell= this.lowerCell -repair;
					}
				}
				else
				{//mutate UPPER
					//repair= Math.ceil(scope* Math.random());// IS_CELL
					if(Math.random()<=0.5)
					{
						this.upperCell= this.upperCell +repair;
					}
					else
					{
						this.upperCell= this.upperCell -repair;
					}
				}

				//do mutation: 2 check lower< upper
				if(this.lowerCell>this.upperCell)
				{
					switchConditionValue();
				}

				//do mutation: 3 make sure cell is limited in the boundary
				checkInBoundary(seq, boundary);

				//make sure it contains the current instance
				//show current VALUE
				//System.out.println("  this value("+this.VALUE+")");
				if(this.lowerCell>this.VALUE)
				{
					this.lowerCell=this.VALUE;
				}
				if(this.upperCell<this.VALUE)
				{
					this.upperCell= this.VALUE;
				}

				/*
				//do mutation: 2 decrease the case that upper == lower
				if(this.lowerCell==this.upperCell)
				{
					if(Math.random()<=0.5)
					{
						this.upperCell= this.upperCell +repair;
					}
					else
					{
						this.lowerCell= this.lowerCell -repair;
					}
				}

				//here set up boundary of upperCell and lowerCell
				if( seq ==0){
					if(this.upperCell>0)
					{
						this.upperCell=0;
						if(this.lowerCell==this.upperCell)
						{
							this.lowerCell= this.lowerCell-1;
						}
					}
					if(this.lowerCell<-5)
					{
						this.lowerCell=-5;
						if(this.upperCell==this.lowerCell)
						{
							this.upperCell=this.upperCell+1;
						}
					}
				}
				if( seq ==1) {
					if (this.upperCell > 12)
					{
						this.upperCell = 12;
						if(this.lowerCell==this.upperCell)
						{
							this.lowerCell= this.lowerCell-1;
						}
					}
					if (this.lowerCell < 0)
					{
						this.lowerCell = 0;
						if(this.upperCell==this.lowerCell)
						{
							this.upperCell=this.upperCell+1;
						}
					}
				}
				if( seq ==2){

					if (this.upperCell > 2)
					{
						if(this.lowerCell==this.upperCell)
						{
							this.upperCell = 2;
							this.lowerCell= this.lowerCell-1;
						}
						this.upperCell = 2;
					}
					if (this.lowerCell < 0)
					{
						this.lowerCell = 0;
						if(this.upperCell==this.lowerCell)
						{
							this.upperCell=this.upperCell+1;
						}
					}
				}
				*/

				//System.out.println("in mutation loop for attribute("+seq+") lowerCell ("+this.lowerCell+")- upperCell("+this.upperCell+") ");
				loop_watchdog += 1;

				if(loop_watchdog>200)
				{
					//equalBounary(seq, boundary);
					//System.out.println("loop >200 time, equal boundary");
					this.lowerCell = restored_lowerCell;
					this.upperCell = restored_upperCell;
					System.out.println("loop >200 time, restore lower and upper...");
					break;
				}
				if(loop_watchdog>205)
				{
					//just in case of something strange happen...
					equalBounary(seq, boundary);
					System.out.println("loop >205 time, equal boundary");
					break;
				}
			}while(this.lowerCell>=this.upperCell);


			//end of mutation
			//debug clone//System.out.println("		mutation cell("+this.getIsCell()+"):  after:" + " lowerCell("+this.lowerCell+")	upperCell("+this.upperCell+")		VALUE ('"+this.VALUE+"') ");
		}
		else
		{//apply continuous boundary

			//test
			double scope=this.UPPER-this.LOWER;// repair method! should change if boundary apply
			if(scope==0)
			{
				scope=Math.random();// for the case that upper == lower
			}
			double repair = scope* Math.random();

			//System.out.println("		mutation (continuous):  before LOWER("+this.LOWER+")	UPPER("+this.UPPER+")		VALUE ('"+this.VALUE+"') ");
			//do muatation
			do{
				if(Math.random()<= 0.5)
				{
					//mutate LOWER
					//scope=one.UPPER-one.LOWER;
					repair = scope* Math.random();
					if(Math.random()<=0.5)
					{
						this.LOWER= this.LOWER +repair;
					}
					else
					{
						this.LOWER= this.LOWER -repair;
					}
				}
				else
				{//mutate UPPER
					repair = scope* Math.random();
					if(Math.random()<=0.5)
					{
						this.UPPER= this.UPPER +repair;
					}
					else
					{
						this.UPPER= this.UPPER -repair;
					}
				}

			}while(this.LOWER>=this.UPPER);


			//end of mutation
			//System.out.println("		mutation:  after LOWER("+this.LOWER+")	UPPER("+this.UPPER+")		VALUE ('"+this.VALUE+"') ");
		}

	}
	public void checkInBoundary(int seq)
	{
		//TODO: automatic seting boundary
		double a_1_low  =  0;
		double a_1_high =  6;

		double a_2_low  = 0;
		double a_2_high = 6;

		switch(seq)
		{
			case 0:
				if(this.lowerCell<=a_1_low){this.lowerCell=a_1_low;}
				if(this.upperCell<=a_1_low){this.upperCell=a_1_low+0.1;}
				//upper boundary
				if(this.lowerCell>=a_1_high){this.lowerCell=a_1_high-0.1;}
				if(this.upperCell>=a_1_high){this.upperCell=a_1_high;}
				break;
			case 1:
				//lower boundary
				if(this.lowerCell<=a_2_low){this.lowerCell=a_2_low;}
				if(this.upperCell<=a_2_low){this.upperCell=a_2_low+0.1;}
				//upper boundary
				if(this.lowerCell>=a_2_high){this.lowerCell=a_2_high-0.1;}
				if(this.upperCell>=a_2_high){this.upperCell=a_2_high;}
				break;
			case 2:
				//lower boundary
				if(this.lowerCell<=0){this.lowerCell=0;}
				if(this.upperCell<=0){this.upperCell=0.01;}
				//upper boundary
				if(this.lowerCell>=0.81){this.lowerCell=0.81-0.01;}
				if(this.upperCell>=0.81){this.upperCell=0.81;}
				break;
			case 3:
				//lower boundary
				/*
				if(this.lowerCell<=0){this.lowerCell=0;}
				if(this.upperCell<=0){this.upperCell=1;}
				//upper boundary
				if(this.lowerCell>=this.BOUNDARY){this.lowerCell=this.BOUNDARY-0.001;}
				if(this.upperCell>=this.BOUNDARY){this.upperCell=this.BOUNDARY;}
				*/
				break;

			/*
			case 1:
				//lower boundary
				if(this.lowerCell<=0){this.lowerCell=0;}
				if(this.upperCell<=0){this.upperCell=1;}
				//upper boundary
				if(this.lowerCell>=12){this.lowerCell=11;}
				if(this.upperCell>=12){this.upperCell=12;}

				break;
			case 2:
				//lower boundary
				if(this.lowerCell<=0){this.lowerCell=0;}
				if(this.upperCell<=0){this.upperCell=1;}
				//upper boundary
				if(this.lowerCell>=2){this.lowerCell=1;}
				if(this.upperCell>=2){this.upperCell=2;}

				break;
			*/
			default:
				break;

		}
	}

	public void checkInBoundary(int seq, double[][] boundary)
	{

		double a_low  =  boundary[seq][0];
		double a_high =  boundary[seq][1];
		if(this.lowerCell<=a_low){this.lowerCell=a_low;}
		if(this.upperCell<=a_low){this.upperCell=a_low;}
		if(this.lowerCell>=a_high){this.lowerCell=a_high;}
		if(this.upperCell>=a_high){this.upperCell=a_high;}

	}
	public void equalBounary(int seq, double[][] boundary)
	{

		this.lowerCell  =  boundary[seq][0];
		this.upperCell =  boundary[seq][1];

	}
	public  ConditionValue cloneConditionValue() throws CloneNotSupportedException
	{
		ConditionValue conditionValue1=this;

		ConditionValue conditionValue2=(ConditionValue) conditionValue1.clone();

		return conditionValue2;
	}

	/*
	@Override
	public ConditionValue clone() throws CloneNotSupportedException
	{
		ConditionValue a_copy= null;
		try{
			a_copy= (ConditionValue) super.clone();

		}catch (CloneNotSupportedException e)
		{

		}
		return a_copy;
	}
	*/

	@Override
	protected Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
}
