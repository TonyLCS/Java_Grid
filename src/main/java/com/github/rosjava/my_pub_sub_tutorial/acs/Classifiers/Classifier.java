package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;

import java.awt.Graphics2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

//import main.java.com.github.rosjava.my_pub_sub_tutorial.EmotionProblem.Emotion;
//import main.java.com.github.rosjava.my_pub_sub_tutorial.RobotProblem.ExpectationMap;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerCondition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;

public abstract class Classifier<C extends Condition<C>, A extends ActionInterface> implements Comparable<Classifier<C, A>>, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final int ID;
	
//	protected final Condition condition;
//	
//	protected final ActionSet actions;
//	//private HashMap<Action, ActionFitness> actionSet = new HashMap<Action, ActionFitness>();
//	
//	private ExpectationMap  expectation_map;

	protected Statistics statistics;// = new Statistics();

	private int iterations_since_ga = 1;

	//Now in own Statistics class
	//statistics
	//May put in seperate class. learn seperate set of
//	public static final double MAX_REWARD = 1000;
//	private static final double org_error = MAX_REWARD * 0.1;
//	
//	private double predicted_reward;
//	private double accuracy;
//	private double fitness;
//	
//	private int experience;//aka age
//	private double niche_size;
//	private double prediction_error;
//	private int numerosity = 1;
//	
//	private double beta = 0.2; //learning rate
//	private double alpha = 0.1;
//	private double v = 5;
	/**
	 * Accuracy
	 * age
	 * nicheSize
	 * prediction
	 * error
	 * org_error
	 * numerosity
	 * fitness 
	 */



	public Classifier()
	{
		this.ID = Population.getNextID();
//		this.condition = condition;
//		this.actions = new ActionSet(Statistics.ACTION_SET);
//		this.statistics = statistics;
//		this.expectation_map = createExpectation_Map();
	}
	public Classifier(C condition, Statistics statistics)
	{
		this.ID = Population.getNextID();
//		this.condition = condition;
//		this.actions = new ActionSet(Statistics.ACTION_SET);
		this.statistics = statistics;
//		this.expectation_map = createExpectation_Map();
	}
	
	public Classifier(C condition)
	{
		this.ID = Population.getNextID();
//		this.condition = condition;
//		this.actions = new ActionSet(Statistics.ACTION_SET);
		this.statistics = new Statistics();
//		this.expectation_map = createExpectation_Map();
	}
	public Classifier(int id, C condition)
	{
		this.ID = id;
//		this.condition = condition;
//		this.actions = new ActionSet(Statistics.ACTION_SET);
		this.statistics = new Statistics();
//		this.expectation_map = createExpectation_Map();
	}
	
	public Classifier(Classifier<C,A> classifier)
	{
		this.ID = Population.getNextID();
//		this.condition = classifier.condition.replicate();
//		this.actions = new ActionSet(Statistics.ACTION_SET);
		this.statistics = new Statistics();
//		this.expectation_map = createExpectation_Map();
	}
	
	protected abstract C getCondition();
	
	protected abstract ActionSet<A> getActions();

	//protected abstract A getLayerAction(); //Tony
	public abstract A getLayerAction(); //tony


	public abstract String showCondition();

	//Overridden by all sub class in order to generate each problems sets action set
	public abstract void display(Graphics2D g2d, int offset_x, int offset_y);
//	{
//		this.condition.display(g2d, 0, 0);
//	}
	/***Following "createChild(C condition) method is replaced by "createChild(C condition,A action)"method //Tony
	protected abstract Classifier<C,A> createChild(C condition);
	 ***/
	/**
	 * Will check if the current state is within the current condition
	 * params: state to be compared to the condition
	 * return true if the state is within the current condition
	 */
	protected abstract Classifier<C,A> createChild(C condition,A action);

	protected abstract Classifier<C,A> createMitosisChild(C condition,A action);

	public boolean isStateWithin(C state)
	{
		return getCondition().isStateWithin(state);
	}


	public boolean isSameAction(A action)
	{
		if(action == getLayerAction())
		{
			//System.out.println("test isSameAction: targeted_action ("+ action+"), layerAction("+getLayerAction()+") in a classifier. ");
			return true;
		}
		else
		{return false;}
	}
	
	public A getBestAction(ArrayList<A> viable_actions) 
	{
		// Henry's bow-tie structure
		return getActions().getBestAction(viable_actions);
	}
	public A getClassifierAction()
	{
		return null;
	}

	public A getRandomAction()
	{
		return getActions().getRandomAction();
	}
	
	public int compareTo(Classifier<C, A> other)
	{
		return getCondition().compareTo(other.getCondition());
	}
	
	public double getAccuracy()
	{
		return statistics.getAccuracy();
	}
	
	public int getNumerosity()
	{
		return statistics.getNumerosity();
	}
	public double getNichSize()
	{
		return statistics.getNichSize();
	}
	public void decreaseNumerosity()
	{
		 statistics.decreaseNumerosity();
	}


	public double getPredictedReward(){return statistics.getPredictedReward();}
	public double getExperience()
	{
		return statistics.getExperience();
	}

	/* replace by the one which pass the sum_actionSet_acc
	public void updateAction(A taken_action, double reward)
	{
		getActions().updateAction(taken_action, reward);
	}
	*/
	public void updateAction(A taken_action, double reward,int niche_size)
	{
		getActions().updateAction(taken_action, reward, niche_size);
	}
	public void updateActionFitness(A taken_action, double sum_actionSet_acc)
	{
		getActions().updateActionFitness(taken_action, sum_actionSet_acc);
	}

	public void updateSet(double reward, double action_size)
	{
		statistics.updateSet(reward, action_size);
	}

	public void updateSet_learning_rate(double reward, double action_size, double learning_rate)
	{
		statistics.updateSet_learning_rate(reward, action_size, learning_rate);
	}
	public void updateSet_incremental(double incremental_reward, double action_size)
	{
		statistics.updateSet_incremental(incremental_reward, action_size);
	}
	
	public void updateFitness(double sum_relative_accuracy)
	{
		statistics.updateFitness(sum_relative_accuracy);
	}
	
	public int getIterations_since_ga() 
	{
		return iterations_since_ga;
	}

	public void setIterations_since_ga(int iterations_since_ga) 
	{
		this.iterations_since_ga = iterations_since_ga;
	}

	public double getFitness() 
	{
		return this.statistics.getFitness();
	}

	public void setStatistics(double[] statistics)
	{
		this.statistics.set_statistics(statistics);
	}
	/*this should not be apply anymore
	public Classifier<C, A> createMutantChild()
	{
		C condition = (C)getCondition().replicate();
		Classifier<C, A> child = createChild(condition);
		child.mutateClassifier();
		return child;

	}*/

	public double[] get_statistics()
	{
		return this.statistics.get_statistics();
	}

	public Classifier<C, A> createSameChild()
	{
		//C condition = (C)getCondition().replicate();
		try{
			C condition = (C)getCondition().cloneLayerCondition();
			A action	= (A) getLayerAction();
			//	Classifier<C, A> child = createChild(condition);
			Classifier<C, A> child = createChild(condition, action);
			return child;
		}catch(CloneNotSupportedException e) {}
		return null;
	}
	
	public void mutateClassifier()
	{
		//System.out.println("		mutateClassifier before: 	this.getcondition("+this.getCondition()+")");
		System.out.println("		mutateClassifier before: 	this.showCondition("+this.showCondition()+")");
		this.getCondition().mutateCondition();
		System.out.println("		mutateClassifier after: 	this.showCondition("+this.showCondition()+")");

	}
	public void mutateClassifier(double[][] boundary)
	{
		//System.out.println("		mutateClassifier before: 	this.getcondition("+this.getCondition()+")");
		System.out.println("		mutateClassifier before: 	this.showCondition("+this.showCondition()+")");
		this.getCondition().mutateCondition(boundary);
		System.out.println("		mutateClassifier after: 	this.showCondition("+this.showCondition()+")");

	}
	public void crossoverClassifier( Classifier<C, A> other)
	{//crossoverCondition(C ConditionOne, C ConditionOther)
		//getCondition().crossoverCondition();
		System.out.println("		crossoverClassifier before:");
		System.out.println("			this.showCondition 	("+this.showCondition()+")" );
		System.out.println("			other.showCondition	("+ other.showCondition()+")");
		getCondition().crossoverCondition(this.getCondition(), other.getCondition(),"all");
		//System.out.println("		crossoverClassifier before: this.getcondition ("+this.getCondition()+")  VS  other.getCondition("+ other.getCondition()+") ");
		System.out.println("		crossoverClassifier after:");
		System.out.println("			this.showCondition 	("+this.showCondition()+")" );
		System.out.println("			other.showCondition	("+ other.showCondition()+")");
	}

	public ArrayList<Classifier> generate_Mitosis_Classifiers(C to_mitosis_condition)
	{
		ArrayList<Classifier> generate_Mitosis_Classifiers_lsit= new ArrayList<>();

		try{
			C condition = (C)getCondition().cloneLayerCondition(); //mitosis
			ArrayList<C> generated_condition_list = condition.doMitosis(to_mitosis_condition);
			if(!generated_condition_list.isEmpty())
			{
				for(int classifier_generated_index=0; classifier_generated_index< generated_condition_list.size();classifier_generated_index++)
				{
					//create Mitosis_Classifier according to each mitosis condition
					C mitosis_condition = generated_condition_list.get(classifier_generated_index);
					A mitosis_action	= (A) getLayerAction();
					//Classifier mitosis_child = createChild(mitosis_condition, mitosis_action);
					Classifier mitosis_child = createMitosisChild(mitosis_condition, mitosis_action); //compared to createChild method, it will keep the statisitcs of original classifiers
					generate_Mitosis_Classifiers_lsit.add(mitosis_child);
					System.out.println("		generate a Mitosis_Classifiers! 	");
				}
			}
			/*
			condition.doMorph();
			A action	= (A) getLayerAction();
			//	Classifier<C, A> child = createChild(condition);
			Classifier<C, A> child = createChild(condition, action);
			return child;
			*/
			return generate_Mitosis_Classifiers_lsit;
		}catch(CloneNotSupportedException e) {}
		return null;

	}

	public void doMorph(Classifier<C, A> other)
	{
		System.out.println("		doMorph before:	");
		System.out.println("			this.showCondition 	("+this.showCondition()+")" );
		System.out.println("			other.showCondition	("+ other.showCondition()+")");
		getCondition().doMorph(this.getCondition(), other.getCondition());
		System.out.println("		doMorph after:");
		System.out.println("			this.showCondition 	("+this.showCondition()+")" );
		System.out.println("			other.showCondition	("+ other.showCondition()+")");
	}

	public void increaseNumerosity() 
	{
		this.statistics.increaseNumerosity();
	}

	public boolean checkIsTheSame_unique(Classifier<C, A> other)
	{
		//if other equal to the this classifier, they are the same, return true.

		//1.check if the action is the same
		if(	!checkIsTheSameAction_unique(other))
		{
			return false;
		}

		//2.check if the condition is the same
		return checkIsTheSameCondition_unique(other);
	}


	public boolean checkIsTheSameAction_unique(Classifier<C, A> other)
	{
		int mask= this.getLayerAction().getMask();
		int other_mask=other.getLayerAction().getMask();
		if(mask==other_mask)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	public boolean checkIsTheSameCondition_unique(Classifier<C, A> other)
	{
		return  getCondition().isTheSameCondition_unique((other).getCondition());
	}


	public boolean equals(Object other)
	{
		if(!(other instanceof Classifier))
			return false;
		return getCondition().equals(((Classifier)other).getCondition()); //Henry's empty method and override by Tony,it is contain,no the same

	}
	
	public String toString()
	{
		String s = "**********\n"; 
		s += "	Classifier ID: " + this.ID + "\n";
		s += "		Condition:" + getCondition().toString() + " \n";
		s += "		Action:" +getLayerAction().toString()+" \n";
		// s += getActions().toString() + "\n"; //new structure does not need this...
//		s += getExpectationMap().toString() + "\n";
		s += "		Classifier Statistics: "+statistics.toString() + "\n";
		//s += "**********\n";
		return s;
	}

	public String[] toStringArray_for_csv()
	{


		//1.ID
		String[] id_array= new String[1];
		id_array[0]= Integer.toString(getID());



		//2.condition attributes

		String condition_string= getCondition().toString_for_csv();
		String[] condition_array= condition_string.split("#");
		String[] classifier_array =merge_two_stringArray(id_array,condition_array);

		//3. action
		String[] action_array= new String[1];
		action_array[0]= getLayerAction().toString();
		classifier_array =merge_two_stringArray(classifier_array,action_array);

		//4. statistics
		String[] statistics_array = statistics.statistics_toCSVSet();
		classifier_array=merge_two_stringArray(classifier_array,statistics_array);

		return classifier_array;

	}

	public String[] toHeaderString_for_csv()
	{
		//1.ID
		String[] id_array= new String[1];
		id_array[0]= "ID";

		//2.condition attributes

		String condition_string= getCondition().toHeaderString_csv();
		String[] condition_array= condition_string.split("#");
		String[] classifier_array =merge_two_stringArray(id_array,condition_array);

		//3. action
		String[] action_array= new String[1];
		action_array[0]= "ACT";
		classifier_array =merge_two_stringArray(classifier_array,action_array);

		//4. statistics
		String[] statistics_array = statistics.statistics_HeadertoCSVSet();
		classifier_array=merge_two_stringArray(classifier_array,statistics_array);

		return classifier_array;

	}



	public String[] merge_two_stringArray( String[] str1, String[] str2)
	{
		int str1_length= str1.length;
		int str2_length= str2.length;
		int total_length= str1_length+str2_length;

		//5.1 increase the memory
		str1= Arrays.copyOf(str1,total_length);
		//5.2 merge
		System.arraycopy(str2,0,str1,str1_length,str2_length);
		return  str1;
	}

//	{
//		experience++;//increase experience as it has been used again
//		
//		double reward_difference = reward - predicted_reward;
//		//update prediction value
//		//update prediction error
//		if(experience < 1/beta)
//		{
//			predicted_reward = predicted_reward + reward_difference / experience;
//			prediction_error = prediction_error + Math.abs(reward_difference) / experience;
//			niche_size = niche_size + (action_size - niche_size) / experience;
//		}
//		else
//		{
//			predicted_reward = predicted_reward + beta * (reward_difference - predicted_reward);
//			prediction_error = prediction_error + beta * (Math.abs(reward_difference) - prediction_error);
//			niche_size = niche_size + beta * (action_size - niche_size);
//		}
//		
//		if(prediction_error < org_error)
//			this.accuracy = 1;
//		else
//			this.accuracy = alpha * Math.pow((prediction_error / org_error), v);
//	}
//	
//	{
//		fitness = fitness + beta * ((accuracy * numerosity) / sum_relative_accuracy - fitness);
//	}

	public double getActionAccuarcy(A taken_action)
	{//get the accuarcy of an action of a classifier
		//call function in ActionSet.java
		double acc_action_mapping=getActions().getAccuarcy(taken_action);
		System.out.println("debug(add sum of acc for the update of ActionSet 2), classifier ID is ='"+this.ID+"'." +
				" action is "+taken_action+"'. " +
				"accuracy of action is:'"+acc_action_mapping+"'.");
		//return getActions().getAccuarcy(taken_action);
		return acc_action_mapping;

	}

	public int getID()
	{
		return this.ID;
	}

}
