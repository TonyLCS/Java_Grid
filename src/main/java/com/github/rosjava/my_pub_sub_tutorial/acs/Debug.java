package main.java.com.github.rosjava.my_pub_sub_tutorial.acs;

import java.awt.Color;

import javax.swing.JTextArea;

public abstract class Debug 
{
	private static boolean DISPLAY_DEBUG = true;
	
	public static final JTextArea text_area = createTextArea();
	
	private static JTextArea createTextArea()
	{
		JTextArea text_area = new JTextArea("Filter Text Messages", 20, 20);
		text_area = new JTextArea("FilterMessages");
		return text_area;
	}
	
	public static void DebugMessage(String message)
	{
		if(DISPLAY_DEBUG)
			text_area.append("DEBUG: "+message+"\n");
	}
	
	public static void RobotFilterOutput(String message)
	{
		text_area.setForeground(Color.BLUE);
		text_area.append(message+"\n");
		text_area.setForeground(Color.BLACK);
	}
	
	public static void EmotionFilterOutput(String message)
	{
		text_area.setForeground(Color.RED);
		text_area.append(message+"\n");
		text_area.setForeground(Color.BLACK);
	}
}
