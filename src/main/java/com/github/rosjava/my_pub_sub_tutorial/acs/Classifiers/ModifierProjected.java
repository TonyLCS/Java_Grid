package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;

import java.awt.Graphics2D;
import java.io.Serializable;
import java.util.HashMap;

public abstract class ModifierProjected implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HashMap<Integer, ModifierValue> modifier_set = new HashMap<Integer, ModifierValue>();

	public ModifierProjected(){}

	public void store_modifier_value(int index, ModifierValue a_modifier_value )
	{
		this.modifier_set.put(index, a_modifier_value)	;
	}
	public ModifierValue get_modifier_attribute_value(int index)
	{
		return  this.modifier_set.get(index);
	}
}
