package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ActionSet<A extends ActionInterface> implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected HashMap<A, Statistics> action_mapping = new HashMap<A, Statistics>();
	
	public ActionSet(ArrayList<A> actions)
	{
		for(A a : actions)
			action_mapping.put(a, new Statistics());
	}
	
	public ActionSet(A... actions)
	{
		for(A a : actions)
			action_mapping.put(a, new Statistics());
	}
	
	public A getRandomAction()
	{
		int index = (int)(Math.random() * action_mapping.size());
		System.out.println("Random Action: "+index +" "+action_mapping.size());
		ArrayList<A> actions = new ArrayList<A>(action_mapping.keySet());
		return actions.get(index);
	}


	/*
	public A getBestAction(ArrayList<A> viable_actions)
	{
		ArrayList<Map.Entry<A, Statistics>> entries = new ArrayList<Map.Entry<A, Statistics>>(action_mapping.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<A, Statistics>>() 
		{
			@Override
			public int compare(Map.Entry<A, Statistics> entry1, Map.Entry<A, Statistics> entry2) 
			{
				return entry1.getValue().compareTo(entry2.getValue());
			}
		});
		
		int i = 0;
		A action = null;
		while(true)
		{
			System.out.println("i:"+i);
			action = entries.get(i++).getKey();
			if(viable_actions.contains(action))
				{
		System.out.println("Best Action: "+ action +"; action_mapping.size(): "+action_mapping.size()+" ; fitness: "+entries.get(i).getValue().getFitness() +"; i:"+i);
				break;
				}
			if(i > entries.size())
				break;
		}

		return action;
	}
	*/
	public A getBestAction(ArrayList<A> viable_actions)
	{
		ArrayList<Map.Entry<A, Statistics>> entries = new ArrayList<Map.Entry<A, Statistics>>(action_mapping.entrySet());

		/* debug
		System.out.println("		check sort result for best action 1:: " +
				"before sort: " +
				"action 1("+entries.get(0).getKey()+").fit("+entries.get(0).getValue().getFitness()+") VS " +
				"action 2("+entries.get(1).getKey()+").fit("+entries.get(1).getValue().getFitness()+") <--=========");
		*/
		Collections.sort(entries, new Comparator<Map.Entry<A, Statistics>>()
		{
			@Override
			public int compare(Map.Entry<A, Statistics> entry1, Map.Entry<A, Statistics> entry2)
			{
				int randomFlag=0;
				double fitnessDiff=entry1.getValue().getFitness() - entry2.getValue().getFitness();
				if(Math.abs(fitnessDiff)<=0.000001)
				{

					if(Math.random() >= 0.5)
					{
						randomFlag=1;
					}else
						{
							randomFlag=-1;
						}
					return (randomFlag);
				}
				//TODO: ways define best action:1. greater fitness win
				//TODO: 2.including reward in this process.
				if(fitnessDiff>0.000001)
				{
					randomFlag=-1;// descent mode
				}
				if(fitnessDiff<-0.000001)
				{
					randomFlag=1;
				}

				return (randomFlag);
				//return (int)(entry1.getValue().getFitness() - entry2.getValue().getFitness());

			}

		});


		//check if it select the best fitness
		//ArrayList<A> actions = new ArrayList<A>(action_mapping.keySet());
		A checkBestAction = entries.get(0).getKey();
		//A checkBestAction = actions.get(0);
		Statistics checkBestActSta= entries.get(0).getValue();
		A otherAction = entries.get(1).getKey();
		//A otherAction = actions.get(1);
		Statistics otherBestActSta= entries.get(1).getValue();

		if( Math.abs( checkBestActSta.getFitness()-otherBestActSta.getFitness())<0.000001)
		{
			/* debug
			System.out.println("		check sort result for best action 2:: equal<--======");
			*/
		}
		else
		{
			/* debug
			System.out.println("		check sort result for best action 2:: not equal: " );
			*/
		}

		/* debug
		System.out.println("		check sort result for best action 3:: best action: "+checkBestAction+", " +
				"'"+checkBestActSta.getFitness()+"' - " +
				"'"+otherBestActSta.getFitness()+"'= " +
				"'" + ( checkBestActSta.getFitness()-otherBestActSta.getFitness())+ "'. <--========================");

		System.out.println("		check sort result for best action 4::" +
					" after sort:	best action(" + checkBestAction + ").fit(" + checkBestActSta.getFitness() + ") VS" +
					" 	other action(" + otherAction + ").fit(" + otherBestActSta.getFitness() + ") <--=========");
		*/

		/*
		if((checkBestActSta.getFitness()-otherBestActSta.getFitness())<-0.000001)
		{
			System.out.println("check sort result for best action. WRONG!!!!!!!!!!!!!!!!!!!!!");
			System.out.println("check sort result for best action. WRONG!!!!!!!!!!!!!!!!!!!!!");
		}
		*/
		/*
		for(int j=0;j< action_mapping.size();j++)
		{
			System.out.println("getBestAction:to check. in this classifier('"+j+"/"+action_mapping.size()+"'), " +
					" fitness of action('"+ entries.get(j).getKey()+"')" +
					" is ='"+entries.get(j).getValue().getFitness()+"'. ");
		}
		//end of check
		if((entries.get(0).getValue().getFitness()-entries.get(1).getValue().getFitness())<0.00001)
		{

			System.out.println("getBestAction:to check. WRONG!!!!!!!!!!!!!!!!!!!!!");
			System.out.println("getBestAction:to check. WRONG!!!!!!!!!!!!!!!!!!!!!");
			System.out.println("getBestAction:to check. WRONG!!!!!!!!!!!!!!!!!!!!!");
		}
		*/

		int i = 0;
		A action = null;


		while(true)
		{
			action = entries.get(i).getKey();
			if(viable_actions.contains(action))
			{
				/*
				System.out.println("getBestAction for each classifier in MatchSet:" +
						" Best Action: "+ action +";" +
						" action_mapping.size(): "+action_mapping.size()+" ;" +
						" fitness: "+entries.get(i).getValue().getFitness() +";" +
						" i:"+i+"<-0, if best action is in actions");
				*/
				break;
			}
			i++;
			if(i > entries.size())
				break;
		}
		System.out.println("		check sort result for best action("+action+") 5:: end.");
		return action;
	}


	private A getBestAction()
	{
		ArrayList<Map.Entry<A, Statistics>> entries = new ArrayList<Map.Entry<A, Statistics>>(action_mapping.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<A, Statistics>>() 
		{
			@Override
			public int compare(Map.Entry<A, Statistics> entry1, Map.Entry<A, Statistics> entry2) 
			{
				return (int)(entry1.getValue().getFitness() - entry2.getValue().getFitness());
			}
		});
		A action = entries.get(0).getKey();
		return action;
	}
	/* replace by the one with sum_actionSet_acc
	public void updateAction(A action, double reward)
	{
		Statistics statistics = action_mapping.get(action);
		statistics.updateSet(reward, 1);
		statistics.updateFitness(statistics.getAccuracy());

		String s2= statistics.toString();
		System.out.println("level2.3: statistics of action('"+action+"') (part2) :"+s2+"<----but it don's save into population and niche_size is set to approach to 1");
		//save statistics into action_mapping
		//action_mapping.put(action, statistics);

	}
	*/
	public void updateAction(A action, double reward,int niche_size)
	{
		Statistics statistics = action_mapping.get(action);
		statistics.updateSet(reward, niche_size);
		//statistics.updateFitness(statistics.getAccuracy());
		//statistics.updateFitness(sum_actionSet_acc);
		String s2= statistics.toString();
		System.out.println("level2.3: statistics of action('"+action+"') (part2) :"+s2+"<----but it don's save into population and niche_size is set to approach to 1");
		//save statistics into action_mapping
		//action_mapping.put(action, statistics);

	}

	public void updateActionFitness(A action, double sum_actionSet_acc)
	{
		Statistics statistics = action_mapping.get(action);
		statistics.updateFitness(sum_actionSet_acc);
	}

	public String toString()
	{
		//A best_action = getBestAction();

		ArrayList<A> actions = new ArrayList<A>(action_mapping.keySet());
		A actionOne = actions.get(0);
		A actionTwo = actions.get(1);
		//Tony select the best action(the original line did not pass the arrayList A, therefore)
		A best_action = getBestAction(actions);

		String s = "		Best Action: " + best_action +"\n";
		s += "		Best Action Statistics:\n" + action_mapping.get(best_action)+ "\n";
		s += "			actionOne Statistics:"+ action_mapping.get(actionOne)+ "\n";
		s += "			actionTwo Statistics:"+ action_mapping.get(actionTwo)+ "\n";
		return s;
	}

	public String ActionFitnesstoString()
	{
		//A best_action = getBestAction();

		ArrayList<A> actions = new ArrayList<A>(action_mapping.keySet());
		A actionOne = actions.get(0);
		A actionTwo = actions.get(1);
		//Tony select the best action(the original line did not pass the arrayList A, therefore)
		A best_action = getBestAction(actions);

		String s = "		Best Action: " + best_action +"\n";
		s += "		Best Action Statistics:\n" + action_mapping.get(best_action)+ "\n";
		//s += "			action('"+actionOne+"').fitness("+action_mapping.getValue(actionOne).getFitness()+"' VS" +
		//		" action('"+actionTwo+"').fitness("+action_mapping.getValue(actionTwo).getFitness()+"'.\n";
		s += "			actionOne Statistics:"+ action_mapping.get(actionOne)+ "\n";
		s += "			actionTwo Statistics:"+ action_mapping.get(actionTwo)+ "\n";
		return s;
	}

	public double getAccuarcy(A action)
	{
		//Statistics statistics= action_mapping.get(action);
		//System.out.println("debug(add sum of acc for the update of ActionSet), " +"accuarcy of each action of each classifier is='"+statistics.getAccuracy()+"'. " +"it should equal to action_mapping.get(action).getAccuracy():'"+action_mapping.get(action).getAccuracy()+".");
		//return statistics.getAccuracy();
		System.out.println("debug(add sum of acc for the update of ActionSet 1), action_mapping.get(action).getAccuracy()"
				+ " ='"+action_mapping.get(action).getAccuracy()+"'. action is '"+action+"'." +
				"the fitness of action is:'"+action_mapping.get(action).getFitness()+"'; " +
				" predict_reward ='"+action_mapping.get(action).getPredictedReward()+"'." +
				" predict_reward_error='"+action_mapping.get(action).getPredictedRewardError()+"'. " +
				" experience ='"+action_mapping.get(action).getExperience()+"'."
		);
		return action_mapping.get(action).getAccuracy();

	}




}
