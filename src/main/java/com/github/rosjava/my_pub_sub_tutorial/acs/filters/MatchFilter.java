package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

/**
 * Generic Class that will find a match set, within the population, for the current state.
 * @author pwnbot
 *
 */

public class MatchFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A> 
{	
	public static final String NAME = "Match Filter";
		
	public MatchFilter() 
	{
		super(NAME);
	}
	
	@Override
	public FilterMessage<C,A> apply(P agent, int iterationCount, FilterMessage<C,A> message) 
	{
		//find match set
		ArrayList<Integer> matchSet = matchSet(agent, message.getCurrentState());
		
		//printMessage("Set size: " + matchSet.size(), agent);
		//printMessage("Set size: '" + matchSet+"'<-", agent);
		printMessage("	apply MatchFilter: matchSet.size= '" + matchSet.size()+"'; matchSet='" + matchSet+"'<-", agent);

		message.setMatchSet(matchSet);

//System.out.println("apply MatchFilter: matchSet.size()= "+message.getMatchSet().size() + "; agent = "+ agent );

		return message;
	}

	public ArrayList<Integer> matchSet(P population, C currentState)
	{
		ArrayList<Integer> matchSet = population.getMatchingClassifiers(currentState);
		return matchSet;
	}

	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +"::::"+message;
		agent.printMessage(s);
	}
}
