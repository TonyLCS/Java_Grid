package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Statistics;

public class CoverFilter_Representation<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{
	public static final String NAME = "CoverFilter_Representation Filter";
	
	public CoverFilter_Representation()
	{
		super(NAME);
	}
	
	@Override
	public FilterMessage<C, A> apply(P agent, int iterationCount, FilterMessage<C,A> message) 
	{

		//System.out.println("apply CoverFilter_Representation: message.getMatchSet().size() = "+message.getMatchSet().size() + "; agent = "+ agent );

		//1. match set size should >= Statistics.MINIMUM_SIZE
		boolean matchSetSizeCondition=false;
		if(message.getMatchSet().size() < Statistics.MINIMUM_SIZE)
		{
			matchSetSizeCondition=true;
		}

		//2. calculate the diversity of actions in MatchSet
		boolean actionSetSizeCondition=false;

		//chech if the diversity in action set is enough or not
		actionSetSizeCondition= !agent.actionSetDiversity(message.getMatchSet());

		////3. active COVER operation

		//System.out.println("apply CoverFilter_Representation: matchSetSizeCondition("+matchSetSizeCondition+")" + " || actionSetSizeCondition("+actionSetSizeCondition+").");
		while(actionSetSizeCondition)
		{
			System.out.println("apply CoverFilter_Representation");
			C currentState = message.getCurrentState_representation();
			int newClass = agent.createClassifierFromStateWithAction(currentState, message.getMatchSet());
			//int newClass = cover(agent, currentState);
			if(newClass != -1)
			{
				message.getMatchSet().add(newClass);
				printMessage("apply CoverFilter_Representation: add a new classifier into MatchSet, adding classifier's id(newClass)="+newClass,agent);
			}
			actionSetSizeCondition= !agent.actionSetDiversity(message.getMatchSet());
		}
		if(matchSetSizeCondition)
		{
			/**when XCS cover the state, it should produce the representation(crystal eye),
			 *  which comes from a current states(single eye).**/
			System.out.println("apply CoverFilter_Representation");
			C currentState = message.getCurrentState_representation();
			int newClass = agent.createClassifierFromStateWithAction(currentState, message.getMatchSet());
			//int newClass = cover(agent, currentState);
			if(newClass != -1)
			{
				message.getMatchSet().add(newClass);
				printMessage("apply CoverFilter_Representation: add a new classifier into MatchSet, adding classifier's id(newClass)="+newClass,agent);
			}
		}



//System.out.println("apply CoverFilter: message.getMatchSet().size() = "+message.getMatchSet().size() + "; agent = "+ agent );
		return message;
	}
	
	public int cover(P population, C currentState)
	{
		return population.createClassifierFromState(currentState);
	}
	
	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +"::::"+message;
		agent.printMessage(s);
	}
}
