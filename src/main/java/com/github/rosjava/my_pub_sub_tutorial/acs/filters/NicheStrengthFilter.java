package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;
import java.util.HashMap;

import main.java.com.github.rosjava.my_pub_sub_tutorial.Jama.Matrix;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Modifier_ContinuouslyProjected;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

public class NicheStrengthFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{
	private boolean exploit = false;
	private double explore_rate;// will be override by (" SelectPAFilter(double explore_rate)"), which pass explore_chance to here
	private double iterationCount_threshold;
	//private int instance_id;
	private int iteration;
	public static final String NAME = "NicheStrengthFilter";
	
	public NicheStrengthFilter()
	{
		super(NAME);
	}
	
	public NicheStrengthFilter(double explore_rate, double iterationCount_threshold,int iteration)
	{
		super(NAME);
		this.explore_rate = explore_rate;
		this.iterationCount_threshold= iterationCount_threshold;
		//this.instance_id= instance_id;
		this.iteration= iteration;
	}
	public NicheStrengthFilter(double explore_rate)
	{
		super(NAME);
		this.explore_rate = explore_rate;
	}
	
	@Override
	public FilterMessage<C,A> apply(P agent, int iteration, FilterMessage<C,A> message)
	{
		System.out.println("NicheStrengthFilter:: begin ");
		ArrayList<Integer> matchSet = message.getMatchActionSet();

		System.out.println("NicheStrengthFilter	:::: selected action is ("+message.getCurrentAction()+")");
		String  s=message.getCurrentAction().toString();
		s += "#Iteration NO." + Integer.toString(iteration)+ "#";
		System.out.println("NicheStrengthFilter	:::: going to pass string("+s+") for csvfile.");
		//calculate niche strength distribution
		//HashMap<Integer, Matrix>  modifier_niches_set=agent.calculate_niche_strength_distribution(matchSet);
		//agent.select_a_discrete_value_for_modifier(modifier_niches_set, s);

		//calculate niche strength distribution(continuous version)
		HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches_set2 = agent.calculate_niche_strength_distribution_continuous(matchSet);
			//transform hashmap into matrix
		HashMap<Integer, Modifier_ContinuouslyProjected> modifier_niches_set4= agent.transform_modifier_niches_set_into_matrix_inline(modifier_niches_set2);
			//HashMap<Integer, Matrix>  modifier_niches_set3= agent.transform_modifier_niches_set_into_matrix(modifier_niches_set2);
		agent.save_modifiers(s, modifier_niches_set4);
		// todo: return the modifier_set to message...

		/*
		ArrayList<A> actions_list =		agent.getClassifiersPossibleActionSetList();
		A store_current_action = message.getCurrentAction();
		for(A sact: actions_list)
		{
			message.setCurrentAction(sact);

			System.out.println("NicheStrengthFilter	:::: selected action is ("+sact+")");

			HashMap<Integer, Matrix>  modifier_set=agent.calculate_niche_strength_distribution(matchSet);
			agent.select_a_discrete_value_for_modifier(modifier_set);
		}
		//recover current action
		message.setCurrentAction(store_current_action);
		*/
		return message;
	}
	
	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +":::: "+message;
		agent.printMessage(s);
	}	
}
