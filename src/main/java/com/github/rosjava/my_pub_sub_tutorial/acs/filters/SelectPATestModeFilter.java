package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

public class SelectPATestModeFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{
	private boolean exploit = false;
	private double explore_rate;// will be override by (" SelectPAFilter(double explore_rate)"), which pass explore_chance to here
	private double iterationCount_threshold;
	private int instance_id;
	private int iteration;
	public static final String NAME = "SelectPATestModeFilter";
	
	public SelectPATestModeFilter()
	{
		super(NAME);
	}
	
	public SelectPATestModeFilter(double explore_rate, double iterationCount_threshold,int instance_id,int iteration)
	{
		super(NAME);
		this.explore_rate = explore_rate;
		this.iterationCount_threshold= iterationCount_threshold;
		this.instance_id= instance_id;
		this.iteration= iteration;
	}
	public SelectPATestModeFilter(double explore_rate)
	{
		super(NAME);
		this.explore_rate = explore_rate;
	}
	
	@Override
	public FilterMessage<C,A> apply(P agent, int iteration, FilterMessage<C,A> message)
	{
		System.out.println("SelectPAFilter:: begin ");
		ArrayList<Integer> matchSet = message.getMatchSet();
		ArrayList<Integer> action_set = new ArrayList<Integer>();
		//pass the address of anticipated_reward down to getNextAction().
		// 	In getNextAction() will calculate the anticipated_reward, which will be applied in reward.
		ArrayList<Double> anticipated_reward=new ArrayList<Double>();
		double an_anticipated_reward=-0.031415926;


		//I move the explore procedural ahead to make this decision match this action
		double r = Math.random() * 100;
		if(r >= explore_rate) {exploit = true;}
		else{exploit = false;}
			// exploit habitation. explore protect. no exploit under first 1500 time

		System.out.println("SelectPAFilter:  iterationCount("+iteration+") " +
				"< iterationCount_threshold("+iterationCount_threshold+"), exploit("+exploit+")");
		if(iteration<iterationCount_threshold) //iterationCount_threshold
		{
			//todo:to test the classifier mask the following line
			//todo: test mode
			exploit = false;
		}
		//1.calculate vote for actions.
		//2.select best action from vote if it is an exploit mode.
		//3. return the action and also set the action_set!
		//4. new exploit mode will also calculate anticipated reward, also return it.

		//todo: different from SelectPTFilter.java is here: save it into csv file
		//1. save instance 's every anticipated reward into a hashmap
		A action = agent.getNextAction(message.getCurrentState(), matchSet, action_set, exploit, anticipated_reward);
		//2. get the anticipated_reward_hashmap back
		agent.save_anticipated_reward_hashmap(this.iteration, this.instance_id);// save anticipated reward Hashmap

		//check the action_set is setted in the sub-procedural of the above line.
		System.out.println("SelectPAFilter:: action_set.size() is "+action_set.size()+", and advocate action("+action+")");
		/*debug work
		for(int i=0; i< action_set.size();i++)
		{
			System.out.println("	("+i+"/"+action_set.size()+") classifier.Id("+action_set.get(i)+") in action_set().");
		}
		*/

		//4. pass the action selected and action_set into the message
		message.setCurrentAction(action);
		message.setActionSet(action_set);

		//5. record informations for anticipated reward, such as explore mode(keyword "exploit"), anticipated reward
		if(action_set.size()==0)
		{
			an_anticipated_reward=0;
		}
		else
		{
			an_anticipated_reward=anticipated_reward.get(0);
		}

		message.record_anticipated_reward(an_anticipated_reward);
		boolean exploreMode;
		if(exploit) {exploreMode=false;}
		else {exploreMode=true;}
		//System.out.println("trace 1 : SelectPAFilter:: exploit("+exploit+") - exploreMode("+exploreMode+")");
		message.record_explore_mode(exploreMode);
		//message.get_explore_mode();

		System.out.println("SelectPAFilter::  \t anticipated_reward("+an_anticipated_reward+") for action("+action+")\n");

		return message;
	}
	
	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +":::: "+message;
		agent.printMessage(s);
	}	
}
