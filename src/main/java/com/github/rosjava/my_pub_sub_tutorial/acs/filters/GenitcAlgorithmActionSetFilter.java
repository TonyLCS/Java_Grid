package main.java.com.github.rosjava.my_pub_sub_tutorial.acs.filters;

import java.util.ArrayList;

import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.LCSProblem;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Population;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.ActionInterface;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Condition;

public class GenitcAlgorithmActionSetFilter<P extends LCSProblem<C, A>, C extends Condition<C>, A extends ActionInterface> extends Filter<P, C, A>
{
	private final int GA_THRESHOLD;
	
	public static final String NAME = "Genitc Algorithm Filter";
	
	
	public GenitcAlgorithmActionSetFilter(int ga_threshold)
	{
		super(NAME);
		this.GA_THRESHOLD = ga_threshold;
	}

	@Override
	public FilterMessage<C, A> apply(P agent, int iterationCount, FilterMessage<C, A> message) 
	{
		printMessage("GA Filter (GenitcAlgorithmActionSetFilter)", agent);

		/* change to standard: do GA in action_set
		ArrayList<Integer> match_set_ids = message.getMatchSet();//?? should GA apply to action_set!!!
		agent.applyGA(match_set_ids, iterationCount, GA_THRESHOLD);
		*/
		ArrayList<Integer> action_set_ids = message.getActionSet();//?? should GA apply to action_set!!!
		/**show current instance. VALUE**/




		if(!action_set_ids.isEmpty())
		{
			double[][] perception_boundary = message.get_boundary();
			if(perception_boundary == null || perception_boundary.length == 0 || (perception_boundary.length == 1 && perception_boundary[0].length == 0))
			{
				agent.applyGA(action_set_ids, iterationCount, this.GA_THRESHOLD);
			}else {
				agent.applyGA_boundary(action_set_ids, iterationCount, this.GA_THRESHOLD,perception_boundary);

			}


			agent.increaseTimeSinceGA();
		}
		else
			{
				System.out.println("GA Filter action_set("+action_set_ids.size()+") is empty ");
			}

		return message;
	}
	
	@Override
	protected void printMessage(String message, P agent) 
	{
		String s = NAME +" "+message;
		agent.printMessage(s);
	}
}
