package main.java.GridSim.EnvironmentManager;

import java.io.Serializable;


public class Instance implements Serializable
{
	// for iris problem, for attributes are sepal_length, sepal_width, petal_length and petal_width.
	// the result is species


	public double[] attribute;
	public double ground_truth;

	/*
        public void copy_instance(Attributes an_attribute)
        {
            this.attribute = an_attribute.attribute;
            this.ground_truth= an_attribute.ground_truth;
        }
    */
	public Instance(double[] an_attribute, double ground_truth)
	{
		this.attribute = an_attribute;
		this.ground_truth= ground_truth;
	}

	public Instance(double[] an_attribute, double ground_truth, int length_attribute)
	{
		for(int i=0; i<length_attribute-1; i++)
		{
			this.attribute[i]=an_attribute[i];
		}
		this.ground_truth= ground_truth;
	}

	public void  set_all_attribute(double[] an_attribute)
	{

		this.attribute= an_attribute;
	}
	public double[] get_all_attribute()
	{
		return  this.attribute;
	}

	public int get_attribute_length()
	{
		return  this.attribute.length;
	}

	public void set_ground_truth(double ground_truth)
	{
		this.ground_truth= ground_truth;
	}

	public void show_all_attribute()
	{
		String s= "attribute in a instance: ";
		for(double an_attribute: attribute)
		{
			s +=   Double.toString(an_attribute) +"  ";
		}

		System.out.println( s);

	}
	public void show_ground_truth()
	{
		System.out.println( "ground truth("+this.ground_truth+") in a instance: ");
	}

	public Instance(){}

	public String[] transfer_attributes_to_strings()
	{
		int length = this.attribute.length;
		String[] att_s= new String[length];

		for(int i=0; i< length;i++)
		{
			att_s[i]= Double.toString(this.attribute[i]);
		}

		return att_s;
	}

	public double[] extractValueInstance()
	{
		int length_of_attribute = this.attribute.length/3;
		// extract the value of attribute
		double[] instance_value = new double[length_of_attribute];
		for(int i =0; i<length_of_attribute; i++)
		{
			instance_value[i] = this.attribute[1+3*i];
		}
		return instance_value;

	}
	public double[] extractValueInstance(int length_of_attribute_part)
	{
		int length_of_attribute = length_of_attribute_part/3;
		// extract the value of attribute
		double[] instance_value = new double[length_of_attribute];
		for(int i =0; i<length_of_attribute; i++)
		{
			instance_value[i] = this.attribute[1+3*i];
		}
		return instance_value;

	}

}