package main.java.GridSim.EnvironmentManager;

import com.thoughtworks.xstream.XStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import com.opencsv.bean.CsvBindByName;


/**
 * Created by robot-base on 22/03/18.
 */
public class EnvironmentManager {

    public EnvironmentManager(){}





    public void test_XStream_reading(String file)
    {// to read hashmap file
        System.out.println("grid_read_XStream_test ::'"+file+"'.........");
        int count_entry=0;

        try
        {
            XStream xstream = new XStream();
            HashMap<Integer, Double> hashMap = (HashMap<Integer, Double>)xstream.fromXML(new File(file+".xml"));
            if(hashMap != null) {
                System.out.println("has hashmap");
                //this.classifiers = hashMap;
            }
            for(int id : hashMap.keySet()) {
                if (id >= count_entry) {
                    count_entry = id + 1;
                    //System.out.println(" NEXT_ID("+NEXT_ID+").......");
                }
            }
            System.out.println("the number of entry("+count_entry+").......");
        }
        catch(Exception e)
        {
            System.out.println("fail to read XStream(hashmap) from file:'"+file+".xml' fail.........");
            System.out.println("error message!: " + e.getMessage());
//			e.printStackTrace();
        }
    }


    public void test_XStream_writing(String file)
    {
        HashMap<Integer, Integer> a_hashmap= new HashMap<Integer, Integer>();
        a_hashmap.put(1,2015);
        a_hashmap.put(2,2016);
        a_hashmap.put(3,2017);
        a_hashmap.put(4,2018);




        try
        {
            XStream xstream = new XStream();
            //String xml = xstream.toXML(this);
            String xml = xstream.toXML(a_hashmap);
            BufferedWriter xmlOut = new BufferedWriter(new FileWriter(file+".xml"));
            xmlOut.write(xml);
            xmlOut.close();
            System.out.println("test_XStream_writing, write "+file+" ");


        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}
