package main.java.GridSim.EnvironmentManager;


import java.io.Serializable;


public class SingleTask implements Serializable
{
	// for iris problem, for attributes are sepal_length, sepal_width, petal_length and petal_width.
	// the result is species

	//public  static final String fileDir="/home/robot-base/tony_catkin/chris_catkin_ws/src/rosjava_package_learning/my_pub_sub_tutorial/src/main/java/com/github/rosjava/my_pub_sub_tutorial/Iris/";
	//public  static final String CSVfile  = fileDir +"iris2.csv";//"iris3.csv";//"iris_3Instances_3Actions.csv";//"iris2_fake_2_group.csv";//"iris2.csv";//"iris_3Instances_3Actions.csv";//"iris_separate.csv";//"iris2.csv";//"blockData3.csv";//"iris_separate.csv";
	public  static final String CSVfile  =  "iris2.csv";//"iris3.csv";//"iris_3Instances_3Actions.csv";//"iris2_fake_2_group.csv";//"iris2.csv";//"iris_3Instances_3Actions.csv";//"iris_separate.csv";//"iris2.csv";//"blockData3.csv";//"iris_separate.csv";

	public double sepal_length;
	public double sepal_width;
	public double petal_length;
	public double petal_width;
	public double species;

	public double[] attribute[];
	public double result;

	
	public SingleTask(double sepal_length, double sepal_width, double petal_length, double petal_width, double species)
	{
		this.sepal_length = sepal_length;
		this.sepal_width = sepal_width;
		this.petal_length = petal_length;
		this.petal_width = petal_width;
		this.species = species;
	}

	public SingleTask(SingleTask isirtask)
	{
		this.sepal_length = isirtask.sepal_length;
		this.sepal_width = isirtask.sepal_width;
		this.petal_length = isirtask.petal_length;
		this.petal_width = isirtask.petal_width;
		this.species = isirtask.species;
	}

	public SingleTask(){}

	/*
	public  SingleTask loadCSVData(int required_line_NO) {

		//input: which line of data the for training.
		// output:return the data.

		SingleTask oneSingleTask= new SingleTask();

		try{
			Scanner scanner = new Scanner(new File(CSVfile));
			scanner.useDelimiter(",");

			//int required_line_NO= 1; //specify which line the off-line environment required
			int index_scanner=0;
			double field_value=0;
			while(scanner.hasNextLine())
			{
				if(index_scanner!= required_line_NO)
				{
					scanner.nextLine();
					index_scanner++;
				}
				if(index_scanner== required_line_NO)
				{
					//System.out.println("index_scan ="+ index_scanner);
					//System.out.println( "\n");
					String line = scanner.nextLine();
					String[] fields = line.split(",");
					//System.out.println(fields + "<- String[] fields");    //fields[] address
					//System.out.println( "\n");
					//System.out.println( "fields.length ("+fields.length+") \n");

					int index_attribute=1;
					//operation on each item
					for(String field : fields)
					{

						field_value= Double.parseDouble(field);
						switch(index_attribute)
						{
							case 1:
								oneSingleTask.sepal_length=field_value;
								break;
							case 2:
								oneSingleTask.sepal_width= field_value;
								break;
							case 3:
								oneSingleTask.petal_length= field_value;
								break;
							case 4:
								oneSingleTask.petal_width= field_value;
								break;
							case 5:
								oneSingleTask.species= field_value;
								break;
							default:
								break;
						}
						index_attribute++;
						//save dataframe into condition and result.

						//System.out.println(field_value + " ");
						//put field into a hashmap
					}


					index_scanner++;
				}

			}// end of while

			scanner.close();
			return oneSingleTask;

		}catch (FileNotFoundException e){return null;}

	}
	*/
}
