package main.java.GridSim;

import main.java.GridSim.EnvironmentManager.EnvironmentManager;
import main.java.GridSim.EnvironmentManager.Instance;
import main.java.GridSim.GridOp.GridOp;

import java.util.ArrayList;


public class GridSimMain {

    public static void main(String[] args) {
	// write your code here
        System.out.println("java version: GridSimMain ");

        //1 test environment
        /*
        EnvironmentManager em=new EnvironmentManager();
        String file= "XSteam_test";
        em.test_XStream_writing(file);
        em.test_XStream_reading(file);
        */

        //2 test XCS
        GridOp gridOp= new GridOp();

        if(args==null || (args.length==0&&args!=null) )
        {
            gridOp.set_DataSet_PARAMS();
        }
        else
        {
            gridOp.set_DataSet_PARAMS(Integer.parseInt(args[0]));
        }


        //okay finish:
        //gridOp.MultiStepLearning_Instance();
        /**test**/
        //todo:
        //gridOp.loading_hierarchy_learning_agent();
        /*****
         * modifier layer
        gridOp.MultiStepLearning_Instance();

         ***/
        /**
         * test API
         * **/

        int action_index;
        double feedback_reward =1000;

        //gridOp.XCS_initiate_fiters_API();
        gridOp.XCS_initiate_test_fiters_API();
        gridOp.XCS_set_testing_mode_API();

        double[] perception = {2.1, 4.3};
        action_index = gridOp.XCS_get_prediction_API(perception);
        System.out.println("action select by xcs is: "+ action_index);
        //gridOp.XCS_reward_assignment(feedback_reward);

        //perception[0] = 1;
        //perception[1] = 5;
        //action_index = gridOp.XCS_get_prediction_API(perception);
        //System.out.println("action select by xcs is: "+ action_index);


        /*
        for(int i=0; i< instances.size(); i++)
        {
            System.out.println(instances.get(i).attribute[0]);
        }
        */
        /*
        String mode= "seq";	// seq random // seq random
        String rewardMethod= "linear_regression";// //"linear_regression";// "even";//fitXprd,even, fit,onlyUpdateExploreMode, linear_regression;	//
        int multistep= 3;
        int episode_iterations=1000;
        gridOp.MultiStepLearning_Instance(mode, multistep, rewardMethod,episode_iterations);

        */
        //gridOp.read_dataset_demo();
    }
}
