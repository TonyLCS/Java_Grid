package main.java.GridSim.GridOp;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import main.java.com.github.rosjava.my_pub_sub_tutorial.Iris.LearningAgent;
import main.java.com.github.rosjava.my_pub_sub_tutorial.Iris.IrisCheckPoints;
import main.java.com.github.rosjava.my_pub_sub_tutorial.Iris.IrisRecord;

import main.java.GridSim.EnvironmentManager.SingleTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.FileSystems;
import java.util.*;

import main.java.GridSim.EnvironmentManager.Instance;
import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerAction;

import main.java.com.github.rosjava.my_pub_sub_tutorial.LayerProblem.LayerClassifier;
import main.java.com.github.rosjava.my_pub_sub_tutorial.acs.Classifiers.Modifier_ContinuouslyProjected;

import java.nio.file.Path;
import java.nio.file.Paths;

public class GridOp {


    private String doThisTask="";//whichTask
    private int task_ID;

    public String dataSetDir= "./DataSets/";
    public String DataSet_CSVfile ;// =dataSetDir+"iris.csv";//"zoo.csv";//"lung_cancer.csv";//"zoo.csv";//"wine.csv";//"lung_cancer.csv";//"iris2.csv";//"iris3.csv";//"iris_3Instances_3Actions.csv";//"iris2_fake_2_group.csv";//"iris2.csv";//"iris_3Instances_3Actions.csv";//"iris_separate.csv";//"iris2.csv";//"blockData3.csv";//"iris_separate.csv";
    public int dataSet_instance_total_num= 150;//101;//27;//101;//178;//101;//
    public int the_DIVERSITY_of_actions=2;//7;// also set the same parameter in layerProblem

    private int episode_iterations=0;
    private String rewardMethod="";
    private int multistep=0;
    private String mode="";

    private int exploreRate=40;
    private int iterationCount_threshold=40;// 150*3*10 when iteration < this threshold, no exploit
    private int GATheshold=40;

    private int test_exploreRate=0;
    private int test_iterationCount_threshold=0;
    private int test_GA_threshold=1500000000;

    private boolean one_scenario_test_mode		= false;//true;//
    private boolean dynamic_multistep_mode 		= false;//	true;//false;//		//fixed length of multistep
    public int save_csv_each_episode_num= 1000;

    public LearningAgent learningAgent= new LearningAgent();
    //public LearningAgent testingAgent= new LearningAgent();
    IrisRecord irisRecord =new IrisRecord();
    IrisCheckPoints irisCheckPoints= new IrisCheckPoints();
    private int iteration;

    private double[][] perception_boundary;

    public GridOp(){
        System.out.println( "GridOp initialized.");
    }

    public void set_DataSet_PARAMS(String path)
    {
        System.out.println( "setting parameter.");
        //1.read which task it should work on
        chooseWhichTask(path);// taskID indicate the line in parameters list, path = "/Results/doThisTask.csv"
        //2.read parameters according to which task
        loadTaskParameters(this.task_ID);
        //3. set parameters
        setMembersParameters();
    }

    public void set_DataSet_PARAMS()
    {
        System.out.println( "setting parameter.");
        //1.read which task it should work on
        chooseWhichTask();// taskID indicate the line in parameters list
        //2.read parameters according to which task
        loadTaskParameters(this.task_ID);
        //3. set parameters
        setMembersParameters();
    }
    public void set_DataSet_PARAMS(int task_ID)
    {

        //this task_ID come from main args[] !!!
        this.task_ID=task_ID;
        //2.read parameters according to which task
        loadTaskParameters(this.task_ID);
        //3. set parameters
        setMembersParameters();
    }

    public void setMembersParameters()
    {
        this.learningAgent.set_the_DIVERSITY_of_actions(this.the_DIVERSITY_of_actions);

    }
    public int chooseWhichTask(String task_path)
    {
        Path path = Paths.get("");
        String s_path =path.toAbsolutePath().toString();
        System.out.println("current working path is:"+s_path);
        String doWhichTaskFile=s_path+ task_path ;//task_path = "/Results/doThisTask.csv"

        //String doWhichTaskFile="./Results/doThisTask.csv";

        try{
            Scanner scanner = new Scanner(new File(doWhichTaskFile));
            scanner.useDelimiter(",");

            int index_scanner=0;
            double field_value=0;
            while(scanner.hasNextLine())
            {
                if(index_scanner!= 1)
                {
                    scanner.nextLine();
                    index_scanner++;
                }else
                {
                    if(index_scanner== 1)
                    {
                        //System.out.println( "\n");
                        String line = scanner.nextLine();
                        String[] fields = line.split(",");

                        //System.out.println( "loadCSVData_I:: fields.length ("+fields.length+") \n");
                        double[] field_d = new double[fields.length];
                        double[] attribute= new double[fields.length-1];
                        double groundTruth=0;
                        int count=0;


                        for(String field : fields)
                        {
                            if(count == (fields.length-1))
                            {
                                //the last one is the ground truth
                                this.task_ID=Integer.parseInt(field);

                            }
                            else
                            {
                                this.doThisTask=field;
                            }

                            count++;
                        }

                        System.out.println( "task_id ("+this.task_ID+"): doThisTask ("+this.doThisTask+").");
                        index_scanner++;
                    }
                }


            }// end of while

            scanner.close();
            return task_ID;

        }catch (FileNotFoundException e){return 0;}

    }

    public int chooseWhichTask()
    {
        Path path = Paths.get("");
        String s_path =path.toAbsolutePath().toString();
        System.out.println("current working path is:"+s_path);
        String doWhichTaskFile=s_path+"/Results/doThisTask.csv";

        //String doWhichTaskFile="./Results/doThisTask.csv";

        try{
            Scanner scanner = new Scanner(new File(doWhichTaskFile));
            scanner.useDelimiter(",");

            int index_scanner=0;
            double field_value=0;
            while(scanner.hasNextLine())
            {
                if(index_scanner!= 1)
                {
                    scanner.nextLine();
                    index_scanner++;
                }else
                {
                    if(index_scanner== 1)
                    {
                        //System.out.println( "\n");
                        String line = scanner.nextLine();
                        String[] fields = line.split(",");

                        //System.out.println( "loadCSVData_I:: fields.length ("+fields.length+") \n");
                        double[] field_d = new double[fields.length];
                        double[] attribute= new double[fields.length-1];
                        double groundTruth=0;
                        int count=0;


                        for(String field : fields)
                        {
                            if(count == (fields.length-1))
                            {
                                //the last one is the ground truth
                                this.task_ID=Integer.parseInt(field);

                            }
                            else
                            {
                                this.doThisTask=field;
                            }

                            count++;
                        }

                        System.out.println( "task_id ("+this.task_ID+"): doThisTask ("+this.doThisTask+").");
                        index_scanner++;
                    }
                }


            }// end of while

            scanner.close();
            return task_ID;

        }catch (FileNotFoundException e){return 0;}

    }

    public void loadTaskParameters(int task_ID)
    {
        Path path = Paths.get("");
        String s_path =path.toAbsolutePath().toString();
        String doWhichTaskFile=s_path+"/DataSets/Task_PARAMTERS.csv";
        //String doWhichTaskFile="DataSets/Task_PARAMTERS.csv";
        System.out.println("loading parameters from file::"+doWhichTaskFile);

        try{
            Scanner scanner = new Scanner(new File(doWhichTaskFile));
            scanner.useDelimiter(",");

            int index_scanner=0;
            double field_value=0;
            while(scanner.hasNextLine())
            {
                if(index_scanner!= task_ID)
                {
                    scanner.nextLine();
                    index_scanner++;
                }else
                {
                    if(index_scanner== task_ID)
                    {
                        //task_ID indicate to load what line in csv file
                        System.out.println( "LOADING PARAMETERS!\n");
                        String line = scanner.nextLine();
                        String[] fields = line.split(",");

                        this.doThisTask                 = fields[0];

                        this.dataSet_instance_total_num = Integer.parseInt(fields[2]);
                        this.the_DIVERSITY_of_actions   = Integer.parseInt(fields[3]);
                        this.dataSetDir                 = fields[4];
                        this.DataSet_CSVfile            = dataSetDir+fields[5];

                        this.multistep                  = Integer.parseInt(fields[6]);
                        this.episode_iterations         =Integer.parseInt(fields[7]);
                        this.mode=fields[8];
                        this.rewardMethod=fields[9];
                        this.save_csv_each_episode_num= Integer.parseInt(fields[10]);

                        this.exploreRate= Integer.parseInt(fields[11]);
                        this.iterationCount_threshold=Integer.parseInt(fields[12]);
                        this.GATheshold= Integer.parseInt(fields[13]);

                        //output
                        this.output_whichTask(fields);
                        index_scanner++;
                    }
                }

            }// end of while
            System.out.println(">>> doThisTask ("+doThisTask+") <<< ");
            System.out.println("dataSet_instance_total_num("+dataSet_instance_total_num+"), the_DIVERSITY_of_actions("+the_DIVERSITY_of_actions+")," + " dataSetDir("+dataSetDir+"), DataSet_CSVfile("+DataSet_CSVfile+")");
            System.out.println("multistep("+multistep+"), episode_iterations("+episode_iterations+"), mode("+mode+"), rewardMethod("+rewardMethod+") ");
            scanner.close();

        }catch (FileNotFoundException e){System.out.println("PARAMETERS LOAD ERROR");}

    }

    public void output_whichTask(String[] whichTaskData)
    {


        try
        {
            System.out.println("CSV version:  the classifiers");


            CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/whichTask.csv"));
            csv_writer.writeNext(whichTaskData);
            csv_writer.close();


        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }


//    public void MultiStepLearningBecthmark(String mode, int multistep,String rewardMethod)
//    {
//
//        this.multistep=multistep;
//        /**
//         * this is a benchmark for future improvement.
//         * learn n step together and update reward.
//         * assign the reward equally
//         * */
//
//        /**
//         * pass a task to learning loop.
//         * the task will required multi-step to complete
//         * *****/
//        /**
//         1.initial
//         set learning mode/ training mode/ train mode
//         exploreRate, iterationCount_threshold,  GATheshold
//         */
//        this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); //for iris training,MultiStepLearningBecthmark
//
//        int a_instance_sequence_log=1;
//
//
//        /**
//         * go to task loop.
//         * in each loop, multiple iterations will be trained.
//         * the number of the iterations in each loop is the number of step
//         * **/
//
//        for(int task_seq=1; task_seq<=5000; task_seq++)
//        {
//            this.learningAgent.set_task_seq(task_seq);
//
//			/*
//			if(multistep==0)
//			{
//				multistep=(int)(Math.random()*149)+1 ;// change this for randomly multiple-steps updating.
//			}
//			*/
//            //"test S" ramdon multistep
//            //multistep=(int)(Math.random()*5)+1;
//            //this.multistep=multistep;
//
//            /**
//             * select the number of parasite classifiers
//             *
//             * **********/
//            System.out.println("\n");
//            System.out.println("************************************************\n");
//            System.out.println("************************************************\n");
//            System.out.println("************************************************\n");
//
//            boolean explore_mode_flag= false;
//            this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); //for iris training,MultiStepLearningBecthmark
//
//            for(int a_step=1; a_step<=multistep; a_step++, this.iteration++)
//            {
//                /**
//                 *  randomly instance selection or sequential selection
//                 * */
//
//                int a_instance;
//                if(mode.equals("seq"))
//                {
//                    //A. sequence
//                    a_instance=a_instance_sequence_log; //sequential
//                }
//
//                if(mode.equals("random"))
//                {
//                    //B. random
//
//                    if(this.iteration<=900)
//                    {
//                        //sequentially select instance
//                        a_instance=a_instance_sequence_log; //sequential
//
//                   }
//                    else
//                    {
//                        //select a random instance
//                        a_instance= 1+ (int)Math.ceil(Math.random()*149); //random
//
//                    }
//
//                }
//                else
//                {
//                    a_instance=a_instance_sequence_log; //sequential
//                }
//
//                if(one_scenario_test_mode)
//                {
//                    a_instance=a_step;// (a) only 3 instance!!!!!!!!!!! for  "1 scenario" test
//                }
//                //a_instance=a_step;// (a) only 3 instance!!!!!!!!!!! for  "1 scenario" test
//
//                System.out.println("\n");
//                System.out.println("choose a_instance NO.("+a_instance+") in iteration("+this.iteration+") for multiple-step("+multistep+") problem.");
//                SingleTask a_iteration_singleTask= loadCSVData(a_instance);
//                /**extract ground truth from the instance**/
//                learningAgent.iris_learning_multiStep_groundTruth(a_iteration_singleTask);
//
//                /**goto multistep iterations**/
//
//                System.out.println("a_instance NO.("+a_instance+"), step_seq("+a_step+"), multiple-step("+multistep+"), task_seq("+task_seq+"); iteration seq("+iteration+")");
//                //debug ok//	learningAgent.iris_learning_multiStep_4fliters(a_iteration_singleTask,iteration); //for parasite classifiers
//
//                learningAgent.iris_Learning_Mode_2(a_iteration_singleTask, this.iteration);
//
//				/*
//				System.out.println(" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
//				System.out.println(" ^^^^^^^has explore ("+learningAgent.getExploreMode()+")^^^^^^^^^^^^^^^^^^^^^^^^");
//				System.out.println(" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
//				*/
//
//                /**if it has explore, set exploit mode for the rest of steps**/
//
//				/*
//				if(	learningAgent.getExploreMode())
//				{
//					//System.out.println(" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
//					//System.out.println(" ^^^^^^^has explore ("+learningAgent.getExploreMode()+")^^^^^^^^^^^^^^^^^^^^^^^^");
//					//System.out.println(" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
//
//					explore_mode_flag=true;
//					this.learningAgent.setLearningMode(0, this.iterationCount_threshold, this.GATheshold); //set the rest for exploit mode
//				}
//				*/
//
//                /**reset instance index for loops**/
//                a_instance_sequence_log++;
//                if(a_instance_sequence_log==151)
//                {
//                    a_instance_sequence_log=1;
//                }
//            }
//
//            /**
//             * reward generator
//             * generate reward_total
//             * **/
//            double TaskReward=learningAgent.reward_summary();
//            System.out.println(" TaskReward("+TaskReward+")");
//            /**assign reward**/
//
//            //150*multistep/ {n*(n+1)/2}= 300/(n+1)
//            int scenario_seq;
//            if(dynamic_multistep_mode)
//            {
//                int total_number_of_scenario = 300/(this.multistep+1);
//                scenario_seq= (task_seq-1)%total_number_of_scenario;
//            }
//            else
//            {
//                //150/multistep
//                int total_number_of_scenario = 150/(this.multistep);
//                scenario_seq= (task_seq-1)%total_number_of_scenario;
//            }
//
//
//            //int scenario_seq= (task_seq-1)%50;//50 is the number of scenarios 50= (150/(1+2+3+4+5))*5 ;//150 instances/ (1+2+3+4) steps =50 scenarios, start from 0
//            //int scenario_seq= (task_seq-1)%50;//150 instances/ 3 steps =50 scenarios, start from 0
//            if(one_scenario_test_mode)
//            {
//                //override
//                scenario_seq=1;// (b) only 3 instance!!!!!!!!!!! for "1 scenario" test
//            }
//            //int scenario_seq=1;// (b) only 3 instance!!!!!!!!!!! for "1 scenario" test
//
//            if(rewardMethod.equals("linear_regression"))
//            {
//                //todo: get T. tempary T
//                //int T= 3;
//                //2. //learningAgent.span_span_matrix_steps_rewards_vectors(this.multistep);
//                //3. //learningAgent.span_orthogonal_reward_space(TaskReward, scenario_seq,this.multistep);
//                //4. //Matrix reward_all_steps_matrix = learningAgent.calculate_reward_all_steps_matrix(scenario_seq, this.multistep, T);
//                //5. //learningAgent.extract_reward_S_step_from_all( this.multistep,  reward_all_steps_matrix );
//                //(b)"test S" ramdon multistep
//
//
//                //learningAgent.calculateMatrix_example(TaskReward, scenario_seq);
//            }
//            else
//            {
//                learningAgent.span_span_matrix_steps_rewards_vectors();
//            }
//
//            //  rewardMethod: even fitXprd,fit;
//            //(c)"test S" ramdon multistep commond next line
//            //learningAgent.reward_assign(TaskReward,rewardMethod);
//            int O= scenario_seq;
//            //int S= this.multistep;
//            int T= 3;
//            boolean reward_has_assign = learningAgent.reward_assign(TaskReward,"linear_regression",O,T);
//
//            /**update classifier population**/
//            if(reward_has_assign)
//            {
//                learningAgent.reward_apply();
//            }
//            else
//            {
//                learningAgent.donot_update_reward();
//            }
//
//            /**
//             * reward assignment
//             * **/
//            //this.learningAgent.irisMultistepReward(numberOfParasite,numberOfHost);
//            /**
//             * reward application-update
//             * **/
//
//            /**deletion**/
//            this.learningAgent.irislearningMultistep_del();
//
//            /*********************
//             * record check point
//             * *****************/
//            if(one_scenario_test_mode)
//            {
//                if(task_seq>=this.multistep&& task_seq%this.multistep==0)
//                {
//                    testCheckPoints(iteration);
//                    this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); // reset training mode after testing mode, for iris training
//                }
//            }
//            else
//            {
//                if(task_seq%151==150)
//                {
//                    //debug ok// iris_Learning_Mode_2(iteration);
//                    testCheckPoints(iteration);
//                    /**finish test mode reset the learning mode**/
//                    // in the testCheckPoints, it set the learning agent into a testing mode, therefore training mode should be recoverd.
//                    //this.learningAgent.setLearningMode(20,15000,20); //recover the training mode from the test mode
//                    this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); // reset training mode after testing mode, for iris training
//
//                }
//            }
//
//            /**dynamic multi-step**/
//            if(dynamic_multistep_mode)
//            {
//                multistep--;
//                if(multistep==0)
//                {
//                    multistep=this.multistep;
//
//                }
//            }
//        }
//    }

    /**
     *
     * this is the main XCS API
     *
     * ********************************/

    public void MultiStepLearning_Instance()
    {

        String mode= this.mode;
        String rewardMethod=this.rewardMethod;
        int episode_iterations= this.episode_iterations;

        /**
         1.initial
         set learning mode/ training mode/ train mode
         exploreRate, iterationCount_threshold,  GATheshold
         */

        this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); //for iris training,MultiStepLearningBecthmark

        int a_instance_sequence_log=1;


        /**
         * go to task loop.
         * in each loop, multiple iterations will be trained.
         * the number of the iterations in each loop is the number of step
         * **/

        for(int task_seq=1; task_seq<=episode_iterations; task_seq++)
        {
            this.learningAgent.set_task_seq(task_seq); // for scenario policy
            this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); //for iris training,MultiStepLearningBecthmark



            System.out.println("\n");
            System.out.println("************************************************\n");
            System.out.println("***************go to multi-step episode*************************\n");

            //multistep in an episode
            for(int a_step=1; a_step<=multistep; a_step++, this.iteration++)
            {
                /**
                 *  randomly instance selection or sequential selection
                 * */

                //int a_instance;

                int instance_sequence_in_dataSet;// select the instance number in the dataset

                if(mode.equals("seq"))
                {
                    //A. sequence
                    instance_sequence_in_dataSet=a_instance_sequence_log%dataSet_instance_total_num; //sequential
                    if(instance_sequence_in_dataSet==0)
                    {
                        instance_sequence_in_dataSet=dataSet_instance_total_num;
                    }
                }

                if(mode.equals("random"))
                {
                    //B. random

                    if(this.iteration<=dataSet_instance_total_num*3)
                    {
                        //sequentially select instance
                        instance_sequence_in_dataSet=a_instance_sequence_log%dataSet_instance_total_num; //sequential
                        if(instance_sequence_in_dataSet==0)
                        {
                            instance_sequence_in_dataSet=dataSet_instance_total_num;
                        }

                    }
                    else
                    {
                        //select a random instance
                        instance_sequence_in_dataSet= (int)Math.ceil(Math.random()*dataSet_instance_total_num); //random

                    }

                }
                else
                {
                    instance_sequence_in_dataSet=a_instance_sequence_log%dataSet_instance_total_num; //sequential
                    if(instance_sequence_in_dataSet==0)
                    {
                        instance_sequence_in_dataSet=dataSet_instance_total_num;
                    }
                }

                /**reset instance index in the dataset for loops**/
                a_instance_sequence_log++;//keep rolling




                if(one_scenario_test_mode)
                {
                    instance_sequence_in_dataSet=a_step;// (a) only 3 instance!!!!!!!!!!! for  "1 scenario" test
                }

                //a_instance=a_step;// (a) only 3 instance!!!!!!!!!!! for  "1 scenario" test


                /*******************/
                /** load data set***/
                /*******************/

                System.out.println("\n");
                System.out.println("************************************************\n");
                System.out.println("choose a_instance NO.("+instance_sequence_in_dataSet+") in iteration("+this.iteration+") for multiple-step("+multistep+") problem.");

                /** 1.load instance **/
                Instance a_iteration_instance= loadCSVData_I(instance_sequence_in_dataSet);
                //System.out.println("instance grount truth("+a_iteration_instance.ground_truth+")," + " attribute length("+a_iteration_instance.attribute.length+")");

                /** 2.set ground truth, store ground truth of the instance****/
                learningAgent.set_singleStep_groundTruth_for_Instance(a_iteration_instance.ground_truth);
                learningAgent.set_multiStep_groundTruth_for_Instance(a_iteration_instance.ground_truth);

                /** 3.put instance into an iteration, goto multistep iterations**/
                learningAgent.train_4filters_for_instance(a_iteration_instance.attribute, this.iteration);//Martin exploit

            } /**end of multistep episode***/

            /**
             * reward generator
             * generate reward_total
             * **/
            double TaskReward=learningAgent.reward_summary();
            System.out.println(" TaskReward("+TaskReward+")");
            /**assign reward**/

            //150*multistep/ {n*(n+1)/2}= 300/(n+1)
            int scenario_seq;
            if(dynamic_multistep_mode)
            {
                //todo...dataSet_instance_total_num  *multistep/ {multistep*(multistep+1)/2}
                int total_number_of_scenario = (dataSet_instance_total_num*2)/(this.multistep+1);
                //int total_number_of_scenario = 300/(this.multistep+1);
                //int total_number_of_scenario = 300/(this.multistep+1);
                scenario_seq= (task_seq-1)%total_number_of_scenario;
            }
            else
            {
                //150/multistep
                int total_number_of_scenario = dataSet_instance_total_num/(this.multistep);
                //?scenario_seq= (task_seq-1)%total_number_of_scenario;
                scenario_seq= task_seq%total_number_of_scenario;
                if(scenario_seq== 0)
                {
                    scenario_seq =total_number_of_scenario;
                }
            }


            //int scenario_seq= (task_seq-1)%50;//50 is the number of scenarios 50= (150/(1+2+3+4+5))*5 ;//150 instances/ (1+2+3+4) steps =50 scenarios, start from 0
            //int scenario_seq= (task_seq-1)%50;//150 instances/ 3 steps =50 scenarios, start from 0
            if(one_scenario_test_mode)
            {
                //override
                scenario_seq=1;// (b) only 3 instance!!!!!!!!!!! for "1 scenario" test
            }
            //int scenario_seq=1;// (b) only 3 instance!!!!!!!!!!! for "1 scenario" test

            if(rewardMethod.equals("linear_regression"))
            {
                //todo: get T. tempary T
                //int T= 3;
                //2. //learningAgent.span_span_matrix_steps_rewards_vectors(this.multistep);
                //3. //learningAgent.span_orthogonal_reward_space(TaskReward, scenario_seq,this.multistep);
                //4. //Matrix reward_all_steps_matrix = learningAgent.calculate_reward_all_steps_matrix(scenario_seq, this.multistep, T);
                //5. //learningAgent.extract_reward_S_step_from_all( this.multistep,  reward_all_steps_matrix );
                //(b)"test S" ramdon multistep


                //learningAgent.calculateMatrix_example(TaskReward, scenario_seq);
            }
            else
            {
                learningAgent.span_span_matrix_steps_rewards_vectors();

            }

            //  rewardMethod: even fitXprd,fit;
            //(c)"test S" ramdon multistep commond next line
            //learningAgent.reward_assign(TaskReward,rewardMethod);
            int O= scenario_seq;
            //int S= this.multistep;

            int T= this.the_DIVERSITY_of_actions;
            boolean reward_has_assign = learningAgent.reward_assign(TaskReward,"linear_regression",O,T);

            /**update classifier population**/
            if(reward_has_assign)
            {
                learningAgent.reward_apply();
            }
            else
            {
                learningAgent.donot_update_reward();
            }


            /**deletion and save**/
            this.learningAgent.irislearningMultistep_del(900);


            /**dynamic multi-step**/
            if(dynamic_multistep_mode)
            {
                multistep--;
                if(multistep==0)
                {
                    multistep=this.multistep;
                }
            }

            /*********************
             * record check point
             * *****************/
            if(iteration>5000)
            {
                if((iteration -1)%dataSet_instance_total_num==(dataSet_instance_total_num-1))
                {
                    testCheckPoints(iteration);
                }
            }

            if(task_seq%(dataSet_instance_total_num+1)==dataSet_instance_total_num)
            {
                if(iteration>990000)
                {
                    niche_strength_advocate( iteration);
                    testCheckPoints(iteration);
                }
                //niche_strength_advocate( iteration);
                //testCheckPoints(iteration);

                if(iteration>990000)
                {
                    /** check niche through instances*******/
                    //String generated_attribute_output_file="./Results/attributeSet_generated.csv";
                    //generate_attributes("./Results/loc_points.csv",3,generated_attribute_output_file);

                    String projected_niches_file_name = "./Results/loc_points.csv";  //generated_attribute_boundary_file


                    //int required_line_NO= getCSV_file_row_deep(generated_attribute_output_file);
                    int required_line_NO= getCSV_file_row_deep("./Results/loc_points.csv");

                    if(required_line_NO>1)
                    {
                        //System.out.println("read line NO("+required_line_NO+") from document("+generated_attribute_output_file+") to generate X_prd_values(./Results/attributeSet_generated.csv)");
                        /** SET NUMBER OF ATTRIBUTE HERE**/
                        int num_attribute = 3;
                        /** 1. generate instance according to the niche of action two**/
                        //OLD INSTANCE METHOD: ArrayList<Instance> instances= generate_instances("./Results/attributeSet_generated.csv",3,num_attribute,required_line_NO-2*num_attribute);
                        /****************/
                        /** boundary  ***/
                        /****************/
                        ArrayList<Instance> instances_boundary_part = generate_instances_boundary(projected_niches_file_name,3,num_attribute,required_line_NO-2*num_attribute);
                        //save 1
                        String filename_instance_boundary = "generated_instances_boundary";
                        String[]  header_line_boundary= new String[] {"C1L","C1V","C1H","C2L","C2V","C2H","C3L","C3V","C3H"};
                        save_generated_instance_DoubleVersion(filename_instance_boundary, instances_boundary_part, iteration, header_line_boundary);
                        //save_generated_instance(filename_instance_boundary, instances_boundary_part, iteration);

                        /****************/
                        /** instance  ***/
                        /****************/
                        //ArrayList<Instance> instances_value_part = generate_instances_value_from_boundary(instances_boundary_part);//  new instance_value generate method
                        //instances_boundary_part = null;

                        //save 2
                        //String filename_instance_value = "generated_instances_points";
                        //save_generated_instance(filename_instance_value, instances_value_part, iteration);
                        //save_generated_instance_DoubleVersion(filename_instance_value, instances_value_part, iteration);

                        /*****************************************/
                        /** classifiers niche with statistics  ***/
                        /*****************************************/
                        /** 2. set up header**/
                        String projected_by_which_selected_action = get_selected_action_from_csv_file( "./Results/attributeSet_generated.csv", required_line_NO-2*num_attribute, 0);// read header
                        projected_by_which_selected_action = projected_by_which_selected_action + "/"+ get_selected_action_from_csv_file( "./Results/attributeSet_generated.csv", required_line_NO-2*num_attribute, 1);// read header-iteration
                        /** 3. check niche**/
                        //Check_Niche(instances, iteration,required_line_NO-2*num_attribute, projected_by_which_selected_action);
                        //ArrayList<Instance> instances_prd=  check_niche_instance_and_classifier(instances, iteration,required_line_NO-2*num_attribute, projected_by_which_selected_action);
                        ArrayList<Instance> instances_prd=  check_niche_instance_and_classifier(instances_boundary_part, iteration,required_line_NO-2*num_attribute, projected_by_which_selected_action); // apply new instance_value generate method
                        //instances_value_part = null;
                        instances_boundary_part = null;

                        /** 4. sava instance**/
                        String filename_instance_points = "generated_instances_stat";
                        String[]  header_line_prd= new String[] {"C1L","C1V","C1H","C2L","C2V","C2H","C3L","C3V","C3H","PRD1","PRD2","ACC1","ACC2","EXP1","EXP2"};
                        save_generated_instance_DoubleVersion(filename_instance_points, instances_prd, iteration, header_line_prd);
                        instances_prd = null;

                        /** 5. hierarchy 2 **/

                        /** generate instance according to the niche of action one**/
                        /*
                        instances= generate_instances("./Results/attributeSet_generated.csv",3,num_attribute,required_line_NO-1*num_attribute);
                        projected_by_which_selected_action= get_selected_action_from_csv_file( "./Results/attributeSet_generated.csv", required_line_NO-1*num_attribute, 0);// read header
                        //Check_Niche(instances, iteration,required_line_NO-1*num_attribute, projected_by_which_selected_action);
                        check_niche_instance_and_classifier(instances, iteration,required_line_NO-1*num_attribute, projected_by_which_selected_action);

                        */
                    }
                }


                this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); // reset training mode after testing mode, for iris training
            }
            //save classifiers every 450 episode
            //int save_csv_each_episode_num= 1000;
            if((task_seq-1)%this.save_csv_each_episode_num== (this.save_csv_each_episode_num-1))
            {
                this.learningAgent.save_csv_file((int)(task_seq/this.save_csv_each_episode_num) );



                /*********************
                 * record check point
                 * *****************/
                /*
                if(one_scenario_test_mode)
                {
                    if(task_seq>=this.multistep&& task_seq%this.multistep==0)
                    {
                        testCheckPoints(iteration);
                        this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); // reset training mode after testing mode, for iris training
                    }
                }
                else
                {
                    if(task_seq%(dataSet_instance_total_num+1)==dataSet_instance_total_num)
                    {
                        testCheckPoints(iteration);
                        this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); // reset training mode after testing mode, for iris training
                    }
                }
                */
            }

        }

    }

    /*******
     * input of perception
     * this is for .jar api
     * this is developed from MultiStepLearning_Instance and develop following API:
     *
     * 1. initiate_XCS_training_mode_API
     * 2. get prediction
     * 3. assignment reward
     *
     * ********/
    public void initiate_XCS_training_mode_API()
    {
        String mode= this.mode;
        String rewardMethod=this.rewardMethod;
        int episode_iterations= this.episode_iterations;

        /**
         1.initial
         set learning mode/ training mode/ train mode
         exploreRate, iterationCount_threshold,  GATheshold
         */

        this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); //initiate_XCS_API
        System.out.println("API: set training mode");
    }

    public void XCS_set_testing_mode_API()
    {
        System.out.println("API: set testing mode");
        this.learningAgent.setTestingMode(this.test_exploreRate, this.test_iterationCount_threshold, this.test_GA_threshold); // set test mode
    }
    public void XCS_set_mcts_mode_API()
    {
        System.out.println("API: set MCTS mode");
        this.learningAgent.setTestingMode(this.test_exploreRate, this.test_iterationCount_threshold, this.test_GA_threshold); // set MCTS mode
    }

    public void XCS_set_learning_mode_API()
    {
        System.out.println("API: set learning/training mode");
        this.learningAgent.setLearningMode(this.exploreRate, this.iterationCount_threshold, this.GATheshold); // set learning/training mode
    }


    public void XCS_initiate_fiters_API()
    {
        learningAgent.initial_filters_portfolio();
    }
    public void XCS_initiate_test_fiters_API()
    {
        learningAgent.initial_test_filters_portfolio();
    }
    public void XCS_initiate_mcts_fiters_API()
    {
        learningAgent.initial_mcts_filters_portfolio();
    }


    public int XCS_get_prediction_API(double[] perception)
    {
        this.iteration ++;

        double[][] perception_boundary = XCS_dynamic_upper_and_lower_boundary(perception); // set this.perception_boundary)

        learningAgent.train_4filters(perception, perception_boundary, this.iteration);//maze problem
        //return action_index
        int  action_index =  learningAgent.get_action();
        //todo: no deletion !!!!!!!!!!!!!!!!!!!!!!!
        //this.learningAgent.irislearningMultistep_del();
        return action_index;

    }

    public void XCS_deletion_API(int pop_size)
    {
        this.learningAgent.irislearningMultistep_del(pop_size);
    }

    public void XCS_subsumption_API()
    {
        this.learningAgent.subsumption();
    }

    public int XCS_get_prediction_test_mode_API(double[] perception)
    {
        //this.iteration ++;

        double[][] perception_boundary = XCS_dynamic_upper_and_lower_boundary(perception); // set this.perception_boundary)

        return learningAgent.test_mode_filters(perception, perception_boundary, this.iteration);//maze problem
        //return action_index
        //return learningAgent.get_action();
    }
    public int XCS_get_prediction_mcts_API(double[] perception)
    {
        //this.iteration ++;

        double[][] perception_boundary = XCS_dynamic_upper_and_lower_boundary(perception); // set this.perception_boundary)

        return learningAgent.mcts_mode_filters(perception, perception_boundary, this.iteration);//maze problem, mcts
        //return action_index
        // return learningAgent.get_action();
    }

    public void XCS_mcts_establish_MacthSet_ActionSet(double[] perception, int action)
    {// establish match set according the the current perception

        double[][] perception_boundary = XCS_dynamic_upper_and_lower_boundary(perception); // set this.perception_boundary)
        learningAgent.mcts_establish_MacthSet_ActionSet_filters(perception, perception_boundary,action);

    }
    public ArrayList<Double> XCS_get_actions_reward_hashmap(String training_mode)
    {
        /**
         * select filter => reward hashmap
         * learning agent-> iris_xcs (LayerProblem) -> messages -> reward hashmap
         * ***/
        HashMap<LayerAction, Double>  actions_reward_hashmap = learningAgent.get_actions_reward_hashmap(training_mode);

        //for (LayerAction key: actions_reward_hashmap.keySet()) {
        //    System.out.println(" 	show XCS_get_actions_reward_hashmap : action (" + key + " / " + key.getMask() + ").vote(" + actions_reward_hashmap.get(key) + ").");
        //}

        ArrayList<Double> actions_anticipated_reward_stack = new ArrayList<Double>();
        //get attributeSet of each attribute: save in attributeSet_of_all_instance structure
        for(int i=0; i< 1; i++)
        {
            double reward = 0;
            actions_anticipated_reward_stack.add(reward); // appends to the end of list
        }

        if(actions_reward_hashmap.isEmpty())
        {
            System.out.println(" actions_reward_hashmap isEmpty");
        }
        else{

            for(int i=1; i< actions_reward_hashmap.size(); i++)
            {
                double reward = 0;
                actions_anticipated_reward_stack.add(reward); // appends to the end of list
            }

            if(actions_reward_hashmap.size()>=1)
            {
                for (LayerAction key: actions_reward_hashmap.keySet()) {
                    //System.out.println(" 	show XCS_get_actions_reward_hashmap : action (" + key + " / " + key.getMask() + ").vote(" + actions_reward_hashmap.get(key) + ").");
                    actions_anticipated_reward_stack.set(key.getMask()-1, actions_reward_hashmap.get(key));
                }

            }
        }
        return actions_anticipated_reward_stack;
        //return actions_reward_hashmap;
    }

    public void XCS_update_reward_API(double TaskReward, String reward_method,boolean dynamic_learning, double learning_rate)
    {
        /**
         * reward generator
         * generate reward_total
         * **/
        //double TaskReward=learningAgent.reward_summary();
        System.out.println(" TaskReward("+TaskReward+")");
        /**assign reward**/

        //150*multistep/ {n*(n+1)/2}= 300/(n+1)
        int scenario_seq;

        //150/multistep
        int total_number_of_scenario = dataSet_instance_total_num/(this.multistep);
        //?scenario_seq= (task_seq-1)%total_number_of_scenario;
        scenario_seq= 1;
        if(scenario_seq== 0)
        {
            scenario_seq =total_number_of_scenario;
        }


        //learningAgent.span_span_matrix_steps_rewards_vectors();
        

        //  rewardMethod: even fitXprd,fit;
        //(c)"test S" ramdon multistep commond next line
        //learningAgent.reward_assign(TaskReward,rewardMethod);
        int O= scenario_seq;
        //int S= this.multistep;

        int T= this.the_DIVERSITY_of_actions;
        boolean reward_has_assign = learningAgent.reward_assign(TaskReward,reward_method,O,T); //discount_big_start, discount_big_end, even

        /**update classifier population**/
        if(reward_has_assign)
        {
            if(dynamic_learning)
            {
                learningAgent.reward_dynamic_apply(learning_rate);

            }else
            {
                learningAgent.reward_apply();

            }

        }
        else
        {
            learningAgent.donot_update_reward();
        }

    }

    public void XCS_update_mcts_reward_API(double TaskReward, String reward_method,boolean dynamic_learning, double learning_rate)
    {
        System.out.println(" mcts: TaskReward("+TaskReward+")");
        /**assign reward**/

        //150*multistep/ {n*(n+1)/2}= 300/(n+1)
        int scenario_seq;

        //150/multistep
        int total_number_of_scenario = dataSet_instance_total_num/(this.multistep);
        //?scenario_seq= (task_seq-1)%total_number_of_scenario;
        scenario_seq= 1;
        if(scenario_seq== 0)
        {
            scenario_seq =total_number_of_scenario;
        }

        //learningAgent.span_span_matrix_steps_rewards_vectors();


        //  rewardMethod: even fitXprd,fit;
        //(c)"test S" ramdon multistep commond next line
        //learningAgent.reward_assign(TaskReward,rewardMethod);
        int O= scenario_seq;
        //int S= this.multistep;

        int T= this.the_DIVERSITY_of_actions;
        if(reward_method.equals("mcts_clear"))
        {
            learningAgent.mcts_reward_clear();
        }
        else
        {
            boolean reward_has_assign = learningAgent.reward_assign(TaskReward,reward_method,O,T); //XCS_update_mcts_reward_API: discount_big_start, discount_big_end, even

            /**update classifier population**/
            if(reward_has_assign)
            {
                if(dynamic_learning)
                {
                    learningAgent.mcts_reward_apply_dynamic_learning_rate(learning_rate);
                }
                else
                {
                    learningAgent.mcts_reward_apply();
                }

            }
        }
        /*
        boolean reward_has_assign = learningAgent.reward_assign(TaskReward,reward_method,O,T); //XCS_update_mcts_reward_API: discount_big_start, discount_big_end, even

        //update classifier population
        if(reward_has_assign)
        {
            learningAgent.mcts_reward_apply();
        }
        else
        {
            learningAgent.donot_update_reward();
        }
        */
    }


    public double[][]  XCS_dynamic_upper_and_lower_boundary(double[] perception)
    {
        /**
         *  check upper-boundary and lower-boundary
         * **/
        int perception_length = perception.length;

        //initialization
        if(perception_boundary == null || perception_boundary.length == 0 || (perception_boundary.length == 1 && perception_boundary[0].length == 0))
        {
            perception_boundary = new double[perception_length][2];

            for(int i=0; i<perception_length;i++)
            {
                // lower boundary, minimum
                perception_boundary[i][0] =  perception[i];
                // high boundary, maximum
                perception_boundary[i][1] =  perception[i];
            }
        }

        //update upper and lower boundary
        for(int i=0; i<perception_length;i++)
        {
            // lower boundary, minimum
            if(perception[i] < perception_boundary[i][0])
            {
                perception_boundary[i][0] =  perception[i];
            }
            // lower boundary, minimum
            if(perception[i] > perception_boundary[i][1])
            {
                perception_boundary[i][1] =  perception[i];
            }
        }
        //show boundary
        String s_printout ="";
        for(int i=0; i<perception_length;i++)
        {
            s_printout = s_printout + "  " + perception_boundary[i][0] + " - "+ perception_boundary[i][1] + "; ";
        }
        System.out.println(" perception boundary :"+ s_printout);

        return this.perception_boundary;
    }

    public void XCS_save_classifiers_API(int task_seq) {
        if ((task_seq - 1) % this.save_csv_each_episode_num == (this.save_csv_each_episode_num - 1)) {
            this.learningAgent.save_csv_file((int) (task_seq / this.save_csv_each_episode_num)); //XCS_save_classifiers_API
        }
    }
    public void XCS_save_classifiers_API(int task_seq, String folder_name) {
        if ((task_seq - 1) % this.save_csv_each_episode_num == (this.save_csv_each_episode_num - 1)) {
            this.learningAgent.save_csv_file((int) (task_seq / this.save_csv_each_episode_num), folder_name); //XCS_save_classifiers_API
        }
    }
    public void XCS_save_classifiers_manually_API(String folder_name) {
        this.learningAgent.save_csv_file(0 , folder_name); //XCS_save_classifiers_API

    }
    public void XCS_loading_classifiers_population_from_csv_API(String classifier_file_name, String path)
    {
        //String classifier_file_name = "instance0";
        //LearningAgent hierarchy_Agent= new LearningAgent(classifier_file_name);

        /**  go through filter to load classifier populations **/

        /**  read classifiers string array from a csv file **/
        List<String[]> classifiers_strings;
        classifiers_strings = loadCSV_classifiers_strings(classifier_file_name, path);
        boolean hasCSVData= false;
        if(classifiers_strings.size()>1)
        {
            hasCSVData = true;
        }

        /** translate csv_data into classifiers through iterations  **/
        Iterator<String[]> iter = classifiers_strings.iterator();

        // read header
        String[] csv_header_str_arr = iter.next();
        String[] classifier_csv_str_arr;

        while(iter.hasNext())
        {
            classifier_csv_str_arr =  iter.next();

            /** prepare the original data of each part of a classifier**/
            int id = Integer.parseInt(classifier_csv_str_arr[0]);
            System.out.println("		createClassifierFromCSV: id: "+ id);
            // condition part
            int length_of_attributes = classifier_csv_str_arr.length -9;
            double[] condition_attributes_arr = new double[length_of_attributes];
            for(int i =0; i< length_of_attributes;i++)
            {
                condition_attributes_arr[i] = Double.parseDouble(classifier_csv_str_arr[1+i]);
                System.out.println("		createClassifierFromCSV:	condition_attributes_arr("+i+"): "+ condition_attributes_arr[i]);
            }

            String  action = classifier_csv_str_arr[length_of_attributes+1];
            System.out.println("		createClassifierFromCSV:	action: "+ action);

            double[] statistic = new double[7];
            for(int i=0; i<7; i++)
            {
                statistic[i] =  Double.parseDouble(classifier_csv_str_arr[2+length_of_attributes+i]);
                System.out.println("		createClassifierFromCSV:	statistic("+i+"): "+ statistic[i]);
            }

            // apply filters to establish classifiers population
            /** using a cover filter to establish classifier population**/

            Instance an_instance= new Instance();
            an_instance.set_all_attribute(condition_attributes_arr);

            /** cover this instance by filter and generate a classifier**/
            learningAgent.create_classifier_through_learningAgent(id, condition_attributes_arr, action, statistic);
            //hierarchy_Agent.create_classifier_through_learningAgent(id, condition_attributes_arr, action, statistic);
        }
        // merge hierarchy agent with default agent
        //this.learningAgent.set_xcs_population(hierarchy_Agent.get_xcs().getPopulation());
        learningAgent.save_csv_file(300354340);



    }

    public String get_selected_action_from_csv_file(String input_file_name, int required_line_NO, int which_column)
    {
        String s_required="";

        System.out.println( "read_length_of_line_in_CSV_dataSet :: filename("+input_file_name+"), required_line_NO("+required_line_NO+") ");


        try{
            Scanner scanner = new Scanner(new File(input_file_name));
            scanner.useDelimiter(",");
            int index_scanner=0;
            while(scanner.hasNextLine())
            {
                if(index_scanner!= required_line_NO)
                {
                    scanner.nextLine();
                    index_scanner++;
                }else
                {
                    if(index_scanner== required_line_NO)
                    {
                        //System.out.println("loadCSVData_I:: index_scan ="+ index_scanner);
                        //System.out.println( "\n");
                        String line = scanner.nextLine();
                        String[] fields = line.split(",");

                        s_required= fields[which_column].replaceAll("\"","");
                        break;
                    }
                }

            }// end of while

            scanner.close();
            return s_required;
        }catch (FileNotFoundException e){return s_required;}

    }

    public int getCSV_file_row_deep(String filepath)
    {
        List<String[]> data;
        int deep_count=0;

        try
        {
            FileReader f= new FileReader(filepath);
            CSVReader csv_reader= new CSVReader(f);
            data= csv_reader.readAll();

            deep_count= data.size();

        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return deep_count;
    }

    public void read_dataset_demo()
    {
        //go into task loop iteration
        //csvData length is 150, therefore, every instance is go for 10 times of training
        for(int i=1; i<dataSet_instance_total_num+1;i++)//150 instance *5 actions* 2000 times //8000 popsize *100
        {




            //int random_task_seq= (int)(Math.random() *150);
            int random_task_seq=(i%dataSet_instance_total_num);
            if(random_task_seq==0)
            {
                random_task_seq=dataSet_instance_total_num; // to remove the headline
            }

            System.out.println("\n");
            System.out.println("************************************************\n");
            System.out.println("random_task_seq ("+random_task_seq+") , iteration seq("+i+")");
            //1.load instance
            Instance a_iteration_instance= loadCSVData_I(random_task_seq);
            System.out.println("instance grount truth("+a_iteration_instance.ground_truth+")," + " attribute length("+a_iteration_instance.attribute.length+")");

            //2. store ground truth of the instance
            //learningAgent.set_singleStep_groundTruth_for_Instance(a_iteration_instance.ground_truth);
            //learningAgent.set_multiStep_groundTruth_for_Instance(a_iteration_instance.ground_truth);

            //3.put instance into an iteration
            //learningAgent.train_4filters_for_instance(a_iteration_instance.attribute, this.iteration);//iris_Learning_Mode_2_for_instance


        }


    }


    public SingleTask loadCSVData(int required_line_NO) {

        //input: which line of data the for training.
        // output:return the data.

        SingleTask oneSingleTask= new SingleTask();

        try{
            Scanner scanner = new Scanner(new File(DataSet_CSVfile));
            scanner.useDelimiter(",");

            //int required_line_NO= 1; //specify which line the off-line environment required
            int index_scanner=0;
            double field_value=0;
            while(scanner.hasNextLine())
            {
                if(index_scanner!= required_line_NO)
                {
                    scanner.nextLine();
                    index_scanner++;
                }
                if(index_scanner== required_line_NO)
                {
                    //System.out.println("index_scan ="+ index_scanner);
                    //System.out.println( "\n");
                    String line = scanner.nextLine();
                    String[] fields = line.split(",");
                    //System.out.println(fields + "<- String[] fields");    //fields[] address
                    //System.out.println( "\n");
                    //System.out.println( "fields.length ("+fields.length+") \n");

                    int index_attribute=1;
                    //operation on each item
                    for(String field : fields)
                    {

                        field_value= Double.parseDouble(field);
                        switch(index_attribute)
                        {
                            case 1:
                                oneSingleTask.sepal_length=field_value;
                                break;
                            case 2:
                                oneSingleTask.sepal_width= field_value;
                                break;
                            case 3:
                                oneSingleTask.petal_length= field_value;
                                break;
                            case 4:
                                oneSingleTask.petal_width= field_value;
                                break;
                            case 5:
                                oneSingleTask.species= field_value;
                                break;
                            default:
                                break;
                        }
                        index_attribute++;
                        //save dataframe into condition and result.

                        //System.out.println(field_value + " ");
                        //put field into a hashmap
                    }


                    index_scanner++;
                }

            }// end of while

            scanner.close();
            return oneSingleTask;

        }catch (FileNotFoundException e){return null;}

    }

    public Instance loadCSVData_I(int required_line_NO) {

        //set up instance
        // attributes+ groundtruth

        Instance an_instance= new Instance();

        try{
            Scanner scanner = new Scanner(new File(DataSet_CSVfile));
            scanner.useDelimiter(",");

            //int required_line_NO= 1; //specify which line the off-line environment required
            int index_scanner=0;
            double field_value=0;
            while(scanner.hasNextLine())
            {
                if(index_scanner!= required_line_NO)
                {
                    scanner.nextLine();
                    index_scanner++;
                }else
                {
                    if(index_scanner== required_line_NO)
                    {
                        System.out.println("loadCSVData_I:: index_scan ="+ index_scanner);
                        //System.out.println( "\n");
                        String line = scanner.nextLine();
                        String[] fields = line.split(",");
                        //System.out.println(fields + "<- String[] fields");    //fields[] address
                        //System.out.println( "\n");
                        System.out.println( "loadCSVData_I:: fields.length ("+fields.length+") \n");
                        double[] field_d = new double[fields.length];
                        double[] attribute= new double[fields.length-1];
                        double groundTruth=0;
                        int count=0;

                        //set attribute
                        //set groundtruth

                        for(String field : fields)
                        {
                            if(count == (fields.length-1))
                            {
                                //the last one is the ground truth
                                groundTruth= Double.parseDouble(field);
                            }
                            else
                            {
                                attribute[count]=Double.parseDouble(field);
                            }

                            count++;
                        }

                        an_instance.set_all_attribute(attribute);
                        an_instance.set_ground_truth(groundTruth);

                        //check to iterate the field_d
                        an_instance.show_all_attribute();
                        an_instance.show_ground_truth();

                        index_scanner++;

                    /*
                    int index_attribute=0;
                    int index_of_fields= fields.length -1;
                    //operation on each item
                    for(String field : fields)
                    {
                        double an_field_value = Double.parseDouble(field);
                        System.out.println(" read instance, a field("+an_field_value+ ") ");
                        an_instance.attribute[0]=an_field_value;
                        an_instance.ground_truth=an_field_value;
                        System.out.println(" read instance, a an_instance.ground_truth("+an_instance.ground_truth+ ") ");


                        index_attribute++;

                        //System.out.println(" read instance, a field("+an_instance.attribute[index_of_fields] + " ) and index ("+index_of_fields+")");
                    }


                    index_scanner++;
                    */
                    }
                }


            }// end of while

            scanner.close();
            return an_instance;

        }catch (FileNotFoundException e){return null;}
    }

    public Instance transfer_perception_API(double[] attributes)
    {
        //set up instance
        // attributes+ groundtruth

        Instance an_instance= new Instance();
        an_instance.set_all_attribute(attributes);
        //an_instance.set_ground_truth(groundTruth);

        //check to iterate the field_d
        an_instance.show_all_attribute();
        //an_instance.show_ground_truth();

        return an_instance;
    }
    public List<String[]> loadCSV_classifiers_strings(String file, String path)
    {
        List<String[]> data;
        try
        {
            FileReader f= new FileReader(path+file+".csv");  //path ="./src/robot_nav_plan/result/"
            CSVReader csv_reader= new CSVReader(f);
            data= csv_reader.readAll();
            return data;
        }
        catch(Exception e)
        {
            System.out.println("No previous population!: " + e.getMessage());
            return null;
        }
    }

    public ArrayList<Instance> loadCSVData_I_read_niche(int required_line_NO, String file_name, int line_header_length) {
        //load niche data. each line contains an attribute of niche.
        //@ input: line index
        //@ output: a array contains instances
        // 1. read a required line from csv. the line is an attribute of niche.
        // 2. generate instances according to niche separation.

        Instance an_instance= new Instance();
        ArrayList<Instance> instances= new ArrayList<Instance>();

        try{
            Scanner scanner = new Scanner(new File(file_name));
            scanner.useDelimiter(",");

            //int required_line_NO= 1; //specify which line the off-line environment required
            int index_scanner=0;
            while(scanner.hasNextLine())
            {
                if(index_scanner!= required_line_NO)
                {
                    scanner.nextLine();
                    index_scanner++;
                }else
                {
                    if(index_scanner== required_line_NO)
                    {
                        System.out.println("loadCSVData_I_read_niche:: index_scan ="+ index_scanner);
                        //System.out.println( "\n");
                        String line = scanner.nextLine();
                        String[] fields = line.split(",");
                        //System.out.println(fields + "<- String[] fields");    //fields[] address
                        //System.out.println( "\n");
                        System.out.println( "loadCSVData_I_read_niche:: fields.length ("+fields.length+") \n");
                        double[] niche_points= new double[fields.length-line_header_length];
                        int count=0;

                        //set niche_points
                        for(String field : fields)
                        {
                            // niche attribute start from line_header_length-1
                            if(count< line_header_length)
                            {
                                // field belong to header. do nothing.
                            }
                            else
                            {
                                //record niche separated points
                                niche_points[count-line_header_length] = Double.parseDouble(field);
                            }
                            count++;
                        }

                        // 2. generate instances according to niche separation.
                        //instances = build_instances(niche_points,1);

                        break;// niche points ara stored in niche_points
                    }
                }


            }// end of while.
            scanner.close();



            return instances;

        }catch (FileNotFoundException e){return null;}
    }

    public void generate_attributes(String input_file_name, int line_header_length, String output_file_name)
    {
        //"./Results/loc_points.csv",3,"./Results/attributeSet_generated.csv"
        // generate attributes according to separeted points of a niche attribute
        ArrayList<String[]> attributeSet_generated= new ArrayList<String[]>();

        // iterate each row of the csv data set
        try{
            Scanner scanner = new Scanner(new File(input_file_name));
            scanner.useDelimiter(",");

            //int required_line_NO= 1; //specify which line the off-line environment required
            int index_scanner=0;
            while(scanner.hasNextLine())
            {
                String line = scanner.nextLine();
                String[] fields = line.split(",");
                //System.out.println(fields + "<- String[] fields");    //fields[] address
                //System.out.println( "\n");
                System.out.println( "attributeSet_generated:: fields.length ("+fields.length+") ");
                String[] header_line = new String[line_header_length];
                //float[] niche_points= new float[fields.length-line_header_length];
                double[] niche_points= new double[fields.length-line_header_length];
                int count=0;

                //set niche_points
                for(String field : fields)
                {
                    //System.out.println("a field: "+field);
                    // niche attribute start from line_header_length-1
                    if(count< line_header_length)
                    {
                        // field belong to header. copy
                        header_line[count] = field;
                    }
                    else
                    {
                        //generate an value of an attribute according to two separated points
                        //System.out.println("a field"+field);
                        if(field.contains("-"))
                        {
                            niche_points[count-line_header_length] = 0;
                        }
                        else
                        {
                            //System.out.println("a field"+field.replaceAll("\"",""));
                            //niche_points[count-line_header_length] = Float.parseFloat(field);
                            niche_points[count-line_header_length] = Double.parseDouble(field.replaceAll("\"","")); // remove ""
                        }

                    }
                    count++;
                }
                String[] niche_points_generated = new String[fields.length-line_header_length-1];
                for(int which_point=0; which_point< fields.length-line_header_length-1; which_point++)
                {
                    //emmm...select a middle point between two points as an new generated point.
                    //it does not matter whether it is a middle point or any point.

                    niche_points_generated[which_point] = Double.toString(0.5*(niche_points[which_point] + niche_points[which_point+1]));
                }
                // link header and niche_points_generated
                int str1_length= header_line.length;
                int str2_length= niche_points_generated.length;
                int total_length= str1_length+str2_length;

                //5.1 increase the memory
                header_line= Arrays.copyOf(header_line,total_length);
                //5.2 merge
                System.arraycopy(niche_points_generated,0,header_line,str1_length,str2_length);
                //System.out.println(" a generated niche is :" +header_line);
                attributeSet_generated.add(header_line); // add a line of generated attribute ( similar to a line of loc_points.csv)

                // setup for next iteration
                index_scanner++;


            }// end of while.
            scanner.close();


        }catch (FileNotFoundException e){}


        /****/



        // write csv file, overwrite output file
        try
        {
            System.out.println("CSV version: "+output_file_name);

            //CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/CheckP.csv"));
            CSVWriter csv_writer= new CSVWriter(new FileWriter(output_file_name));
            csv_writer.writeAll(attributeSet_generated);
            csv_writer.close();

        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

    }

    public ArrayList<Instance> generate_instances(String file_name, int line_header_length, int num_attribute, int required_line_NO)
    {
        System.out.println("generate_instances begin");

        //get instaces number
        int[] num_of_instances =new int[num_attribute];
        for(int which_attribute_index =0; which_attribute_index<num_attribute; which_attribute_index++)
        {
            num_of_instances[which_attribute_index] = read_length_of_line_in_CSV_dataSet(file_name, required_line_NO + which_attribute_index  )- line_header_length;
            System.out.println("generate_instances: each attribute has ("+ num_of_instances[which_attribute_index]+") part of attribute niche." +
                    "in dataset line index= ("+(required_line_NO + which_attribute_index)+") ");

        }

        int has_how_many_rows = read_how_many_rows_in_CSV_dataSet(file_name);
        System.out.println("generate_instances:  has_how_many_rows("+has_how_many_rows+").");
        //initate attribute set for instances
        ArrayList<double[]> attributeSet_of_all_instance = new ArrayList<double[]>();
        //get attributeSet of each attribute: save in attributeSet_of_all_instance structure
        for(int attri_index=0; attri_index< num_attribute; attri_index++)
        {
            // get each attributeSet from csv file
            double[] a_attribute_of_all_instances = new double[num_of_instances[attri_index]];
            a_attribute_of_all_instances = get_a_attribute_of_all_instances(file_name,required_line_NO+ attri_index, line_header_length, num_of_instances[attri_index]);
            attributeSet_of_all_instance.add(a_attribute_of_all_instances); // appends to the end of list

        }

        /** try a flexible method to generate instance of any number of attribute
         * attributeSet_of_all_instance:
         * [a1] [] [] []
         * [b1] [] []
         * [c1] [] [] []
         * [d1] [] [] []
         * [e1] []
         *
         *  attribute_index_iter
         *  [i< length_of_an_attribute of line a] [line b]  [line c] [ line d] [line e]
         *
         * attribute_of_new_instance
         *  [value of a1] [b1]  [c1] [d1] [e1]
         *
         * **/
        int[] attribute_index_iter = new int[num_attribute]; //index of an axis, indicate a value of an attributeSet. attribute_index_iter is a pointer
        int[] attribute_index_upbound = new int[num_attribute]; //length of each attribute
        int[] attribute_index_iter_carry = new int[num_attribute+1]; // carry bit, reset the index of other axis

        double[] attribute_of_new_instance = new double[num_attribute];
        //initiate index
        attribute_index_iter_carry[0] = 0;
        for(int i=0; i< num_attribute;i++)
        {
            attribute_index_iter[i]=0;
            attribute_index_iter_carry[i+1]=0;
            attribute_index_upbound[i]= attributeSet_of_all_instance.get(i).length;
        }

        // initiate instanceSet
        ArrayList<Instance> instanceSet= new ArrayList<Instance>();

        while(attribute_index_iter_carry[num_attribute]!=1)
        {//when the final carry flag !=1 continue //

            double[] attributeSet_of_a_instance = new double[num_attribute];// num_attribute= dimension of  an instance

            // attribute iteration// column = which_attribute
            for(int dimension_index=0; dimension_index< num_attribute; dimension_index++)
            {    // set each dimension of an instance one dimension by one dimension
                int index_in_a_dim = attribute_index_iter[dimension_index]; // index_in_a_dim points to the sequence number in an axis
                attributeSet_of_a_instance[dimension_index]=  attributeSet_of_all_instance.get(dimension_index)[index_in_a_dim];
            }



            Instance an_instance= new Instance();
            an_instance.set_all_attribute(attributeSet_of_a_instance);
            instanceSet.add(an_instance);

            // parepare for next loop
            // essentially, it is a meshgrid method. but take care of the carry bit.
            // the value of pointer +1
            attribute_index_iter[0] = attribute_index_iter[0]+1;    // increment of each step for the meshgrid method
            attribute_index_iter_carry[0]=0;    //set the carry bit of the first dimension alway equals to 0
            //iterate each attribute index

            for(int i=0; i<num_attribute;i++)
            {
                //update (3=num_attribute) pointer value from lower dimension to a higher dimension
                //meshgrid 1: add carry bit
                attribute_index_iter[i] = attribute_index_iter[i] + attribute_index_iter_carry[i];
                //check if reach the upbound.
                if(attribute_index_iter[i]==attribute_index_upbound[i])
                {
                    // if reach the upbound, set carry bit and set the current pointer to original place(0)
                    attribute_index_iter_carry[i+1] = 1;
                    attribute_index_iter[i] = 0;
                }
                else
                {   //reset carry bit
                    attribute_index_iter_carry[i+1] = 0;
                }

            }
            //chech iteration
            System.out.println("check carry iteration:"+attribute_index_iter[0] +" /"+attribute_index_iter[1]  );


        }
        return instanceSet;
        /*
        //check attributeSet_of_all_instance
        System.out.println("size of attribute is" +attributeSet_of_all_instance.size()+" vs. num of attr ("+num_attribute+")" );
        for(int attri_index=0; attri_index< num_attribute; attri_index++)
        {
            System.out.println(" attributeSet (index="+attri_index+"):"  );

            double[] a_attribute_of_all_instances = attributeSet_of_all_instance.get(attri_index);
            String s = " ";
            for(int niche_index=0; niche_index< a_attribute_of_all_instances.length;niche_index++)
            {
                s += a_attribute_of_all_instances[niche_index]+"#";
            }
            System.out.println(s);
            //attribute 1 attributeSet_of_all_instance.get(attri_index)




        }

        // initiate instanceSet
        ArrayList<Instance> instanceSet= new ArrayList<Instance>();

        //construct instances attribute by attribute


        for(int which_instance=0; which_instance< num_of_instances[0]; which_instance++)
        {
            // construct each instance from attributes. instance iteration// row = instance

            double[] attributeSet_of_one_instance = new double[num_attribute];

            // attribute iteration// column = which_attribute
            for(int which_attribute=0; which_attribute< num_attribute; which_attribute++)
            {
                attributeSet_of_one_instance[which_attribute]=  attributeSet_of_all_instance.get(which_attribute)[which_instance];
            }
            Instance an_instance= new Instance();
            an_instance.set_all_attribute(attributeSet_of_one_instance);
            instanceSet.add(an_instance);
        }


        return instanceSet;

        */

    }

    public  ArrayList<Instance> generate_instances_value_from_boundary( ArrayList<Instance> instances_boundary_part)
    {


        /*
        ArrayList<Instance> instances_value= new ArrayList<Instance>();

        Iterator<Instance> it =instances_boundary_part.iterator();
        Instance a_instance_value = new Instance();




        // iter first one
        Instance an_boundarys = it.next();
        double[] attributes = an_boundarys.attribute;
        int att_length = attributes.length /2;
        double[] new_attributes = new double[att_length];
        for(int i=0; i<att_length;i++)
        {
            an_boundarys = it.next();
            attributes = an_boundarys.attribute;
            new_attributes[i] = 0.5*(attributes[2*i]+ attributes[2*i+1]);
        }


        a_instance_value.set_all_attribute(new_attributes);
        instances_value.add(a_instance_value);

        while(it.hasNext())
        {
            for(int i=0; i<att_length;i++)
            {
                an_boundarys = it.next();
                attributes = an_boundarys.attribute;
                new_attributes = new double[att_length];

                new_attributes[i] = 0.5*(attributes[2*i]+ attributes[2*i+1]);
                //System.out.println("generate_instances_value_from_boundary : "+new_attributes );
            }


            a_instance_value.set_all_attribute(new_attributes);
            instances_value.add(a_instance_value);
        }
        */

        ArrayList<Instance> instances_value= new ArrayList<Instance>();

        Iterator<Instance> it =instances_boundary_part.iterator();
        //Instance a_instance_value = new Instance();
        double[] attributes;
        int att_length;
        while(it.hasNext())
        {
            Instance a_instance_value = new Instance();
            Instance an_boundarys = it.next();
            attributes = an_boundarys.attribute;
            att_length = attributes.length /2;
            double[] new_attributes = new double[att_length];

            for(int i=0; i<att_length;i++)
            {
                new_attributes[i] = 0.5*(attributes[2*i]+ attributes[2*i+1]);
                //System.out.println("generate_instances_value_from_boundary : "+new_attributes );
            }

            a_instance_value.set_all_attribute(new_attributes);
            instances_value.add(a_instance_value);
        }
        /*

        for (Iterator<Instance> iter = instances_boundary_part.iterator(); iter.hasNext(); ) {

            double[] attributes = iter.next().attribute;
            int att_length = attributes.length /2;
            double[] new_attributes = new double[att_length];

            for(int i=0; i<att_length;i++)
            {
                new_attributes[i] = 0.5*(attributes[2*i]+ attributes[2*i+1]);
                //System.out.println("generate_instances_value_from_boundary : "+new_attributes );
            }

            Instance a_instance_value = new Instance();
            a_instance_value.set_all_attribute(new_attributes);
            instances_value.add(a_instance_value);
        }
        */

        return instances_value;

    }

    public ArrayList<Instance> generate_instances_boundary(String file_name, int line_header_length, int num_attribute, int required_line_NO)
    {
        System.out.println("generate_instances_boundary begin");

        //get instaces number
        int[] num_of_instances =new int[num_attribute];
        for(int which_attribute_index =0; which_attribute_index<num_attribute; which_attribute_index++)
        {
            num_of_instances[which_attribute_index] = read_length_of_line_in_CSV_dataSet(file_name, required_line_NO + which_attribute_index  )- line_header_length;
            System.out.println("generate_instances_boundary: each attribute has ("+ num_of_instances[which_attribute_index]+") of original data point." +
                    "in dataset line index= ("+(required_line_NO + which_attribute_index)+") ");

        }

        int has_how_many_rows = read_how_many_rows_in_CSV_dataSet(file_name);
        System.out.println("generate_instances_boundary:  has_how_many_rows("+has_how_many_rows+").");
        /*
        //initate attribute set for instances
        ArrayList<double[]> attributeSet_of_all_instance = new ArrayList<double[]>(); // original data set
        //get attributeSet of each attribute: save in attributeSet_of_all_instance structure // get dimensions for meshgrid
        for(int attri_index=0; attri_index< num_attribute; attri_index++)
        {
            // get each attributeSet from csv file
            double[] a_attribute_of_all_instances = new double[num_of_instances[attri_index]];
            a_attribute_of_all_instances = get_a_attribute_of_all_instances(file_name,required_line_NO+ attri_index, line_header_length, num_of_instances[attri_index]);
            attributeSet_of_all_instance.add(a_attribute_of_all_instances); // appends to the end of list
        }
        */

        //initate attribute set for instances
        ArrayList<double[]> original_data_set = new ArrayList<double[]>(); // original data set //attributeSet_of_all_instance
        //get attributeSet of each attribute: save in attributeSet_of_all_instance structure // get dimensions for meshgrid
        for(int index_dim=0; index_dim< num_attribute; index_dim++)
        {
            // get each attributeSet from csv file
            double[] a_dim_ori_data = new double[num_of_instances[index_dim]];
            a_dim_ori_data = get_a_attribute_of_all_instances(file_name,required_line_NO+ index_dim, line_header_length, num_of_instances[index_dim]);
            original_data_set.add(a_dim_ori_data); // appends to the end of list
        }

        /**check the mesh method in dairy May15**/

        /** 1. mesh operator**/
        int[] meshOperator_iter = new int[num_attribute]; //index of an axis, indicate a value of an attributeSet. attribute_index_iter is a pointer
        int[] meshOperator_iter_upbound = new int[num_attribute]; //length of each attribute
        int[] meshOperator_iter_carry = new int[num_attribute+1]; // carry bit, reset the index of other axis

        //initiate index
        meshOperator_iter_carry[0] = 0;
        for(int i=0; i< num_attribute;i++)
        {
            meshOperator_iter[i]=0;

            meshOperator_iter_carry[i+1]=0;
            meshOperator_iter_upbound[i]= original_data_set.get(i).length;
        }

        /** 2. section trunked from dimensions of original dataset**/

        int length_of_output_dataSet = 1; // meshgrid output date
        int[] length_of_dimension_of_original_dataSet = new int[num_attribute];
        for(int i= 0; i< num_attribute;i++)
        {
            length_of_dimension_of_original_dataSet[i] = original_data_set.get(i).length;
            length_of_output_dataSet = length_of_output_dataSet * (length_of_dimension_of_original_dataSet[i]-1);
        }

        /** 3. boundary data trunked from original dataSet**/
        double[][] boundary_of_all_attribute = new double[num_attribute][3];// [number of dimenison] [lower and higher boundary + value]

        // initiate instanceSet
        ArrayList<Instance> instanceSet= new ArrayList<Instance>();

        /** 4. goto meshgrid procedural**/
        while(meshOperator_iter_carry[num_attribute]!=1)
        {//when the final carry flag !=1 continue //
            int case_instanse_boundary = 2;
            //int case_instanse_boundary_and_value = 3;
            //int length_of_a_dim_of_meshgrid = num_attribute*case_instanse_boundary_and_value; //select boundary
            //double[] a_dim_of_meshgrid = new double[length_of_a_dim_of_meshgrid];// num_attribute= dimension of  an instance

            // 1. get individual boundary of each dimension first , save them into boundary_of_all_attribute
            for(int dimension_index=0; dimension_index< num_attribute; dimension_index++)
            {   int index= meshOperator_iter[dimension_index];
                //lower
                boundary_of_all_attribute[dimension_index][0]=  original_data_set.get(dimension_index)[index];
                //higher
                boundary_of_all_attribute[dimension_index][2]=  original_data_set.get(dimension_index)[index+1];
                //value
                boundary_of_all_attribute[dimension_index][1]= 0.5*(boundary_of_all_attribute[dimension_index][0]+boundary_of_all_attribute[dimension_index][2]);
            }
            //2. combine lower and upper boundary from each dimension together to establish a new instance. flatten the boundary_of_all_attribute into one dimensional array
            int length_of_boundary_meshgrid = 3*num_attribute;
            double[] boundary_meshgrid = new double[length_of_boundary_meshgrid]; //equals to an instance's attributes
            for(int dimension_index=0; dimension_index< num_attribute; dimension_index++)
            {
                boundary_meshgrid[dimension_index*3] = boundary_of_all_attribute[dimension_index][0]; //upper boundary of niche instance
                boundary_meshgrid[dimension_index*3+2] = boundary_of_all_attribute[dimension_index][2]; //lower boundary of niche instance
                boundary_meshgrid[dimension_index*3+1] =  boundary_of_all_attribute[dimension_index][1]; // value of niche instance

            }

            // add a random method to select niche
            /*
            if(Math.random()<= 0.01)
            {
                Instance an_instance= new Instance();
                an_instance.set_all_attribute(boundary_meshgrid);
                instanceSet.add(an_instance);
            }
            */
            Instance an_instance= new Instance();
            an_instance.set_all_attribute(boundary_meshgrid);
            instanceSet.add(an_instance);


            /*
            // set each dimension
            for(int dimension_index=0; dimension_index< num_attribute; dimension_index++)
            {



                // set each dimension of an instance one dimension by one dimension
                int index_in_a_dim = meshOperator_iter[dimension_index]; // index_in_a_dim points to the sequence number in an axis


                //move lower boundary and high boundary one by one
                for(int seq_index_in_a_dim_meshgrid = 0; seq_index_in_a_dim_meshgrid < case_instanse_boundary-1;seq_index_in_a_dim_meshgrid++)
               {
                   //move lower boundary and high boundary one by one
                   a_dim_of_meshgrid[dimension_index*case_instanse_boundary+seq_index_in_a_dim_meshgrid] = original_data_set.get(dimension_index)[index_in_a_dim+seq_index_in_a_dim_meshgrid];
               }
            }

            Instance an_instance= new Instance();
            an_instance.set_all_attribute(a_dim_of_meshgrid);
            instanceSet.add(an_instance);
            */
            // parepare for next loop, mesh operator iteration
            // essentially, it is a meshgrid method. but take care of the carry bit.
            // the value of pointer +1
            meshOperator_iter[0] = meshOperator_iter[0]+1;    // increment of each step for the meshgrid method
            meshOperator_iter_carry[0]=0;    //set the carry bit of the first dimension alway equals to 0
            //iterate each attribute index

            for(int i=0; i<num_attribute;i++)
            {
                //update (3=num_attribute) pointer value from lower dimension to a higher dimension
                //meshgrid 1: add carry bit
                meshOperator_iter[i] = meshOperator_iter[i] + meshOperator_iter_carry[i];
                //check if reach the upbound.
                if(meshOperator_iter[i]==meshOperator_iter_upbound[i]-case_instanse_boundary+1)
                {
                    // if reach the upbound, set carry bit and set the current pointer to original place(0)
                    meshOperator_iter_carry[i+1] = 1;
                    meshOperator_iter[i] = 0;
                }
                else
                {   //reset carry bit
                    meshOperator_iter_carry[i+1] = 0;
                }

            }
            //chech iteration
            //System.out.println("check carry iteration:"+meshOperator_iter[0] +" /"+meshOperator_iter[1]  );


        }
        /*
        while(attribute_index_iter_carry[num_attribute]!=1)
        {//when the final carry flag !=1 continue //

            double[] attributeSet_of_a_instance = new double[num_attribute];// num_attribute= dimension of  an instance

            // attribute iteration// column = which_attribute
            for(int dimension_index=0; dimension_index< num_attribute; dimension_index++)
            {    // set each dimension of an instance one dimension by one dimension
                int index_in_a_dim = attribute_index_iter[dimension_index]; // index_in_a_dim points to the sequence number in an axis
                attributeSet_of_a_instance[dimension_index]=  attributeSet_of_all_instance.get(dimension_index)[index_in_a_dim];
            }



            Instance an_instance= new Instance();
            an_instance.set_all_attribute(attributeSet_of_a_instance);
            instanceSet.add(an_instance);

            // parepare for next loop
            // essentially, it is a meshgrid method. but take care of the carry bit.
            // the value of pointer +1
            attribute_index_iter[0] = attribute_index_iter[0]+1;    // increment of each step for the meshgrid method
            attribute_index_iter_carry[0]=0;    //set the carry bit of the first dimension alway equals to 0
            //iterate each attribute index

            for(int i=0; i<num_attribute;i++)
            {
                //update (3=num_attribute) pointer value from lower dimension to a higher dimension
                //meshgrid 1: add carry bit
                attribute_index_iter[i] = attribute_index_iter[i] + attribute_index_iter_carry[i];
                //check if reach the upbound.
                if(attribute_index_iter[i]==attribute_index_upbound[i])
                {
                    // if reach the upbound, set carry bit and set the current pointer to original place(0)
                    attribute_index_iter_carry[i+1] = 1;
                    attribute_index_iter[i] = 0;
                }
                else
                {   //reset carry bit
                    attribute_index_iter_carry[i+1] = 0;
                }

            }
            //chech iteration
            System.out.println("check carry iteration:"+attribute_index_iter[0] +" /"+attribute_index_iter[1]  );
        }
        */


        return instanceSet;
        /*
        //check attributeSet_of_all_instance
        System.out.println("size of attribute is" +attributeSet_of_all_instance.size()+" vs. num of attr ("+num_attribute+")" );
        for(int attri_index=0; attri_index< num_attribute; attri_index++)
        {
            System.out.println(" attributeSet (index="+attri_index+"):"  );

            double[] a_attribute_of_all_instances = attributeSet_of_all_instance.get(attri_index);
            String s = " ";
            for(int niche_index=0; niche_index< a_attribute_of_all_instances.length;niche_index++)
            {
                s += a_attribute_of_all_instances[niche_index]+"#";
            }
            System.out.println(s);
            //attribute 1 attributeSet_of_all_instance.get(attri_index)




        }

        // initiate instanceSet
        ArrayList<Instance> instanceSet= new ArrayList<Instance>();

        //construct instances attribute by attribute


        for(int which_instance=0; which_instance< num_of_instances[0]; which_instance++)
        {
            // construct each instance from attributes. instance iteration// row = instance

            double[] attributeSet_of_one_instance = new double[num_attribute];

            // attribute iteration// column = which_attribute
            for(int which_attribute=0; which_attribute< num_attribute; which_attribute++)
            {
                attributeSet_of_one_instance[which_attribute]=  attributeSet_of_all_instance.get(which_attribute)[which_instance];
            }
            Instance an_instance= new Instance();
            an_instance.set_all_attribute(attributeSet_of_one_instance);
            instanceSet.add(an_instance);
        }


        return instanceSet;

        */

    }

    public double[] get_a_attribute_of_all_instances(String file_name, int required_line_NO, int line_header_length,int num_of_instances)
    {
        double[] a_attribute_of_all_instances = new double[num_of_instances];
        try{
            Scanner scanner = new Scanner(new File(file_name));
            scanner.useDelimiter(",");

            //int required_line_NO= 1; //specify which line the off-line environment required
            int index_scanner=0;
            while(scanner.hasNextLine())
            {
                if(index_scanner!= required_line_NO)
                {
                    scanner.nextLine();
                    index_scanner++;
                }else
                {
                    if(index_scanner== required_line_NO)
                    {
                        System.out.println("loadCSVData_I_read_niche:: index_scan ="+ index_scanner);
                        //System.out.println( "\n");
                        String line = scanner.nextLine();
                        String[] fields = line.split(",");
                        //System.out.println(fields + "<- String[] fields");    //fields[] address
                        //System.out.println( "\n");
                        System.out.println( "loadCSVData_I_read_niche:: fields.length ("+fields.length+") \n");
                        double[] niche_points= new double[fields.length-line_header_length];
                        int count=0;

                        //set niche_points
                        for(String field : fields)
                        {
                            field = field.replaceAll("\"","");
                            // niche attribute start from line_header_length-1
                            if(count< line_header_length)
                            {
                                // field belong to header. do nothing.
                            }
                            else
                            {
                                //record niche separated points
                                niche_points[count-line_header_length] = Double.parseDouble(field);

                            }
                            count++;
                        }

                        // 2. generate instances according to niche separation.
                        a_attribute_of_all_instances = niche_points;

                        break;// niche points ara stored in niche_points
                    }
                }


            }// end of while.
            scanner.close();

            return a_attribute_of_all_instances;

        }catch (FileNotFoundException e){return null;}
    }

    public int read_how_many_rows_in_CSV_dataSet(String file_name)
    {
        System.out.println("read_how_many_rows_in_CSV_dataSet:: filename("+file_name+")");
        int has_how_many_rows = 0;
        try{
            Scanner scanner = new Scanner(new File(file_name));
            scanner.useDelimiter(",");
            while(scanner.hasNextLine())
            {
                has_how_many_rows++;
                scanner.nextLine();

            }// end of while

            scanner.close();
            System.out.println("read_how_many_rows_in_CSV_dataSet:: has_how_many_rows("+has_how_many_rows+")");
            return has_how_many_rows;
        }catch (FileNotFoundException e){return 0;}

    }

    public int read_length_of_line_in_CSV_dataSet(String file_name,int required_line_NO)
    {
        //set up instance
        // attributes+ groundtruth
        System.out.println( "read_length_of_line_in_CSV_dataSet :: filename("+file_name+"), required_line_NO("+required_line_NO+") ");
        int instance_total_num=0;

        try{
            Scanner scanner = new Scanner(new File(file_name));
            scanner.useDelimiter(",");
            int index_scanner=0;
            while(scanner.hasNextLine())
            {
                if(index_scanner!= required_line_NO)
                {
                    scanner.nextLine();
                    index_scanner++;
                }else
                {
                    if(index_scanner== required_line_NO)
                    {
                        //System.out.println("loadCSVData_I:: index_scan ="+ index_scanner);
                        //System.out.println( "\n");
                        String line = scanner.nextLine();
                        String[] fields = line.split(",");
                        instance_total_num = fields.length;
                        System.out.println( "read_length_of_line_in_CSV_dataSet :: instance_total_num ("+instance_total_num+") \n");
                        break;
                    }
                }

            }// end of while

            scanner.close();
            return instance_total_num;
        }catch (FileNotFoundException e){return 0;}
    }


    public void niche_strength_advocate(int iteration)
    {
        /**
         * this procedure calculate the strength of each possible niche.
         * it calculate niche strength distribution according to population of classifiers, which meet requirements.
         * the requirements can be the selected action(sact), accuracy classifiers, and so on.
         * it might be similar to Value Network of AlphaGO. LOL
         * make this separate from the testing procedures.
         * **********/
        /**
         * 1. Match filter:select requirements: selected action.
         * 2. Select filter: calculate the niche strength.
         * ***/

        learningAgent.niche_strength_advocate( this.test_exploreRate, this.test_iterationCount_threshold, this.test_GA_threshold);// todo: return action confidence

        System.out.println("niche_strength_advocate...developing");

    }

    public void testCheckPoints(int iteration)
    {
        //go into task loop iteration
            //csvData length is 150, therefore, every instance is go for 10 times of training
            boolean one_iterationResult=false;
            double successNum=0;
            double failureNum=0;
            double performanceRate=0;
            double performanceRateF=0;

            //for(int i=1;i<4;i++)// for test 3 examples //(d)  only 3 instance!!!!!!!!!!! for  "1 scenario" test
            for(int i=1; i<=dataSet_instance_total_num;i++)
            {

                int test_task_seq=i;

                System.out.println("	+++++++++++++++++++++ check ++++++++++++++++++++++++++++++++++++++++");
                System.out.println("	testCheckPoints >>>>>test_task_seq ("+test_task_seq+") , iteration seq("+i+")");
                //SingleTask a_iteration_singleTask= loadCSVData(test_task_seq);
                Instance an_iteration_instance= loadCSVData_I(test_task_seq);
                // add an testingAgent agent
                one_iterationResult=learningAgent.instance_testMode_check_instance_action_anticipatedPRD(test_task_seq, an_iteration_instance, this.test_exploreRate, this.test_iterationCount_threshold, this.test_GA_threshold);// todo: return action confidence

            if(one_iterationResult)
            {
                successNum++;
            }
            else
            {
                failureNum++;
                System.out.println("	testCheckPoints >>>>>test_task_seq ("+test_task_seq+") , iteration seq("+i+"): fail task(NO."+i+")");
            }

            /**save the sequence number of iris task**/
            /*
            if(iteration>1500)
            {
                if(!one_iterationResult)
                {
                    if(iteration%6000==1)
                    {
                        this.irisCheckPoints.cleanFailureTaskLog();
                    }
                    this.irisCheckPoints.saveFailureTaskSeq(test_task_seq);
                }
            }
            */

            //one_scenario_test_mode
            if(one_scenario_test_mode)
            {
                if(i==this.multistep)
                {
                    i=151;// for test 3 examples //(d)  only 3 instance!!!!!!!!!!! for  "1 scenario" test
                }
            }




        }
        performanceRate= successNum/(successNum+failureNum);
        //performanceRateF= failureNum/(successNum+failureNum);

        this.irisCheckPoints.saveCheckPointsSuccessNum( successNum, iteration);
        //this.irisCheckPoints.saveCheckPoints(performanceRate,iteration); //.xml
        this.irisCheckPoints.saveCheckPoints_csv_file(performanceRate,iteration);
    }


    public void Check_Niche(ArrayList<Instance> instances, int iteration_check, int required_line_index, String which_selected_action_projected)
    {


        System.out.println(" >>>>>>>>>>>>L1. check Niche begin>>>>>======================");

        HashMap<LayerAction, String> niche_anticipated_reward_hashmap = new HashMap<LayerAction, String>();
        //for(int i=1;i<4;i++)// for test 3 examples //(d)  only 3 instance!!!!!!!!!!! for  "1 scenario" test
        for(int i=0; i<instances.size();i++)
        {
            System.out.println(">>>>>>>>>>>>L2. check Niche ::instance NO.("+i+") in instances.size("+instances.size()+") " );

            Instance an_instance =  instances.get(i);

            HashMap<LayerAction, Double> anticipated_reward_hashmap = learningAgent.get_instance_action_anticipatedPRD(an_instance, this.test_exploreRate, this.test_iterationCount_threshold, this.test_GA_threshold);// todo: return anticipated reward HashMap

            /*
            // show anticipated reward of niche projected by an instance
            for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
            {
                System.out.println(">>>>>>>>>>>>check Niche :: (Accuracy-based) calcuate anticipated reward :\taction("+a_anticipated_reward_map.getKey()+")" +
                        "\t- reward("+a_anticipated_reward_map.getValue()+"). ");
            }
            */

            //save hashmap into string
            for(Map.Entry<LayerAction, Double>a_anticipated_reward_map: anticipated_reward_hashmap.entrySet())
            {

                //System.out.println(">>>>>>>>>>>>check Niche :: (Accuracy-based) calcuate anticipated reward :\taction("+a_anticipated_reward_map.getKey()+")" + "\t- reward("+a_anticipated_reward_map.getValue()+"). ");


                LayerAction an_action= a_anticipated_reward_map.getKey();
                String a_reward_s= Double.toString(a_anticipated_reward_map.getValue()) + "#";

                if(! niche_anticipated_reward_hashmap.containsKey(an_action ))
                {
                    // does not has the action
                    String header= Integer.toString(an_action.getMask())+ "#"+ "iteration NO."+Integer.toString(iteration_check) + "#"+
                            which_selected_action_projected+"//required_line_NO."+ Integer.toString(required_line_index)+"//attribute?? #" + "0" + "#";
                    a_reward_s= header+ a_reward_s;
                    niche_anticipated_reward_hashmap.put(an_action, a_reward_s);
                }
                else
                {
                    String temp_s = niche_anticipated_reward_hashmap.get(an_action) + a_reward_s;
                    niche_anticipated_reward_hashmap.put(an_action, temp_s);
                }

            }


            System.out.println("\n");
        }

        // show niche_anticipated_reward_hashmap
        for(Map.Entry<LayerAction, String>a_anticipated_reward_map: niche_anticipated_reward_hashmap.entrySet())
        {
            System.out.println(">>>>>>>>>>>>check Niche :: final calcuate anticipated reward :\taction("+a_anticipated_reward_map.getKey()+")" +
                    "\t- reward("+a_anticipated_reward_map.getValue()+"). ");
        }

        //save this hashmap to csv file
        String accuarcy_based_niche_strength_distribution_file_name = "X_prd_values";
        Modifier_ContinuouslyProjected niche_projected= new Modifier_ContinuouslyProjected();
        for(Map.Entry<LayerAction, String>a_anticipated_reward_map: niche_anticipated_reward_hashmap.entrySet())
        {
            niche_projected.save_a_row_string_to_csv(accuarcy_based_niche_strength_distribution_file_name,a_anticipated_reward_map.getValue() );
        }


        System.out.println(" >>>>>>>>>>>>L1. check Niche end>>>>>======================");

    }

    public void save_generated_instance(String file_name  ,ArrayList<Instance> instances, int iteration_num )
    {
        String complete_file_name = file_name+Integer.toString(iteration_num);

        List<String[]> data = new ArrayList<String[]>();
        /*
        for(int i =0; i< instances.size();i++)
        {
            //version 1
            //Instance a_instance= instances.get(i);
            //translate attribute to string
            //String[] att_s= a_instance.transfer_attributes_to_strings();
            //data.add(att_s);

            //version 2 save JVM memory
            data.add(instances.get(i).transfer_attributes_to_strings());

        }
        */
        //version 3
        /*
        Iterator<Instance> instance_iterator = instances.iterator();
        Instance a_instance =instance_iterator.next();
        data.add(a_instance.transfer_attributes_to_strings());
        while (instance_iterator.hasNext()) {
            a_instance =instance_iterator.next();
            data.add(a_instance.transfer_attributes_to_strings());
        }
        */
        /*
        //version 4
        for (Iterator<Instance> iter = instances.iterator(); iter.hasNext(); ) {

            data.add(iter.next().transfer_attributes_to_strings());
        }
        */
        //version 5
        Iterator<Instance> iter = instances.iterator();
        while(iter.hasNext())
        {
            data.add(iter.next().transfer_attributes_to_strings());

        }


        try
        {
            System.out.println("save generated_instance into CSV");

            CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/"+complete_file_name+".csv"));
            csv_writer.writeAll(data);
            csv_writer.close();

        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        /*
        //1. read csv
        try
        {
            FileReader f= new FileReader("./Results/"+file_name+".csv");
            CSVReader csv_reader= new CSVReader(f);
            data= csv_reader.readAll();

            //2.add current data //transform to hashmap

            String[] s_data_to_add= a_row_data_s.split("#");
            data.add(s_data_to_add);
            //3. write csv Data



        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        */

    }
    public void save_generated_instance_DoubleVersion(String file_name  ,ArrayList<Instance> instances, int iteration_num, String[] header_line )
    {
        int count;
        int count_threshold = 500000;
        int section = 1;

        //write header


        Iterator<Instance> iter = instances.iterator();
        int length_of_attribute = iter.next().get_attribute_length();
        String[] new_line = new String[length_of_attribute];

        while(iter.hasNext())
        {
            //write section
            //initiate
            count=0;
            String complete_file_name = file_name+"_iteration_"+Integer.toString(iteration_num)+"_section_"+Integer.toString(section);


            // write each section
            /**
             * write each section
             * ********/
            try
            {
                System.out.println("save generated_instance into CSV");

                CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/"+complete_file_name+".csv"));

                //write header
                csv_writer.writeNext(header_line);
                csv_writer.flush();

                //writer line by line version
                while(iter.hasNext() && count < count_threshold)
                {
                    // 1. translate each line from double[] into str[]
                    new_line = iter.next().transfer_attributes_to_strings();
                    // 2. write new line into csv file
                    csv_writer.writeNext(new_line);
                    csv_writer.flush();
                    count++;
                }

                csv_writer.close();

            }
            catch(Exception e)
            {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }


            section++;
        }




    }

    public ArrayList<Instance> check_niche_instance_and_classifier(ArrayList<Instance> instances, int iteration_check, int required_line_index, String header_line)
    {

        System.out.println(" >>>>>>>>>>>>L1. check_niche_instance_and_classifier begin>>>>>======================");

        HashMap<LayerAction, HashSet<Integer>> existing_ids_hashmap = new HashMap<LayerAction,  HashSet<Integer>>();// to chech if this classifier has before
        HashMap<LayerAction, List<String[]>> best_population = new HashMap<LayerAction, List<String[]>>();
        //for(int i=1;i<4;i++)// for test 3 examples //(d)  only 3 instance!!!!!!!!!!! for  "1 scenario" test
        ArrayList<Instance> instances_prd= new ArrayList<Instance>();
        String[] header_csv;

        //iterator version
        Iterator<Instance> iter = instances.iterator();
       // Instance an_instance_constructed = new Instance();
       // an_instance_constructed.set_all_attribute(iter.next().extractValueInstance());
        Instance an_instance;
        int count=0;
        int section =0;

        while(iter.hasNext())
        {
            System.out.println(">>>>>>>>>>>>L2. check_niche_instance_and_classifier. in iteration loop count("+count+")/section("+section+")");
            count++;
            if(count >= 500000)
            {
                count=0;
                section++;
            }

            //an_instance.set_all_attribute(iter.next().extractValueInstance()); // extract instance value
            an_instance = iter.next();
            //this return the  best represented layerclassifier hashmap for each niche!!
            HashMap<LayerAction, LayerClassifier> instance_classifier = learningAgent.get_instance_classifier(an_instance, this.test_exploreRate, this.test_iterationCount_threshold, this.test_GA_threshold);// todo: return classifier HashMap

            // save layer classifier into file


            //add value of prd into instance_prd
            int num_stat = 6;
            double[] prd_value= new double[num_stat]; // todo: 2 = num of action
            for(int j=0;j<num_stat;j++)
            {
                prd_value[j]=0;
            }
            for(Map.Entry<LayerAction, LayerClassifier> a_classifier_in_hashmap: instance_classifier.entrySet())
            {
                /* SHOW instance_classifier
                System.out.println("	L2:	(Accuracy-based) most fitness classifier :\t s-action(" + a_classifier_in_hashmap.getKey() + ")" +
                        "\t- id(" + a_classifier_in_hashmap.getValue().getID() + ")," +
                        " condition (" + a_classifier_in_hashmap.getValue().getCondition() + " )," +
                        " action(" + a_classifier_in_hashmap.getValue().getLayerAction() + ")," +
                        " acc("+ a_classifier_in_hashmap.getValue().getAccuracy()+" ), " +
                        " prd("+ a_classifier_in_hashmap.getValue().getPredictedReward()+")," +
                        " fit("+ a_classifier_in_hashmap.getValue().getFitness()+")");

                */
                if( a_classifier_in_hashmap.getValue().getLayerAction().getMask()==1)
                {
                    prd_value[0]= a_classifier_in_hashmap.getValue().getPredictedReward();
                    prd_value[2]= a_classifier_in_hashmap.getValue().getAccuracy();
                    prd_value[4]= a_classifier_in_hashmap.getValue().getExperience();

                }
                if( a_classifier_in_hashmap.getValue().getLayerAction().getMask()==2)
                {
                    prd_value[1]= a_classifier_in_hashmap.getValue().getPredictedReward();
                    prd_value[3]= a_classifier_in_hashmap.getValue().getAccuracy();
                    prd_value[5]= a_classifier_in_hashmap.getValue().getExperience();


                }

            }
            //merge old attribute from instance and new prd value to form new attribute of new instance
            //double[] new_att= merge_two_DoubleArray(an_instance.attribute,prd_value);
            // setup new instance
            Instance new_instance= new Instance(merge_two_DoubleArray(an_instance.attribute,prd_value),0);
            //save new instance into array and return it
            instances_prd.add(new_instance);

            //remove this one to save memory
            /*
            for(Map.Entry<LayerAction, LayerClassifier> a_classifier_in_hashmap: instance_classifier.entrySet())
            {


                LayerAction sact = a_classifier_in_hashmap.getKey();
                LayerClassifier classifier= a_classifier_in_hashmap.getValue();
                String[] cl_s= classifier.toStringArray_for_csv();
                HashSet<Integer> ids = new HashSet<Integer>();
                List<String[]> classifier_for_sact_array_csv = new ArrayList<String[]>();


                if(best_population.containsKey(sact))
                {
                    classifier_for_sact_array_csv = best_population.get(sact);
                    ids = existing_ids_hashmap.get(sact);
                }
                else
                {
                    header_csv = classifier.toHeaderString_for_csv();
                    classifier_for_sact_array_csv.add(header_csv);
                }


                int id = classifier.getID();
                if(! ids.contains(id))
                {
                    //check cls
                    String check_cls="";
                    for(int k=0; k<cl_s.length;k++)
                    {
                        check_cls= check_cls+ "#" + cl_s[k];
                    }
                    System.out.println(check_cls);
                    classifier_for_sact_array_csv.add(cl_s);
                    ids.add(id);
                    System.out.println("add classifier has id("+id+")");
                }
                best_population.put(sact,classifier_for_sact_array_csv);
                existing_ids_hashmap.put(sact,ids);
            }
            */

            System.out.println("\n");

        }


        // for-loop version
        /*
        for(int i=0; i<instances.size();i++)
        {
            System.out.println(">>>>>>>>>>>>L2. check_niche_instance_and_classifier ::instance NO.("+i+") in instances.size("+instances.size()+") " );

            Instance an_instance =  instances.get(i);

            HashMap<LayerAction, LayerClassifier> instance_classifier = learningAgent.get_instance_classifier(an_instance, this.test_exploreRate, this.test_iterationCount_threshold, this.test_GA_threshold);// todo: return classifier HashMap

            // save layer classifier into file


            //add value of prd into instance_prd
            int num_stat = 6;
            double[] prd_value= new double[num_stat]; // todo: 2 = num of action
            for(int j=0;j<num_stat;j++)
            {
                prd_value[j]=0;
            }
            for(Map.Entry<LayerAction, LayerClassifier> a_classifier_in_hashmap: instance_classifier.entrySet())
            {
                //SHOW instance_classifier
                System.out.println("	L2:	(Accuracy-based) most fitness classifier :\t s-action(" + a_classifier_in_hashmap.getKey() + ")" +
                        "\t- id(" + a_classifier_in_hashmap.getValue().getID() + ")," +
                        " condition (" + a_classifier_in_hashmap.getValue().getCondition() + " )," +
                        " action(" + a_classifier_in_hashmap.getValue().getLayerAction() + ")," +
                        " acc("+ a_classifier_in_hashmap.getValue().getAccuracy()+" ), " +
                        " prd("+ a_classifier_in_hashmap.getValue().getPredictedReward()+")," +
                        " fit("+ a_classifier_in_hashmap.getValue().getFitness()+")");


                if( a_classifier_in_hashmap.getValue().getLayerAction().getMask()==1)
                {
                    prd_value[0]= a_classifier_in_hashmap.getValue().getPredictedReward();
                    prd_value[2]= a_classifier_in_hashmap.getValue().getAccuracy();
                    prd_value[4]= a_classifier_in_hashmap.getValue().getExperience();

                }
                if( a_classifier_in_hashmap.getValue().getLayerAction().getMask()==2)
                {
                    prd_value[1]= a_classifier_in_hashmap.getValue().getPredictedReward();
                    prd_value[3]= a_classifier_in_hashmap.getValue().getAccuracy();
                    prd_value[5]= a_classifier_in_hashmap.getValue().getExperience();


                }

            }
            //merge old attribute from instance and new prd value to form new attribute of new instance
            double[] new_att= merge_two_DoubleArray(an_instance.attribute,prd_value);
            // setup new instance
            Instance new_instance= new Instance(new_att,0);
            //save new instance into array and return it
            instances_prd.add(new_instance);

            for(Map.Entry<LayerAction, LayerClassifier> a_classifier_in_hashmap: instance_classifier.entrySet())
            {


                LayerAction sact = a_classifier_in_hashmap.getKey();
                LayerClassifier classifier= a_classifier_in_hashmap.getValue();
                String[] cl_s= classifier.toStringArray_for_csv();
                HashSet<Integer> ids = new HashSet<Integer>();
                List<String[]> classifier_for_sact_array_csv = new ArrayList<String[]>();


                if(best_population.containsKey(sact))
                {
                    classifier_for_sact_array_csv = best_population.get(sact);
                    ids = existing_ids_hashmap.get(sact);
                }
                else
                {
                    header_csv = classifier.toHeaderString_for_csv();
                    classifier_for_sact_array_csv.add(header_csv);
                }


                int id = classifier.getID();
                if(! ids.contains(id))
                {
                    //check cls
                    String check_cls="";
                    for(int k=0; k<cl_s.length;k++)
                    {
                        check_cls= check_cls+ "#" + cl_s[k];
                    }
                    System.out.println(check_cls);
                    classifier_for_sact_array_csv.add(cl_s);
                    ids.add(id);
                    System.out.println("add classifier has id("+id+")");
                }
                best_population.put(sact,classifier_for_sact_array_csv);
                existing_ids_hashmap.put(sact,ids);
            }

            System.out.println("\n");
        }// end of instances
        */
        //remove this one to save memory
        //save_instance_classifier_pop_into_csv(best_population, iteration_check);

        return instances_prd;
    }

    public double[] merge_two_DoubleArray(double[] doub1, double[] doub2)
    {
        int str1_length= doub1.length;
        int str2_length= doub2.length;
        int total_length= str1_length+str2_length;

        //5.1 increase the memory
        doub1= Arrays.copyOf(doub1,total_length);
        //5.2 merge
        System.arraycopy(doub2,0,doub1,str1_length,str2_length);
        return  doub1;
    }

    public void save_instance_classifier_pop_into_csv( HashMap<LayerAction, List<String[]>> best_population , int iteration_check )
    {

        for(Map.Entry<LayerAction, List<String[]>> pop_sact: best_population.entrySet())
        {
            LayerAction sact= pop_sact.getKey();
            String file_name =sact.toString() + iteration_check;
            List<String[]> data = pop_sact.getValue();
            try
            {
                System.out.println("save save_instance_classifier_pop_into_csv ,file name:"+file_name );

                CSVWriter csv_writer= new CSVWriter(new FileWriter("./Results/"+file_name+".csv"));

                csv_writer.writeAll(data);
                csv_writer.close();

            }
            catch(Exception e)
            {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
        }

    }



}
